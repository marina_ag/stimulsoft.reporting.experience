﻿using YumaPos.Reporting.Data.Entity;
using YumaPos.Reporting.Infrastructure.Data;
using YumaPos.Reporting.Infrastructure.Ioc;

namespace YumaPos.Reporting.Data.Ioc
{
    public class Registrator : IRegistrator
    {
        public void Register(IBootstrapperContainer container)
        {
            container.RegisterType<IReportingEntityContainer, EntityPosReportingContainer>(Lifetime.PerLifetimeScope);
            container.Register(typeof(IReportingRepository<>), typeof(EntityReportingRepository<>), Lifetime.PerLifetimeScope);
        }
    }
}
