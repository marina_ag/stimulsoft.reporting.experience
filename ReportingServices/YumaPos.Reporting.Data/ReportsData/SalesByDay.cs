//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YumaPos.Reporting.Data.ReportsData
{
    using System;
    using System.Collections.Generic;
    
    public partial class SalesByDay
    {
        public int Id { get; set; }
        public System.DateTime WorkDate { get; set; }
        public int SoldQuantity { get; set; }
        public decimal NetSalesAmount { get; set; }
        public decimal TaxesAmount { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal SalesAndTaxAmount { get; set; }
        public int NumberOfSales { get; set; }
        public double AverageUnitsSold { get; set; }
        public decimal AverageValueSold { get; set; }
        public System.Guid TenantId { get; set; }
        public System.Guid StoreId { get; set; }
        public string StoreName { get; set; }
    }
}
