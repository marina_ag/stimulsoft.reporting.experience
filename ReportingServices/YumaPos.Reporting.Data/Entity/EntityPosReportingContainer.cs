﻿using System;

using YumaPos.Reporting.Data.ReportsData;
using YumaPos.Reporting.Infrastructure.Data;

namespace YumaPos.Reporting.Data.Entity
{
    public sealed class EntityPosReportingContainer : IReportingEntityContainer, IDisposable
    {
        private POSReportingEntities _objectContext;

        public object Context
        {
            get { return _objectContext ?? (_objectContext = new POSReportingEntities()); }
        }

        public void Dispose()
        {
            if (_objectContext != null)
            {
                _objectContext.Dispose();
                _objectContext = null;
            }
        }
    }
}