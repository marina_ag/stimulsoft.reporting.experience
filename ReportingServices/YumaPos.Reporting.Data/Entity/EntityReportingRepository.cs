﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using YumaPos.Reporting.Infrastructure.Data;

namespace YumaPos.Reporting.Data.Entity
{
    public class EntityReportingRepository<T> : IReportingRepository<T> where T : class, new()
    {
        private readonly DbContext _objContext;
        private readonly object _queryLock;
        private DbSet<T> _query;

        public EntityReportingRepository(IReportingEntityContainer entityContainer)
        {
            _queryLock = new object();
            _objContext = (DbContext) entityContainer.Context;
        }

        public virtual IQueryable<T> Query()
        {
            if (_query == null)
            {
                lock (_queryLock)
                {
                    if (_query == null)
                    {
                        _query = _objContext.Set<T>();
                    }
                }
            }

            return _query;
        }

        public virtual T Add(T entity)
        {
            return _objContext.Set<T>().Add(entity);
        }

        public virtual void AddRange(IEnumerable<T> entities)
        {
            _objContext.Set<T>().AddRange(entities);
        }

        public virtual void Remove(T entity)
        {
            _objContext.Set<T>().Remove(entity);
        }

        public virtual void RemoveRange(IEnumerable<T> entities)
        {
            _objContext.Set<T>().RemoveRange(entities);
        }

        public T Find(object[] keyValues)
        {
            return _objContext.Set<T>().Find(keyValues);
        }

        public async Task<T> FindAsync(object[] keyValues)
        {
            return await _objContext.Set<T>().FindAsync(keyValues);
        }

        public int SaveChanges()
        {
            return _objContext.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _objContext.SaveChangesAsync();
        }
    }
}