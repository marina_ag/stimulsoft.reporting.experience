﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

using YumaPos.Reporting.Infrastructure.Data;

namespace YumaPos.Reporting.Data.Entity
{
    public class EntityRepository<T> : IRepository<T> where T : class, new()
    {
        private readonly DbContext _objContext;

        private readonly object _queryLock;

        private DbSet<T> _query;

        public EntityRepository(IEntityContainer entityContainer)
        {
            this._queryLock = new object();
            this._objContext = (DbContext) entityContainer.Context;
        }

        public virtual IQueryable<T> Query()
        {
            if (this._query == null)
            {
                lock (this._queryLock)
                {
                    if (this._query == null)
                    {
                        this._query = this._objContext.Set<T>();
                    }
                }
            }

            return this._query;
        }

        public virtual T Add(T entity)
        {
            return this._objContext.Set<T>().Add(entity);
        }

        public virtual void AddRange(IEnumerable<T> entities)
        {
            this._objContext.Set<T>().AddRange(entities);
        }

        public virtual void Remove(T entity)
        {
            this._objContext.Set<T>().Remove(entity);
        }

        public virtual void RemoveRange(IEnumerable<T> entities)
        {
            this._objContext.Set<T>().RemoveRange(entities);
        }

        public T Find(object[] keyValues)
        {
            return this._objContext.Set<T>().Find(keyValues);
        }

        public async Task<T> FindAsync(object[] keyValues)
        {
            return await this._objContext.Set<T>().FindAsync(keyValues);
        }

        public int SaveChanges()
        {
            return this._objContext.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await this._objContext.SaveChangesAsync();
        }
    }
}