﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YumaPos.Reporting.Reports.Resources.Payments {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class PaymentsByDateResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal PaymentsByDateResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("YumaPos.Reporting.Reports.Resources.Payments.PaymentsByDateResource", typeof(PaymentsByDateResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to By card.
        /// </summary>
        public static string PaymentsByDate_ByCardColumn {
            get {
                return ResourceManager.GetString("PaymentsByDate_ByCardColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to By card – Miscellaneous.
        /// </summary>
        public static string PaymentsByDate_ByCardMiscColumn {
            get {
                return ResourceManager.GetString("PaymentsByDate_ByCardMiscColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This is a report that was created on.
        /// </summary>
        public static string PaymentsByDate_CreatedStr {
            get {
                return ResourceManager.GetString("PaymentsByDate_CreatedStr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Via credit invoice.
        /// </summary>
        public static string PaymentsByDate_CreditInvoiceColumn {
            get {
                return ResourceManager.GetString("PaymentsByDate_CreditInvoiceColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to USD.
        /// </summary>
        public static string PaymentsByDate_CurrencyParam {
            get {
                return ResourceManager.GetString("PaymentsByDate_CurrencyParam", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Currency:.
        /// </summary>
        public static string PaymentsByDate_CurrencyStr {
            get {
                return ResourceManager.GetString("PaymentsByDate_CurrencyStr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date.
        /// </summary>
        public static string PaymentsByDate_DateColumn {
            get {
                return ResourceManager.GetString("PaymentsByDate_DateColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Filters:.
        /// </summary>
        public static string PaymentsByDate_FiltersStr {
            get {
                return ResourceManager.GetString("PaymentsByDate_FiltersStr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to With gift card.
        /// </summary>
        public static string PaymentsByDate_GiftCardColumn {
            get {
                return ResourceManager.GetString("PaymentsByDate_GiftCardColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payments By Date.
        /// </summary>
        public static string PaymentsByDate_HeaderStr {
            get {
                return ResourceManager.GetString("PaymentsByDate_HeaderStr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Сash.
        /// </summary>
        public static string PaymentsByDate_InCashColumn {
            get {
                return ResourceManager.GetString("PaymentsByDate_InCashColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Period:.
        /// </summary>
        public static string PaymentsByDate_PeriodStr {
            get {
                return ResourceManager.GetString("PaymentsByDate_PeriodStr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Store:.
        /// </summary>
        public static string PaymentsByDate_StoreStr {
            get {
                return ResourceManager.GetString("PaymentsByDate_StoreStr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Terminal:.
        /// </summary>
        public static string PaymentsByDate_TerminalStr {
            get {
                return ResourceManager.GetString("PaymentsByDate_TerminalStr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total.
        /// </summary>
        public static string PaymentsByDate_TotalColumn {
            get {
                return ResourceManager.GetString("PaymentsByDate_TotalColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total ($).
        /// </summary>
        public static string PaymentsByDate_TotalStr {
            get {
                return ResourceManager.GetString("PaymentsByDate_TotalStr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Transfer.
        /// </summary>
        public static string PaymentsByDate_TransferColumn {
            get {
                return ResourceManager.GetString("PaymentsByDate_TransferColumn", resourceCulture);
            }
        }
    }
}
