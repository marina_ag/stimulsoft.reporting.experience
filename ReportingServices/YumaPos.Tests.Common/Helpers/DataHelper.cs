﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;

using YumaPos.WCF.Test.Data;

namespace YumaPos.Tests.Common.Helpers
{
    public static class DataHelper
    {
        private static string ConnectionString
        {
            get
            {
                var connectionStringOne = ConfigurationManager.ConnectionStrings["POSFdatTestsEntities"].ConnectionString;

                var connectionStringTwo = ConfigurationManager.ConnectionStrings["POSFdatTests"].ConnectionString;

                var connectionString = connectionStringOne.Replace("{0}", connectionStringTwo);

                return connectionString;
            }
        }

        public static void AddToEntity<T, Y>(Func<T, bool> idFunc, Func<Y, bool> idAddFunc)
            where T : class
            where Y : class
        {
            using (var context = new POSFdatTestsEntities(ConnectionString))
            {
                var obj = context.Set<T>().FirstOrDefault(idFunc);
                var addObj = context.Set<Y>().FirstOrDefault(idAddFunc);
                var props = typeof(T).GetProperties();
                foreach (var prop in props)
                {
                    var propType = prop.PropertyType;
                    if (propType.GetInterfaces().Contains(typeof(IEnumerable)) && propType.GenericTypeArguments.Any(x => x == typeof(Y)))
                    {
                        prop.PropertyType.GetMethod("Add").Invoke(prop.GetValue(obj), new object[] { addObj });
                    }
                }

                context.SaveChanges();
            }
        }

        public static void AddDataToDb<T>(T obj) where T : class
        {

            FindTenant(obj);
         
            using (var context = new POSFdatTestsEntities(ConnectionString))
            {
                context.Set<T>().Add(obj);
                context.SaveChanges();
            }
        }

        public static void FindTenant(object obj, int deep = 0)
        {
            if (deep > 6)
            {
                return;
            }
            var t = obj.GetType();

            if (t.IsArray || t.IsGenericType)
            {
                var item = obj as IEnumerable;
                foreach (var it in item)
                {
                    var subType = it.GetType();
                    PropertyInfo pro = subType.GetProperties().FirstOrDefault(x => x.Name == "TenantId");
                    if (pro != null && pro.PropertyType.Name == "Guid")
                    {
                        pro.SetValue(it, new Guid(new byte[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 }));
                    }
                    FindTenant(it, deep + 1);
                }
            }

            PropertyInfo props = t.GetProperties().FirstOrDefault(x => x.Name == "TenantId");
            if (props != null && props.PropertyType.Name == "Guid")
            {
                props.SetValue(obj, new Guid(new byte[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 }));
            }

            IEnumerable<PropertyInfo> virtualProperties = t.GetProperties().Where(p => p.GetMethod.IsVirtual);
            var count = virtualProperties.FirstOrDefault(x => x.Name == "Count");
            if (virtualProperties.Count() == 2 && count != null && (int)count.GetValue(obj) != 0)
            {
                int _count = (int)count.GetValue(obj);
                foreach (var prop in virtualProperties)
                {
                    if (prop.PropertyType.IsGenericType)
                    {
                        for (int i = 0; i < _count; i++)
                        {
                            var val = prop.GetValue(obj, new object[] { i });
                            if (val != null && val.GetType().Namespace != "System")
                            {
                                int qty_prop = 1;
                                if (val.GetType().IsGenericType)
                                {
                                    var ttt = val.GetType();
                                    qty_prop = (int)ttt.GetProperty("Count").GetValue(val);
                                }
                                if (qty_prop != 0)
                                {
                                    FindTenant(val, deep + 1);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (PropertyInfo prop in virtualProperties)
                {
                    object val = null;
                    val = prop.GetValue(obj, null);

                    if (val != null && val.GetType().Namespace != "System")
                    {
                        int qty_prop = 1;
                        if (val.GetType().IsGenericType)
                        {
                            var ttt = val.GetType();
                            qty_prop = (int)ttt.GetProperty("Count").GetValue(val);
                        }
                        if (qty_prop > 0)
                        {
                            FindTenant(val, deep + 1);
                        }
                    }
                }
            }
        }

        public static void AddDataToDb<T>(T[] objects) where T : class
        {
            foreach (var obj in objects)
            {
                FindTenant(obj);
            }
            
            using (var context = new POSFdatTestsEntities(ConnectionString))
            {
                context.Set<T>().AddRange(objects);
                context.SaveChanges();
            }
        }

        public static T GetFromDb<T>(Func<T, bool> idFunc, string includes = null) where T : class
        {
            using (var context = new POSFdatTestsEntities(ConnectionString))
            {
                return (string.IsNullOrEmpty(includes) ? context.Set<T>() : context.Set<T>().Include(includes)).FirstOrDefault(idFunc);
            }
        }

        public static T[] GetFromDb<T>(string includes = null) where T : class
        {
            using (var context = new POSFdatTestsEntities(ConnectionString))
            {
                return (string.IsNullOrEmpty(includes) ? context.Set<T>() : context.Set<T>().Include(includes)).ToArray();
            }
        }
    }
}
