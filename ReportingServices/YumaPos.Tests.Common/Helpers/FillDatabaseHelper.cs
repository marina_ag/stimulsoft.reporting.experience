﻿using System;
using System.Collections.Generic;

using YumaPos.WCF.Test.Data;

namespace YumaPos.Tests.Common.Helpers
{
    // Class to fill tables with predefined data before tests are run
    public static class FillDatabaseHelper
    {
        public static void AddTriggerTypes()
        {
            List<TriggerTypes> triggerTypes = new List<TriggerTypes>
            {
                new TriggerTypes{ Name = "GroupAmount",     Caption = "Group Amount"},
                new TriggerTypes{ Name = "GroupQuantity", Caption = "Group Quantity"},
                new TriggerTypes{ Name = "LinkedCoupon", Caption = "Linked Coupon"},
                new TriggerTypes{ Name = "MinOrder", Caption = "Min Order"},
                new TriggerTypes{ Name = "ProductsAmount", Caption = "Products Amount"},
                new TriggerTypes{ Name = "ProductsQuantity", Caption = "Products Quantity"},
                new TriggerTypes{ Name = "TargetCustomer", Caption = "Target Customer"}
            };

            foreach (var triggerType in triggerTypes)
            {
                DataHelper.AddDataToDb(triggerType);
            }
        }
        public static void AddDefaultLanguage()
        {
            var language = new Languages()
            {
                Name = "English",
                language = "en"
            };
            DataHelper.AddDataToDb(language);
        }

        public static void AddDefaultTenant()
        {

            var subscriptionPlans = new SubscriptionPlans(){ Name = "Basic" };
            var subscriptionPlans1 = new SubscriptionPlans() {Name = "Standart"};
            var subscriptionPlans2 = new SubscriptionPlans() {Name = "Advanced" };

            DataHelper.AddDataToDb(subscriptionPlans);
            DataHelper.AddDataToDb(subscriptionPlans1);
            DataHelper.AddDataToDb(subscriptionPlans2);
            var tenants=new Tenants()
            {
                TenantId = new Guid(new byte[16] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}),
                Name = "Default",
                Description = "Simple_Description",
                ContactPerson = "Tim Cook",
                City = "NY",
                Country = "USA",
                Zip = "10007",
                IsActive = true,
                Registered = DateTime.Now,
                SubscriptionPlanId = 3,
                Alias = "localhost"
            };
            DataHelper.AddDataToDb(tenants);
        }

        public static void AddAwardTypes()
        {
            List<AwardTypes> awardTypes = new List<AwardTypes>
            {
                new AwardTypes{ Name = "LinkedCoupon", Caption = "Linked Coupon"},
                new AwardTypes{ Name = "PercentReduction", Caption = "Percent Reduction"},
                new AwardTypes{ Name = "Points", Caption = "Points"},
                new AwardTypes{ Name = "PriceReduction", Caption = "Price Reduction"},
                new AwardTypes{ Name = "SpecifiedPrice", Caption = "Specified Price"}
            };

            foreach (var awardType in awardTypes)
            {
                DataHelper.AddDataToDb(awardType);
            }
        }
        public static void AddBusinessTypes()
        {
            List<BusinessType> businessTypes = new List<BusinessType>
            {
                new BusinessType{Id = 0, Name = "Store"},
                new BusinessType{Id = 1, Name = "Restaurant"},
                new BusinessType{Id = 2, Name = "Coffeeshop"},
                new BusinessType{Id = 3, Name = "Bar"},
                new BusinessType{Id = 4, Name = "FastFood"},
            };

            foreach (var type in businessTypes)
            {
                DataHelper.AddDataToDb(type);
            }
        }

        public static void AddOrderStatuses()
        {
            var orderStatuses = new []
            {
                new RestaurantOrderStatuses
                {
                    Name = "New",
                    StatusId = 1
                },
                new RestaurantOrderStatuses
                {
                    Name = "On Hold",
                    StatusId = 2
                },
                new RestaurantOrderStatuses
                {
                    Name = "Changed",
                    StatusId = 3
                },
                new RestaurantOrderStatuses
                {
                    Name = "Confirmed",
                    StatusId = 4
                },
                new RestaurantOrderStatuses
                {
                    Name = "In Progress",
                    StatusId = 5
                },
                new RestaurantOrderStatuses
                {
                    Name = "Prepared",
                    StatusId = 6
                },
                new RestaurantOrderStatuses
                {
                    Name = "Assigned to Driver",
                    StatusId = 7
                },
                new RestaurantOrderStatuses
                {
                    Name = "On Delivery",
                    StatusId = 8
                },
                new RestaurantOrderStatuses
                {
                    Name = "Delivered",
                    StatusId = 9
                },
                new RestaurantOrderStatuses
                {
                    Name = "Canceled",
                    StatusId = 10
                },
                new RestaurantOrderStatuses
                {
                    Name = "Closed",
                    StatusId = 11
                },
                new RestaurantOrderStatuses
                {
                    Name = "Void",
                    StatusId = 12
                },

            };
            DataHelper.AddDataToDb(orderStatuses);
        }

        public static void AddOrderTypes()
        {
            var orderTypes = new []
            {
                new RestaurantOrderTypes
                {
                    Name = "Dine In",
                    TypeId = 1,
                },
                new RestaurantOrderTypes
                {
                    Name = "Tab",
                    TypeId = 2,
                },
                new RestaurantOrderTypes
                {
                    Name = "Take Out",
                    TypeId = 3,
                },
                new RestaurantOrderTypes
                {
                    Name = "Delivery",
                    TypeId = 4,
                },
                new RestaurantOrderTypes
                {
                    Name = "Quick",
                    TypeId = 5,
                }
            };
            DataHelper.AddDataToDb(orderTypes);
        }

        public static void AddOrderSplittingTypes()
        {
            var orderTypes = new []
            {
                new RestaurantOrderSplittingTypes
                {
                    Name = "Void",
                    TypeId = 0
                },
                new RestaurantOrderSplittingTypes
                {
                    Name = "AllOnOne",
                    TypeId = 1
                },
                new RestaurantOrderSplittingTypes
                {
                    Name = "SplitBySeating",
                    TypeId = 2
                },
                new RestaurantOrderSplittingTypes
                {
                    Name = "SplitEvently",
                    TypeId = 3
                },
                new RestaurantOrderSplittingTypes
                {
                    Name = "SplitByMenuItem",
                    TypeId = 4
                },
                new RestaurantOrderSplittingTypes
                {
                    Name = "SplitProportionally",
                    TypeId = 5
                },
            };
            DataHelper.AddDataToDb(orderTypes);
        }

        public static void AddTenders()
        {
            var tenders = new[]
            {
                new Tenders
                {
                    TenderCode = "CA",
                    TenderName = "Cash",
                    TenderType = "CA",
                    CurrencyID = 1,
                    OffLineLimit = 0,
                    PrimChangeTender = "CA",
                    PrimChangeThresh = 100,
                    ChangeMessage = "Change",
                    MinAmount = 0,
                    MaxAmount = 325,
                    OverAllMaxAmount = 500,
                    MaxRefund = 10,
                    OverAllMaxRefund = 100,
                    AuthWaitDur = 0,
                    PickupLimit = 99000,
                    OverAllPickupLimit = 100000,
                    Attributes = 2310701,
                    Modified = DateTime.Now,
                    modifiedby = 1,
                    SecondaryId = 0,
                    NegativeAmountRequired = 0,
                    Attributes2 = 0,
                    SignOver = 0,
                    BitAttributes = "1000",
                    OfflineMax = 0,
                    OfflineOverallMax = 0
                }
            };
            DataHelper.AddDataToDb(tenders);
        }
    }
}
