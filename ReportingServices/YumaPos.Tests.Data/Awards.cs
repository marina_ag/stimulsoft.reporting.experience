//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YumaPos.WCF.Test.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Awards
    {
        public Awards()
        {
            this.AwardArguments = new HashSet<AwardArguments>();
        }
    
        public int Id { get; set; }
        public int CouponId { get; set; }
        public string TypeName { get; set; }
        public string ProductUpc { get; set; }
        public Nullable<System.Guid> ProductGroupId { get; set; }
        public Nullable<short> DepartmentId { get; set; }
        public Nullable<short> SubDepartment { get; set; }
        public Nullable<int> SectionId { get; set; }
        public Nullable<int> VendorId { get; set; }
        public Nullable<int> Mixmatch { get; set; }
        public System.Guid TenantId { get; set; }
    
        public virtual ICollection<AwardArguments> AwardArguments { get; set; }
        public virtual Tenants Tenants { get; set; }
        public virtual AwardTypes AwardTypes { get; set; }
        public virtual ElectronicCoupons ElectronicCoupons { get; set; }
        public virtual ProductGroups ProductGroups { get; set; }
        public virtual Departments Departments { get; set; }
        public virtual Sections Sections { get; set; }
    }
}
