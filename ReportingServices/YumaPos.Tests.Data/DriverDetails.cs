//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YumaPos.WCF.Test.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DriverDetails
    {
        public DriverDetails()
        {
            this.RestaurantOrders = new HashSet<RestaurantOrders>();
        }
    
        public int EmployeeId { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<decimal> CashAmount { get; set; }
        public Nullable<System.DateTime> LastStatusChange { get; set; }
        public string Location { get; set; }
        public System.Guid TenantId { get; set; }
    
        public virtual Tenants Tenants { get; set; }
        public virtual ICollection<RestaurantOrders> RestaurantOrders { get; set; }
        public virtual Employees Employees { get; set; }
    }
}
