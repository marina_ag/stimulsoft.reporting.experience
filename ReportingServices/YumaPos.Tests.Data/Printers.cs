//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YumaPos.WCF.Test.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Printers
    {
        public Printers()
        {
            this.MenuCategories = new HashSet<MenuCategories>();
        }
    
        public System.Guid PrinterId { get; set; }
        public string Name { get; set; }
        public System.Guid TenantId { get; set; }
    
        public virtual ICollection<MenuCategories> MenuCategories { get; set; }
        public virtual Tenants Tenants { get; set; }
    }
}
