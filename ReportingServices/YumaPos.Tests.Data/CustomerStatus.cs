//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YumaPos.WCF.Test.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerStatus
    {
        public CustomerStatus()
        {
            this.CustomerAddresses = new HashSet<CustomerAddresses>();
            this.CustomerCoupons = new HashSet<CustomerCoupons>();
            this.CustomerUsers = new HashSet<CustomerUsers>();
            this.RestaurantOrders2Customers = new HashSet<RestaurantOrders2Customers>();
            this.CustomerGroups = new HashSet<CustomerGroups>();
        }
    
        public string CardNo { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Mi { get; set; }
        public Nullable<short> Sex { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public Nullable<short> MaritalStatus { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public Nullable<short> GeographicID { get; set; }
        public Nullable<short> Category { get; set; }
        public string HomePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string DL { get; set; }
        public Nullable<decimal> CashBack { get; set; }
        public Nullable<decimal> Balance { get; set; }
        public Nullable<short> Discount { get; set; }
        public Nullable<decimal> CreditLimit { get; set; }
        public Nullable<bool> ChargeOk { get; set; }
        public Nullable<bool> FrequentShopper { get; set; }
        public Nullable<bool> WriteChecks { get; set; }
        public Nullable<bool> StoreCoupons { get; set; }
        public Nullable<bool> GovermentCheck { get; set; }
        public Nullable<bool> PayrollCheck { get; set; }
        public Nullable<short> TermsID { get; set; }
        public string Type { get; set; }
        public string ResaleNo { get; set; }
        public string Link { get; set; }
        public Nullable<short> ShoppedFor { get; set; }
        public Nullable<decimal> Purchases { get; set; }
        public Nullable<short> NumberOfChecks { get; set; }
        public Nullable<int> PointsEarned { get; set; }
        public string Memo { get; set; }
        public Nullable<short> Deleted { get; set; }
        public Nullable<System.DateTime> CardIssued { get; set; }
        public Nullable<System.DateTime> CardExpire { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<short> PricingLevel { get; set; }
        public Nullable<short> ReceiptType { get; set; }
        public string Email { get; set; }
        public string BillToName { get; set; }
        public string BillToAddress1 { get; set; }
        public string BillToAddress2 { get; set; }
        public string BillToCity { get; set; }
        public string BillToState { get; set; }
        public string BillToZip { get; set; }
        public Nullable<int> Attributes { get; set; }
        public Nullable<int> DiscountSheduleId { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.Guid> Photo { get; set; }
    
        public virtual ICollection<CustomerAddresses> CustomerAddresses { get; set; }
        public virtual ICollection<CustomerCoupons> CustomerCoupons { get; set; }
        public virtual DiscountShedules DiscountShedules { get; set; }
        public virtual ICollection<CustomerUsers> CustomerUsers { get; set; }
        public virtual ICollection<RestaurantOrders2Customers> RestaurantOrders2Customers { get; set; }
        public virtual ICollection<CustomerGroups> CustomerGroups { get; set; }
        public virtual Images Images { get; set; }
    }
}
