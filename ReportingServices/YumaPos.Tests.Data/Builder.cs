//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YumaPos.WCF.Test.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Builder
    {
        public Builder()
        {
            this.BuilderCategories = new HashSet<BuilderCategories>();
        }
    
        public System.Guid BuilderId { get; set; }
        public System.Guid MenuCategoryId { get; set; }
        public bool IsHidden { get; set; }
        public Nullable<System.Guid> ImageId { get; set; }
        public System.Guid TenantId { get; set; }
    
        public virtual Tenants Tenants { get; set; }
        public virtual MenuCategories MenuCategories { get; set; }
        public virtual ICollection<BuilderCategories> BuilderCategories { get; set; }
    }
}
