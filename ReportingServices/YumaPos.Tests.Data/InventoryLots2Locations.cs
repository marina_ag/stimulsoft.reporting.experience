//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YumaPos.WCF.Test.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class InventoryLots2Locations
    {
        public System.Guid LotId { get; set; }
        public System.Guid InventoryLocationId { get; set; }
        public decimal Quantity { get; set; }
        public System.Guid TenantId { get; set; }
    
        public virtual InventoryLocations InventoryLocations { get; set; }
        public virtual Tenants Tenants { get; set; }
        public virtual InventoryLots InventoryLots { get; set; }
    }
}
