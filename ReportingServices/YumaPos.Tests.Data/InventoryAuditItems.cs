//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YumaPos.WCF.Test.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class InventoryAuditItems
    {
        public System.Guid InventoryAuditItemId { get; set; }
        public decimal CurrentQuantity { get; set; }
        public decimal RealQuantity { get; set; }
        public int Reason { get; set; }
        public string Comment { get; set; }
        public System.DateTime Created { get; set; }
        public System.Guid InventoryAuditId { get; set; }
        public System.Guid InventoryId { get; set; }
        public System.Guid TenantId { get; set; }
    
        public virtual InventoryAudits InventoryAudits { get; set; }
        public virtual Tenants Tenants { get; set; }
        public virtual InventoryItems InventoryItems { get; set; }
    }
}
