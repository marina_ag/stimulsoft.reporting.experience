//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YumaPos.WCF.Test.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class InventoryCategories
    {
        public InventoryCategories()
        {
            this.InventoryCategories1 = new HashSet<InventoryCategories>();
            this.InventoryItems = new HashSet<InventoryItems>();
        }
    
        public System.Guid InventoryCategoryId { get; set; }
        public string Name { get; set; }
        public Nullable<System.Guid> ParentCategoryId { get; set; }
        public System.Guid TenantId { get; set; }
    
        public virtual ICollection<InventoryCategories> InventoryCategories1 { get; set; }
        public virtual InventoryCategories InventoryCategories2 { get; set; }
        public virtual Tenants Tenants { get; set; }
        public virtual ICollection<InventoryItems> InventoryItems { get; set; }
    }
}
