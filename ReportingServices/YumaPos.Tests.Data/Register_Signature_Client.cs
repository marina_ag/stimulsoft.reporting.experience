//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YumaPos.WCF.Test.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Register_Signature_Client
    {
        public Nullable<System.DateTime> Proc_Date { get; set; }
        public Nullable<System.DateTime> Datetime { get; set; }
        public Nullable<short> emp_no { get; set; }
        public Nullable<short> till_no { get; set; }
        public Nullable<short> Register_no { get; set; }
        public Nullable<int> Trans_no { get; set; }
        public string Tender_Type { get; set; }
        public Nullable<decimal> Tender_amt { get; set; }
        public string Last_Name { get; set; }
        public string First_Name { get; set; }
        public string Mi { get; set; }
        public string Account { get; set; }
        public Nullable<System.DateTime> Expire_Date { get; set; }
        public string Authtype { get; set; }
        public string Authcode { get; set; }
        public string Data_type { get; set; }
        public string Data_format { get; set; }
        public Nullable<int> Data_length { get; set; }
        public string Data_blob { get; set; }
        public Nullable<short> CardOrder { get; set; }
        public string Receipt { get; set; }
        public Nullable<bool> Accepted { get; set; }
        public int trans_id { get; set; }
        public byte uploaded { get; set; }
        public System.Guid TenantId { get; set; }
    
        public virtual Tenants Tenants { get; set; }
    }
}
