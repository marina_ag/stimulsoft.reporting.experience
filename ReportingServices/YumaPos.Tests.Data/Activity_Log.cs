//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YumaPos.WCF.Test.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Activity_Log
    {
        public short EmployeeID { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string Activity { get; set; }
        public System.Guid TenantId { get; set; }
    
        public virtual Tenants Tenants { get; set; }
    }
}
