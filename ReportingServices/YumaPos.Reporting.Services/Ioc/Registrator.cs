﻿using YumaPos.Reporting.Infrastructure.Ioc;
using YumaPos.Reporting.Infrastructure.Logic.Services.SalesSummary;
using YumaPos.Reporting.Services.SalesSummary;

namespace YumaPos.Reporting.Services.Ioc
{
    public class Registrator : IRegistrator
    {
        public void Register(IBootstrapperContainer container)
        {
            container.Register(typeof(ISalesByDayService), typeof(SalesByDayService), Lifetime.PerLifetimeScope);
            container.Register(typeof(ISalesSummaryService), typeof(SalesSummaryService), Lifetime.PerLifetimeScope);
            container.Register(typeof(ISalesByWeekdaysService), typeof(SalesByWeekdaysService), Lifetime.PerLifetimeScope);
            container.Register(typeof(ISalesByHoursService), typeof(SalesByHoursService), Lifetime.PerLifetimeScope);
        }
    }
}
