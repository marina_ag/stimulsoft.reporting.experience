﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

using YumaPos.Reporting.Data.ReportsData;
using YumaPos.Reporting.Infrastructure.Data;
using YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary;
using YumaPos.Reporting.Infrastructure.Logic.Services.SalesSummary;

namespace YumaPos.Reporting.Services.SalesSummary
{
    public class SalesSummaryService : ISalesSummaryService
    {
        private readonly IReportingRepository<SalesByDay> _salesByDayRepository;

        public SalesSummaryService(IReportingRepository<SalesByDay> salesByDayRepository)
        {
            _salesByDayRepository = salesByDayRepository;
        }

        public async Task<SalesSummaryModel> GetSalesSummary(DateTime startDate, DateTime endDate, Guid[] stores)
        {
            var query = _salesByDayRepository.Query().Where(x => x.WorkDate <= endDate && x.WorkDate >= startDate && stores.Contains(x.StoreId));

            decimal salesAmount = 0m, taxesAmount = 0m, fullAmount = 0m;
            if (query.Any())
            {
                salesAmount = await query.SumAsync(x => x.NetSalesAmount);
                taxesAmount = await query.SumAsync(x => x.TaxesAmount);
                fullAmount = await query.SumAsync(x => x.SalesAndTaxAmount);
            }

            return new SalesSummaryModel { NetSalesTotal = salesAmount, NetSalesTaxesAmount = taxesAmount, NetSalesTotalWithTaxes = fullAmount };
        }

        public async Task<SalesSummaryModel> GetSalesSummary(DateTime startDate, DateTime endDate)
        {
            var query = _salesByDayRepository.Query().Where(x => x.WorkDate <= endDate && x.WorkDate >= startDate);

            decimal salesAmount = 0m, taxesAmount = 0m, fullAmount = 0m;
            if (query.Any())
            {
                salesAmount = await query.SumAsync(x => x.NetSalesAmount);
                taxesAmount = await query.SumAsync(x => x.TaxesAmount);
                fullAmount = await query.SumAsync(x => x.SalesAndTaxAmount);
            }

            return new SalesSummaryModel { NetSalesTotal = salesAmount, NetSalesTaxesAmount = taxesAmount, NetSalesTotalWithTaxes = fullAmount };
        }
    }
}