﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Threading.Tasks;

using YumaPos.Reporting.Data.ReportsData;
using YumaPos.Reporting.Infrastructure.Data;
using YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary;
using YumaPos.Reporting.Infrastructure.Logic.Services.SalesSummary;

namespace YumaPos.Reporting.Services.SalesSummary
{
    public class SalesByWeekdaysService : ISalesByWeekdaysService
    {
        private readonly IReportingRepository<SalesByDay> _salesByDayRepository;

        public SalesByWeekdaysService(IReportingRepository<SalesByDay> salesByDayRepository)
        {
            _salesByDayRepository = salesByDayRepository;
        }

        public async Task<List<SalesByWeekdaysModel>> GetSalesByWeekdays(DateTime startDate, DateTime endDate, Guid[] stores)
        {
            return
                await
                _salesByDayRepository.Query()
                    .Where(t => t.WorkDate <= endDate && t.WorkDate >= startDate && stores.Contains(t.StoreId))
                    .GroupBy(x => System.Data.Entity.SqlServer.SqlFunctions.DatePart("dw", x.WorkDate))
                    .Select(
                        x =>
                        new SalesByWeekdaysModel
                            {
                                Weekday = (DayOfWeek)(x.Key - 1), 
                                SoldQuantity = x.Sum(s => s.SoldQuantity), 
                                NetSalesTotal = x.Sum(s => s.NetSalesAmount), 
                                AllTaxesAmount = x.Sum(s => s.TaxesAmount), 
                                SalesTotalWithTax = x.Sum(s => s.SalesAndTaxAmount), 
                                NetDiscountTotal = x.Sum(s => s.DiscountAmount)
                            })
                    .ToListAsync();
        }

        public async Task<List<SalesByWeekdaysModel>> GetSalesByWeekdays(DateTime startDate, DateTime endDate)
        {
            return
                await
                _salesByDayRepository.Query()
                    .Where(t => t.WorkDate <= endDate && t.WorkDate >= startDate)
                    .GroupBy(x => SqlFunctions.DatePart("dw", x.WorkDate))
                    .Select(
                        x =>
                        new SalesByWeekdaysModel
                            {
                                Weekday = (DayOfWeek)(x.Key - 1), 
                                SoldQuantity = x.Sum(s => s.SoldQuantity), 
                                NetSalesTotal = x.Sum(s => s.NetSalesAmount), 
                                AllTaxesAmount = x.Sum(s => s.TaxesAmount), 
                                SalesTotalWithTax = x.Sum(s => s.SalesAndTaxAmount), 
                                NetDiscountTotal = x.Sum(s => s.DiscountAmount)
                            })
                    .ToListAsync();
        }
    }
}