﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

using YumaPos.Reporting.Data.ReportsData;
using YumaPos.Reporting.Infrastructure.Data;
using YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary;
using YumaPos.Reporting.Infrastructure.Logic.Services.SalesSummary;

namespace YumaPos.Reporting.Services.SalesSummary
{
    public class SalesByDayService : ISalesByDayService
    {
        private readonly IReportingRepository<SalesByDay> _salesByDayRepository;

        public SalesByDayService(IReportingRepository<SalesByDay> salesByDayRepository)
        {
            _salesByDayRepository = salesByDayRepository;
        }

        public async Task<List<SalesByDayModel>> GetSalesByDay(DateTime startDate, DateTime endDate, Guid[] stores)
        {
            return
                await
                _salesByDayRepository.Query()
                    .Where(x => x.WorkDate <= endDate && x.WorkDate >= startDate && stores.Contains(x.StoreId))
                    .GroupBy(x => DbFunctions.TruncateTime(x.WorkDate))
                    .Select(
                        x =>
                        new SalesByDayModel
                            {
                                Date = x.FirstOrDefault().WorkDate, 
                                SoldQuantity = x.Sum(t => t.SoldQuantity), 
                                NetSalesTotal = x.Sum(t => t.NetSalesAmount), 
                                AllTaxesAmount = x.Sum(t => t.TaxesAmount), 
                                NetSalesWithTax = x.Sum(t => t.SalesAndTaxAmount), 
                                NetDiscountTotal = x.Sum(t => t.DiscountAmount), 
                                NumberOfSales = x.Sum(t => t.NumberOfSales), 
                                AverageUnits = x.Sum(t => t.AverageUnitsSold), 
                                AverageValueSold = x.Sum(t => t.AverageValueSold)
                            })
                    .ToListAsync();
        }

        public async Task<List<SalesByDayModel>> GetSalesByDay(DateTime startDate, DateTime endDate)
        {
            return
                await
                _salesByDayRepository.Query()
                   .Where(x => x.WorkDate <= endDate && x.WorkDate >= startDate)
                   .GroupBy(x => DbFunctions.TruncateTime(x.WorkDate))
                    .Select(
                        x =>
                        new SalesByDayModel
                        {
                            Date = x.FirstOrDefault().WorkDate,
                            SoldQuantity = x.Sum(t => t.SoldQuantity),
                            NetSalesTotal = x.Sum(t => t.NetSalesAmount),
                            AllTaxesAmount = x.Sum(t => t.TaxesAmount),
                            NetSalesWithTax = x.Sum(t => t.SalesAndTaxAmount),
                            NetDiscountTotal = x.Sum(t => t.DiscountAmount),
                            NumberOfSales = x.Sum(t => t.NumberOfSales),
                            AverageUnits = x.Sum(t => t.AverageUnitsSold),
                            AverageValueSold = x.Sum(t => t.AverageValueSold)
                        })
                    .ToListAsync();
        }
    }
}