﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

using YumaPos.Reporting.Data.ReportsData;
using YumaPos.Reporting.Infrastructure.Data;
using YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary;
using YumaPos.Reporting.Infrastructure.Logic.Services.SalesSummary;

namespace YumaPos.Reporting.Services.SalesSummary
{
    public class SalesByHoursService : ISalesByHoursService
    {
        private readonly IReportingRepository<SalesByHour> _salesByHoursRepository;

        public SalesByHoursService(IReportingRepository<SalesByHour> salesByHoursRepository)
        {
            _salesByHoursRepository = salesByHoursRepository;
        }

        public async Task<List<SalesByHoursModel>> GetSalesByHours(DateTime startDate, DateTime endDate, Guid[] stores)
        {
            return
                await
                _salesByHoursRepository.Query()
                    .Where(t => t.WorkDate <= endDate && t.WorkDate >= startDate && stores.Contains(t.StoreId))
                    .GroupBy(x => x.Hour)
                    .Select(
                        item =>
                        new SalesByHoursModel
                            {
                                Hour = item.Key, 
                                SoldQuantity = item.Sum(t => t.SoldQuantity), 
                                NetSalesTotal = item.Sum(t => t.NetSalesAmount), 
                                AllTaxesAmount = item.Sum(t => t.TaxesAmount), 
                                SalesTotalWithTax = item.Sum(t => t.SalesAndTaxAmount), 
                                NetDiscountTotal = item.Sum(t => t.DiscountAmount)
                            })
                    .ToListAsync();
        }

        public async Task<List<SalesByHoursModel>> GetSalesByHours(DateTime startDate, DateTime endDate)
        {
            return
                await
                _salesByHoursRepository.Query()
                    .Where(t => t.WorkDate <= endDate && t.WorkDate >= startDate)
                    .GroupBy(x => x.Hour)
                    .Select(
                        item =>
                        new SalesByHoursModel
                            {
                                Hour = item.Key, 
                                SoldQuantity = item.Sum(t => t.SoldQuantity), 
                                NetSalesTotal = item.Sum(t => t.NetSalesAmount), 
                                AllTaxesAmount = item.Sum(t => t.TaxesAmount), 
                                SalesTotalWithTax = item.Sum(t => t.SalesAndTaxAmount), 
                                NetDiscountTotal = item.Sum(t => t.DiscountAmount)
                            })
                    .ToListAsync();
        }
    }
}