﻿using System;

namespace YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary
{
    public class SalesByDayModel
    {
        public DateTime Date { get; set; }

        public long SoldQuantity { get; set; }

        public decimal NetSalesTotal { get; set; }

        public decimal AllTaxesAmount { get; set; }

        public decimal NetSalesWithTax { get; set; }

        public decimal NetDiscountTotal { get; set; }

        public int NumberOfSales { get; set; }

        public double AverageUnits { get; set; }

        public decimal AverageValueSold { get; set; }
    }
}
