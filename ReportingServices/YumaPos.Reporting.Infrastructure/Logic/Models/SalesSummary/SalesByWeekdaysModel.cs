﻿using System;

namespace YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary
{
    public class SalesByWeekdaysModel
    {
        public DayOfWeek Weekday { get; set; }

        public long SoldQuantity { get; set; }

        public decimal NetSalesTotal { get; set; }

        public decimal AllTaxesAmount { get; set; }

        public decimal SalesTotalWithTax { get; set; }

        public decimal NetDiscountTotal { get; set; }
    }
}
