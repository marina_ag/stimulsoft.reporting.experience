﻿namespace YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary
{
    public class SalesSummaryModel
    {
        public decimal NetSalesTotal { get; set; }

        public decimal NetSalesTaxesAmount { get; set; }

        public decimal NetSalesTotalWithTaxes { get; set; }
    }
}
