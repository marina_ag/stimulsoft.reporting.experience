﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary;

namespace YumaPos.Reporting.Infrastructure.Logic.Services.SalesSummary
{
    public interface ISalesByDayService
    {
        Task<List<SalesByDayModel>> GetSalesByDay(DateTime startDate, DateTime endDate, Guid[] stores);

        Task<List<SalesByDayModel>> GetSalesByDay(DateTime startDate, DateTime endDate);
    }
}
