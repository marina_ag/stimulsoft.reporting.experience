﻿using System;
using System.Threading.Tasks;

using YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary;

namespace YumaPos.Reporting.Infrastructure.Logic.Services.SalesSummary
{
    public interface ISalesSummaryService
    {
        Task<SalesSummaryModel> GetSalesSummary(DateTime startDate, DateTime endDate, Guid[] stores);

        Task<SalesSummaryModel> GetSalesSummary(DateTime startDate, DateTime endDate); 
    }
}