﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary;

namespace YumaPos.Reporting.Infrastructure.Logic.Services.SalesSummary
{
    public interface ISalesByHoursService
    {
        Task<List<SalesByHoursModel>> GetSalesByHours(DateTime startDate, DateTime endDate, Guid[] stores);

        Task<List<SalesByHoursModel>> GetSalesByHours(DateTime startDate, DateTime endDate);  
    }
}