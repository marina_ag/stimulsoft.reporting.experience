﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary;

namespace YumaPos.Reporting.Infrastructure.Logic.Services.SalesSummary
{
    public interface ISalesByWeekdaysService
    {
        Task<List<SalesByWeekdaysModel>> GetSalesByWeekdays(DateTime startDate, DateTime endDate, Guid[] stores);

        Task<List<SalesByWeekdaysModel>> GetSalesByWeekdays(DateTime startDate, DateTime endDate); 
    }
}