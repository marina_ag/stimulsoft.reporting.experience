﻿namespace YumaPos.Reporting.Infrastructure.Ioc
{
    public enum Lifetime
    {
        Singleton,
        PerLifetimeScope,
        PerRequest,
        PerThread
    }
}
