﻿namespace YumaPos.Reporting.Infrastructure.Ioc
{
    public interface IRegistrator
    {
        void Register(IBootstrapperContainer container);
    }
}
