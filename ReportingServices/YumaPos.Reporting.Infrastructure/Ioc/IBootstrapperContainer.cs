﻿using System;
using System.Reflection;

namespace YumaPos.Reporting.Infrastructure.Ioc
{
    public interface IBootstrapperContainer
    {
        void Build(object config, Assembly addAssembly);

        void RegisterType<I, T>() where T : I;

        void RegisterType<I, T>(Lifetime lifetime) where T : I;

        void Register(Type @interface, Type implementation, Lifetime lifetime);

        void RegisterKeyed(Type @interface, Type implementation, Enum key, Lifetime lifetime);

        void Update(Type @interface, Type implementation, Lifetime lifetime);

        I Resolve<I>();
    }
}
