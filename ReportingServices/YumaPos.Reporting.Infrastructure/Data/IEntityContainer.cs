﻿namespace YumaPos.Reporting.Infrastructure.Data
{
    public interface IEntityContainer
    {
        object Context { get; }
    }
}
