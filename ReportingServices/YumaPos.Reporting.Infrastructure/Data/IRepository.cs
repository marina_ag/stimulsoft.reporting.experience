﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YumaPos.Reporting.Infrastructure.Data
{
    public interface IRepository<T> : IRepository
    {
        IQueryable<T> Query();

        T Add(T entity);

        void AddRange(IEnumerable<T> entities);

        void Remove(T entity);

        void RemoveRange(IEnumerable<T> entities);

        T Find(object[] keyValues);

        Task<T> FindAsync(object[] keyValues);

        int SaveChanges();

        Task<int> SaveChangesAsync();
    }

    public interface IRepository
    {
    }
}
