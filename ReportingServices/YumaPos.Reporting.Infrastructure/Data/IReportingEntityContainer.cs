﻿namespace YumaPos.Reporting.Infrastructure.Data
{
    public interface IReportingEntityContainer
    {
        object Context { get; } 
    }
}