﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using Bleu.Reporting.Infrastructure.Logic.Models;
using Bleu.Reporting.Infrastructure.Logic.Services;
using Stimulsoft.Report;
using Stimulsoft.Report.MvcMobile;

namespace Bleu.Reporting.ReportsDesigner.Controllers
{
    public class SalesSummaryReportsController : Controller
    {
        private readonly List<SalesByDayModel> _data;
        public ActionResult Index()
        {
            return View();
        }
        public SalesSummaryReportsController(ISalesSummaryByDayService service)
        {
            _data = service.GetAllSalesByDay(new DateTime(2015, 1, 1), DateTime.Today);
        }

        public ActionResult GetReportTemplate()
        {
            const int busobjLevel = 1;

            StiReport report = new StiReport();
            report.Load(Server.MapPath("~/Reports/SalesByDayReport.mrt"));

            report.RegBusinessObject("SalesByDayBO", _data);
            report.Dictionary.SynchronizeBusinessObjects(busobjLevel);

            return StiMvcMobileDesigner.GetReportTemplateResult(HttpContext, report);
        }

        public ActionResult GetReportSnapshot()
        {
            StiReport report = new StiReport();
            report.Load(Server.MapPath("~/Reports/SalesByDayReport.mrt"));

            report.RegBusinessObject("SalesByDayBO", _data);

            // Return the report snapshot result to the client
            return StiMvcMobileDesigner.GetReportSnapshotResult(HttpContext, report);
        }

        public ActionResult OpenReportTemplate()
        {
            return StiMvcMobileDesigner.OpenReportTemplateResult(HttpContext);
        }

        public ActionResult SaveReportTemplate()
        {
            StiReport report = StiMvcMobileDesigner.GetReportObject(HttpContext);
            report.Save("~/Reports/" + report.ReportName + ".mrt");

            return StiMvcMobileDesigner.SaveReportTemplateResult(HttpContext);
        }

        public ActionResult GetNewReportData()
        {
            StiReport report = StiMvcMobileDesigner.GetReportObject(HttpContext);

            return StiMvcMobileDesigner.GetNewReportDataResult(HttpContext, report);
        }

        public ActionResult DesignerEvent()
        {
            return StiMvcMobileDesigner.DesignerEventResult(HttpContext);
        }
    }
}