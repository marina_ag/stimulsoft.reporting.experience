﻿using System.Web.Mvc;

namespace Bleu.Reporting.ReportsDesigner.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SalesByDay(object start, object end)
        {
            return RedirectToAction("Index", "SalesSummaryReports");
        }
    }
}