﻿using Bleu.Reporting.Infrastructure.Ioc;

namespace Bleu.Reporting.ReportsDesigner.Ioc
{
    public class Registrator : IRegistrator
    {
        public void Register(IBootstrapperContainer container)
        {
            // example
            //container.Register(typeof(IImageService), typeof(ImageService), Lifetime.PerLifetimeScope);
        }
    }
}