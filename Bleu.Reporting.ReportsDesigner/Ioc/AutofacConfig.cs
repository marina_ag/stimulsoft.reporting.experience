﻿using System;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Bleu.Reporting.Infrastructure.Ioc;

namespace Bleu.Reporting.ReportsDesigner.Ioc
{
    public class AutofacConfig : IBootstrapperContainer
    {
        private ContainerBuilder _builder;
        private IContainer _container;

        public AutofacConfig()
        {
            _builder = new ContainerBuilder();
        }

        public void RegisterType<I, T>() where T : I
        {
            RegisterType<I, T>(Lifetime.PerLifetimeScope);
        }

        public void RegisterType<I, T>(Lifetime lifetime) where T : I
        {
            var builder = _builder.RegisterType<T>().As<I>();
            switch (lifetime)
            {
                case Lifetime.Singleton:
                    builder.SingleInstance();
                    break;
                case Lifetime.PerLifetimeScope:
                    builder.InstancePerLifetimeScope();
                    break;
                case Lifetime.PerRequest:
                    builder.InstancePerRequest();
                    break;
            }
        }

        public void Register(Type @interface, Type implementation, Lifetime lifetime)
        {
            if (@interface.IsGenericType)
            {
                var builder = _builder.RegisterGeneric(implementation).As(@interface);
                switch (lifetime)
                {
                    case Lifetime.Singleton:
                        builder.SingleInstance();
                        break;
                    case Lifetime.PerLifetimeScope:
                        builder.InstancePerLifetimeScope();
                        break;
                    case Lifetime.PerRequest:
                        builder.InstancePerRequest();
                        break;
                }
            }
            else
            {
                var builder = _builder.RegisterType(implementation).As(@interface);
                switch (lifetime)
                {
                    case Lifetime.Singleton:
                        builder.SingleInstance();
                        break;
                    case Lifetime.PerLifetimeScope:
                        builder.InstancePerLifetimeScope();
                        break;
                    case Lifetime.PerRequest:
                        builder.InstancePerRequest();
                        break;
                }
            }
        }

        public void RegisterKeyed(Type @interface, Type implementation, Enum key, Lifetime lifetime)
        {
            if (@interface.IsGenericType)
            {
                var builder = _builder.RegisterGeneric(implementation).Keyed(key, @interface);
                switch (lifetime)
                {
                    case Lifetime.Singleton:
                        builder.SingleInstance();
                        break;
                    case Lifetime.PerLifetimeScope:
                        builder.InstancePerLifetimeScope();
                        break;
                    case Lifetime.PerRequest:
                        builder.InstancePerRequest();
                        break;
                }
            }
            else
            {
                var builder = _builder.RegisterType(implementation).Keyed(key, @interface);
                switch (lifetime)
                {
                    case Lifetime.Singleton:
                        builder.SingleInstance();
                        break;
                    case Lifetime.PerLifetimeScope:
                        builder.InstancePerLifetimeScope();
                        break;
                    case Lifetime.PerRequest:
                        builder.InstancePerRequest();
                        break;
                }
            }
        }

        public void Update(Type @interface, Type implementation, Lifetime lifetime)
        {
            _builder = new ContainerBuilder();
            if (@interface.IsGenericType)
            {
                var builder = _builder.RegisterGeneric(implementation).As(@interface);
                switch (lifetime)
                {
                    case Lifetime.Singleton:
                        builder.SingleInstance();
                        break;
                    case Lifetime.PerLifetimeScope:
                        builder.InstancePerLifetimeScope();
                        break;
                    case Lifetime.PerRequest:
                        builder.InstancePerRequest();
                        break;
                }
            }
            else
            {
                var builder = _builder.RegisterType(implementation).As(@interface);
                switch (lifetime)
                {
                    case Lifetime.Singleton:
                        builder.SingleInstance();
                        break;
                    case Lifetime.PerLifetimeScope:
                        builder.InstancePerLifetimeScope();
                        break;
                    case Lifetime.PerRequest:
                        builder.InstancePerRequest();
                        break;
                }
            }

            _builder.Update(_container);
        }

        public I Resolve<I>()
        {
            return _container.Resolve<I>();
        }

        public void Build(object config, Assembly addAssembly)
        {
            _builder.RegisterControllers(Assembly.GetExecutingAssembly());
            if (addAssembly != null)
            {
                _builder.RegisterApiControllers(Assembly.GetExecutingAssembly(), addAssembly);
            }
            else
            {
                _builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            }

            _container = _builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(_container));
            ((HttpConfiguration) config).DependencyResolver = new AutofacWebApiDependencyResolver(_container);
            _builder = null;
        }
    }
}