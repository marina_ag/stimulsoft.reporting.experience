﻿using System.Web;
using System.Web.Mvc;

namespace Bleu.Reporting.ReportsDesigner
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
