﻿using System.Web;

using YumaPos.Reporting.Infrastructure.Logic;

namespace Bleu.Web.Services
{
    public class TenantService : ITenantService
    {
        private string _alias = string.Empty;

        public string TenantAlias
        {
            get
            {
                if (_alias != string.Empty)
                {
                    return _alias;
                }

                _alias = HttpContext.Current.Request.Url.Host;
                return _alias;
            }
        }
    }
}