﻿using System;
using System.Configuration;

namespace Bleu.Web.Helpers
{
    public static class RequestUrlHelper
    {
        public static string GetConnectionString(Uri url, string connectionString)
        {
            var connectionStringUri = new Uri(connectionString);
            var alias = GetTenantAlias(url, connectionStringUri.Host);
            return string.Format(
                "{0}://{1}{2}:{3}{4}",
                connectionStringUri.Scheme,
                string.IsNullOrWhiteSpace(alias) ? string.Empty : alias + '.',
                connectionStringUri.Host,
                connectionStringUri.Port,
                connectionStringUri.PathAndQuery);
        }

        private static string GetTenantAlias(Uri url, string domain)
        {
            var hostIndex = url.Host.IndexOf(domain, StringComparison.InvariantCulture);
            if (hostIndex != -1)
            {
                if (hostIndex != 0)
                {
                    return url.Host.Substring(0, url.Host.IndexOf(domain, StringComparison.InvariantCulture) - 1);
                }
            }

            if (url.Host.IndexOf('.') != -1)
            {
                return url.Host.Substring(0, url.Host.IndexOf('.'));
            }

            return string.Empty;
        }
    }
}