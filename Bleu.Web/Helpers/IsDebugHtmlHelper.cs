﻿using System.Web.Mvc;

namespace Bleu.Web.Helpers
{
    public static class IsDebugHtmlHelper
    {
        public static bool IsDebug(this HtmlHelper htmlHelper)
        {
#if DEBUG
            return true;
#else
            return false;
#endif
        }
    }
}