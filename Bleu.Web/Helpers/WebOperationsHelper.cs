﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;

using Newtonsoft.Json;

namespace Bleu.Web.Helpers
{
    public static class WebOperationsHelper
    {
        public static T GetFromCookie<T>(this HttpRequestBase request, string key) where T : class
        {
            var cookie = request.Cookies.Get(ClientConfig.EmployeeCookie);
            if (cookie != null)
            {
                return JsonConvert.DeserializeObject<T>(cookie.Value, new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Objects });
            }

            return null;
        }

        public static void SetToCookie<T>(this HttpResponseBase response, string key, T value, int expireMinutes)
        {
            var cookie = new HttpCookie(ClientConfig.EmployeeCookie)
            {
                Value = JsonConvert.SerializeObject(value, new JsonSerializerSettings() { DateFormatHandling = DateFormatHandling.MicrosoftDateFormat }),
                Expires = DateTime.Now.AddMinutes(expireMinutes)
            };
            response.SetCookie(cookie);
        }

        public static T SendRequest<T, Y>(string url, string method, Dictionary<string, string> headers, Y postData) where T : class
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            
            request.ContentType = "text/json";
            request.Method = method;
            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Set(header.Key, header.Value);
                }
            }
            if (postData != null) { 
                var json = JsonConvert.SerializeObject(postData, new JsonSerializerSettings() { DateFormatHandling = DateFormatHandling.MicrosoftDateFormat });
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }

            var response = (HttpWebResponse)request.GetResponse();
            using (var stream = response.GetResponseStream())
            {
                if (stream != null)
                {
                    var streamReader = new StreamReader(stream);
                    var responseData = streamReader.ReadToEnd();
                    return JsonConvert.DeserializeObject<T>(responseData, new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Objects });
                }
            }

            return null;
        }

        public static void SendRequest(string url, string method, Dictionary<string, string> headers)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "text/json";
            request.Method = method;
            foreach (var header in headers)
            {
                request.Headers.Set(header.Key, header.Value);
            }

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(string.Empty);
                streamWriter.Flush();
                streamWriter.Close();
            }

            request.GetResponse();
        }
    }
}