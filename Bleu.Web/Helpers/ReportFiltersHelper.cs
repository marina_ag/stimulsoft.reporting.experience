﻿using System;

using Bleu.Web.Models.Reporting;

using YumaPos.Reporting.Reports.Resources;

namespace Bleu.Web.Helpers
{
    public static class ReportFiltersHelper
    {
        public static void GetReportFilters(ReportsParamsModel filter, out DateTime startDate, out DateTime endDate, out string[] customer, out int[] customerGroups, out Guid[] menuItems, out Guid[] categories, out Guid[] stores)
        {
            // todo: full complect of filters in this function
            menuItems = null;
            stores = null;
            customerGroups = null;
            categories = null;
            customer = new string[] { };

            if (filter.Store != null)
            {
                stores = filter.Store.Length == 0 ? null : filter.Store;
            }

            if (filter.Group != null)
            {
                customerGroups = filter.Group.Length == 0 ? null : filter.Group;
            }

            if (filter.Category != null)
            {
                categories = filter.Category.Length == 0 ? null : filter.Category;
            }

            if (filter.Item != null)
            {
                menuItems = filter.Item.Length == 0 ? null : filter.Item;
            }

            if (filter.Customer != null)
            {
                customer = filter.Customer.Length == 0 ? null : filter.Customer;
            }

            if (filter.StartDate != null && filter.EndDate != null)
            {
                startDate = filter.StartDate.Value;
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate == null && filter.EndDate != null)
            {
                startDate = filter.EndDate.Value.AddMonths(-1);
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate != null && filter.EndDate == null)
            {
                startDate = filter.StartDate.Value;
                endDate = DateTime.Today;
            }
            else
            {
                startDate = DateTime.Today.AddMonths(-1);
                endDate = DateTime.Today;
            }

            if (startDate.Date > endDate.Date)
            {
                endDate = startDate;
            }
        }

        public static void GetReportFilters(ReportsParamsModel filter, out DateTime startDate, out DateTime endDate, out Guid[] stores, out string storesStr)
        {
            stores = null;
            storesStr = Filters.AllStoresStr;

            if (filter.Store != null)
            {
                stores = filter.Store.Length == 0 ? null : filter.Store;
            }

            if (filter.StoreName != null)
            {
                storesStr = filter.StoreName == string.Empty ? Filters.AllStoresStr : filter.StoreName;
            }

            if (filter.StartDate != null && filter.EndDate != null)
            {
                startDate = filter.StartDate.Value;
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate == null && filter.EndDate != null)
            {
                startDate = filter.EndDate.Value.AddMonths(-1);
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate != null && filter.EndDate == null)
            {
                startDate = filter.StartDate.Value;
                endDate = DateTime.Today;
            }
            else
            {
                startDate = DateTime.Today.AddMonths(-1);
                endDate = DateTime.Today;
            }

            if (startDate.Date > endDate.Date)
            {
                endDate = startDate;
            }
        }

        public static void GetReportFilters(ReportsParamsModel filter, out DateTime startDate, out DateTime endDate, out Guid[] stores, out string[] terminals, out string terminalStr,  out string storesStr)
        {
            stores = null;
            terminals = null;
            terminalStr = Filters.AllTerminalsStr;
            storesStr = Filters.AllStoresStr;

            if (filter.Store != null)
            {
                stores = filter.Store.Length == 0 ? null : filter.Store;
            }

            if (filter.Terminal != null)
            {
                terminals = filter.Terminal.Length == 0 ? null : filter.Terminal;
            }

            if (filter.StoreName != null)
            {
                storesStr = filter.StoreName == string.Empty ? Filters.AllStoresStr : filter.StoreName;
            }

            if (filter.TerminalName != null)
            {
                terminalStr = filter.TerminalName == string.Empty ? Filters.AllTerminalsStr : filter.TerminalName;
            }

            if (filter.StartDate != null && filter.EndDate != null)
            {
                startDate = filter.StartDate.Value;
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate == null && filter.EndDate != null)
            {
                startDate = filter.EndDate.Value.AddMonths(-1);
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate != null && filter.EndDate == null)
            {
                startDate = filter.StartDate.Value;
                endDate = DateTime.Today;
            }
            else
            {
                startDate = DateTime.Today.AddMonths(-1);
                endDate = DateTime.Today;
            }

            if (startDate.Date > endDate.Date)
            {
                endDate = startDate;
            }
        }

        public static void GetReportFilters(ReportsParamsModel filter, out DateTime startDate, out DateTime endDate, out Guid[] stores, out int[] employees, out string storesStr, out string employeesStr)
        {
            stores = null;
            employees = null;
            storesStr = Filters.AllStoresStr;
            employeesStr = Filters.AllEmployeesStr;

            if (filter.Store != null)
            {
                stores = filter.Store.Length == 0 ? null : filter.Store;
            }

            if (filter.StoreName != null)
            {
                storesStr = filter.StoreName == string.Empty ? Filters.AllStoresStr : filter.StoreName;
            }

            if (filter.Employee != null)
            {
                employees = filter.Employee.Length == 0 ? null : filter.Employee;
            }

            if (filter.EmployeeName != null)
            {
                employeesStr = filter.EmployeeName == string.Empty ? Filters.AllEmployeesStr : filter.EmployeeName;
            }

            if (filter.StartDate != null && filter.EndDate != null)
            {
                startDate = filter.StartDate.Value;
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate == null && filter.EndDate != null)
            {
                startDate = filter.EndDate.Value.AddMonths(-1);
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate != null && filter.EndDate == null)
            {
                startDate = filter.StartDate.Value;
                endDate = DateTime.Today;
            }
            else
            {
                startDate = DateTime.Today.AddMonths(-1);
                endDate = DateTime.Today;
            }

            if (startDate.Date > endDate.Date)
            {
                endDate = startDate;
            }
        }

        public static void GetReportFilters(ReportsParamsModel filter, out DateTime startDate, out DateTime endDate, out string[] customer, out int[] customerGroups, out Guid[] menuItems, out Guid[] categories, out Guid[] stores, out int[] employees, out string[] terminals)
        {
            menuItems = null;
            stores = null;
            customerGroups = null;
            categories = null;
            terminals = null;
            employees = null;
            customer = new string[] { };

            if (filter.Store != null)
            {
                stores = filter.Store.Length == 0 ? null : filter.Store;
            }

            if (filter.Group != null)
            {
                customerGroups = filter.Group.Length == 0 ? null : filter.Group;
            }

            if (filter.Category != null)
            {
                categories = filter.Category.Length == 0 ? null : filter.Category;
            }

            if (filter.Item != null)
            {
                menuItems = filter.Item.Length == 0 ? null : filter.Item;
            }

            if (filter.Customer != null)
            {
                customer = filter.Customer.Length == 0 ? null : filter.Customer;
            }

            if (filter.Terminal != null)
            {
                terminals = filter.Terminal.Length == 0 ? null : filter.Terminal;
            }

            if (filter.Employee != null)
            {
                employees = filter.Employee.Length == 0 ? null : filter.Employee;
            }

            if (filter.StartDate != null && filter.EndDate != null)
            {
                startDate = filter.StartDate.Value;
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate == null && filter.EndDate != null)
            {
                startDate = filter.EndDate.Value.AddMonths(-1);
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate != null && filter.EndDate == null)
            {
                startDate = filter.StartDate.Value;
                endDate = DateTime.Today;
            }
            else
            {
                startDate = DateTime.Today.AddMonths(-1);
                endDate = DateTime.Today;
            }

            if (startDate.Date > endDate.Date)
            {
                endDate = startDate;
            }
        }

        public static void GetReportFilters(ReportsParamsModel filter, out DateTime startDate, out DateTime endDate, out Guid[] stores, out Guid[] categories, out string storesStr, out string categoriesStr)
        {
            stores = null;
            categories = null;
            storesStr = Filters.AllStoresStr;
            categoriesStr = Filters.AllItemsCategoriesStr;

            if (filter.Store != null)
            {
                stores = filter.Store.Length == 0 ? null : filter.Store;
            }

            if (filter.Category != null)
            {
                categories = filter.Category.Length == 0 ? null : filter.Category;
            }

            if (filter.StoreName != null)
            {
                storesStr = filter.StoreName == string.Empty ? Filters.AllStoresStr : filter.StoreName;
            }

            if (filter.CategoryName != null)
            {
                categoriesStr = filter.CategoryName == string.Empty ? Filters.AllItemsCategoriesStr : filter.CategoryName;
            }

            if (filter.StartDate != null && filter.EndDate != null)
            {
                startDate = filter.StartDate.Value;
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate == null && filter.EndDate != null)
            {
                startDate = filter.EndDate.Value.AddMonths(-1);
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate != null && filter.EndDate == null)
            {
                startDate = filter.StartDate.Value;
                endDate = DateTime.Today;
            }
            else
            {
                startDate = DateTime.Today.AddMonths(-1);
                endDate = DateTime.Today;
            }

            if (startDate.Date > endDate.Date)
            {
                endDate = startDate;
            }
        }

        public static void GetReportFilters(ReportsParamsModel filter, out DateTime startDate, out DateTime endDate, out Guid[] stores, out int[] customerGroups, out string[] customer, out string storesStr, out string groupsStr, out string customerStr)
        {
            stores = null;
            customerGroups = null;
            customer = null;
            storesStr = Filters.AllStoresStr;
            groupsStr = Filters.AllCustomersGroupsStr;
            customerStr = Filters.AllCustomersStr;

            if (filter.Store != null)
            {
                stores = filter.Store.Length == 0 ? null : filter.Store;
            }

            if (filter.Group != null)
            {
                customerGroups = filter.Group.Length == 0 ? null : filter.Group;
            }

            if (filter.Customer != null)
            {
                customer = filter.Customer.Length == 0 ? null : filter.Customer;
            }

            if (filter.StoreName != null)
            {
                storesStr = filter.StoreName == string.Empty ? Filters.AllStoresStr : filter.StoreName;
            }

            if (filter.GroupName != null)
            {
                groupsStr = filter.GroupName == string.Empty ? Filters.AllCustomersGroupsStr : filter.GroupName;
            }

            if (filter.CustomerName != null)
            {
                customerStr = filter.CustomerName == string.Empty ? Filters.AllCustomersStr : filter.CustomerName;
            }

            if (filter.StartDate != null && filter.EndDate != null)
            {
                startDate = filter.StartDate.Value;
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate == null && filter.EndDate != null)
            {
                startDate = filter.EndDate.Value.AddMonths(-1);
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate != null && filter.EndDate == null)
            {
                startDate = filter.StartDate.Value;
                endDate = DateTime.Today;
            }
            else
            {
                startDate = DateTime.Today.AddMonths(-1);
                endDate = DateTime.Today;
            }

            if (startDate.Date > endDate.Date)
            {
                endDate = startDate;
            }
        }

        public static void GetReportFilters(ReportsParamsModel filter, out DateTime startDate, out DateTime endDate, out int[] customerGroups, out Guid[] stores, out string groupsStr, out string storesStr)
        {
            stores = null;
            customerGroups = null;
            groupsStr = Filters.AllCustomersGroupsStr;
            storesStr = Filters.AllStoresStr;

            if (filter.Store != null)
            {
                stores = filter.Store.Length == 0 ? null : filter.Store;
            }

            if (filter.Group != null)
            {
                customerGroups = filter.Group.Length == 0 ? null : filter.Group;
            }

            if (filter.StoreName != null)
            {
                storesStr = filter.StoreName == string.Empty ? Filters.AllStoresStr : filter.StoreName;
            }

            if (filter.GroupName != null)
            {
                groupsStr = filter.GroupName == string.Empty ? Filters.AllCustomersGroupsStr : filter.GroupName;
            }

            if (filter.StartDate != null && filter.EndDate != null)
            {
                startDate = filter.StartDate.Value;
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate == null && filter.EndDate != null)
            {
                startDate = filter.EndDate.Value.AddMonths(-1);
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate != null && filter.EndDate == null)
            {
                startDate = filter.StartDate.Value;
                endDate = DateTime.Today;
            }
            else
            {
                startDate = DateTime.Today.AddMonths(-1);
                endDate = DateTime.Today;
            }

            if (startDate.Date > endDate.Date)
            {
                endDate = startDate;
            }
        }

        public static void GetReportFilters(ReportsParamsModel filter, out DateTime startDate, out DateTime endDate, out Guid[] stores, out Guid[] menuItems, out Guid[] categories, out string itemStr, out string categoryStr, out string storesStr)
        {
            stores = null;
            menuItems = null;
            categories = null;
            storesStr = Filters.AllStoresStr;
            categoryStr = Filters.AllItemsCategoriesStr;
            itemStr = Filters.AllItemsStr;

            if (filter.Store != null)
            {
                stores = filter.Store.Length == 0 ? null : filter.Store;
            }

            if (filter.Item != null)
            {
                menuItems = filter.Item.Length == 0 ? null : filter.Item;
            }

            if (filter.Category != null)
            {
                categories = filter.Category.Length == 0 ? null : filter.Category;
            }

            if (filter.StoreName != null)
            {
                storesStr = filter.StoreName == string.Empty ? Filters.AllStoresStr : filter.StoreName;
            }

            if (filter.ItemName != null)
            {
                itemStr = filter.ItemName == string.Empty ? Filters.AllItemsStr : filter.ItemName;
            }

            if (filter.CategoryName != null)
            {
                categoryStr = filter.CategoryName == string.Empty ? Filters.AllItemsCategoriesStr : filter.CategoryName;
            }

            if (filter.StartDate != null && filter.EndDate != null)
            {
                startDate = filter.StartDate.Value;
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate == null && filter.EndDate != null)
            {
                startDate = filter.EndDate.Value.AddMonths(-1);
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate != null && filter.EndDate == null)
            {
                startDate = filter.StartDate.Value;
                endDate = DateTime.Today;
            }
            else
            {
                startDate = DateTime.Today.AddMonths(-1);
                endDate = DateTime.Today;
            }

            if (startDate.Date > endDate.Date)
            {
                endDate = startDate;
            }
        }

        public static void GetReportFilters(ZReportsParamsModel filter, out DateTime startDate, out DateTime endDate, out string[] customer, out int[] customerGroups, out Guid[] menuItems, out Guid[] categories, out Guid[] stores, out int[] employees, out string[] terminals, out string storesStr, out string terminalsStr)
        {
            menuItems = null;
            stores = null;
            customerGroups = null;
            categories = null;
            terminals = null;
            employees = null;
            customer = null;
            storesStr = Filters.AllStoresStr;
            terminalsStr = Filters.AllTerminalsStr;

            if (filter.Store != null)
            {
                stores = filter.Store.Length == 0 ? null : filter.Store;
            }

            if (filter.Group != null)
            {
                customerGroups = filter.Group.Length == 0 ? null : filter.Group;
            }

            if (filter.Category != null)
            {
                categories = filter.Category.Length == 0 ? null : filter.Category;
            }

            if (filter.Item != null)
            {
                menuItems = filter.Item.Length == 0 ? null : filter.Item;
            }

            if (filter.Customer != null)
            {
                customer = filter.Customer.Length == 0 ? null : filter.Customer;
            }

            if (filter.Terminal != null)
            {
                terminals = filter.Terminal.Length == 0 ? null : filter.Terminal;
            }

            if (filter.Employee != null)
            {
                employees = filter.Employee.Length == 0 ? null : filter.Employee;
            }

            if (filter.StoreName != null)
            {
                storesStr = filter.StoreName == string.Empty ? Filters.AllStoresStr : filter.StoreName;
            }

            if (filter.TerminalName != null)
            {
                terminalsStr = filter.TerminalName == string.Empty ? Filters.AllTerminalsStr : filter.TerminalName;
            }

            if (filter.StartDate != null && filter.EndDate != null)
            {
                startDate = filter.StartDate.Value;
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate == null && filter.EndDate != null)
            {
                startDate = filter.EndDate.Value.AddMonths(-1);
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate != null && filter.EndDate == null)
            {
                startDate = filter.StartDate.Value;
                endDate = DateTime.Today;
            }
            else
            {
                startDate = DateTime.Today.AddMonths(-1);
                endDate = DateTime.Today;
            }

            if (startDate.Date > endDate.Date)
            {
                endDate = startDate;
            }
        }

        public static void GetReportFilters(ZReportsParamsModel filter, out DateTime startDate, out DateTime endDate, out Guid[] stores, out string[] terminals, out string storesStr, out string terminalsStr)
        {
            stores = null;
            terminals = null;
            storesStr = Filters.AllStoresStr;
            terminalsStr = Filters.AllTerminalsStr;

            if (filter.Store != null)
            {
                stores = filter.Store.Length == 0 ? null : filter.Store;
            }

            if (filter.Terminal != null)
            {
                terminals = filter.Terminal.Length == 0 ? null : filter.Terminal;
            }

            if (filter.StoreName != null)
            {
                storesStr = filter.StoreName == string.Empty ? Filters.AllStoresStr : filter.StoreName;
            }

            if (filter.TerminalName != null)
            {
                terminalsStr = filter.TerminalName == string.Empty ? Filters.AllTerminalsStr : filter.TerminalName;
            }

            if (filter.StartDate != null && filter.EndDate != null)
            {
                startDate = filter.StartDate.Value;
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate == null && filter.EndDate != null)
            {
                startDate = filter.EndDate.Value.AddMonths(-1);
                endDate = filter.EndDate.Value;
            }
            else if (filter.StartDate != null && filter.EndDate == null)
            {
                startDate = filter.StartDate.Value;
                endDate = DateTime.Today;
            }
            else
            {
                startDate = DateTime.Today.AddMonths(-1);
                endDate = DateTime.Today;
            }

            if (startDate.Date > endDate.Date)
            {
                endDate = startDate;
            }
        }
    }
}