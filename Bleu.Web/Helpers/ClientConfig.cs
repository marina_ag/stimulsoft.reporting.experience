﻿namespace Bleu.Web.Helpers
{
    public static class ClientConfig
    {
        public static string ClientIdentificationHeader = "ClientIdentification";

        public static string ClientIdentificationTokken = "ClientIdentificationTokken";

        public static string EmployeeCookie = "bleu.Employee";

        public static string[] SuportedLanguages = { "en-US", "ru-RU" };

        public static string DefaultLanguage = "en-US";
    }
}