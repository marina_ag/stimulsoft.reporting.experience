﻿using System;
using System.Reflection;
using System.Web.Http;

using YumaPos.Reporting.Infrastructure.Ioc;

namespace Bleu.Web.Ioc
{
    public class Bootstrapper
    {
        private static IBootstrapperContainer container;

        public static void Register(HttpConfiguration config, Assembly addAssembly)
        {
            container = new AutofacConfig();
            RegisterModules();
            container.Build(config, addAssembly);
        }

        public static void UpdateRegistration(Type @interface, Type implementation, Lifetime lifetime)
        {
            container.Update(@interface, implementation, lifetime);
        }

        public static I Resolve<I>()
        {
            return container.Resolve<I>();
        }

        private static void RegisterModules()
        {
            new YumaPos.Reporting.Background.Ioc.Registrator().Register(container);
            new YumaPos.Reporting.Services.Ioc.Registrator().Register(container);
            new YumaPos.Reporting.Data.Ioc.Registrator().Register(container);
            new Registrator().Register(container);
        }
    }
}