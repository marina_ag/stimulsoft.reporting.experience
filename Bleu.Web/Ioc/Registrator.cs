﻿using Bleu.Web.Services;

using YumaPos.Common.Infrastructure.Logging;
using YumaPos.Common.Tools.Logging;
using YumaPos.Reporting.Infrastructure.Ioc;
using YumaPos.Reporting.Infrastructure.Logic;
using YumaPos.Shared.Server.Infrastructure;

namespace Bleu.Web.Ioc
{
    public class Registrator : IRegistrator
    {
        public void Register(IBootstrapperContainer container)
        {
            container.Register(typeof(ITenantService), typeof(TenantService), Lifetime.PerLifetimeScope);
            container.Register(typeof(ILoggingService), typeof(LoggingService), Lifetime.PerLifetimeScope);
            container.Register(typeof(IConnectionContainer), typeof(ConnectionContainer), Lifetime.PerLifetimeScope);
        }
    }
}