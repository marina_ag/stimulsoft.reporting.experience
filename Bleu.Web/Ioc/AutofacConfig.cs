﻿using System;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;

using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;

using YumaPos.Reporting.Infrastructure.Ioc;

namespace Bleu.Web.Ioc
{
    public class AutofacConfig : IBootstrapperContainer
    {
        private ContainerBuilder _builder;
        private IContainer _container;

        public AutofacConfig()
        {
            this._builder = new ContainerBuilder();
        }

        public void RegisterType<I, T>() where T : I
        {
            this.RegisterType<I, T>(Lifetime.PerLifetimeScope);
        }

        public void RegisterType<I, T>(Lifetime lifetime) where T : I
        {
            var builder = this._builder.RegisterType<T>().As<I>();
            switch (lifetime)
            {
                case Lifetime.Singleton:
                    builder.SingleInstance();
                    break;
                case Lifetime.PerLifetimeScope:
                    builder.InstancePerLifetimeScope();
                    break;
                case Lifetime.PerRequest:
                    builder.InstancePerRequest();
                    break;
            }
        }

        public void Register(Type @interface, Type implementation, Lifetime lifetime)
        {
            if (@interface.IsGenericType)
            {
                var builder = this._builder.RegisterGeneric(implementation).As(@interface);
                switch (lifetime)
                {
                    case Lifetime.Singleton:
                        builder.SingleInstance();
                        break;
                    case Lifetime.PerLifetimeScope:
                        builder.InstancePerLifetimeScope();
                        break;
                    case Lifetime.PerRequest:
                        builder.InstancePerRequest();
                        break;
                }
            }
            else
            {
                var builder = this._builder.RegisterType(implementation).As(@interface);
                switch (lifetime)
                {
                    case Lifetime.Singleton:
                        builder.SingleInstance();
                        break;
                    case Lifetime.PerLifetimeScope:
                        builder.InstancePerLifetimeScope();
                        break;
                    case Lifetime.PerRequest:
                        builder.InstancePerRequest();
                        break;
                }
            }
        }

        public void RegisterKeyed(Type @interface, Type implementation, Enum key, Lifetime lifetime)
        {
            if (@interface.IsGenericType)
            {
                var builder = this._builder.RegisterGeneric(implementation).Keyed(key, @interface);
                switch (lifetime)
                {
                    case Lifetime.Singleton:
                        builder.SingleInstance();
                        break;
                    case Lifetime.PerLifetimeScope:
                        builder.InstancePerLifetimeScope();
                        break;
                    case Lifetime.PerRequest:
                        builder.InstancePerRequest();
                        break;
                }
            }
            else
            {
                var builder = this._builder.RegisterType(implementation).Keyed(key, @interface);
                switch (lifetime)
                {
                    case Lifetime.Singleton:
                        builder.SingleInstance();
                        break;
                    case Lifetime.PerLifetimeScope:
                        builder.InstancePerLifetimeScope();
                        break;
                    case Lifetime.PerRequest:
                        builder.InstancePerRequest();
                        break;
                }
            }
        }

        public void Update(Type @interface, Type implementation, Lifetime lifetime)
        {
            this._builder = new ContainerBuilder();
            if (@interface.IsGenericType)
            {
                var builder = this._builder.RegisterGeneric(implementation).As(@interface);
                switch (lifetime)
                {
                    case Lifetime.Singleton:
                        builder.SingleInstance();
                        break;
                    case Lifetime.PerLifetimeScope:
                        builder.InstancePerLifetimeScope();
                        break;
                    case Lifetime.PerRequest:
                        builder.InstancePerRequest();
                        break;
                }
            }
            else
            {
                var builder = this._builder.RegisterType(implementation).As(@interface);
                switch (lifetime)
                {
                    case Lifetime.Singleton:
                        builder.SingleInstance();
                        break;
                    case Lifetime.PerLifetimeScope:
                        builder.InstancePerLifetimeScope();
                        break;
                    case Lifetime.PerRequest:
                        builder.InstancePerRequest();
                        break;
                }
            }

            this._builder.Update(this._container);
        }

        public I Resolve<I>()
        {
            return this._container.Resolve<I>();
        }

        public void Build(object config, Assembly addAssembly)
        {
            this._builder.RegisterControllers(Assembly.GetExecutingAssembly());
            if (addAssembly != null)
            {
                this._builder.RegisterApiControllers(Assembly.GetExecutingAssembly(), addAssembly);
            }
            else
            {
                this._builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            }

            this._container = this._builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(this._container));
            ((HttpConfiguration) config).DependencyResolver = new AutofacWebApiDependencyResolver(this._container);
            this._builder = null;
        }
    }
}