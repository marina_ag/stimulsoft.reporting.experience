﻿using System;
using System.Data;
using System.Data.SqlClient;

using YumaPos.Shared.Server.Infrastructure;

namespace Bleu.Web.Ioc
{
    public class ConnectionContainer : IConnectionContainer, IDisposable
    {
        private SqlConnection _dataConnection;
        private SqlConnection _reportsConnection;

        public IDbConnection DataConnection
        {
            get
            {
                if (_dataConnection != null)
                {
                    return _dataConnection;
                }

                var connectionStr = string.Empty;

                if (System.Configuration.ConfigurationManager.ConnectionStrings["POSFdat"] != null)
                {
                    connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["POSFdat"].ConnectionString;
                }

                if (connectionStr == string.Empty)
                {
                    throw new Exception("Connection string not defined! See db.config");
                }

                _dataConnection = new SqlConnection(connectionStr);
                _dataConnection.Open();

                return _dataConnection;
            }
        }

        public IDbConnection ReportingConnection
        {
            get
            {
                if (_reportsConnection != null)
                {
                    return _reportsConnection;
                }

                var connectionStr = string.Empty;

                if (System.Configuration.ConfigurationManager.ConnectionStrings["POSReporting"] != null)
                {
                    connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["POSReporting"].ConnectionString;
                }

                if (connectionStr == string.Empty)
                {
                    throw new Exception("Connection string not defined! See db.config");
                }

                _reportsConnection = new SqlConnection(connectionStr);
                _reportsConnection.Open();

                return _reportsConnection;
            }
        }

        public IDbConnection ServiceConnection
        {
            get
            {
                return null;
            }
        }


        public void Dispose()
        {
            if (_dataConnection != null)
            {
                _dataConnection.Dispose();
                _dataConnection = null;
            }

            if (_reportsConnection != null)
            {
                _reportsConnection.Dispose();
                _reportsConnection = null;
            }
        }
    }
}