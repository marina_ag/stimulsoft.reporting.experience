angular.module('core.main').factory('StateExtension', ['$rootScope', '$state', function ($rootScope, $state) {
    var previousState = null,
        previousParams = null,
        init = function () {
            $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                previousState = fromState;
                previousParams = fromParams;
            });
            $state.goBack = function (to, params, options) {
                if (previousState != null && !previousState.abstract) {
                    $state.go(previousState.name, previousParams);
                } else if (to != null) {
                    $state.go(to, params, options);
                } else {
                    $state.go($rootScope.$defaultRouteStateName);
                }
            };
            $state.goHome = function () {
                $state.go($rootScope.$defaultRouteStateName);
            };
        };
    return {
        init: init
    };
}]);