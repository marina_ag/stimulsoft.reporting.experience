angular.module('core.main').factory('BaseViewModel', ['$q', '$timeout', function ($q, $timeout) {
    var baseViewModel = function () {
        this.$modelState = {
            $isValid: false,
            $isSubmited: false,
            $isPristine: true,
            $errors: [],
            $hasErrors: false
        }
    };


    baseViewModel.prototype.$validate = function () {
        var dfd = $q.defer();

        this.$modelState.$errors.forEach(function (error) {
            error.state = true;
        });
        this.$modelState.$hasErrors = false;

        $timeout(angular.bind(this, function () {
            this.$modelState.$isSubmited = true;
            if (this.$modelState.$isValid) {
                dfd.resolve();
            } else {
                dfd.reject();
            }
        }));

        return dfd.promise;
    };

    baseViewModel.prototype.$clear = function () {
        this.$modelState.$isPristine = true;
    };

    baseViewModel.prototype.$setValidity = function (field, error, state) {
        this.$modelState.$hasErrors = true;
        this.$modelState.$errors.push({
            field: field,
            error: error,
            state: state
        });
    };

    return baseViewModel;
}]);