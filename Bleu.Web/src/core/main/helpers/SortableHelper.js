angular.module('core.main').factory('SortableHelper', [
    function () {
        var
            sortableHelper = function (options) {
                this.items = [];
                this.originalItems = [];
                this.positionChanged = false;
                this.onPositionChanged = options.onPositionChanged;
                this.positionFieldName = options.positionFieldName;
                this.dragControlListeners = {
                    orderChanged: angular.bind(this, function () {
                        for (var i = 0; i < this.items.length; i++) {
                            this.items[i][this.positionFieldName] = i;
                        }

                        this.change(true);
                    })
                }
            };
        
        sortableHelper.prototype.change = function (positionState) {
            this.positionChanged = positionState;
            this.onPositionChanged(this.positionChanged);
        }

        sortableHelper.prototype.isPositionChanged = function () {
            return this.positionChanged;
        };

        sortableHelper.prototype.resetItems = function () {
            this.items = angular.copy(this.originalItems);
            this.change(false);
        };

        sortableHelper.prototype.save = function () {
            this.originalItems = angular.copy(this.items);
            this.change(false);
        };

        sortableHelper.prototype.setItems = function (data) {
            this.originalItems = data;
            this.resetItems();
        };

        sortableHelper.prototype.getItems = function () {
            return this.items;
        };

        return sortableHelper;
    }
]);