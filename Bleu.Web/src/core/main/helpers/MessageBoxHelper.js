angular.module('core.main').factory('MessageBoxHelper', ['$modal', '$q', function ($modal, $q) {
    var
        openModal = function (model) {
            var dfd = $q.defer();
            $modal.open({
                templateUrl: 'src/app/main/views/templates/MessageBoxView.html',
                controller: 'MessageBoxController',
                backdrop: model.notCloseable ? 'static' : true,
                resolve: {
                    options: function () {
                        return {
                            title: model.title,
                            message: model.message,
                            buttons: model.buttons,
                            showClose: !model.notCloseable
                        }
                    }
                }
            }).result.then(function (result) {
                if (model.successValue != null) {
                    if (model.successValue == result) {
                        dfd.resolve(result);
                    } else {
                        dfd.reject(result);
                    }
                } else {
                    dfd.resolve(result);
                }
            });

            return dfd.promise;
        }

    return {
        openModal: openModal
    }
}]);