angular.module('core.main').factory('DataMapHelper', function () {
    var
        serverDateToClientDate = function (value) {
            if (typeof (value) != 'string') {
                return null;
            }
            var date = new Date();
            date.setTime(parseInt(value.substr(6)));
            return date;
        },
        clientDateToServerDate = function (date) {
            if (!(date instanceof Date)) {
                return null;
            }

            return '/Date(' + date.getTime() + '-0000)/';
        },
        getGuid = function () {
            var d = new Date().getTime();
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
            });
        },
        parseNumber = function (str) {
            var result = parseInt(str);
            if (isNaN(result)) return null;
            return result;
        },
        getBirthFromAge = function (age) {
            if (age != null) {
                var birth = moment().subtract(age, 'years');
                return birth;
            }

            return null;
        },
        getDaysAgo = function (date) {
            if (date != null) {
                var days = moment().diff(date, 'days');
                return days;
            }

            return null;
        },
        prepareDateForFilter = function (date) {
            if (date != null) {
                var result = moment(date);
                result.startOf('day');
                result.set('hour', result.get('hour') + result.utcOffset() / 60);
                return result;
            }

            return null;
        },
        prepareDateForReports = function (date) {
            if (date != null) {
                return moment(date).format('YYYY-MM-DD');
            }

            return null;
        },
        getGuidEmpty = function () {
            return "00000000-0000-0000-0000-000000000000";
        };


    return {
        serverDateToClientDate: serverDateToClientDate,
        clientDateToServerDate: clientDateToServerDate,
        getGuid: getGuid,
        parseNumber: parseNumber,
        getBirthFromAge: getBirthFromAge,
        getDaysAgo: getDaysAgo,
        prepareDateForFilter: prepareDateForFilter,
        prepareDateForReports: prepareDateForReports,
        getGuidEmpty: getGuidEmpty
    }
});