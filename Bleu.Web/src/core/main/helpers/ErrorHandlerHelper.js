angular.module('core.main').factory('ErrorHandlerHelper', [
    '$q',
    'ErrorStatusHandlerHelper',
    function ($q, errorStatusHandlerHelper) {
        var handleError = function (promise) {
            var dfd = $q.defer();

            promise.success(function (data) {
                dfd.resolve(data);
            }).error(function (errorMsg, status) {
                errorStatusHandlerHelper.handleErrorStatus(status);
                switch (status) {
                    case 400:
                        dfd.reject(errorMsg.ValidationMessages);
                        break;
                    case 401:
                        dfd.reject(401);
                        break;
                    case 403:
                        dfd.reject(403);
                        break;
                    case 404:
                        dfd.reject(404);
                        break;
                    default:
                        dfd.reject();
                }
            });

            return dfd.promise;
        };
        return {
            handleError: handleError
        }
    }
]);
