angular.module('core.main').factory('FileUploadHelper', [
    '$q',
    'ImagesService',
    'ImageSizeType',
    function ($q, imagesService, imageSizeType) {
        var fileUploadHelper = function (options) {
            this.files = [];
            this.placeholder = options.placeholder;
        };

        fileUploadHelper.prototype.updatePlaceholder = function (imageId, type) {
            if (!imageId) {
                return;
            }

            switch (type) {
                case imageSizeType.Original:
                    this.placeholder = imagesService.getImageOriginUrl(imageId);
                    break;
                case imageSizeType.Small:
                    this.placeholder = imagesService.getImageSmallUrl(imageId);
                    break;
                case imageSizeType.Medium:
                    this.placeholder = imagesService.getImageMediumUrl(imageId);
                    break;
            }
        };

        fileUploadHelper.prototype.addPhoto = function (onSuccessUpload) {
            var deferred = $q.defer();
            if (this.files.name == null) {
                deferred.resolve();
            } else {
                imagesService.addImage(this.files).then(function (imageId) {
                    onSuccessUpload(imageId);
                    deferred.resolve();
                }, function () {
                    deferred.reject();
                });
            }

            return deferred.promise;
        };

        fileUploadHelper.prototype.clear = function () {
            this.files = [];
        }

        return fileUploadHelper;
    }
]);