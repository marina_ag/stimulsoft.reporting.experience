angular.module('core.main').factory('RequestHandlerHelper', [
    '$q', '$rootScope', '$timeout',
    function ($q, $rootScope, $timeout) {
        var loadingCounter = 0,
            showLoaderDelay = 300,
            hideLoaderDelay = 300,
            handleRequest = function (promise) {
                var dfd = $q.defer();
                loadingCounter++;
                $timeout(function () {
                    if (loadingCounter != 0) {
                        $rootScope.isLoading = true;
                    }
                }, showLoaderDelay);
                promise.then(function (data) {
                    $timeout(function () {
                        if (loadingCounter == 0) {
                            $rootScope.isLoading = false;
                        }
                    }, hideLoaderDelay);
                    loadingCounter--;
                    dfd.resolve(data);
                }, function (errorInfo) {
                    var criticalError = errorInfo == null;
                    $timeout(function () {
                        $rootScope.isError = criticalError;
                        if (loadingCounter == 0) {
                            $rootScope.isLoading = false;
                        }
                    }, hideLoaderDelay);
                    loadingCounter--;
                    dfd.reject(errorInfo);
                });
                return dfd.promise;
            };
        return {
            handleRequest: handleRequest
        }
    }
]);