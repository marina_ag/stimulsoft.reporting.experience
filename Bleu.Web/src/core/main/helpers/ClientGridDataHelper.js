angular.module('core.main').factory('ClientGridDataHelper', [
    'FilterCompareMethod',
    'FilterRelationMethod', function (filterCompareMethod, filterRelationMethod) {
        var gridDataHelper = function (gridOptions) {
            this.getDataMethod = null;
            this.gridApi = null;
            this.gridOptions = gridOptions;
            this.onRegisterApi = gridOptions.onRegisterApi;
            this.filters = [];
            this.defaultFilters = [];
            this.eventsBinded = false;
        }

        gridDataHelper.prototype.getSelection = function () {
            if (this.gridApi != null && this.gridApi.selection != null) {
                return this.gridApi.selection.getSelectedRows();
            }
            return null;
        };

        gridDataHelper.prototype.getFilters = function () {
            return this.applyDefaultFilters().Filters;
        };

        gridDataHelper.prototype.applyDefaultFilters = function () {
            return this.filters.concat(this.defaultFilters);
        };

        gridDataHelper.prototype.reload = function () {
            this.clearSelection();
            this.loadData().then(angular.bind(this, function() {
                if (this.gridApi != null) {
                    this.gridApi.grid.refresh();
                }
            }));
        };

        gridDataHelper.prototype.clearSelection = function () {
            if (this.gridApi != null && this.gridApi.selection != null) {
                this.gridApi.selection.clearSelectedRows();
            }
        };

        gridDataHelper.prototype.loadData = function () {
            return this.getDataMethod().then(angular.bind(this, function (data) {
                this.gridOptions.data = data;
                this.gridOptions.totalItems = data.length;
            }));
        };

        gridDataHelper.prototype.updateFilters = function (f) {
            var
                addFilter = angular.bind(this, function (selector, method, value, relation) {
                    this.filters.push({
                        Selector: selector,
                        Value: value,
                        Method: method,
                        Relation: relation
                    });
                }),
                removeFilter = angular.bind(this, function (selector, method) {
                    this.filters = this.filters.filter(function (item) {
                        return (item.Selector != selector) || ((method != null && item.Method != method) || method == null);
                    });
                });

            f.forEach(angular.bind(this, function (filter) {
                removeFilter(filter.Selector, filter.Method);
                if (filter.Value != null && filter.Value !== '') {
                    addFilter(filter.Selector, filter.Method, filter.Value, filter.Relation);
                }
            }));
            return this.reload();
        };

        gridDataHelper.prototype.init = function (initOptions) {
            this.getDataMethod = initOptions.getDataMethod;
            this.defaultFilters = initOptions.defaultFilters || [];
            var $scope = initOptions.grid,
                onSelectionChanged = initOptions.onSelectionChanged,
                callSelectionChanged = angular.bind(this, function () {
                    if (typeof (onSelectionChanged) == 'function' && this.gridApi.selection != null) {
                        onSelectionChanged(this.getSelection());
                    }
                }),
                matchCell = function (cell, filter) {
                    var filteredCell = cell,
                        val = filter.Value;
                    switch (filter.Method) {
                        case filterCompareMethod.Equals:
                            return filteredCell == val;
                        case filterCompareMethod.Contains:
                            return filteredCell != null && filteredCell.toString().toLowerCase().indexOf(val.toLowerCase()) != -1;
                        default:
                            throw new Error('Compare method unknown or not implemented');
                    }
                },
                matchRow = angular.bind(this, function (row) {
                    var rowMatch = '',
                        relation = '',
                        openBrackets = false;

                    this.filters.forEach(function (filter) {

                        var filteredCell = row.entity[filter.Selector],
                            filterMatch = matchCell(filteredCell, filter);

                        rowMatch = rowMatch + relation;
                        switch (filter.Relation) {
                            case filterRelationMethod.And:
                                break;
                            case filterRelationMethod.Or:
                                if (!openBrackets) {
                                    openBrackets = true;
                                    rowMatch = rowMatch + '(';
                                }
                                break;
                            default:
                                throw new Error('Filters relation unknown or not implemented');
                        }
                        rowMatch = rowMatch + filterMatch.toString();
                        switch (filter.Relation) {
                            case filterRelationMethod.And:
                                if (openBrackets) {
                                    openBrackets = false;
                                    rowMatch = rowMatch + ')';
                                }
                                relation = " && ";
                                break;
                            case filterRelationMethod.Or:
                                relation = " || ";
                                break;
                            default:
                                throw new Error('Filters relation unknown or not implemented');
                        }
                    });

                    return eval(rowMatch) || this.filters.length == 0;
                }),
                filterProcessor = function (renderableRows) {
                    renderableRows.forEach(function (row) {
                        row.visible = matchRow(row);
                    });
                    return renderableRows;
                };

            if (!this.eventsBinded) {
                this.gridOptions.onRegisterApi = angular.bind(this, function (gApi) {
                    this.gridApi = gApi;
                    this.gridApi.grid.registerRowsProcessor(filterProcessor, 200);
                    if (this.gridApi.selection != null) {
                        this.gridApi.selection.on.rowSelectionChanged($scope, angular.bind(this, function (row) {
                            callSelectionChanged();
                        }));
                        this.gridApi.selection.on.rowSelectionChangedBatch($scope, angular.bind(this, function (rows, event) {
                            callSelectionChanged();
                        }));
                    }
                    if (typeof (this.onRegisterApi) == 'function') {
                        this.onRegisterApi(this.gridApi);
                    }
                });
                this.eventsBinded = true;
            }
            return this.loadData();
        };

        return gridDataHelper;
    }]);