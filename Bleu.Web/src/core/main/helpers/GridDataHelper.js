angular.module('core.main').factory('GridDataHelper', ['uiGridConstants', '$timeout', '$q', function (uiGridConstants, $timeout, $q) {
    var gridDataHelper = function (gridOptions) {
        this.getDataMethod = null;
        this.gridApi = null;
        this.gridOptions = gridOptions;
        this.onRegisterApi = gridOptions.onRegisterApi;
        this.requestOptions = {
            Skip: (gridOptions.paginationCurrentPage - 1) * gridOptions.paginationPageSize,
            Take: gridOptions.paginationPageSize,
            Count: true,
            SortFilters: [],
            Filters: [],
            Fields: []
        };
        this.infiniteScrollCurrentPage = gridOptions.infiniteScrollCurrentPage;
        this.infiniteScrollPageSize = gridOptions.infiniteScrollPageSize;
        this.selection = [];
        this.primaryFields = [];
        this.fieldsForSelectAll = [];
        this.defaultFilters = [];
        this.eventsBinded = false;
    };

    gridDataHelper.prototype.getSelection = function () {
        return this.selection;
    };

    gridDataHelper.prototype.getFilters = function () {
        return this.applyDefaultFilters().Filters;
    };

    gridDataHelper.prototype.applyDefaultFilters = function () {
        var requestData = angular.copy(this.requestOptions);
        requestData.Filters = requestData.Filters.concat(this.defaultFilters);
        return requestData;
    };

    gridDataHelper.prototype.reload = function () {
        this.clearSelection();
        return this.loadData();
    };

    gridDataHelper.prototype.compareObjectsById = function (obj1, obj2) {
        for (var i = 0; i < this.primaryFields.length; i++) {
            var key = this.primaryFields[i];
            if (obj1[key] != obj2[key]) {
                return false;
            }
        }
        return true;
    };

    gridDataHelper.prototype.clearSelection = function () {
        if (this.gridApi != null && this.gridApi.selection != null) {
            this.gridApi.selection.clearSelectedRows();
            this.selection = [];
        }
    };

    gridDataHelper.prototype.getData = function () {
        var requestData = this.applyDefaultFilters();
        return this.getDataMethod(requestData).then(angular.bind(this, function (data) {
            if (data.Count != null) {
                this.gridOptions.totalItems = data.Count;
            }

            return data.Results;
        }));
    };

    gridDataHelper.prototype.restoreSelection = function (rows) {
        return $timeout(angular.bind(this, function () {
            if (this.gridApi != null && this.gridApi.selection != null) {
                rows.forEach(angular.bind(this, function (entity) {
                    this.selection.forEach(angular.bind(this, function (selected) {
                        if (this.compareObjectsById(entity, selected)) {
                            this.gridApi.selection.selectRow(entity);
                        }
                    }));
                }));
            }
        }), 1);
    };

    gridDataHelper.prototype.loadData = function () {
        return this.getData().then(angular.bind(this, function (rows) {
            this.gridOptions.data = rows;
            return this.restoreSelection(rows);
        }));
    };

    gridDataHelper.prototype.loadDataScrollFirst = function () {
        this.requestOptions.Skip = (this.infiniteScrollCurrentPage - 1) * this.infiniteScrollPageSize;
        this.requestOptions.Take = this.infiniteScrollPageSize;
        return this.getData().then(angular.bind(this, function (rows) {
            this.gridOptions.data = rows;
            this.gridApi.infiniteScroll.dataLoaded(this.infiniteScrollCurrentPage > 1, this.infiniteScrollCurrentPage < Math.ceil(this.gridOptions.totalItems / this.infiniteScrollPageSize));
        }));
    };

    //gridDataHelper.prototype.loadDataScrollUp = function () {
    //    return this.getData().then(angular.bind(this, function (rows) {
    //        this.gridApi.infiniteScroll.saveScrollPercentage();
    //        this.gridOptions.data = rows.concat(this.gridOptions.data);
    //        this.gridApi.infiniteScroll.dataLoaded(this.infiniteScrollCurrentPage > 1, this.infiniteScrollCurrentPage < Math.ceil(this.gridOptions.totalItems / this.infiniteScrollPageSize));
    //        return this.restoreSelection(rows);
    //    }));
    //};

    gridDataHelper.prototype.loadDataScrollDown = function () {
        return this.getData().then(angular.bind(this, function (rows) {
            this.gridApi.infiniteScroll.saveScrollPercentage();
            this.gridOptions.data = this.gridOptions.data.concat(rows);
            this.gridApi.infiniteScroll.dataLoaded(this.infiniteScrollCurrentPage > 1, this.infiniteScrollCurrentPage < Math.ceil(this.gridOptions.totalItems / this.infiniteScrollPageSize));
            return this.restoreSelection(rows);
        }));
    };

    gridDataHelper.prototype.updateFilters = function (filters) {
        var
            addFilter = angular.bind(this, function (selector, method, value, relation) {
                this.requestOptions.Filters.push({
                    Selector: selector,
                    Value: value,
                    Method: method,
                    Relation: relation
                });
                this.requestOptions.Count = true;
            }),
            removeFilter = angular.bind(this, function (selector, method) {
                this.requestOptions.Filters = this.requestOptions.Filters.filter(function (item) {
                    return (item.Selector != selector) || ((method != null && item.Method != method) || method == null);
                });
                this.requestOptions.Count = true;
            });

        filters.forEach(angular.bind(this, function (filter) {
            removeFilter(filter.Selector, filter.Method);
            if (filter.Value != null && filter.Value !== '') {
                addFilter(filter.Selector, filter.Method, filter.Value, filter.Relation);
            }
        }));
        this.clearSelection();
        return this.loadData();
    };

    gridDataHelper.prototype.init = function (initOptions) {
        this.getDataMethod = initOptions.getDataMethod;
        this.requestOptions.Fields = initOptions.fields;
        this.fieldsForSelectAll = initOptions.fieldsForSelectAll;
        this.primaryFields = initOptions.primaryFields;
        this.defaultFilters = initOptions.defaultFilters || [];
        this.sortMapping = initOptions.sortMapping;
        this.initFilters = initOptions.initFilters;
        var $scope = initOptions.grid,
            onSelectionChanged = initOptions.onSelectionChanged,
            callSelectionChanged = angular.bind(this, function () {
                if (typeof (onSelectionChanged) == 'function' && this.gridApi.selection != null) {
                    onSelectionChanged(this.getSelection());
                }
            }),
            findSortSelectors = angular.bind(this, function (column) {
                for (var i = 0; i < this.sortMapping.length; i++) {
                    var columnMapping = this.sortMapping[i];
                    if (columnMapping.column == column) {
                        return columnMapping.data;
                    }
                }
                return null;
            }),
            updateSorting = angular.bind(this, function (column, desc) {
                var sortSelectors = [];
                if (column != null) {
                    sortSelectors = findSortSelectors(column);
                    if (sortSelectors == null || sortSelectors.length == 0) {
                        throw new Error('Sorting mapping error for column "' + column + '"');
                    }
                }

                this.requestOptions.SortFilters = [];
                sortSelectors.forEach(angular.bind(this, function (selector) {
                    this.requestOptions.SortFilters.push({
                        Selector: selector,
                        Desc: desc
                    });
                }));
                this.requestOptions.Count = false;
            }),
            changePaging = angular.bind(this, function (page, pageSize) {
                this.requestOptions.Skip = (page - 1) * pageSize;
                this.requestOptions.Take = pageSize;
                this.requestOptions.Count = false;
            }),
            addSelection = angular.bind(this, function (row) {
                for (var i = 0; i < this.selection.length; i++) {
                    if (this.compareObjectsById(this.selection[i], row.entity)) {
                        return;
                    }
                }
                this.selection.push(row.entity);
            }),
            removeSelection = angular.bind(this, function (row) {
                for (var i = 0; i < this.selection.length; i++) {
                    if (this.compareObjectsById(this.selection[i], row.entity)) {
                        this.selection.splice(i, 1);
                        return;
                    };
                };
            }),
            updateSelection = angular.bind(this, function (row) {
                if (row.isSelected) {
                    addSelection(row);
                } else {
                    removeSelection(row);
                }
            }),
            updateSelectionMultiple = function (rows) {
                rows.forEach(function (row) {
                    updateSelection(row);
                });
            },
            getAllData = angular.bind(this, function () {
                var requestData = this.applyDefaultFilters();
                requestData.Skip = 0;
                requestData.Take = null;
                requestData.Count = false;
                requestData.Fields = this.fieldsForSelectAll;
                return this.getDataMethod(requestData).then(angular.bind(this, function (data) {
                    this.selection = data.Results;
                }));
            });

        if (!this.eventsBinded) {
            this.gridOptions.onRegisterApi = angular.bind(this, function (gApi) {
                this.gridApi = gApi;
                this.gridApi.core.on.sortChanged($scope, angular.bind(this, function (grid, sortColumns) {
                    var selector = null,
                        desc = false;
                    if (sortColumns.length !== 0) {
                        selector = sortColumns[0].name;
                        desc = sortColumns[0].sort.direction == uiGridConstants.DESC;
                    }
                    updateSorting(selector, desc);
                    if (this.gridApi != null && this.gridApi.infiniteScroll != null) {
                        this.gridApi.infiniteScroll.saveScrollPercentage();
                    }
                    this.loadData();
                }));
                if (this.gridApi.pagination != null) {
                    this.gridApi.pagination.on.paginationChanged($scope, angular.bind(this, function (newPage, pageSize) {
                        changePaging(newPage, pageSize);
                        this.loadData();
                    }));
                }
                if (this.gridApi.infiniteScroll != null) {
                    this.gridApi.infiniteScroll.on.needLoadMoreData($scope, angular.bind(this, function () {
                        //if (this.gridOptions.data.length > this.infiniteScrollPageSize *2) {
                        //    this.gridApi.infiniteScroll.saveScrollPercentage();
                        //    var removeFromTop = this.gridOptions.data.length - this.infiniteScrollPageSize * 2;
                        //    this.gridOptions.data = this.gridOptions.data.slice(removeFromTop);
                        //    $timeout(angular.bind(this, function () {
                        //        this.gridApi.infiniteScroll.dataRemovedTop(this.infiniteScrollCurrentPage > 1, this.infiniteScrollCurrentPage < Math.ceil(this.gridOptions.totalItems / this.infiniteScrollPageSize));
                        //    }));
                        //}
                        this.infiniteScrollCurrentPage++;
                        this.requestOptions.Skip = (this.infiniteScrollCurrentPage - 1) * this.infiniteScrollPageSize;
                        return this.loadDataScrollDown();
                    }));
                    //this.gridApi.infiniteScroll.on.needLoadMoreDataTop($scope, angular.bind(this, function () {
                    //    if (this.gridOptions.data.length > this.infiniteScrollPageSize * 2) {
                    //        this.gridApi.infiniteScroll.saveScrollPercentage();
                    //        var removeFromEnd = this.gridOptions.data.length - this.infiniteScrollPageSize * 2;
                    //        this.gridOptions.data = this.gridOptions.data.slice(-removeFromEnd);
                    //        $timeout(angular.bind(this, function () {
                    //            this.gridApi.infiniteScroll.dataRemovedBottom(this.infiniteScrollCurrentPage > 1, this.infiniteScrollCurrentPage < Math.ceil(this.gridOptions.totalItems / this.infiniteScrollPageSize));
                    //        }));
                    //    }
                    //    this.infiniteScrollCurrentPage--;
                    //    this.requestOptions.Skip = (this.infiniteScrollCurrentPage - 1) * this.infiniteScrollPageSize;
                    //    return this.loadDataScrollUp();
                    //}));
                }
                if (this.gridApi.selection != null) {
                    this.gridApi.selection.on.rowSelectionChanged($scope, angular.bind(this, function (row) {
                        updateSelection(row);
                        callSelectionChanged();
                    }));
                    this.gridApi.selection.on.rowSelectionChangedBatch($scope, angular.bind(this, function (rows, event) {
                        /// using timeout for fix ui grid bug with getSelectAllState()
                        $timeout(angular.bind(this, function () {
                            /// user tap on select all
                            if (event == null) {
                                if (this.gridApi.selection.getSelectAllState()) {
                                    getAllData().then(function () {
                                        callSelectionChanged();
                                    });
                                } else {
                                    this.clearSelection();
                                    callSelectionChanged();
                                }
                            } else {
                                updateSelectionMultiple(rows);
                                callSelectionChanged();
                            }
                        }, 100));
                    }));
                }
                if (typeof (this.onRegisterApi) == 'function') {
                    this.onRegisterApi(this.gridApi);
                }
            });
            this.eventsBinded = true;
        }
        if (this.gridOptions.enableInfiniteScroll) {
            return this.loadDataScrollFirst();
        } else {
            if (this.initFilters != null) {
                return this.updateFilters(this.initFilters);
            }
            return this.loadData();
        }
    };

    return gridDataHelper;
}]);