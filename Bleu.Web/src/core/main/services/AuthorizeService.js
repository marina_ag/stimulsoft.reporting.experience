angular.module('core.main').factory('AuthorizeService', [
    '$q',
    'localStorageService',
    'StorageServiceKeys',
    'AuthorizeTransport',
    function ($q, localStorageService, storageServiceKeys, authorizeTransport) {
        var
            login = function (password) {
                return authorizeTransport.login(password).then(function (data) {
                    if (data != null && data.LoginResult != null) {
                        localStorageService.cookie.set(storageServiceKeys.employee, data.LoginResult);
                    } else {
                        localStorageService.cookie.remove(storageServiceKeys.employee);
                    }
                });
            },
            logout = function () {
                localStorageService.cookie.remove(storageServiceKeys.employee);
            },
            isAuthenticated = function () {
                return localStorageService.cookie.get(storageServiceKeys.employee) != null;
            },
            isAuthPromise = function () {
                var dfd = $q.defer();
                if (isAuthenticated()) {
                    dfd.resolve();
                } else {
                    dfd.reject();
                }
                return dfd.promise;
            };
        return {
            login: login,
            logout: logout,
            isAuthenticated: isAuthenticated,
            isAuthPromise: isAuthPromise
        }
    }
]);