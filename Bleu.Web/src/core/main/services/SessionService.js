angular.module('core.main').factory('SessionService', ['SessionServiceKeys', function (sessionServiceKeys) {
    var sessionStorage = {},
        testKey = function (key) {
            if (sessionServiceKeys[key] === undefined) {
                throw new Error('Unknown session key "' + key + '", add key to session service keys first.');
            }
        },
        set = function (key, object, replace) {
            testKey(key);
            if (sessionStorage[key] == null || replace) {
                sessionStorage[key] = object;
            }
            return sessionStorage[key];
        },
        get = function (key) {
            testKey(key);
            return sessionStorage[key];
        };
    return {
        set: set,
        get: get
    }
}]);