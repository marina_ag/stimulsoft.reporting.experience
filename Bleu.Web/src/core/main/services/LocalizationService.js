angular.module('core.main').factory('LocalizationService', [
    '$injector',
	function ($injector) {
	    var getLocalizedString = function (text, includes) {
	        if (typeof (text) != 'string' || text == '') {
	            throw new Error('Localization string key must be a string and non empty');
	        }

	        var namespace = text.substring(0, text.indexOf('.')),
                resourceName = text.substring(text.indexOf('.') + 1),
                result = '';

	        $injector.invoke([namespace + 'Localization', function (localization) {
	            if (localization.hasOwnProperty(resourceName)) {
	                result = localization[resourceName].replace(/{(\d+)}/g, function (match, number) {
	                    return typeof includes[number] != 'undefined'
                            ? includes[number]
                            : match;
	                });
	            }
	        }]);

	        return result;
	    };
        return {
            getLocalizedString: getLocalizedString
        };
    }
]);