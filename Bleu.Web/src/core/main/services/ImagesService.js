angular.module('core.main').factory('ImagesService', [
    '$rootScope',
    '$q',
    'RequestHandlerHelper',
    'ImagesTransport',
    function ($rootScope, $q, requestHandlerHelper, imagesTransport) {
        var getImageUrl = '/GetImage',
            convertArrayBufferToByteArray = function (array) {
                return Array.prototype.slice.call(new Uint8Array(array), 0);
            },
            addImage = function (file) {
                var deferred = $q.defer(),
                    fileReader = new FileReader();

                fileReader.onload = function (e) {
                    requestHandlerHelper.handleRequest(imagesTransport.addImage({
                        Picture: convertArrayBufferToByteArray(e.target.result),
                        ImageType: 3
                    })).then(function (imageId) {
                        deferred.resolve(imageId);
                    }, function () {
                        deferred.reject();
                    });
                };

                fileReader.readAsArrayBuffer(file);
                return deferred.promise;
            },
            getImageSmallUrl = function (imageId) {
                return $rootScope.serverConnectionString + getImageUrl + '?imageId=' + imageId + "&imageSize=Small";
            },
            getImageMediumUrl = function (imageId) {
                return $rootScope.serverConnectionString + getImageUrl + '?imageId=' + imageId + "&imageSize=Medium";
            },
            getImageOriginUrl = function (imageId) {
                return $rootScope.serverConnectionString + getImageUrl + '?imageId=' + imageId + "&imageSize=Origin";
            };

        return {
            addImage: addImage,
            getImageSmallUrl: getImageSmallUrl,
            getImageMediumUrl: getImageMediumUrl,
            getImageOriginUrl: getImageOriginUrl
        }
    }
]);