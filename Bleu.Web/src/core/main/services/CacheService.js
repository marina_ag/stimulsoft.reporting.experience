angular.module('core.main').factory('CacheService', ['$q', 'CacheServiceKeys', function ($q, cacheServiceKeys) {
    var cacheStorage = {},
        testKey = function (key) {
            if (cacheServiceKeys[key] === undefined) {
                throw new Error('Unknown cache key "' + key + '", add key to cache service keys first.');
            }
        },
        cache = function (key, promiseFunc) {
            testKey(key);
            var dfd = $q.defer();
            if (cacheStorage[key] == null) {
                promiseFunc().then(function (data) {
                    cacheStorage[key] = data;
                    dfd.resolve(cacheStorage[key]);
                });
            } else {
                dfd.resolve(cacheStorage[key]);
            }
            return dfd.promise;
        },
        clear = function (key) {
            testKey(key);
            cacheStorage[key] = null;
        };
    return {
        cache: cache,
        clear: clear
    }
}]);