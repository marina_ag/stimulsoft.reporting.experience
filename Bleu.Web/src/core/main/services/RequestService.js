angular.module('core.main').factory('RequestService', [
    '$rootScope',
    '$http',
	'ErrorHandlerHelper',
	function ($rootScope, $http, errorHandlerHelper) {
	    var request = function (options) {
	        var headers = {
	            ClientIdentification: $rootScope.clientIdentificationTokken
	        };
	        options.headers = options.headers != null ? angular.extend(options.headers, headers) : options.headers = headers;
	        options.url = $rootScope.serverConnectionString + options.url;
            return errorHandlerHelper.handleError($http(options));
        };
        return {
            request: request
        };
    }
]);