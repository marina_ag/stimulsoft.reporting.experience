angular.module('core.main', [
]).run(['$rootScope', function ($rootScope) {
    $rootScope.isLoading = false;
    $rootScope.isError = false;
    $rootScope.clientIdentificationTokken = null;
    $rootScope.serverConnectionString = null;
}]);