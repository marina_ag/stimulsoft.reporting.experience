/* File generated by web core EnumHelper.tt */
angular.module('core.main').value('FilterCompareMethod', {
	Equals: 0,
	Contains: 1,
	Exists: 2,
	NotExists: 3,
	GreatherThan: 4,
	GreatherEqualThan: 5,
	LowerThan: 6,
	LowerEqualThan: 7,
});
