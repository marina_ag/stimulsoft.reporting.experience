angular.module('core.main').filter('percentage', ['$filter', function ($filter) {
    return function (input) {
        if (isNaN(parseFloat(input)) || !isFinite(input)) {
            input = 0;
        }

        return $filter('number')(input, 2) + '%';
    };
}]);