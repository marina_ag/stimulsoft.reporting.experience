angular.module('core.main').filter('i18n', ['LocalizationService', function (localizationService) {
    return function (text, includes) {
        return localizationService.getLocalizedString(text, includes);
    };
}])