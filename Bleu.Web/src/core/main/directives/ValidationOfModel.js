angular.module('core.main').directive('cdValidationOfModel', [function () {
    return {
        require: 'form',
        restrict: 'A',
        scope: {
            cdValidationOfModel: '=',
            name: '='
        },
        link: function ($scope, $element, $attrs, $formController) {

            $scope.$watch('name.$valid', function (value) {
                if ($scope.cdValidationOfModel) {
                    $scope.cdValidationOfModel.$modelState.$isValid = value;
                }
            });

            $scope.$watch('name.$submited', function (value) {
                if ($scope.cdValidationOfModel) {
                    $scope.cdValidationOfModel.$modelState.$isSubmited = value;
                }
            });

            $scope.$watch('name.$pristine', function (value) {
                if ($scope.cdValidationOfModel) {
                    $scope.cdValidationOfModel.$modelState.$isPristine = value;
                }
            });

            $scope.$watch('cdValidationOfModel.$modelState.$isSubmited', function (value) {
                if (value) {
                    $formController.$setSubmitted();
                }
            });

            $scope.$watch('cdValidationOfModel.$modelState.$isPristine', function (value) {
                if (value) {
                    $formController.$setPristine();
                }
            });

            $scope.$watch('cdValidationOfModel.$modelState.$hasErrors', function (value) {
                $scope.cdValidationOfModel.$modelState.$errors.forEach(function (item) {
                    $formController[item.field].$setValidity(item.error, item.state);
                });
            });
        }
    };
}]);