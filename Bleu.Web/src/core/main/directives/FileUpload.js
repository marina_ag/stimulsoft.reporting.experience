angular.module('core.main').directive('cdFileUpload', function () {
    return {
        restrict: 'A',
        scope: {
            helper: '=cdFileUpload'
        },
        templateUrl: 'src/app/main/views/directives/FileUpload.html',
        link: function ($scope, $element, $attrs) {
        }
    };
});