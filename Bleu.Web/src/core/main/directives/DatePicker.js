angular.module('core.main').directive('cdPopupDatepicker', ['$timeout', function ($timeout) {
    return {
        scope: {
            id: '@',
            name: '@',
            placeholder: '@'
        },
        restrict: 'AE',
        replace: true,
        require: 'ngModel',
        templateUrl: 'src/app/main/views/directives/DatePicker.html',
        link: function ($scope, $element, $attrs, $modelController) {
            $scope.datePickerOpen = false;
            $scope.openDatePicker = function () {
                $scope.datePickerOpen = !$scope.datePickerOpen;
                if ($scope.datePickerOpen) {
                    $timeout(function () {
                        $element.find('input').focus();
                    }, 10);
                }
            };
            $modelController.$render = function () {
                $scope.dateValue = $modelController.$modelValue;
            };
            $scope.$watch('dateValue', function (value) {
                var val = null;
                if (value instanceof Date) {
                    val = new Date(value.getTime());
                }
                $modelController.$setViewValue(val);
                $timeout(function () {
                    if ($element.find('input').hasClass('ng-invalid')) {
                        $element.addClass('ng-invalid');
                    } else {
                        $element.removeClass('ng-invalid');
                    }
                }, 10);
            });
            $scope.setDate = function () {
                $modelController.$setViewValue($scope.dateValue);
                $modelController.$render();
            };
        }
    };
}]);