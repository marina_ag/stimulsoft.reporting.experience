angular.module('core.main').directive('cdLoader', function () {
    return {
        scope: {
            isLoading: '=',
            isError: '='
        },
        restrict: 'A',
        templateUrl: 'src/app/main/views/directives/Loader.html',
        link: function ($scope) {
            $scope.$watch('isLoading', function (value) {
                $scope.loading = value;
            });
            $scope.$watch('isError', function (value) {
                $scope.error = value;
            });
        }
    };
});