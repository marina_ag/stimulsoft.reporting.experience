angular.module('core.main').directive('cdConfirmValidation',
    function () {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=cdConfirmValidation"
            },
            link: function ($scope, $element, $attrs, ngModel) {

                ngModel.$validators.confirm = function (modelValue) {
                    return modelValue == $scope.otherModelValue;
                };

                $scope.$watch("otherModelValue", function () {
                    ngModel.$validate();
                });
            }
        };
    }
);