angular.module('core.main').directive('cdPercentageInput', [
    '$filter',
    function ($filter) {
        return {
            require: 'ngModel',
            link: function ($scope, $elem, $attrs, $ctrl) {
                $elem.bind('focus', function () {
                    $elem.val($ctrl.$modelValue);
                });

                $elem.bind('input', function () {
                    var value = $elem.val();
                    $ctrl.$setViewValue(value != '' ? value : 0);
                });

                $elem.bind('blur', function () {
                    $elem.val($filter('percentage')($ctrl.$modelValue));
                });

                $ctrl.$render = function () {
                    $elem.val($filter('percentage')($ctrl.$modelValue));
                };
            }
        }
    }
]);