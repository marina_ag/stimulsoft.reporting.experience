angular.module('core.main').directive('cdServerValidationInput', [function () {
    return {
        require: 'ngModel',
        scope: {
            errorsList: '=cdServerValidationInput',
        },
        link: function ($scope, $elem, $attrs, $modelController) {
            $elem.on('keydown', function () {
                $scope.errorsList.forEach(function (error) {
                    $modelController.$setValidity(error, true);
                });
            });
        }
    }
}]);