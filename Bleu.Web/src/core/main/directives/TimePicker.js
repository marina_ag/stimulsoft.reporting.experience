angular.module('core.main').directive('cdPopupTimepicker', ['$timeout', function ($timeout) {
    return {
        scope: {
            id: '@',
            name: '@',
            placeholder: '@'
        },
        restrict: 'AE',
        replace: true,
        require: 'ngModel',
        templateUrl: 'src/app/main/views/directives/TimePicker.html',
        link: function ($scope, $element, $attrs, $modelController) {
            $scope.timePickerOpen = false;
            $scope.openTimePicker = function () {
                $scope.timePickerOpen = !$scope.timePickerOpen;
                if ($scope.timePickerOpen) {
                    $timeout(function () {
                        $element.find('input').focus();
                    }, 10);
                }
            };
            $modelController.$render = function () {
                $scope.timeValue = $modelController.$modelValue;
            };
            $scope.$watch('timeValue', function (value) {
                $modelController.$setViewValue(value);
                $timeout(function () {
                    if ($element.find('input').hasClass('ng-invalid')) {
                        $element.addClass('ng-invalid');
                    } else {
                        $element.removeClass('ng-invalid');
                    }
                }, 10);
            });
            $scope.setTime = function () {
                $modelController.$setViewValue($scope.timeValue);
                $modelController.$render();
            };
        }
    };
}]);