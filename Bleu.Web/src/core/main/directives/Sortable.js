angular.module('core.main').directive('cdSortable', function () {
    return {
        restrict: 'A',
        scope: {
            helper: '=cdSortable',
            template: '=cdSortableTemplate'
        },
        templateUrl: 'src/app/main/views/directives/Sortable.html',
        link: function ($scope, $element, $attrs) {
        }
    };
});