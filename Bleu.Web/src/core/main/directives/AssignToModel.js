angular.module('core.main').directive('cdAssignToModel', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function ($scope, $elemement, $attrs, $modelController) {
            $modelController.$setViewValue($attrs.cdAssignToModel);
        }
    };
});