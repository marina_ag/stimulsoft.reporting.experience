angular.module('core.main').controller('MessageBoxController', [
    '$scope',
    '$modalInstance',
    'options',
    function ($scope, $modalInstance, options) {
        var
            title = options.title,
            message = options.message,
            buttons = options.buttons,
            showClose = options.showClose,
            close = function (value) {
                $modalInstance.close(value);
            };

        $scope.title = title;
        $scope.message = message;
        $scope.buttons = buttons;
        $scope.showClose = showClose;
        $scope.close = close;
    }
]);