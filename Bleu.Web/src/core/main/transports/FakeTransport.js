angular.module('core.main').factory('FakeTransport', [ '$q', '$timeout', '$log', function ($q, $timeout, $log) {
	return {
	    request: function (message, returnData, error) {
	        var dfd = $q.defer();
	        $log.log('Start '+message);
	        $timeout(function () {
	            if (error) {
	                dfd.reject();
	            } else {
	                dfd.resolve(returnData);
	            }
	            $log.log('Finish ' + message);
	        }, 500);
	        return dfd.promise;
		}
	}
}]);
