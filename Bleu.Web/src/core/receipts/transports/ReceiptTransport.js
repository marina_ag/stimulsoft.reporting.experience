/* File generated by web core ApiHelper.tt */
angular.module('core.receipts').factory('ReceiptTransport', [ 'RequestService', function (requestService) {
	return {
		updateReceiptSettings: function (model) {
			return requestService.request({ method: 'POST', url: '/UpdateReceiptSettings', data: model });
		},
		getReceiptSettings: function () {
			return requestService.request({ method: 'GET', url: '/GetReceiptSettings' });
		},
	}
}]);
