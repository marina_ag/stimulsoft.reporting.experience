/* File generated by web core ApiHelper.tt */
angular.module('core.profile').factory('AuthorizeTransport', [ 'RequestService', function (requestService) {
	return {
		addTerminalToStore: function (terminalDto) {
			return requestService.request({ method: 'POST', url: '/AddTerminalToStore', data: terminalDto });
		},
	}
}]);
