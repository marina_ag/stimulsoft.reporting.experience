﻿angular.module('bleu.beacons', [
    'bleu.main',
    'ui.router'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {

    $stateProvider
        .state({
            name: 'root.settingsBeaconsList',
            url: '/beacons',
            views: {
                'content-view': {
                    templateUrl: 'src/app/beacons/views/BeaconsListView.html',
                    controller: 'BeaconsListController'
                }
            },
            menu: {
                section: menuSections.settings,
                item: 'Main.LeftMenu_MenuItemBeacons',
                order: 90
            }
        });
}]).run();