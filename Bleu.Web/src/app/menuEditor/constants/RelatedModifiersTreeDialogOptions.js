﻿angular.module('bleu.menuEditor').constant('RelatedModifiersTreeDialogOptions', {
    animation: true,
    templateUrl: '/src/app/menuEditor/views/RelatedModifierDialogView.html',
    controller: 'RelatedModifierDialogController',
    size: 'lg'
});