﻿angular.module('core.menuEditor').directive('cdCommonModifiersGroupsCategories', [
    function () {
        return {
            restrict: 'A',
            scope: {
                menuItemsCategories: '=cdMenuItemsCategories',
                commonModifiersGroup: '=cdCommonModifiersGroup',
                onCategoryOpen: '=cdOnCategoryOpen',
                onCategoryClose: '=cdOnCategoryClose'
            },
            templateUrl: 'src/app/menuEditor/views/directives/CommonModifiersGroupsCategories.html',
            link: function ($scope, $element, $attrs) {
                var categories = [],
                    categoryClick = function (menuItemCategory) {
                        if (menuItemCategory.isOpen) {
                            $scope.onCategoryClose(menuItemCategory);
                            menuItemCategory.isEmpty = false;
                            menuItemCategory.isLoading = false;
                        } else {
                            menuItemCategory.isLoading = true;
                            $scope.onCategoryOpen(menuItemCategory).then(function () {
                                menuItemCategory.isLoading = false;
                                menuItemCategory.isEmpty = menuItemCategory.menuItems.length == 0;
                            });
                        }
                    },
                    isMenuItemChecked = function (menuItemId) {
                        return $scope.commonModifiersGroup.menuItemsIds.filter(function (item) {
                            return menuItemId == item;
                        }).length > 0;
                    },
                    checkMenuItem = function (menuItemId) {
                        if (!isMenuItemChecked(menuItemId)) {
                            $scope.commonModifiersGroup.menuItemsIds.push(menuItemId);
                        } else {
                            $scope.commonModifiersGroup.menuItemsIds = $scope.commonModifiersGroup.menuItemsIds.filter(function (item) {
                                return menuItemId != item;
                            });
                        }
                    };

                $scope.$watch('menuItemsCategories', function () {
                    $scope.categories = $scope.menuItemsCategories.map(function (category) {
                        var result = angular.copy(category);
                        result.isOpen = false;
                        result.isEmpty = false;
                        result.isLoading = false;
                        return result;
                    });
                });

                $scope.isMenuItemChecked = isMenuItemChecked;
                $scope.checkMenuItem = checkMenuItem;
                $scope.categoryClick = categoryClick;
                $scope.categories = categories;
            }
        };
    }
]);