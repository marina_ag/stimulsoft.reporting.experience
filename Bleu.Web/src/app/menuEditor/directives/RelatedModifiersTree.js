﻿angular.module('core.menuEditor').directive('cdRelatedModifiersTree', [
    '$modal',
    'ImagesService',
    'SessionService',
    'PathesOfDefaultImages',
    'RelatedModifiersTreeDialogOptions',
    function (
        $modal,
        imagesService,
        sessionService,
        pathesOfDefaultImages,
        relatedModifiersTreeDialogOptions) {
        return {
            restrict: 'A',
            scope: {
                relatedModifiers: '=cdRelatedModifiersTree'
            },
            templateUrl: 'src/app/menuEditor/views/directives/RelatedModifiersTree.html',
            link: function ($scope, $element, $attrs) {
                var
                    clearIds = function (node) {
                        node.relatedModifiers.forEach(function (n) {
                            clearIds(n);
                        });
                        node.id = null;
                    },
                    removeRelatedModifier = function (item) {
                        item.remove();
                    },
                    duplicateRelatedModifier = function (item) {
                        var duplicate = angular.copy(item.$modelValue);
                        clearIds(duplicate);
                        $scope.relatedModifiers.push(duplicate);
                    },
                    editRelatedModifier = function (item) {
                        sessionService.set('relatedModifierViewModel', angular.copy(item.$modelValue), true);
                        $modal.open(relatedModifiersTreeDialogOptions).result.then(function () {
                            angular.extend(item.$modelValue, sessionService.get('relatedModifierViewModel'));
                        });
                    },
                    getRelatedModifierImage = function (imageId) {
                        return imageId != null ? imagesService.getImageSmallUrl(imageId) : pathesOfDefaultImages.placeholder;
                    };

                $scope.removeRelatedModifier = removeRelatedModifier;
                $scope.duplicateRelatedModifier = duplicateRelatedModifier;
                $scope.editRelatedModifier = editRelatedModifier;
                $scope.getRelatedModifierImage = getRelatedModifierImage;
            }
        };
    }
]);