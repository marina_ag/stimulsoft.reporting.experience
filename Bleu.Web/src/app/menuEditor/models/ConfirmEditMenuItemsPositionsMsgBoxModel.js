﻿angular.module('bleu.menuEditor').factory('ConfirmEditMenuItemsPositionsMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeMenuItemsModel = function () {
            this.title = localizationService.getLocalizedString('MenuEditor.EditMenuItemsPositionsPageConfirmPositionsSaveDlg_Title');
            this.message = localizationService.getLocalizedString('MenuEditor.EditMenuItemsPositionsPageConfirmPositionsSaveDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('MenuEditor.EditMenuItemsPositionsPageConfirmPositionsSaveDlg_BtnTextYes'),
                    value: 'yes'
                },
                {
                    text: localizationService.getLocalizedString('MenuEditor.EditMenuItemsPositionsPageConfirmPositionsSaveDlg_BtnTextNo'),
                    value: 'no'
                }
            ];
            this.successValue = 'yes';
        };

        return removeMenuItemsModel;
    }
]);