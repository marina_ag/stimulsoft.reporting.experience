﻿angular.module('bleu.menuEditor').value('CommonModifiersListGridDataModel', {
    fields: [
        'ModifierId',
        'GroupId',
        'DisplayOrder',
        'IsHidden',
        'Price',
        'Languages'
    ],
    fieldsForSelectAll: ['ModifierId'],
    primaryFields: ['id'],
    sortMapping: [
        { column: 'name', data: ['Name'] },
        { column: 'group', data: ['GroupId'] },
        { column: 'position', data: ['DisplayOrder'] },
        { column: 'isActive', data: ['IsHidden'] },
        { column: 'price', data: ['Price'] }
    ]
});