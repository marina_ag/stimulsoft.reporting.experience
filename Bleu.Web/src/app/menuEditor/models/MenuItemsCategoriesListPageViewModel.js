﻿angular.module('bleu.menuEditor').factory('MenuItemsCategoriesListPageViewModel', [
    function () {
        var menuItemsCategoriesListPageViewModel = function () {
            this.textFilter = null;
            this.canDelete = false;
            this.canEditPositions = false;
        };

        return menuItemsCategoriesListPageViewModel;
    }
]);