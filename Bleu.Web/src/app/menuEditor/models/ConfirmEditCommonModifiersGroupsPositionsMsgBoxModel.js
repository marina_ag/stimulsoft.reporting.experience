﻿angular.module('bleu.menuEditor').factory('ConfirmEditCommonModifiersGroupsPositionsMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeMenuItemsModel = function () {
            this.title = localizationService.getLocalizedString('MenuEditor.EditCommonModifiersGroupsPositionsPageConfirmPositionsSaveDlg_Title');
            this.message = localizationService.getLocalizedString('MenuEditor.EditCommonModifiersGroupsPositionsPageConfirmPositionsSaveDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('MenuEditor.EditCommonModifiersGroupsPositionsPageConfirmPositionsSaveDlg_BtnTextYes'),
                    value: 'yes'
                },
                {
                    text: localizationService.getLocalizedString('MenuEditor.EditCommonModifiersGroupsPositionsPageConfirmPositionsSaveDlg_BtnTextNo'),
                    value: 'no'
                }
            ];
            this.successValue = 'yes';
        };

        return removeMenuItemsModel;
    }
]);