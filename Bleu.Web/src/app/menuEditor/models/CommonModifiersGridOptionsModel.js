﻿angular.module('bleu.menuEditor').factory('CommonModifiersGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var commonModifiersGridOptionsModel = function () {
            this.columnDefs = [
                {
                    field: 'name',
                    displayName: localizationService.getLocalizedString('MenuEditor.CommonModifiersGrid_HeaderName'),
                    type: 'string',
                    cellTemplate: 'src/app/menuEditor/views/grid/CommonModifiersGridNameTemplate.html'
                },
                {
                    field: 'group',
                    type: 'string',
                    displayName: localizationService.getLocalizedString('MenuEditor.CommonModifiersGrid_HeaderGroup'),
                    cellTemplate: 'src/app/menuEditor/views/grid/CommonModifiersGridGroupTemplate.html'
                },
                {
                    field: 'isActive',
                    type: 'string',
                    displayName: localizationService.getLocalizedString('MenuEditor.CommonModifiersGrid_HeaderActive'),
                    cellTemplate: 'src/app/menuEditor/views/grid/CommonModifiersGridIsActiveTemplate.html'
                },
                {
                    field: 'price',
                    type: 'number',
                    cellFilter: 'currency',
                    displayName: localizationService.getLocalizedString('MenuEditor.CommonModifiersGrid_HeaderPrice')
                }
            ];
        };

        angular.extend(commonModifiersGridOptionsModel.prototype, new gridDefaultOptions());

        return commonModifiersGridOptionsModel;
    }
]);