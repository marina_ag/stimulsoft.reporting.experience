﻿angular.module('bleu.menuEditor').factory('RemoveCommonModifiersGroupsMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeCommonModifiersGroupsModel = function () {
            this.title = localizationService.getLocalizedString('MenuEditor.CommonModifiersGroupsListPageRemoveCommonModifiersGroupsDlg_Title');
            this.message = localizationService.getLocalizedString('MenuEditor.CommonModifiersGroupsListPageRemoveCommonModifiersGroupsDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('MenuEditor.CommonModifiersGroupsListPageRemoveCommonModifiersGroupsDlg_BtnTextOk'),
                    value: 'ok'
                },
                {
                    text: localizationService.getLocalizedString('MenuEditor.CommonModifiersGroupsListPageRemoveCommonModifiersGroupsDlg_BtnTextCancel'),
                    value: 'cancel'
                }
            ];
            this.successValue = 'ok';
        };

        return removeCommonModifiersGroupsModel;
    }
]);