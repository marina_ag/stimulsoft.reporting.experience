﻿angular.module('bleu.menuEditor').factory('RemoveMenuItemsMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeMenuItemsModel = function () {
            this.title = localizationService.getLocalizedString('MenuEditor.MenuItemsListPageRemoveMenuItemsDlg_Title');
            this.message = localizationService.getLocalizedString('MenuEditor.MenuItemsListPageRemoveMenuItemsDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('MenuEditor.MenuItemsListPageRemoveMenuItemsDlg_BtnTextOk'),
                    value: 'ok'
                },
                {
                    text: localizationService.getLocalizedString('MenuEditor.MenuItemsListPageRemoveMenuItemsDlg_BtnTextCancel'),
                    value: 'cancel'
                }
            ];
            this.successValue = 'ok';
        };

        return removeMenuItemsModel;
    }
]);