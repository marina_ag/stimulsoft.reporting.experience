﻿angular.module('bleu.menuEditor').factory('MenuItemsCategoriesGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var menuItemsCategoriesGridOptionsModel = function () {
            this.useExternalSorting = false;
            this.columnDefs = [
                {
                    field: 'name',
                    displayName: localizationService.getLocalizedString('MenuEditor.MenuItemsCategoriesGrid_HeaderName'),
                    type: 'string',
                    cellTemplate: 'src/app/menuEditor/views/grid/MenuItemsCategoriesGridNameTemplate.html'
                },
                {
                    field: 'imageId',
                    displayName: localizationService.getLocalizedString('MenuEditor.MenuItemsCategoriesGrid_HeaderImage'),
                    type: 'string',
                    cellTemplate: 'src/app/menuEditor/views/grid/MenuItemsCategoriesGridImageTemplate.html',
                    enableSorting: false
                },
                {
                    field: 'isActive',
                    displayName: localizationService.getLocalizedString('MenuEditor.MenuItemsCategoriesGrid_HeaderIsActive'),
                    type: 'string',
                    cellTemplate: 'src/app/menuEditor/views/grid/MenuItemsCategoriesGridIsActiveTemplate.html'
                },
                {
                    field: 'itemsAssigned',
                    displayName: localizationService.getLocalizedString('MenuEditor.MenuItemsCategoriesGrid_HeaderItemsAssigned'),
                    type: 'string',
                    cellTemplate: 'src/app/menuEditor/views/grid/MenuItemsCategoriesGridCountTemplate.html'
                }
            ];
        };

        angular.extend(menuItemsCategoriesGridOptionsModel.prototype, new gridDefaultOptions());

        return menuItemsCategoriesGridOptionsModel;
    }
]);