﻿angular.module('bleu.menuEditor').factory('RemoveCommonModifiersMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeCommonModifiersModel = function () {
            this.title = localizationService.getLocalizedString('MenuEditor.CommonModifiersListPageRemoveCommonModifiersDlg_Title');
            this.message = localizationService.getLocalizedString('MenuEditor.CommonModifiersListPageRemoveCommonModifiersDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('MenuEditor.CommonModifiersListPageRemoveCommonModifiersDlg_BtnTextOk'),
                    value: 'ok'
                },
                {
                    text: localizationService.getLocalizedString('MenuEditor.CommonModifiersListPageRemoveCommonModifiersDlg_BtnTextCancel'),
                    value: 'cancel'
                }
            ];
            this.successValue = 'ok';
        };

        return removeCommonModifiersModel;
    }
]);