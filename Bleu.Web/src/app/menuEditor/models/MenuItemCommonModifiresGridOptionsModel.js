﻿angular.module('bleu.menuEditor').factory('MenuItemCommonModifiresGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var menuItemCommonModifiresGridOptionsModel = function () {
            this.useExternalSorting = false;
            this.columnDefs = [
                {
                    field: 'name',
                    displayName: localizationService.getLocalizedString('MenuEditor.MenuItemCommonModifiersGrid_HeaderName'),
                    type: 'string',
                    cellTemplate: 'src/app/menuEditor/views/grid/MenuItemCommonModifiersGridNameTemplate.html'
                },
                {
                    field: 'price',
                    displayName: localizationService.getLocalizedString('MenuEditor.MenuItemCommonModifiersGrid_HeaderPrice'),
                    type: 'string',
                    cellFilter: 'currency'
                }
            ];
        };

        angular.extend(menuItemCommonModifiresGridOptionsModel.prototype, new gridDefaultOptions());

        return menuItemCommonModifiresGridOptionsModel;
    }
]);