﻿angular.module('bleu.menuEditor').factory('CommonModifiersListPageViewModel', [
    function () {
        var commonModifiersListPageViewModel = function () {
            this.textFilter = null;
            this.groupFilter = null;
            this.canDelete = false;
            this.canDuplicate = false;
            this.canEditPositions = false;
        };

        return commonModifiersListPageViewModel;
    }
]);