﻿angular.module('bleu.menuEditor').factory('MenuItemsListPageViewModel', [
    function () {
        var menuItemsListPageViewModel = function () {
            this.textFilter = null;
            this.categoryFilter = null;
            this.canDelete = false;
            this.canDuplicate = false;
            this.canEditPositions = false;
        };

        return menuItemsListPageViewModel;
    }
]);