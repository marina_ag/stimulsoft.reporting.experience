﻿angular.module('bleu.menuEditor').factory('MenuItemViewModel', [
    'BaseViewModel',
    'UOM',
    'PathesOfDefaultImages',
    'LanguageViewModel',
    function (baseViewModel, uom, pathesOfDefaultImages, languageViewModel) {
        var menuItemViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.name = '';
            this.upc = null;
            this.sku = null;
            this.categoryId = null;
            this.position = null;
            this.isActive = true;
            this.price = 0;
            this.isOpenPrice = false;
            this.uom = uom.Pcs;
            this.isFoodStamp = false;
            this.taxes = [];
            this.imageId = null;
            this.description = '';
            this.relatedModifiers = [];
            this.commonModifiersGroups = [];
            this.image = pathesOfDefaultImages.placeholder;
            this.languages = [new languageViewModel()];
        };

        menuItemViewModel.prototype = Object.create(baseViewModel.prototype);

        return menuItemViewModel;
    }
]);