﻿angular.module('bleu.menuEditor').factory('ConfirmEditCommonModifiersPositionsMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeMenuItemsModel = function () {
            this.title = localizationService.getLocalizedString('MenuEditor.EditCommonModifiersPositionsPageConfirmPositionsSaveDlg_Title');
            this.message = localizationService.getLocalizedString('MenuEditor.EditCommonModifiersPositionsPageConfirmPositionsSaveDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('MenuEditor.EditCommonModifiersPositionsPageConfirmPositionsSaveDlg_BtnTextYes'),
                    value: 'yes'
                },
                {
                    text: localizationService.getLocalizedString('MenuEditor.EditCommonModifiersPositionsPageConfirmPositionsSaveDlg_BtnTextNo'),
                    value: 'no'
                }
            ];
            this.successValue = 'yes';
        };

        return removeMenuItemsModel;
    }
]);