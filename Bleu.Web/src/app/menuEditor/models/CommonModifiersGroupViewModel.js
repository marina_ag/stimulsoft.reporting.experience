﻿angular.module('bleu.menuEditor').factory('CommonModifiersGroupViewModel', [
    'BaseViewModel',
    'MenuSortingType',
    'PathesOfDefaultImages',
    function (baseViewModel, menuSortingType, pathesOfDefaultImages) {
        var commonModifiersGroupViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.name = '';
            this.imageId = null;
            this.isActive = true;
            this.modifiersAssigned = 0;
            this.position = null;
            this.menuItemsSorting = 0;
            this.stores = [];
            this.menuItemsIds = [];
            this.commonModifiers = [];
            this.sorting = menuSortingType.Custom;
            this.image = pathesOfDefaultImages.placeholder;
        };

        commonModifiersGroupViewModel.prototype = Object.create(baseViewModel.prototype);

        return commonModifiersGroupViewModel;
    }
]);