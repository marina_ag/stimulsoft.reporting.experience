﻿angular.module('bleu.menuEditor').factory('RemoveMenuItemsCategoriesMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeMenuItemsCategoriesModel = function () {
            this.title = localizationService.getLocalizedString('MenuEditor.MenuItemsCategoriesListPageRemoveMenuItemsCategoriesDlg_Title');
            this.message = localizationService.getLocalizedString('MenuEditor.MenuItemsCategoriesListPageRemoveMenuItemsCategoriesDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('MenuEditor.MenuItemsCategoriesListPageRemoveMenuItemsCategoriesDlg_BtnTextOk'),
                    value: 'ok'
                },
                {
                    text: localizationService.getLocalizedString('MenuEditor.MenuItemsCategoriesListPageRemoveMenuItemsCategoriesDlg_BtnTextCancel'),
                    value: 'cancel'
                }
            ];
            this.successValue = 'ok';
        };

        return removeMenuItemsCategoriesModel;
    }
]);