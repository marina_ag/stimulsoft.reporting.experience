﻿angular.module('bleu.menuEditor').factory('LanguageViewModel', [
    function () {
        var languageViewModel = function () {
            this.id = null;
            this.name = '';
            this.description = '';
            this.language = 'en';
        };

        return languageViewModel;
    }
]);