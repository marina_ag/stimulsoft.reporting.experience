﻿angular.module('bleu.menuEditor').factory('CommonModifiersGroupsGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var commonModifiersGroupsGridOptionsModel = function () {
            this.useExternalSorting = false;
            this.columnDefs = [
                {
                    field: 'name',
                    displayName: localizationService.getLocalizedString('MenuEditor.CommonModifiersGroupsGrid_HeaderName'),
                    type: 'string',
                    cellTemplate: 'src/app/menuEditor/views/grid/CommonModifiersGroupsGridNameTemplate.html'
                },
                {
                    field: 'image',
                    displayName: localizationService.getLocalizedString('MenuEditor.CommonModifiersGroupsGrid_HeaderImage'),
                    type: 'string',
                    cellTemplate: 'src/app/menuEditor/views/grid/CommonModifiersGroupsGridImageTemplate.html',
                    enableSorting: false
                },
                {
                    field: 'isActive',
                    displayName: localizationService.getLocalizedString('MenuEditor.CommonModifiersGroupsGrid_HeaderIsActive'),
                    type: 'string',
                    cellTemplate: 'src/app/menuEditor/views/grid/CommonModifiersGroupsGridIsActiveTemplate.html'
                },
                {
                    field: 'modifiersAssigned',
                    displayName: localizationService.getLocalizedString('MenuEditor.CommonModifiersGroupsGrid_HeaderModifiersAssigned'),
                    type: 'string',
                    cellTemplate: 'src/app/menuEditor/views/grid/CommonModifiersGroupsGridCountTemplate.html'
                }
            ];
        };

        angular.extend(commonModifiersGroupsGridOptionsModel.prototype, new gridDefaultOptions());

        return commonModifiersGroupsGridOptionsModel;
    }
]);