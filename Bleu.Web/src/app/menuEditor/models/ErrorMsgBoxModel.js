﻿angular.module('bleu.menuEditor').factory('ErrorMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var errorMsgBoxModel = function (title,message) {
            this.title = title;
            this.message = message;
            this.buttons = [
                {
                    text: 'OK',//TODO localization
                    value: 'ok',
                    icon: 'icon-checkmark'
                }
            ];
            this.notCloseable = true;
        };

        return errorMsgBoxModel;
    }
]);