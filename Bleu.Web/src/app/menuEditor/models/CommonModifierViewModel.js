﻿angular.module('bleu.menuEditor').factory('CommonModifierViewModel', [
    'BaseViewModel',
    'UOM',
    'PathesOfDefaultImages',
    'LanguageViewModel',
    function (baseViewModel, uom, pathesOfDefaultImages, languageViewModel) {
        var commonModifierViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.itemId = null;
            this.name = '';
            this.group = null;
            this.position = null;
            this.isActive = true;
            this.price = 0;
            this.isOpenPrice = false;
            this.uom = uom.Pcs;
            this.firstFreeQuantity = 0;
            this.imageId = null;
            this.description = '';
            this.maxQty = 0;
            this.minQty = 0;
            this.image = pathesOfDefaultImages.placeholder;
            this.languages = [new languageViewModel()];
        };

        commonModifierViewModel.prototype = Object.create(baseViewModel.prototype);

        return commonModifierViewModel;
    }
]);