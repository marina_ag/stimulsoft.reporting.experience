﻿angular.module('bleu.menuEditor').factory('MenuItemsGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var menuItemsGridOptionsModel = function () {
            this.columnDefs = [
                {
                    field: 'name',
                    displayName: localizationService.getLocalizedString('MenuEditor.MenuItemsGrid_HeaderName'),
                    type: 'string',
                    cellTemplate: 'src/app/menuEditor/views/grid/MenuItemsGridNameTemplate.html'
                },
                {
                    field: 'upc',
                    displayName: localizationService.getLocalizedString('MenuEditor.MenuItemsGrid_HeaderUpc'),
                    type: 'number'
                },
                {
                    field: 'sku',
                    displayName: localizationService.getLocalizedString('MenuEditor.MenuItemsGrid_HeaderSku'),
                    type: 'number'
                },
                {
                    field: 'category',
                    type: 'string',
                    displayName: localizationService.getLocalizedString('MenuEditor.MenuItemsGrid_HeaderCategory'),
                    cellTemplate: 'src/app/menuEditor/views/grid/MenuItemsGridCategoryTemplate.html'
                },
                {
                    field: 'isActive',
                    type: 'string',
                    displayName: localizationService.getLocalizedString('MenuEditor.MenuItemsGrid_HeaderActive'),
                    cellTemplate: 'src/app/menuEditor/views/grid/MenuItemsGridIsActiveTemplate.html'
                },
                {
                    field: 'price',
                    type: 'number',
                    cellFilter: 'currency',
                    displayName: localizationService.getLocalizedString('MenuEditor.MenuItemsGrid_HeaderPrice')
                }
            ];
        };

        angular.extend(menuItemsGridOptionsModel.prototype, new gridDefaultOptions());

        return menuItemsGridOptionsModel;
    }
]);