﻿angular.module('bleu.menuEditor').factory('EditCommonModifiersGroupsPositionsPageViewModel', [
    function () {
        var editCommonModifiersGroupsPositionsPageViewModel = function () {
            this.canSave = false;
            this.canReset = false;
        };

        return editCommonModifiersGroupsPositionsPageViewModel;
    }
]);