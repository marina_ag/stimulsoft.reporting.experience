﻿angular.module('bleu.menuEditor').factory('EditMenuItemsCategoriesPositionsPageViewModel', [
    function () {
        var editMenuItemsCategoriesPositionsPageViewModel = function () {
            this.canSave = false;
            this.canReset = false;
        };

        return editMenuItemsCategoriesPositionsPageViewModel;
    }
]);