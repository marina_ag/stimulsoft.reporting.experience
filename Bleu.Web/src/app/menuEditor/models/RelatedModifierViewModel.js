﻿angular.module('bleu.menuEditor').factory('RelatedModifierViewModel', [
    'BaseViewModel',
    'LanguageViewModel',
    function (baseViewModel, languageViewModel) {
        var relatedModifierViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.name = '';
            this.price = 0;
            this.position = 0;
            this.description = '';
            this.uom = null;
            this.imageId = null;
            this.relatedModifiers = [];
            this.languages = [new languageViewModel()];
        };

        relatedModifierViewModel.prototype = Object.create(baseViewModel.prototype);

        return relatedModifierViewModel;
    }
]);