﻿angular.module('bleu.menuEditor').factory('EditCommonModifiersPositionsPageViewModel', [
    function () {
        var editCommonModifiersPositionsPageViewModel = function () {
            this.canSave = false;
            this.canReset = false;
            this.groupId = null;
        };

        return editCommonModifiersPositionsPageViewModel;
    }
]);