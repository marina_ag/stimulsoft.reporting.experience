﻿angular.module('bleu.menuEditor').factory('EditMenuItemsPositionsPageViewModel', [
    function () {
        var editMenuItemsPositionsPageViewModel = function () {
            this.canSave = false;
            this.canReset = false;
            this.categoryId = null;
        };

        return editMenuItemsPositionsPageViewModel;
    }
]);