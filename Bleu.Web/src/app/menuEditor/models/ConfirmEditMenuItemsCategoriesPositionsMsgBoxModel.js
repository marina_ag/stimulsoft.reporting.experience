﻿angular.module('bleu.menuEditor').factory('ConfirmEditMenuItemsCategoriesPositionsMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeMenuItemsModel = function () {
            this.title = localizationService.getLocalizedString('MenuEditor.EditMenuItemsCategoriesPositionsPageConfirmPositionsSaveDlg_Title');
            this.message = localizationService.getLocalizedString('MenuEditor.EditMenuItemsCategoriesPositionsPageConfirmPositionsSaveDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('MenuEditor.EditMenuItemsCategoriesPositionsPageConfirmPositionsSaveDlg_BtnTextYes'),
                    value: 'yes'
                },
                {
                    text: localizationService.getLocalizedString('MenuEditor.EditMenuItemsCategoriesPositionsPageConfirmPositionsSaveDlg_BtnTextNo'),
                    value: 'no'
                }
            ];
            this.successValue = 'yes';
        };

        return removeMenuItemsModel;
    }
]);