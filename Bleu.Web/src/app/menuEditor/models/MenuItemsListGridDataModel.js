﻿angular.module('bleu.menuEditor').value('MenuItemsListGridDataModel', {
    fields: [
        'MenuItemId',
        'UPC',
        'SKU',
        'DisplayOrder',
        'IsHidden',
        'Price',
        'Languages',
        'CategoryId'
    ],
    fieldsForSelectAll: ['MenuItemId'],
    primaryFields: ['id'],
    sortMapping: [
        { column: 'name', data: ['Name'] },
        { column: 'upc', data: ['UPC'] },
        { column: 'sku', data: ['SKU'] },
        { column: 'category', data: ['CategoryId'] },
        { column: 'position', data: ['DisplayOrder'] },
        { column: 'isActive', data: ['IsHidden'] },
        { column: 'price', data: ['Price'] }
    ]
});