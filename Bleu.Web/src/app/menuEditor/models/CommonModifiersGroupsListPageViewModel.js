﻿angular.module('bleu.menuEditor').factory('CommonModifiersGroupsListPageViewModel', [
    function () {
        var commonModifiersGroupsListPageViewModel = function () {
            this.textFilter = null;
            this.canDelete = false;
        };

        return commonModifiersGroupsListPageViewModel;
    }
]);