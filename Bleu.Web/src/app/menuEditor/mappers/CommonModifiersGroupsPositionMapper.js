﻿angular.module('bleu.menuEditor').factory('CommonModifiersGroupsPositionMapper', [
    function () {
        var
            transformToServer = function (category) {
                return {
                    Id: category.id,
                    DisplayOrder: category.position
                }
            },
            mapCollectionToServer = function (data) {
                return data.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (catogoryId, menuItems) {
                return transformToServer(catogoryId, menuItems);
            };

        return {
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);