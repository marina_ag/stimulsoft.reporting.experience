﻿angular.module('bleu.menuEditor').factory('CommonModifiersGroupsMapper', [
    'CommonModifiersGroupViewModel',
    'StoresSharedMapper',
    'CommonModifiersMapper',
    'ImagesService',
    'PathesOfDefaultImages',
    function (
        commonModifiersGroupViewModel,
        storesSharedMapper,
        commonModifiersMapper,
        imagesService,
        pathesOfDefaultImages) {
        var
            transformToClient = function (response) {
                var commonModifierGroup = new commonModifiersGroupViewModel();

                commonModifierGroup.id = response.Id;
                commonModifierGroup.name = response.Name;
                commonModifierGroup.imageId = response.Image != null ? response.Image.ImageId : null;
                commonModifierGroup.image = commonModifierGroup.imageId != null ? imagesService.getImageSmallUrl(commonModifierGroup.imageId) : pathesOfDefaultImages.placeholder;
                commonModifierGroup.isActive = response.IsActive;
                commonModifierGroup.modifiersAssigned = response.ModifiersAssigned != null ? response.ModifiersAssigned : 0;
                commonModifierGroup.position = response.DisplayOrder;
                commonModifierGroup.sorting = response.Sorting;
                commonModifierGroup.stores = response.Stores != null ? storesSharedMapper.mapCollectionToClient(response.Stores) : [];
                commonModifierGroup.menuItemsIds = response.MenuItems;
                commonModifierGroup.commonModifiers = response.Modifiers != null ? commonModifiersMapper.mapCollectionToClient(response.Modifiers) : [];

                return commonModifierGroup;
            },
            transformToServer = function (request) {
                return {
                    Id: request.id,
                    Name: request.name,
                    Image: {
                        ImageId: request.imageId,
                        Picture: [],
                        ImageType : 3
                    },
                    IsActive: request.isActive,
                    DisplayOrder: request.position,
                    Sorting: request.sorting,
                    Stores: storesSharedMapper.mapCollectionToServer(request.stores),
                    MenuItems: request.menuItemsIds
                }
            },
            mapCollectionToClient = function (response) {
                return response.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (response) {
                return transformToClient(response);
            },
            mapCollectionToServer = function (request) {
                return request.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            };

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);