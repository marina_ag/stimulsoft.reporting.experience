﻿angular.module('bleu.menuEditor').factory('CommonModifiersMapper', [
    'CommonModifierViewModel',
    'LanguagesMapper',
    'ImagesService',
    'PathesOfDefaultImages',
    function (commonModifierViewModel, languagesMapper, imagesService, pathesOfDefaultImages) {
        var
            transformToClient = function (response) {
                var commonModifier = new commonModifierViewModel();

                commonModifier.id = response.Id;
                commonModifier.itemId = response.ItemId;
                commonModifier.groupId = response.GroupId;
                commonModifier.position = response.DisplayOrder;
                commonModifier.isActive = response.IsHidden;
                commonModifier.price = response.Price;
                commonModifier.isOpenPrice = response.IsOpenPrice;
                commonModifier.uom = +response.UOM;
                commonModifier.firstFreeQuantity = response.FirstFreeQuantity;
                commonModifier.imageId = response.Image != null ? response.Image.ImageId : null;
                commonModifier.image = commonModifier.imageId != null ? imagesService.getImageSmallUrl(commonModifier.imageId) : pathesOfDefaultImages.placeholder;
                commonModifier.maxQty = response.MaxNumberSelections;
                commonModifier.minQty = response.MinNumberSelections;
                commonModifier.languages = response.Languages != null ? languagesMapper.mapCollectionToClient(response.Languages) : [];
                commonModifier.name = commonModifier.languages.length > 0 ? commonModifier.languages[0].name : '';
                commonModifier.description = commonModifier.languages.length > 0 ? commonModifier.languages[0].description : '';

                return commonModifier;
            },
            transformToServer = function (request) {
                var languages = request.languages.map(function (item, key) {
                    if (key == 0) {
                        item.name = request.name;
                        item.description = request.description;
                    }

                    return item;
                });
                return {
                    Id: request.id,
                    ItemId: request.itemId,
                    GroupId: request.groupId,
                    DisplayOrder: request.position,
                    IsHidden: request.isActive,
                    Price: request.price,
                    IsOpenPrice: request.isOpenPrice,
                    UOM: request.uom.toString(),
                    FirstFreeQuantity: request.firstFreeQuantity,
                    Image: {
                        ImageId: request.imageId,
                        Picture: [],
                        ImageType: 3
                    },
                    MaxNumberSelections: request.maxQty,
                    MinNumberSelections: request.minQty,
                    Languages: languagesMapper.mapCollectionToServer(languages)
                }
            },
            mapCollectionToClient = function (response) {
                return response.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (response) {
                return transformToClient(response);
            },
            mapCollectionToServer = function (request) {
                return request.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            };

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);