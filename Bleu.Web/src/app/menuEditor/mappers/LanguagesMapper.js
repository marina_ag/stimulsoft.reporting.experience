﻿angular.module('bleu.menuEditor').factory('LanguagesMapper', [
    'LanguageViewModel',
    function (languageViewModel) {
        var
            transformToClient = function (response) {
                var language = new languageViewModel();

                language.id = response.ItemId;
                language.name = response.Name;
                language.description = response.Description;
                language.language = response.Language;

                return language;
            },
            transformToServer = function (request) {
                return {
                    ItemId: request.id,
                    Name: request.name,
                    Description: request.description,
                    Language: request.language
                }
            },
            mapCollectionToClient = function (response) {
                return response.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (response) {
                return transformToClient(response);
            },
            mapCollectionToServer = function (request) {
                return request.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            };

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);