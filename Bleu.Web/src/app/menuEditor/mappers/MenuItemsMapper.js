﻿angular.module('bleu.menuEditor').factory('MenuItemsMapper', [
    'MenuItemViewModel',
    'TaxesSharedMapper',
    'RelatedModifiersMapper',
    'CommonModifiersGroupsMapper',
    'LanguagesMapper',
    'DataMapHelper',
    'ImagesService',
    'PathesOfDefaultImages',
    function (
        menuItemViewModel,
        taxesSharedMapper,
        relatedModifiersMapper,
        commonModifiersGroupsMapper,
        languagesMapper,
        dataMapHelper,
        imagesService,
        pathesOfDefaultImages) {
        var
            transformToClient = function (response) {
                var menuItem = new menuItemViewModel();

                menuItem.id = response.MenuItemId;
                menuItem.itemId = response.ItemId;
                menuItem.upc = dataMapHelper.parseNumber(response.UPC);
                menuItem.sku = dataMapHelper.parseNumber(response.SKU);
                menuItem.categoryId = response.CategoryId;
                menuItem.position = response.DisplayOrder;
                menuItem.isActive = response.IsHidden;
                menuItem.price = response.Price;
                menuItem.isOpenPrice = response.IsOpenPrice;
                menuItem.uom = dataMapHelper.parseNumber(response.UOM);
                menuItem.isFoodStamp = response.IsFoodStamp;
                menuItem.taxes = response.Taxes != null ? taxesSharedMapper.mapCollectionToClient(response.Taxes) : [];
                menuItem.imageId = response.Image != null ? response.Image.ImageId : null;
                menuItem.image = menuItem.imageId != null ? imagesService.getImageSmallUrl(menuItem.imageId) : pathesOfDefaultImages.placeholder;
                menuItem.relatedModifiers = response.MenuItemModifiers != null ? relatedModifiersMapper.mapTreeToClient(response.MenuItemModifiers) : [];
                menuItem.commonModifiersGroups = response.CommonModifierGroups != null ? commonModifiersGroupsMapper.mapCollectionToClient(response.CommonModifierGroups) : [];
                menuItem.languages = response.Languages != null ? languagesMapper.mapCollectionToClient(response.Languages) : [];
                menuItem.name = menuItem.languages.length > 0 ? menuItem.languages[0].name : '';
                menuItem.description = menuItem.languages.length > 0 ? menuItem.languages[0].description : '';

                return menuItem;
            },
            transformToServer = function (request) {
                var languages = request.languages.map(function (item, key) {
                    if (key == 0) {
                        item.name = request.name;
                        item.description = request.description;
                    }

                    return item;
                });
                return {
                    MenuItemId: request.id,
                    ItemId: request.itemId,
                    UPC: request.upc !=null ? request.upc.toString() : '',
                    SKU: request.sku !=null ? request.sku.toString() : '',
                    CategoryId: request.categoryId,
                    DisplayOrder: request.position,
                    IsHidden: request.isActive,
                    Price: request.price,
                    IsOpenPrice: request.isOpenPrice,
                    UOM: request.uom.toString(),
                    IsFoodStamp: request.isFoodStamp,
                    Taxes: taxesSharedMapper.mapCollectionToServer(request.taxes),
                    Image: {
                        ImageId: request.imageId,
                        Picture: [],
                        ImageType: 3
                    },
                    MenuItemModifiers: relatedModifiersMapper.mapTreeToServer(request.relatedModifiers),
                    Languages: languagesMapper.mapCollectionToServer(languages)
                }
            },
            mapCollectionToClient = function (response) {
                return response.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (response) {
                return transformToClient(response);
            },
            mapCollectionToServer = function (request) {
                return request.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            },
            mapMenuItemCommonModifiers = function (menuItem) {
                var commonModifiers = [];
                menuItem.commonModifiersGroups.forEach(function (group) {
                    commonModifiers = commonModifiers.concat(group.commonModifiers);
                });
                return commonModifiers;
            };

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer,
            mapMenuItemCommonModifiers: mapMenuItemCommonModifiers
        }
    }
]);