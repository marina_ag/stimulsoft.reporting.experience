﻿angular.module('bleu.menuEditor').factory('CommonModifiersPositionsMapper', [
    function () {
        var
            transformToServer = function (groupId, commonModifiers) {
                return {
                    Id: groupId,
                    Positions: commonModifiers.map(function (item) {
                        return {
                            Id: item.id,
                            DisplayOrder: item.position
                        }
                    })
                };
            },
            mapInstanceToServer = function (groupId, commonModifiers) {
                return transformToServer(groupId, commonModifiers);
            };

        return {
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);