﻿angular.module('bleu.menuEditor').factory('MenuItemsPositionsMapper', [
    function () {
        var
            transformToServer = function (categoryId, menuItems) {
                return {
                    Id: categoryId,
                    Positions: menuItems.map(function (item) {
                        return {
                            Id: item.id,
                            DisplayOrder: item.position
                        }
                    })
                };
            },
            mapInstanceToServer = function (catogoryId, menuItems) {
                return transformToServer(catogoryId, menuItems);
            };

        return {
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);