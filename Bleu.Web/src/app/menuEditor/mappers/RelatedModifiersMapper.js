﻿angular.module('bleu.menuEditor').factory('RelatedModifiersMapper', [
    'RelatedModifierViewModel',
    'LanguagesMapper',
    function (relatedModifierViewModel, languagesMapper) {
        var
            transformToClient = function (response) {
                var relatedModifier = new relatedModifierViewModel();

                relatedModifier.id = response.Id;
                relatedModifier.price = response.Price;
                relatedModifier.position = response.DisplayOrder;
                relatedModifier.uom = response.UOM;
                relatedModifier.imageId = response.Image != null ? response.Image.ImageId : null;
                relatedModifier.relatedModifiers = mapTreeToClient(response.Modifiers);
                relatedModifier.languages = response.Languages != null ? languagesMapper.mapCollectionToClient(response.Languages) : [];
                relatedModifier.name = relatedModifier.languages.length > 0 ? relatedModifier.languages[0].name : '';
                relatedModifier.description = relatedModifier.languages.length > 0 ? relatedModifier.languages[0].description : '';

                return relatedModifier;
            },
            transformToServer = function (request) {
                var languages = request.languages.map(function (item, key) {
                    if (key == 0) {
                        item.name = request.name;
                        item.description = request.description;
                    }

                    return item;
                });
                return {
                    Id: request.id,
                    Price: request.price,
                    DisplayOrder: request.position,
                    UOM: request.uom,
                    Image: {
                        ImageId: request.imageId,
                        Picture: [],
                        ImageType: 3
                    },
                    Modifiers: mapTreeToServer(request.relatedModifiers),
                    Languages: languagesMapper.mapCollectionToServer(languages)
                }
            },
            mapTreeToClient = function (response) {
                return response.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (response) {
                return transformToClient(response);
            },
            mapTreeToServer = function (request) {
                return request.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            };

        return {
            mapTreeToClient: mapTreeToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapTreeToServer: mapTreeToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);