﻿angular.module('bleu.menuEditor').controller('MenuItemController', [
    '$scope',
    '$state',
    '$stateParams',
    '$timeout',
    'MenuItemsService',
    'MenuItemsCategoriesSharedService',
    'TaxesSharedService',
    'MenuEditorViewService',
    'MenuItemViewModel',
    'RelatedModifierViewModel',
    'MenuItemCommonModifiresGridOptionsModel',
    'FileUploadHelper',
    'ClientGridDataHelper',
    'PathesOfDefaultImages',
    'ImageSizeType',
    'MessageBoxHelper',
    'ErrorMsgBoxModel',
    function (
        $scope,
        $state,
        $stateParams,
        $timeout,
        menuItemsService,
        menuItemsCategoriesSharedService,
        taxesSharedService,
        menuEditorViewService,
        menuItemViewModel,
        relatedModifierViewModel,
        menuItemCommonModifiresGridOptionsModel,
        fileUploadHelper,
        clientGridDataHelper,
        pathesOfDefaultImages,
        imageSizeType,
        messageBoxHelper,
        errorMsgBoxModel) {
        var gridOptions = new menuItemCommonModifiresGridOptionsModel(),
            gridHelper = new clientGridDataHelper(gridOptions),
            menuItem = new menuItemViewModel(),
            relatedModifier = new relatedModifierViewModel(),
            categories = [],
            uoms = menuEditorViewService.getUoms(),
            menuItemImageUploader = new fileUploadHelper({ placeholder: pathesOfDefaultImages.placeholder }),
            relatedModifierImageUploader = new fileUploadHelper({ placeholder: pathesOfDefaultImages.placeholder }),
            getTaxes = function(query) {
                return taxesSharedService.getTaxesByName(query);
            },
            onSuccessMenuItemImageUpload = function(imageId) {
                $scope.menuItem.imageId = imageId;
            },
            updateMenuItem = function() {
                $scope.menuItem.$validate().then(function() {
                    menuItemImageUploader.addPhoto(onSuccessMenuItemImageUpload).then(function() {
                        menuItemsService.updateMenuItem($scope.menuItem).then(function() {
                            backToMenuItemsList();
                        },
                        function (validationInfo) {
                            var message = validationInfo["Error"];

                            messageBoxHelper.openModal(new errorMsgBoxModel("Error", message)).then(function (value) {
                                if (value == 'ok') {
                                    $state.goBack('root.menuEditorMenuItemsList');
                                } 
                            });
                        }
                        );
                    });
                });
            },
            backToMenuItemsList = function() {
                $state.goBack('root.menuEditorMenuItemsList');
            },
            cancel = function() {
                backToMenuItemsList();
            },
            onSuccessRelatedModifierImageUpload = function(imageId) {
                $scope.relatedModifier.imageId = imageId;
            },
            addRelatedModifier = function() {
                $scope.relatedModifier.$validate().then(function() {
                    relatedModifierImageUploader.addPhoto(onSuccessRelatedModifierImageUpload).then(function() {
                        $scope.relatedModifier.uom = $scope.menuItem.uom; //todo fix it
                        $scope.menuItem.relatedModifiers.push($scope.relatedModifier);
                        $scope.relatedModifier = new relatedModifierViewModel();
                        $scope.relatedModifier.$clear();
                        relatedModifierImageUploader.clear();
                    });
                });
            },
            init = function() {
                menuItemsCategoriesSharedService.getAllMenuItemsCategories().then(function(menuItemsCategories) {
                    $scope.categories = menuItemsCategories;
                });

                menuItemsService.getMenuItem($stateParams.id).then(function(item) {
                    menuItemImageUploader.updatePlaceholder(item.imageId, imageSizeType.Medium);
                    $scope.menuItem = item;
                    gridHelper.init({
                        grid: $scope,
                        getDataMethod: function() {
                            return menuItemsService.getMenuItemCommonModifiers(item);
                        }
                    });
                });
            };

        $scope.menuItem = menuItem;
        $scope.relatedModifier = relatedModifier;
        $scope.categories = categories;
        $scope.menuItemImageUploader = menuItemImageUploader;
        $scope.relatedModifierImageUploader = relatedModifierImageUploader;
        $scope.uoms = uoms;
        $scope.getTaxes = getTaxes;
        $scope.updateMenuItem = updateMenuItem;
        $scope.cancel = cancel;
        $scope.addRelatedModifier = addRelatedModifier;
        $scope.gridOptions = gridOptions;

        init();
    }
]);