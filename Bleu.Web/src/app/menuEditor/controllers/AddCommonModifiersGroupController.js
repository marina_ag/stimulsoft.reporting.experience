﻿angular.module('bleu.menuEditor').controller('AddCommonModifiersGroupController', [
    '$scope',
    '$state',
    'CommonModifiersGroupsService',
    'StoresSharedService',
    'MenuEditorViewService',
    'MenuItemsCategoriesSharedService',
    'CommonModifiersGroupViewModel',
    'FileUploadHelper',
    'PathesOfDefaultImages',
    function (
        $scope,
        $state,
        commonModifiersGroupsService,
        storesSharedService,
        menuEditorViewService,
        menuItemsCategoriesSharedService,
        commonModifiersGroupViewModel,
        fileUploadHelper,
        pathesOfDefaultImages) {
        var menuItemsSortings = menuEditorViewService.getMenuSortingTypes(),
            menuItemsCategories = [],
            commonModifiersGroup = new commonModifiersGroupViewModel(),
            fileUpload = new fileUploadHelper({ placeholder: pathesOfDefaultImages.placeholder }),
            getStores = function (query) {
                return storesSharedService.getStoresByName(query);
            },
            onSuccessImageUpload = function (imageId) {
                $scope.commonModifiersGroup.imageId = imageId;
            },
            backToCommonModifiersGroupsList = function () {
                $state.goBack('root.menuEditorCommonModifiersGroupsList');
            },
            saveCommonModifiersGroup = function () {
                $scope.commonModifiersGroup.$validate().then(function () {
                    fileUpload.addPhoto(onSuccessImageUpload).then(function () {
                        commonModifiersGroupsService.addCommonModifiersGroup($scope.commonModifiersGroup).then(function () {
                            backToCommonModifiersGroupsList();
                        });
                    });
                });
            },
            cancel = function () {
                backToCommonModifiersGroupsList();
            },
            onCategoryOpen = function (menuItemCategory) {
                return menuEditorViewService.getMenuItemsByCategory(menuItemCategory).then(function (menuItems) {
                    menuItemCategory.menuItems = menuItems;
                });
            },
            onCategoryClose = function (menuItemCategory) {
                menuItemCategory.menuItems = [];
            },
            init = function () {
                menuItemsCategoriesSharedService.getAllMenuItemsCategories().then(function (categories) {
                    $scope.menuItemsCategories = categories;
                });
            };

        $scope.commonModifiersGroup = commonModifiersGroup;
        $scope.menuItemsSortings = menuItemsSortings;
        $scope.menuItemsCategories = menuItemsCategories;
        $scope.fileUpload = fileUpload;
        $scope.getStores = getStores;
        $scope.saveCommonModifiersGroup = saveCommonModifiersGroup;
        $scope.cancel = cancel;
        $scope.onCategoryOpen = onCategoryOpen;
        $scope.onCategoryClose = onCategoryClose;

        init();
    }
]);