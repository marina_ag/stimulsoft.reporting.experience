﻿angular.module('bleu.menuEditor').controller('CommonModifiersGroupsListController', [
    '$scope',
    '$state',
    'CommonModifiersGroupsService',
    'SessionService',
    'CommonModifiersGroupsGridOptionsModel',
    'CommonModifiersGroupsListPageViewModel',
    'RemoveCommonModifiersGroupsMsgBoxModel',
    'CommonModifiersListPageViewModel',
    'ClientGridDataHelper',
    'MessageBoxHelper',
    'FilterCompareMethod',
    'FilterRelationMethod',
    function (
        $scope,
        $state,
        commonModifiersGroupsService,
        sessionService,
        commonModifiersGroupsGridOptionsModel,
        commonModifiersGroupsListPageViewModel,
        removeCommonModifiersGroupsMsgBoxModel,
        commonModifiersListPageViewModel,
        clientGridDataHelper,
        messageBoxHelper,
        filterCompareMethod,
        filterRelationMethod) {
        var
            gridOptions = new commonModifiersGroupsGridOptionsModel(),
            viewModel = new commonModifiersGroupsListPageViewModel(),
            gridHelper = new clientGridDataHelper(gridOptions),
            onSelectionChanged = function (selection) {
                var hasSelection = selection.length != 0;
                viewModel.canDelete = hasSelection;
            },
            getCommonModifiersGroupsIds = function () {
                return gridHelper.getSelection().map(function (item) {
                    return item.id;
                });
            },
            filterChange = function () {
                gridHelper.updateFilters([
                    { Selector: 'name', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.And }
                ]);
            },
            addCommonModifiersGroup = function () {
                $state.go('root.menuEditorAddCommonModifiersGroup');
            },
            initCommonModifiersListPageViewModel = function () {
                return sessionService.set('commonModifiersListPageViewModel', new commonModifiersListPageViewModel(), true);
            },
            removeCommonModifiersGroups = function () {
                messageBoxHelper.openModal(new removeCommonModifiersGroupsMsgBoxModel()).then(function () {
                    commonModifiersGroupsService.removeCommonModifiersGroups(getCommonModifiersGroupsIds()).then(function () {
                        initCommonModifiersListPageViewModel();
                        gridHelper.reload();
                    });
                });
            },
            setActiveState = function (id, isActive) {
                commonModifiersGroupsService.setActiveState(id, isActive).then(function () {
                    gridHelper.reload();
                });
            },
            editPositions = function () {
                $state.go('root.menuEditorCommonModifiersGroupsPositions');
            },
            navigateToModifiers = function (groupId) {
                var commonModifiersListModel = initCommonModifiersListPageViewModel();
                commonModifiersListModel.groupFilter = groupId;
                $state.go('root.menuEditorCommonModifiersList');
            },
            init = function () {
                gridHelper.init({
                    grid: $scope,
                    getDataMethod: function () {
                        return commonModifiersGroupsService.getAllCommonModifiersGroups().then(function (categories) {
                            viewModel.canEditPositions = categories.length != 0;
                            return categories;
                        });
                    },
                    onSelectionChanged: onSelectionChanged
                });
            };

        $scope.gridOptions = gridOptions;
        $scope.filterChange = filterChange;
        $scope.viewModel = viewModel;
        $scope.addCommonModifiersGroup = addCommonModifiersGroup;
        $scope.removeCommonModifiersGroups = removeCommonModifiersGroups;
        $scope.setActiveState = setActiveState;
        $scope.editPositions = editPositions;
        $scope.navigateToModifiers = navigateToModifiers;

        init();
    }
]);