﻿angular.module('bleu.menuEditor').controller('EditMenuItemsPositionsController', [
    '$scope',
    '$state',
    'SessionService',
    'MenuItemsService',
    'MenuItemsCategoriesService',
    'MenuItemsCategoriesSharedService',
    'MenuEditorViewService',
    'EditMenuItemsPositionsPageViewModel',
    'ConfirmEditMenuItemsPositionsMsgBoxModel',
    'MessageBoxHelper',
    'SortableHelper',
    function (
        $scope,
        $state,
        sessionService,
        menuItemsService,
        menuItemsCategoriesService,
        menuItemsCategoriesSharedService,
        menuEditorViewService,
        editMenuItemsPositionsPageViewModel,
        confirmEditMenuItemsPositionsMsgBoxModel,
        messageBoxHelper,
        sortableHelper) {
        var
            categories = [],
            viewModel = sessionService.set('editMenuItemsPositionsPageViewModel', new editMenuItemsPositionsPageViewModel()),
            updateViewModelState = function (positionChanged) {
                viewModel.canSave = positionChanged;
                viewModel.canReset = positionChanged;
            },
            sortable = new sortableHelper({ positionFieldName: 'position', onPositionChanged: updateViewModelState }),
            savePositions = function () {
                menuItemsService.updateMenuItemsPositions(viewModel.categoryId, sortable.getItems()).then(function () {
                    sortable.save();
                    backToMenuItemsList();
                });
            },
            confirmSave = function (action) {
                if (sortable.isPositionChanged()) {
                    messageBoxHelper.openModal(new confirmEditMenuItemsPositionsMsgBoxModel()).then(function () {
                        savePositions();
                    }).finally(function () {
                        action();
                    });
                } else {
                    action();
                }
            },
            backToMenuItemsList = function () {
                $state.goBack('root.menuEditorMenuItemsList');
            },
            cancel = function () {
                confirmSave(backToMenuItemsList);
            },
            getMenuItemsByCategory = function () {
                menuItemsCategoriesService.getMenuItemsCategoryByIdOrFirst(viewModel.categoryId).then(function (menuItemsCategory) {
                    if (menuItemsCategory != null) {
                        viewModel.categoryId = menuItemsCategory.id;
                        menuEditorViewService.getMenuItemsByCategory(menuItemsCategory).then(function (menuItems) {
                            sortable.setItems(menuItems);
                        });
                    }
                });
            },
            changeCategory = function () {
                confirmSave(getMenuItemsByCategory);
            },
            reset = function () {
                sortable.resetItems();
            },
            init = function () {
                menuItemsCategoriesSharedService.getAllMenuItemsCategories().then(function (menuItemsCategories) {
                    $scope.categories = menuItemsCategories;
                    getMenuItemsByCategory();
                });
            };

        $scope.categories = categories;
        $scope.viewModel = viewModel;
        $scope.sortable = sortable;
        $scope.changeCategory = changeCategory;
        $scope.savePositions = savePositions;
        $scope.cancel = cancel;
        $scope.reset = reset;

        init();
    }
]);