﻿angular.module('bleu.menuEditor').controller('EditMenuItemsCategoriesPositionsController', [
    '$scope',
    '$state',
    'MenuItemsCategoriesService',
    'MenuItemsCategoriesSharedService',
    'EditMenuItemsCategoriesPositionsPageViewModel',
    'ConfirmEditMenuItemsCategoriesPositionsMsgBoxModel',
    'MessageBoxHelper',
    'SortableHelper',
    function (
        $scope,
        $state,
        menuItemsCategoriesService,
        menuItemsCategoriesSharedService,
        editMenuItemsCategoriesPositionsPageViewModel,
        confirmEditMenuItemsCategoriesPositionsMsgBoxModel,
        messageBoxHelper,
        sortableHelper) {
        var viewModel = new editMenuItemsCategoriesPositionsPageViewModel(),
            updateViewModelState = function (positionChanged) {
                viewModel.canSave = positionChanged;
                viewModel.canReset = positionChanged;
            },
            sortable = new sortableHelper({ positionFieldName: 'position', onPositionChanged: updateViewModelState }),
            savePositions = function () {
                menuItemsCategoriesService.updateMenuItemsCategoriesPositions(sortable.getItems()).then(function () {
                    sortable.save();
                    backToMenuItemsList();
                });
            },
            confirmSave = function (action) {
                if (sortable.isPositionChanged()) {
                    messageBoxHelper.openModal(new confirmEditMenuItemsCategoriesPositionsMsgBoxModel()).then(function () {
                        savePositions();
                    }).finally(function () {
                        action();
                    });
                } else {
                    action();
                }
            },
            backToMenuItemsList = function () {
                $state.goBack('root.menuEditorMenuItemsCategoriesList');
            },
            cancel = function () {
                confirmSave(backToMenuItemsList);
            },
            reset = function () {
                sortable.resetItems();
            },
            init = function () {
                menuItemsCategoriesSharedService.getAllMenuItemsCategories().then(function (menuItemsCategories) {
                    sortable.setItems(menuItemsCategories);
                });
            };

        $scope.viewModel = viewModel;
        $scope.sortable = sortable;
        $scope.savePositions = savePositions;
        $scope.cancel = cancel;
        $scope.reset = reset;

        init();
    }
]);