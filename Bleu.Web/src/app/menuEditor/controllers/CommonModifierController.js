﻿angular.module('bleu.menuEditor').controller('CommonModifierController', [
    '$scope',
    '$state',
    '$stateParams',
    'CommonModifiersService',
    'CommonModifiersGroupsService',
    'TaxesSharedService',
    'MenuEditorViewService',
    'CommonModifierViewModel',
    'FileUploadHelper',
    'PathesOfDefaultImages',
    'ImageSizeType',
    function (
        $scope,
        $state,
        $stateParams,
        commonModifiersService,
        commonModifiersGroupsService,
        taxesSharedService,
        menuEditorViewService,
        commonModifierViewModel,
        fileUploadHelper,
        pathesOfDefaultImages,
        imageSizeType) {
        var commonModifier = new commonModifierViewModel(),
            commonModifiersGroups = [],
            uoms = menuEditorViewService.getUoms(),
            fileUpload = new fileUploadHelper({ placeholder: pathesOfDefaultImages.placeholder }),
            getTaxes = function (query) {
                return taxesSharedService.getTaxesByName(query);
            },
            onSuccessImageUpload = function (imageId) {
                $scope.commonModifier.imageId = imageId;
            },
            backToCommonModifiersList = function () {
                $state.goBack('root.menuEditorCommonModifiersList');
            },
            updateCommonModifier = function () {
                $scope.commonModifier.$validate().then(function () {
                    fileUpload.addPhoto(onSuccessImageUpload).then(function () {
                        commonModifiersService.updateCommonModifier($scope.commonModifier).then(function () {
                            backToCommonModifiersList();
                        });
                    });
                });
            },
            cancel = function () {
                backToCommonModifiersList();
            },
            init = function () {
                commonModifiersGroupsService.getAllCommonModifiersGroups().then(function (groups) {
                    $scope.commonModifiersGroups = groups;
                });

                commonModifiersService.getCommonModifier($stateParams.id).then(function (modifier) {
                    fileUpload.updatePlaceholder(modifier.imageId, imageSizeType.Medium);
                    $scope.commonModifier = modifier;
                });
            };

        $scope.commonModifier = commonModifier;
        $scope.commonModifiersGroups = commonModifiersGroups;
        $scope.uoms = uoms;
        $scope.fileUpload = fileUpload;
        $scope.getTaxes = getTaxes;
        $scope.updateCommonModifier = updateCommonModifier;
        $scope.cancel = cancel;

        init();
    }
]);