﻿angular.module('bleu.menuEditor').controller('CommonModifiersListController', [
    '$scope',
    '$state',
    'CommonModifiersService',
    'CommonModifiersGroupsService',
    'SessionService',
    'CommonModifiersGridOptionsModel',
    'CommonModifiersListGridDataModel',
    'CommonModifiersListPageViewModel',
    'RemoveCommonModifiersMsgBoxModel',
    'EditCommonModifiersPositionsPageViewModel',
    'GridDataHelper',
    'MessageBoxHelper',
    'FilterCompareMethod',
    'FilterRelationMethod',
    function (
        $scope,
        $state,
        commonModifiersService,
        commonModifiersGroupsService,
        sessionService,
        commonModifiersGridOptionsModel,
        commonModifiersListGridDataModel,
        commonModifiersListPageViewModel,
        removeCommonModifiersMsgBoxModel,
        editCommonModifiersPositionsPageViewModel,
        gridDataHelper,
        messageBoxHelper,
        filterCompareMethod,
        filterRelationMethod) {
        var gridOptions = new commonModifiersGridOptionsModel(),
            groups = [],
            viewModel = sessionService.set('commonModifiersListPageViewModel', new commonModifiersListPageViewModel()),
            gridHelper = new gridDataHelper(gridOptions),
            onSelectionChanged = function (selection) {
                var hasSelection = selection.length != 0;
                viewModel.canDelete = hasSelection;
                viewModel.canDuplicate = selection.length == 1;
            },
            getCommonModifiersIds = function () {
                return gridHelper.getSelection().map(function (item) {
                    return item.id;
                });
            },
            getFilters = function () {
                return [
                    { Selector: 'GroupId', Method: filterCompareMethod.Equals, Value: viewModel.groupFilter, Relation: filterRelationMethod.And },
                    { Selector: 'Name', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.And }
                ];
            },
            filterChange = function () {
                gridHelper.updateFilters(getFilters());
            },
            addCommonModifier = function () {
                $state.go('root.menuEditorAddCommonModifier');
            },
            duplicateCommonModifier = function () {
                var ids = getCommonModifiersIds();

                if (ids.length > 0) {
                    commonModifiersService.duplicateCommonModifier(ids[0]).then(function () {
                        gridHelper.reload();
                    });
                }
            },
            removeCommonModifiers = function () {
                messageBoxHelper.openModal(new removeCommonModifiersMsgBoxModel()).then(function () {
                    commonModifiersService.removeCommonModifiers(getCommonModifiersIds()).then(function () {
                        gridHelper.reload();
                    });
                });
            },
            setActiveState = function (id, isActive) {
                commonModifiersService.setActiveState(id, isActive).then(function () {
                    gridHelper.reload();
                });
            },
            getGroupName = function (id) {
                var filteredGroups = $scope.groups.filter(function (item) {
                    return id == item.id;
                });

                return filteredGroups.length != 0 ? filteredGroups[0].name : '';
            },
            editPositions = function () {
                var editMenuItemsPositions = sessionService.set('editCommonModifiersPositionsPageViewModel', new editCommonModifiersPositionsPageViewModel(), true);
                editMenuItemsPositions.groupId = viewModel.groupFilter;
                $state.go('root.menuEditorCommonModifiersPositions');
            },
            init = function () {
                commonModifiersGroupsService.getAllCommonModifiersGroups().then(function (commonModifiersGroups) {
                    $scope.groups = commonModifiersGroups;
                    viewModel.canEditPositions = commonModifiersGroups.length != 0;
                });

                gridHelper.init({
                    fields: commonModifiersListGridDataModel.fields,
                    fieldsForSelectAll: commonModifiersListGridDataModel.fieldsForSelectAll,
                    primaryFields: commonModifiersListGridDataModel.primaryFields,
                    sortMapping: commonModifiersListGridDataModel.sortMapping,
                    grid: $scope,
                    getDataMethod: commonModifiersService.getCommonModifiers,
                    onSelectionChanged: onSelectionChanged,
                    initFilters: getFilters()
                });
            };

        $scope.getGroupName = getGroupName;
        $scope.groups = groups;
        $scope.gridOptions = gridOptions;
        $scope.filterChange = filterChange;
        $scope.viewModel = viewModel;
        $scope.addCommonModifier = addCommonModifier;
        $scope.duplicateCommonModifier = duplicateCommonModifier;
        $scope.removeCommonModifiers = removeCommonModifiers;
        $scope.setActiveState = setActiveState;
        $scope.editPositions = editPositions;

        init();
    }
]);