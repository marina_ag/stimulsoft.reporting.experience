﻿angular.module('bleu.menuEditor').controller('AddMenuItemsCategoryController', [
    '$scope',
    '$state',
    'MenuItemsCategoriesService',
    'StoresSharedService',
    'MenuEditorViewService',
    'MenuItemsCategorySharedViewModel',
    'FileUploadHelper',
    'PathesOfDefaultImages',
    function (
        $scope,
        $state,
        menuItemsCategoriesService,
        storesSharedService,
        menuEditorViewService,
        menuItemsCategorySharedViewModel,
        fileUploadHelper,
        pathesOfDefaultImages) {
        var
            menuItemsCategory = new menuItemsCategorySharedViewModel(),
            fileUpload = new fileUploadHelper({ placeholder: pathesOfDefaultImages.placeholder }),
            menuItemsSortings = menuEditorViewService.getMenuSortingTypes(),
            getStores = function (query) {
                return storesSharedService.getStoresByName(query);
            },
            onSuccessImageUpload = function (imageId) {
                $scope.menuItemsCategory.imageId = imageId;
            },
            backToMenuCategoriesList = function () {
                $state.goBack('root.menuEditorMenuItemsCategoriesList');
            },
            saveMenuItemsCategory = function () {
                $scope.menuItemsCategory.$validate().then(function () {
                    fileUpload.addPhoto(onSuccessImageUpload).then(function () {
                        menuItemsCategoriesService.addMenuItemsCategory($scope.menuItemsCategory).then(function () {
                            backToMenuCategoriesList();
                        });
                    });
                });
            },
            cancel = function () {
                backToMenuCategoriesList();
            };

        $scope.menuItemsCategory = menuItemsCategory;
        $scope.menuItemsSortings = menuItemsSortings;
        $scope.fileUpload = fileUpload;
        $scope.getStores = getStores;
        $scope.saveMenuItemsCategory = saveMenuItemsCategory;
        $scope.cancel = cancel;
    }
]);