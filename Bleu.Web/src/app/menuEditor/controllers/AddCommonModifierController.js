﻿angular.module('bleu.menuEditor').controller('AddCommonModifierController', [
    '$scope',
    '$state',
    'CommonModifiersService',
    'CommonModifiersGroupsService',
    'TaxesSharedService',
    'MenuEditorViewService',
    'CommonModifierViewModel',
    'FileUploadHelper',
    'PathesOfDefaultImages',
    function (
        $scope,
        $state,
        commonModifiersService,
        commonModifiersGroupsService,
        taxesSharedService,
        menuEditorViewService,
        commonModifierViewModel,
        fileUploadHelper,
        pathesOfDefaultImages) {
        var commonModifiersGroups = [],
            commonModifier = new commonModifierViewModel(),
            uoms = menuEditorViewService.getUoms(),
            fileUpload = new fileUploadHelper({ placeholder: pathesOfDefaultImages.placeholder }),
            init = function () {
                commonModifiersGroupsService.getAllCommonModifiersGroups().then(function (groups) {
                    $scope.commonModifiersGroups = groups;
                });
            },
            getTaxes = function (query) {
                return taxesSharedService.getTaxesByName(query);
            },
            onSuccessImageUpload = function (imageId) {
                $scope.commonModifier.imageId = imageId;
            },
            backToCommonModifiersList = function () {
                $state.goBack('root.menuEditorCommonModifiersList');
            },
            saveCommonModifier = function () {
                $scope.commonModifier.$validate().then(function () {
                    fileUpload.addPhoto(onSuccessImageUpload).then(function () {
                        commonModifiersService.addCommonModifier($scope.commonModifier).then(function () {
                            backToCommonModifiersList();
                        });
                    });
                });
            },
            cancel = function () {
                backToCommonModifiersList();
            };

        $scope.commonModifier = commonModifier;
        $scope.commonModifiersGroups = commonModifiersGroups;
        $scope.uoms = uoms;
        $scope.fileUpload = fileUpload;
        $scope.getTaxes = getTaxes;
        $scope.saveCommonModifier = saveCommonModifier;
        $scope.cancel = cancel;

        init();
    }
]);