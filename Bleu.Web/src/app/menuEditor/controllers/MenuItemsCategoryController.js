﻿angular.module('bleu.menuEditor').controller('MenuItemsCategoryController', [
    '$scope',
    '$state',
    '$stateParams',
    'MenuItemsCategoriesService',
    'StoresSharedService',
    'MenuEditorViewService',
    'MenuItemsCategorySharedViewModel',
    'FileUploadHelper',
    'PathesOfDefaultImages',
    'ImageSizeType',
    function (
        $scope,
        $state,
        $stateParams,
        menuItemsCategoriesService,
        storesSharedService,
        menuEditorViewService,
        menuItemsCategorySharedViewModel,
        fileUploadHelper,
        pathesOfDefaultImages,
        imageSizeType) {
        var
            menuItemsCategory = new menuItemsCategorySharedViewModel(),
            fileUpload = new fileUploadHelper({ placeholder: pathesOfDefaultImages.placeholder }),
            menuItemsSortings = menuEditorViewService.getMenuSortingTypes(),
            getStores = function (query) {
                return storesSharedService.getStoresByName(query);
            },
            onSuccessImageUpload = function (imageId) {
                $scope.menuItemsCategory.imageId = imageId;
            },
            backToMenuCategoriesList = function () {
                $state.goBack('root.menuEditorMenuItemsCategoriesList');
            },
            updateMenuItemsCategory = function () {
                $scope.menuItemsCategory.$validate().then(function () {
                    fileUpload.addPhoto(onSuccessImageUpload).then(function () {
                        menuItemsCategoriesService.updateMenuItemsCategory($scope.menuItemsCategory).then(function () {
                            backToMenuCategoriesList();
                        });
                    });
                });
            },
            cancel = function () {
                backToMenuCategoriesList();
            },
            init = function () {
                menuItemsCategoriesService.getMenuItemsCategory($stateParams.id).then(function (category) {
                    fileUpload.updatePlaceholder(category.imageId, imageSizeType.Medium);
                    $scope.menuItemsCategory = category;
                });
            };

        $scope.menuItemsCategory = menuItemsCategory;
        $scope.menuItemsSortings = menuItemsSortings;
        $scope.fileUpload = fileUpload;
        $scope.getStores = getStores;
        $scope.updateMenuItemsCategory = updateMenuItemsCategory;
        $scope.cancel = cancel;

        init();
    }
]);