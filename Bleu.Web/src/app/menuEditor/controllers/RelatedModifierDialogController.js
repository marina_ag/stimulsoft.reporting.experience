﻿angular.module('bleu.customers').controller('RelatedModifierDialogController', [
    '$scope',
    '$modalInstance',
    'SessionService',
    'FileUploadHelper',
    'PathesOfDefaultImages',
    'ImageSizeType',
    function (
        $scope,
        $modalInstance,
        sessionService,
        fileUploadHelper,
        pathesOfDefaultImages,
        imageSizeType) {
        var
            relatedModifier = sessionService.get('relatedModifierViewModel'),
            relatedModifierImageUploader = new fileUploadHelper({ placeholder: pathesOfDefaultImages.placeholder }),
            onSuccessImageUpload = function (imageId) {
                $scope.relatedModifier.imageId = imageId;
            },
            cancel = function () {
                $modalInstance.dismiss();
            },
            ok = function () {
                $scope.relatedModifier.$validate().then(function () {
                    relatedModifierImageUploader.addPhoto(onSuccessImageUpload).then(function () {
                        sessionService.set('relatedModifierViewModel', $scope.relatedModifier, true);
                        $modalInstance.close();
                    });
                });
            },
            init = function () {
                relatedModifierImageUploader.updatePlaceholder($scope.relatedModifier.imageId, imageSizeType.Medium);
            };

        $scope.relatedModifierImageUploader = relatedModifierImageUploader;
        $scope.relatedModifier = relatedModifier;
        $scope.cancel = cancel;
        $scope.ok = ok;

        init();
    }
]);