﻿angular.module('bleu.menuEditor').controller('EditCommonModifiersPositionsController', [
    '$scope',
    '$state',
    'SessionService',
    'CommonModifiersService',
    'CommonModifiersGroupsService',
    'MenuEditorViewService',
    'EditCommonModifiersPositionsPageViewModel',
    'ConfirmEditCommonModifiersPositionsMsgBoxModel',
    'MessageBoxHelper',
    'SortableHelper',
    function (
        $scope,
        $state,
        sessionService,
        commonModifiersService,
        commonModifiersGroupsService,
        menuEditorViewService,
        editCommonModifiersPositionsPageViewModel,
        confirmEditCommonModifiersPositionsMsgBoxModel,
        messageBoxHelper,
        sortableHelper) {
        var
            groups = [],
            viewModel = sessionService.set('editCommonModifiersPositionsPageViewModel', new editCommonModifiersPositionsPageViewModel()),
            updateViewModelState = function (positionChanged) {
                viewModel.canSave = positionChanged;
                viewModel.canReset = positionChanged;
            },
            sortable = new sortableHelper({ positionFieldName: 'position', onPositionChanged: updateViewModelState }),
            savePositions = function () {
                commonModifiersService.updateCommonModifiersPositions(viewModel.groupId, sortable.getItems()).then(function () {
                    sortable.save();
                    backToCommonModifiersList();
                });
            },
            confirmSave = function (action) {
                if (sortable.isPositionChanged()) {
                    messageBoxHelper.openModal(new confirmEditCommonModifiersPositionsMsgBoxModel()).then(function () {
                        savePositions();
                    }).finally(function () {
                        action();
                    });
                } else {
                    action();
                }
            },
            backToCommonModifiersList = function () {
                $state.goBack('root.menuEditorCommonModifiersList');
            },
            cancel = function () {
                confirmSave(backToCommonModifiersList);
            },
            getCommonModifiersByGroup = function () {
                commonModifiersGroupsService.getCommonModifiersGroupByIdOrFirst(viewModel.groupId).then(function (commonModifiersGroup) {
                    if (commonModifiersGroup != null) {
                        viewModel.groupId = commonModifiersGroup.id;
                        menuEditorViewService.getCommonModifiersByGroup(commonModifiersGroup).then(function (modifiers) {
                            sortable.setItems(modifiers);
                        });
                    }
                });
            },
            changeGroup = function () {
                confirmSave(getCommonModifiersByGroup);
            },
            reset = function () {
                sortable.resetItems();
            },
            init = function () {
                commonModifiersGroupsService.getAllCommonModifiersGroups().then(function (commonModifiersGroups) {
                    $scope.groups = commonModifiersGroups;
                    getCommonModifiersByGroup();
                });
            };

        $scope.groups = groups;
        $scope.viewModel = viewModel;
        $scope.sortable = sortable;
        $scope.changeGroup = changeGroup;
        $scope.savePositions = savePositions;
        $scope.cancel = cancel;
        $scope.reset = reset;

        init();
    }
]);