﻿angular.module('bleu.menuEditor').controller('EditCommonModifiersGroupsPositionsController', [
    '$scope',
    '$state',
    'CommonModifiersGroupsService',
    'EditCommonModifiersGroupsPositionsPageViewModel',
    'ConfirmEditCommonModifiersGroupsPositionsMsgBoxModel',
    'MessageBoxHelper',
    'SortableHelper',
    function (
        $scope,
        $state,
        commonModifiersGroupsService,
        editCommonModifiersGroupsPositionsPageViewModel,
        confirmEditCommonModifiersGroupsPositionsMsgBoxModel,
        messageBoxHelper,
        sortableHelper) {
        var viewModel = new editCommonModifiersGroupsPositionsPageViewModel(),
            updateViewModelState = function (positionChanged) {
                viewModel.canSave = positionChanged;
                viewModel.canReset = positionChanged;
            },
            sortable = new sortableHelper({ positionFieldName: 'position', onPositionChanged: updateViewModelState }),
            savePositions = function () {
                commonModifiersGroupsService.updateCommonModifierGroupsPositions(sortable.getItems()).then(function () {
                    sortable.save();
                    backToMenuItemsList();
                });
            },
            confirmSave = function (action) {
                if (sortable.isPositionChanged()) {
                    messageBoxHelper.openModal(new confirmEditCommonModifiersGroupsPositionsMsgBoxModel()).then(function () {
                        savePositions();
                    }).finally(function () {
                        action();
                    });
                } else {
                    action();
                }
            },
            backToMenuItemsList = function () {
                $state.goBack('root.menuEditorCommonModifiersGroupsList');
            },
            cancel = function () {
                confirmSave(backToMenuItemsList);
            },
            reset = function () {
                sortable.resetItems();
            },
            init = function () {
                commonModifiersGroupsService.getAllCommonModifiersGroups().then(function (groups) {
                    sortable.setItems(groups);
                });
            };

        $scope.viewModel = viewModel;
        $scope.sortable = sortable;
        $scope.savePositions = savePositions;
        $scope.cancel = cancel;
        $scope.reset = reset;

        init();
    }
]);