﻿angular.module('bleu.menuEditor').controller('MenuItemsCategoriesListController', [
    '$scope',
    '$state',
    'MenuItemsCategoriesService',
    'MenuItemsCategoriesSharedService',
    'MenuEditorViewService',
    'SessionService',
    'MenuItemsCategoriesGridOptionsModel',
    'MenuItemsCategoriesListPageViewModel',
    'MenuItemsListPageViewModel',
    'RemoveMenuItemsCategoriesMsgBoxModel',
    'ClientGridDataHelper',
    'MessageBoxHelper',
    'FilterCompareMethod',
    'FilterRelationMethod',
    function (
        $scope,
        $state,
        menuItemsCategoriesService,
        menuItemsCategoriesSharedService,
        menuEditorViewService,
        sessionService,
        menuItemsCategoriesGridOptionsModel,
        menuItemsCategoriesListPageViewModel,
        menuItemsListPageViewModel,
        removeMenuItemsCategoriesMsgBoxModel,
        clientGridDataHelper,
        messageBoxHelper,
        filterCompareMethod,
        filterRelationMethod) {
        var
            gridOptions = new menuItemsCategoriesGridOptionsModel(),
            viewModel = new menuItemsCategoriesListPageViewModel(),
            gridHelper = new clientGridDataHelper(gridOptions),
            onSelectionChanged = function (selection) {
                var hasSelection = selection.length != 0;
                viewModel.canDelete = hasSelection;
            },
            getMenuItemsCategoriesIds = function () {
                return gridHelper.getSelection().map(function (item) {
                    return item.id;
                });
            },
            filterChange = function () {
                gridHelper.updateFilters([
                    { Selector: 'name', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.And }
                ]);
            },
            addMenuItemsCategory = function () {
                $state.go('root.menuEditorAddMenuItemsCategory');
            },
            initMenuItemsListPageViewModel = function () {
                return sessionService.set('menuItemsListPageViewModel', new menuItemsListPageViewModel(), true);
            },
            removeMenuItemsCategories = function () {
                messageBoxHelper.openModal(new removeMenuItemsCategoriesMsgBoxModel()).then(function () {
                    menuItemsCategoriesService.removeMenuItemsCategories(getMenuItemsCategoriesIds()).then(function () {
                        initMenuItemsListPageViewModel();
                        gridHelper.reload();
                    });
                });
            },
            setActiveState = function (id, isActive) {
                menuItemsCategoriesService.setActiveState(id, isActive).then(function () {
                    gridHelper.reload();
                });
            },
            editPositions = function () {
                $state.go('root.menuEditorMenuItemsCategoriesPositions');
            },
            navigateToMenuItems = function (categoryId) {
                var customersListGridFilter = initMenuItemsListPageViewModel();
                customersListGridFilter.categoryFilter = categoryId;
                $state.go('root.menuEditorMenuItemsList');
            },
            init = function () {
                gridHelper.init({
                    grid: $scope,
                    getDataMethod: function () {
                        return menuItemsCategoriesSharedService.getAllMenuItemsCategories().then(function (categories) {
                            viewModel.canEditPositions = categories.length != 0;
                            return categories;
                        });
                    },
                    onSelectionChanged: onSelectionChanged
                });
            };

        $scope.gridOptions = gridOptions;
        $scope.filterChange = filterChange;
        $scope.viewModel = viewModel;
        $scope.addMenuItemsCategory = addMenuItemsCategory;
        $scope.removeMenuItemsCategories = removeMenuItemsCategories;
        $scope.setActiveState = setActiveState;
        $scope.editPositions = editPositions;
        $scope.navigateToMenuItems = navigateToMenuItems;

        init();
    }
]);