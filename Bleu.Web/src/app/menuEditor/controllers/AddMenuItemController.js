﻿angular.module('bleu.menuEditor').controller('AddMenuItemController', [
    '$scope',
    '$state',
    '$timeout',
    'MenuItemsService',
    'MenuItemsCategoriesSharedService',
    'TaxesSharedService',
    'MenuEditorViewService',
    'MenuItemViewModel',
    'RelatedModifierViewModel',
    'FileUploadHelper',
    'PathesOfDefaultImages',
    function (
        $scope,
        $state,
        $timeout,
        menuItemsService,
        menuItemsCategoriesSharedService,
        taxesSharedService,
        menuEditorViewService,
        menuItemViewModel,
        relatedModifierViewModel,
        fileUploadHelper,
        pathesOfDefaultImages) {
        var menuItem = new menuItemViewModel(),
            relatedModifier = new relatedModifierViewModel(),
            categories = [],
            uoms = menuEditorViewService.getUoms(),
            menuItemImageUploader = new fileUploadHelper({ placeholder: pathesOfDefaultImages.placeholder }),
            relatedModifierImageUploader = new fileUploadHelper({ placeholder: pathesOfDefaultImages.placeholder }),
            getTaxes = function (query) {
                return taxesSharedService.getTaxesByName(query);
            },
            onSuccessMenuItemImageUpload = function (imageId) {
                $scope.menuItem.imageId = imageId;
            },
            backToMenuItemsList = function () {
                $state.goBack('root.menuEditorMenuItemsList');
            },
            saveMenuItem = function () {
                $scope.menuItem.$validate().then(function () {
                    menuItemImageUploader.addPhoto(onSuccessMenuItemImageUpload).then(function () {
                        menuItemsService.addMenuItem($scope.menuItem).then(function () {
                            backToMenuItemsList();
                        });
                    });
                });
            },
            cancel = function () {
                backToMenuItemsList();
            },
            onSuccessRelatedModifierImageUpload = function (imageId) {
                $scope.relatedModifier.imageId = imageId;
            },
            addRelatedModifier = function () {
                $scope.relatedModifier.$validate().then(function () {
                    relatedModifierImageUploader.addPhoto(onSuccessRelatedModifierImageUpload).then(function () {
                        $scope.relatedModifier.uom = $scope.menuItem.uom; //todo fix it
                        $scope.menuItem.relatedModifiers.push($scope.relatedModifier);
                        $scope.relatedModifier = new relatedModifierViewModel();
                        $scope.relatedModifier.$clear();
                        relatedModifierImageUploader.clear();
                    });
                });
            },
            init = function () {
                menuItemsCategoriesSharedService.getAllMenuItemsCategories().then(function (menuItemsCategories) {
                    $scope.categories = menuItemsCategories;
                });
            };

        $scope.menuItem = menuItem;
        $scope.relatedModifier = relatedModifier;
        $scope.categories = categories;
        $scope.menuItemImageUploader = menuItemImageUploader;
        $scope.relatedModifierImageUploader = relatedModifierImageUploader;
        $scope.uoms = uoms;
        $scope.getTaxes = getTaxes;
        $scope.saveMenuItem = saveMenuItem;
        $scope.cancel = cancel;
        $scope.addRelatedModifier = addRelatedModifier;

        init();
    }
]);