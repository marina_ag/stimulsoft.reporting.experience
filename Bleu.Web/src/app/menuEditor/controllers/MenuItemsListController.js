﻿angular.module('bleu.menuEditor').controller('MenuItemsListController', [
    '$scope',
    '$state',
    'SessionService',
    'MenuItemsService',
    'MenuItemsCategoriesSharedService',
    'MenuItemsGridOptionsModel',
    'MenuItemsListGridDataModel',
    'MenuItemsListPageViewModel',
    'RemoveMenuItemsMsgBoxModel',
    'EditMenuItemsPositionsPageViewModel',
    'GridDataHelper',
    'MessageBoxHelper',
    'FilterCompareMethod',
    'FilterRelationMethod',
    function (
        $scope,
        $state,
        sessionService,
        menuItemsService,
        menuItemsCategoriesSharedService,
        menuItemsGridOptionsModel,
        menuItemsListGridDataModel,
        menuItemsListPageViewModel,
        removeMenuItemsMsgBoxModel,
        editMenuItemsPositionsPageViewModel,
        gridDataHelper,
        messageBoxHelper,
        filterCompareMethod,
        filterRelationMethod) {
        var categories = [],
            gridOptions = new menuItemsGridOptionsModel(),
            viewModel = sessionService.set('menuItemsListPageViewModel', new menuItemsListPageViewModel()),
            gridHelper = new gridDataHelper(gridOptions),
            onSelectionChanged = function (selection) {
                var hasSelection = selection.length != 0;
                viewModel.canDelete = hasSelection;
                viewModel.canDuplicate = selection.length == 1;
            },
            getMenuItemsIds = function () {
                return gridHelper.getSelection().map(function (item) {
                    return item.id;
                });
            },
            getFilters = function () {
                return [
                    { Selector: 'CategoryId', Method: filterCompareMethod.Equals, Value: viewModel.categoryFilter, Relation: filterRelationMethod.And },
                    { Selector: 'Name', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.Or },
                  //  { Selector: 'UPC', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.Or },
                  // { Selector: 'SKU', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.And }
                ];
            },
            filterChange = function () {
                gridHelper.updateFilters(getFilters());
            },
            addMenuItem = function () {
                $state.go('root.menuEditorAddMenuItem');
            },
            duplicateMenuItem = function () {
                var ids = getMenuItemsIds();

                if (ids.length > 0) {
                    menuItemsService.duplicateMenuItem(ids[0]).then(function () {
                        gridHelper.reload();
                    });
                }
            },
            removeMenuItems = function () {
                messageBoxHelper.openModal(new removeMenuItemsMsgBoxModel()).then(function () {
                    menuItemsService.removeMenuItems(getMenuItemsIds()).then(function () {
                        gridHelper.reload();
                    });
                });
            },
            setActiveState = function (id, isActive) {
                menuItemsService.setActiveState(id, isActive).then(function () {
                    gridHelper.reload();
                });
            },
            getCategoryName = function (id) {
                var filteredCategories = $scope.categories.filter(function (category) {
                    return category.id == id;
                });
                if (filteredCategories.length == 0) return '';
                return filteredCategories[0].name;
            },
            editPositions = function () {
                var editMenuItemsPositions = sessionService.set('editMenuItemsPositionsPageViewModel', new editMenuItemsPositionsPageViewModel(), true);
                editMenuItemsPositions.categoryId = viewModel.categoryFilter;
                $state.go('root.menuEditorEditMenuItemsPositions');
            },
            init = function () {
                menuItemsCategoriesSharedService.getAllMenuItemsCategories().then(function (menuItemsCategories) {
                    $scope.categories = menuItemsCategories;
                    viewModel.canEditPositions = menuItemsCategories.length != 0;
                });

                gridHelper.init({
                    fields: menuItemsListGridDataModel.fields,
                    fieldsForSelectAll: menuItemsListGridDataModel.fieldsForSelectAll,
                    primaryFields: menuItemsListGridDataModel.primaryFields,
                    sortMapping: menuItemsListGridDataModel.sortMapping,
                    grid: $scope,
                    getDataMethod: menuItemsService.getMenuItems,
                    onSelectionChanged: onSelectionChanged,
                    initFilters: getFilters()
                });
            };

        $scope.getCategoryName = getCategoryName;
        $scope.categories = categories;
        $scope.gridOptions = gridOptions;
        $scope.filterChange = filterChange;
        $scope.viewModel = viewModel;
        $scope.addMenuItem = addMenuItem;
        $scope.duplicateMenuItem = duplicateMenuItem;
        $scope.removeMenuItems = removeMenuItems;
        $scope.setActiveState = setActiveState;
        $scope.editPositions = editPositions;

        init();
    }
]);