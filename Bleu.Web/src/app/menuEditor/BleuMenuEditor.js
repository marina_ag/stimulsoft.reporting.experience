﻿angular.module('bleu.menuEditor', [
    'core.menuEditor',
    'bleu.menuEditor.shared',
    'bleu.stores.shared',
    'bleu.taxes.shared',
    'bleu.main',
    'ui.router'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {

    $stateProvider
        .state({
            name: 'root.menuEditorMenuItemsList',
            url: '/menuitems',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/MenuItemsListView.html',
                    controller: 'MenuItemsListController'
                }
            },
            menu: {
                section: menuSections.itemsList,
                item: 'Main.LeftMenu_MenuItemItems',
                order: 10
            }
        })
        .state({
            name: 'root.menuEditorAddMenuItem',
            url: '/addmenuitem',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/AddMenuItemView.html',
                    controller: 'AddMenuItemController'
                }
            }
        })
        .state({
            name: 'root.menuEditorMenuItem',
            url: '/menuitem/:id',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/MenuItemView.html',
                    controller: 'MenuItemController'
                }
            }
        })
        .state({
            name: 'root.menuEditorEditMenuItemsPositions',
            url: '/editmenuitemspositions',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/EditMenuItemsPositionsView.html',
                    controller: 'EditMenuItemsPositionsController'
                }
            }
        })
        .state({
            name: 'root.menuEditorMenuItemsCategoriesList',
            url: '/menuitemscategories',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/MenuItemsCategoriesListView.html',
                    controller: 'MenuItemsCategoriesListController'
                }
            },
            menu: {
                section: menuSections.itemsList,
                item: 'Main.LeftMenu_MenuItemCategories',
                order: 20
            }
        })
        .state({
            name: 'root.menuEditorAddMenuItemsCategory',
            url: '/addmenuitemscategory',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/AddMenuItemsCategoryView.html',
                    controller: 'AddMenuItemsCategoryController'
                }
            }
        })
        .state({
            name: 'root.menuEditorMenuItemsCategory',
            url: '/menuitemscategory/:id',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/MenuItemsCategoryView.html',
                    controller: 'MenuItemsCategoryController'
                }
            }
        })
        .state({
            name: 'root.menuEditorMenuItemsCategoriesPositions',
            url: '/editmenuitemscategoriespositions',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/EditMenuItemsCategoriesPositionsView.html',
                    controller: 'EditMenuItemsCategoriesPositionsController'
                }
            }
        })
        .state({
            name: 'root.menuEditorCommonModifiersGroupsList',
            url: '/commonmodifiersgroups',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/CommonModifiersGroupsListView.html',
                    controller: 'CommonModifiersGroupsListController'
                }
            },
            menu: {
                section: menuSections.itemsList,
                item: 'Main.LeftMenu_CommonModifiersGroups',
                order: 30
            }
        })
        .state({
            name: 'root.menuEditorAddCommonModifiersGroup',
            url: '/addcommonmodifiersgroup',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/AddCommonModifiersGroupView.html',
                    controller: 'AddCommonModifiersGroupController'
                }
            }
        })
        .state({
            name: 'root.menuEditorCommonModifiersGroup',
            url: '/commonmodifiersgroup/:id',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/CommonModifiersGroupView.html',
                    controller: 'CommonModifiersGroupController'
                }
            }
        })
        .state({
            name: 'root.menuEditorCommonModifiersGroupsPositions',
            url: '/commonmodifiersgroupspositions',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/EditCommonModifiersGroupsPositionsView.html',
                    controller: 'EditCommonModifiersGroupsPositionsController'
                }
            }
        })
        .state({
            name: 'root.menuEditorCommonModifiersList',
            url: '/commonmodifiers',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/CommonModifiersListView.html',
                    controller: 'CommonModifiersListController'
                }
            },
            menu: {
                section: menuSections.itemsList,
                item: 'Main.LeftMenu_CommonModifiers',
                order: 40
            }
        })
        .state({
            name: 'root.menuEditorAddCommonModifier',
            url: '/addcommonmodifier',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/AddCommonModifierView.html',
                    controller: 'AddCommonModifierController'
                }
            }
        })
        .state({
            name: 'root.menuEditorCommonModifier',
            url: '/commonmodifier/:id',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/CommonModifierView.html',
                    controller: 'CommonModifierController'
                }
            }
        })
        .state({
            name: 'root.menuEditorCommonModifiersPositions',
            url: '/commonmodifierspositions',
            views: {
                'content-view': {
                    templateUrl: 'src/app/menuEditor/views/EditCommonModifiersPositionsView.html',
                    controller: 'EditCommonModifiersPositionsController'
                }
            }
        });
}]).run();