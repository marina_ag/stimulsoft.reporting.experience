﻿angular.module('bleu.menuEditor').factory('MenuItemsCategoriesService', [
    'MenuCategoriesTransport',
    'MenuItemsCategoriesSharedService',
    'MenuItemsCategoriesSharedMapper',
    'MenuItemsCategoriesPositionMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (
        menuCategoriesTransport,
        menuItemsCategoriesSharedService,
        menuItemsCategoriesSharedMapper,
        menuItemsCategoriesPositionMapper,
        requestHandlerHelper,
        cacheService) {
        var
            getMenuItemsCategoryByIdOrFirst = function (categoryId) {
                return menuItemsCategoriesSharedService.getAllMenuItemsCategories().then(function (categories) {
                    if (categories.length == 0) return null;
                    if (categoryId == null) {
                        return categories[0];
                    } else {
                        var filteredCategories = categories.filter(function (item) {
                            return item.id == categoryId;
                        });

                        if (filteredCategories.length == 0) return null;

                        return filteredCategories[0];
                    }
                });
            },
            getMenuItemsCategory = function (id) {
                return requestHandlerHelper.handleRequest(menuCategoriesTransport.getMenuCategory(id)).then(function (response) {
                    return menuItemsCategoriesSharedMapper.mapInstanceToClient(response);
                });
            },
            updateMenuItemsCategory = function (menuItemsCategory) {
                return requestHandlerHelper.handleRequest(menuCategoriesTransport.updateMenuCategory(menuItemsCategoriesSharedMapper.mapInstanceToServer(menuItemsCategory))).then(function () {
                    cacheService.clear('menuItemsCategories');
                });
            },
            addMenuItemsCategory = function (menuItemsCategory) {
                return requestHandlerHelper.handleRequest(menuCategoriesTransport.addMenuCategory(menuItemsCategoriesSharedMapper.mapInstanceToServer(menuItemsCategory))).then(function (categoryId) {
                    cacheService.clear('menuItemsCategories');
                    return categoryId;
                });
            },
            removeMenuItemsCategories = function (menuItemsCategoriesIds) {
                return requestHandlerHelper.handleRequest(menuCategoriesTransport.removeMenuCategories(menuItemsCategoriesIds)).then(function () {
                    cacheService.clear('menuItemsCategories');
                });
            },
            setActiveState = function (id, isActive) {
                return getMenuItemsCategory(id).then(function (menuItemsCategory) {
                    menuItemsCategory.isActive = isActive;
                    return updateMenuItemsCategory(menuItemsCategory);
                });
            },
            updateMenuItemsCategoriesPositions = function (menuCategories) {
                return requestHandlerHelper.handleRequest(menuCategoriesTransport.updateMenuCategoriesPositions(menuItemsCategoriesPositionMapper.mapCollectionToServer(menuCategories))).then(function () {
                    cacheService.clear('menuItemsCategories');
                });
            };

        return {
            getMenuItemsCategory: getMenuItemsCategory,
            updateMenuItemsCategory: updateMenuItemsCategory,
            addMenuItemsCategory: addMenuItemsCategory,
            removeMenuItemsCategories: removeMenuItemsCategories,
            setActiveState: setActiveState,
            updateMenuItemsCategoriesPositions: updateMenuItemsCategoriesPositions,
            getMenuItemsCategoryByIdOrFirst: getMenuItemsCategoryByIdOrFirst
        }
    }
])