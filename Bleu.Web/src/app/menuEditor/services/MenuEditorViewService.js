﻿angular.module('bleu.menuEditor').factory('MenuEditorViewService', [
    '$q',
    'LocalizationService',
    'MenuItemsService',
    'CommonModifiersService',
    'UOM',
    'MenuSortingType',
    'FilterCompareMethod',
    'FilterRelationMethod',
    function (
        $q,
        localizationService,
        menuItemsService,
        commonModifiersService,
        uom,
        menuSortingType,
        filterCompareMethod,
        filterRelationMethod) {
        var
            uoms = [
                { id: uom.Oz, name: localizationService.getLocalizedString('MenuEditor.UOM_Oz') },
                { id: uom.Ct, name: localizationService.getLocalizedString('MenuEditor.UOM_Ct') },
                { id: uom.Lb, name: localizationService.getLocalizedString('MenuEditor.UOM_Lb') },
                { id: uom.Ml, name: localizationService.getLocalizedString('MenuEditor.UOM_Ml') },
                { id: uom.Ea, name: localizationService.getLocalizedString('MenuEditor.UOM_Ea') },
                { id: uom.Gr, name: localizationService.getLocalizedString('MenuEditor.UOM_Gr') },
                { id: uom.Ltr, name: localizationService.getLocalizedString('MenuEditor.UOM_Ltr') },
                { id: uom.Gal, name: localizationService.getLocalizedString('MenuEditor.UOM_Gal') },
                { id: uom.Qt, name: localizationService.getLocalizedString('MenuEditor.UOM_Qt') },
                { id: uom.Pt, name: localizationService.getLocalizedString('MenuEditor.UOM_Pt') },
                { id: uom.Yd, name: localizationService.getLocalizedString('MenuEditor.UOM_Yd') },
                { id: uom.Grm, name: localizationService.getLocalizedString('MenuEditor.UOM_Grm') },
                { id: uom.Doz, name: localizationService.getLocalizedString('MenuEditor.UOM_Doz') },
                { id: uom.Kg, name: localizationService.getLocalizedString('MenuEditor.UOM_Kg') },
                { id: uom.Sqft, name: localizationService.getLocalizedString('MenuEditor.UOM_Sqft') },
                { id: uom.Sqyd, name: localizationService.getLocalizedString('MenuEditor.UOM_Sqyd') },
                { id: uom.Mm, name: localizationService.getLocalizedString('MenuEditor.UOM_Mm') },
                { id: uom.Cm, name: localizationService.getLocalizedString('MenuEditor.UOM_Cm') },
                { id: uom.In, name: localizationService.getLocalizedString('MenuEditor.UOM_In') },
                { id: uom.Ft, name: localizationService.getLocalizedString('MenuEditor.UOM_Ft') },
                { id: uom.Pcs, name: localizationService.getLocalizedString('MenuEditor.UOM_Pcs') }
            ],
            menuSortingTypes = [
                { id: menuSortingType.Custom, name: localizationService.getLocalizedString('MenuEditor.MenuSortingType_Custom') },
                { id: menuSortingType.PriceAsc, name: localizationService.getLocalizedString('MenuEditor.MenuSortingType_PriceAsc') },
                { id: menuSortingType.PriceDesc, name: localizationService.getLocalizedString('MenuEditor.MenuSortingType_PriceDesc') },
                { id: menuSortingType.NameAsc, name: localizationService.getLocalizedString('MenuEditor.MenuSortingType_NameAsc') },
                { id: menuSortingType.NameDesc, name: localizationService.getLocalizedString('MenuEditor.MenuSortingType_NameDesc') }
            ],
            getUoms = function () {
                return uoms;
            },
            getMenuSortingTypes = function () {
                return menuSortingTypes;
            },
            getSortFilter = function (sorting) {
                switch (sorting) {
                    case menuSortingType.Custom:
                        return { Selector: "DisplayOrder", Desc: false };
                    case menuSortingType.PriceAsc:
                        return { Selector: "Price", Desc: false };
                    case menuSortingType.PriceDesc:
                        return { Selector: "Price", Desc: true };
                    case menuSortingType.NameAsc:
                        return { Selector: "Name", Desc: false };
                    case menuSortingType.NameDesc:
                        return { Selector: "Name", Desc: true };
                    default:
                        throw new Error("Unknown menu sorting type");
                }
            },
            getMenuItemsByCategory = function (menuItemCategory) {
                var sortFilter = getSortFilter(menuItemCategory.sorting),
                    filter = {
                        Fields: ['MenuItemId', 'ImageId', 'Languages'],
                        Filters: [{ Selector: "CategoryId", Value: menuItemCategory.id, Method: filterCompareMethod.Equals, Relation: filterRelationMethod.And }],
                        SortFilters: [sortFilter]
                    };

                return menuItemsService.getMenuItems(filter).then(function (menuItems) {
                    return menuItems.Results;
                });
            },
            getCommonModifiersByGroup = function (commonModifiersGroup) {
                var sortFilter = getSortFilter(commonModifiersGroup.sorting),
                    filter = {
                        Fields: ['ModifierId', 'ImageId', 'Languages'],
                        Filters: [{ Selector: "GroupId", Value: commonModifiersGroup.id, Method: filterCompareMethod.Equals, Relation: filterRelationMethod.And }],
                        SortFilters: [sortFilter]
                    };

                return commonModifiersService.getCommonModifiers(filter).then(function (modifiers) {
                    return modifiers.Results;
                });
            };

        return {
            getUoms: getUoms,
            getMenuSortingTypes: getMenuSortingTypes,
            getMenuItemsByCategory: getMenuItemsByCategory,
            getCommonModifiersByGroup: getCommonModifiersByGroup
        }
    }
]);