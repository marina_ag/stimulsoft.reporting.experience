﻿angular.module('bleu.menuEditor').factory('CommonModifiersGroupsService', [
    '$q',
    'CommonModifierGroupsTransport',
    'CommonModifiersGroupsMapper',
    'CommonModifiersGroupsPositionMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (
        $q,
        commonModifierGroupsTransport,
        commonModifiersGroupsMapper,
        commonModifiersGroupsPositionMapper,
        requestHandlerHelper,
        cacheService) {
        var
            getCommonModifiersGroups = function () {
                return commonModifierGroupsTransport.getAllCommonModifierGroups().then(function (response) {
                    return commonModifiersGroupsMapper.mapCollectionToClient(response);
                });
            },
            getAllCommonModifiersGroups = function () {
                return requestHandlerHelper.handleRequest(cacheService.cache('commonModifiersGroups', getCommonModifiersGroups));
            },
            getCommonModifiersGroupByIdOrFirst = function (groupId) {
                return getAllCommonModifiersGroups().then(function (groups) {
                    if (groups.length == 0) return null;
                    if (groupId == null) {
                        return groups[0];
                    } else {
                        var filteredGroups = groups.filter(function (item) {
                            return item.id == groupId;
                        });

                        if (filteredGroups.length == 0) return null;

                        return filteredGroups[0];
                    }
                });
            },
            getCommonModifiersGroup = function (id) {
                return requestHandlerHelper.handleRequest(commonModifierGroupsTransport.getCommonModifierGroup(id)).then(function (response) {
                    return commonModifiersGroupsMapper.mapInstanceToClient(response);
                });
            },
            updateCommonModifiersGroup = function (commonModifiersGroup) {
                return requestHandlerHelper.handleRequest(commonModifierGroupsTransport.updateCommonModifierGroup(commonModifiersGroupsMapper.mapInstanceToServer(commonModifiersGroup))).then(function () {
                    cacheService.clear('commonModifiersGroups');
                });
            },
            addCommonModifiersGroup = function (commonModifiersGroup) {
                return requestHandlerHelper.handleRequest(commonModifierGroupsTransport.addCommonModifierGroup(commonModifiersGroupsMapper.mapInstanceToServer(commonModifiersGroup))).then(function (groupId) {
                    cacheService.clear('commonModifiersGroups');
                    return groupId;
                });
            },
            removeCommonModifiersGroups = function (commonModifiersGroupsIds) {
                return requestHandlerHelper.handleRequest(commonModifierGroupsTransport.removeCommonModifierGroups(commonModifiersGroupsIds)).then(function () {
                    cacheService.clear('commonModifiersGroups');
                });
            },
            setActiveState = function (id, isActive) {
                return getCommonModifiersGroup(id).then(function (commonModifiersGroup) {
                    commonModifiersGroup.isActive = isActive;
                    return updateCommonModifiersGroup(commonModifiersGroup);
                });
            },
            updateCommonModifierGroupsPositions = function (commonModifiersGroups) {
                return requestHandlerHelper.handleRequest(commonModifierGroupsTransport.updateCommonModifierGroupsPositions(commonModifiersGroupsPositionMapper.mapCollectionToServer(commonModifiersGroups))).then(function () {
                    cacheService.clear('commonModifiersGroups');
                });
            },
            addMenuItemToGroup = function (commonModifierGroupId, menuItemId) {
                return getCommonModifiersGroup(commonModifierGroupId).then(function (group) {
                    group.menuItemsIds.push(menuItemId);
                    return updateCommonModifiersGroup(group);
                });
            },
            copyCommonModifiersToMenuItem = function (menuItem) {
                var promises = [];
                menuItem.commonModifiersGroups.forEach(function (commonModifierGroup) {
                    promises.push(addMenuItemToGroup(commonModifierGroup.id, menuItem.id));
                });
                return $q.all(promises);
            };

        return {
            getAllCommonModifiersGroups: getAllCommonModifiersGroups,
            getCommonModifiersGroup: getCommonModifiersGroup,
            updateCommonModifiersGroup: updateCommonModifiersGroup,
            addCommonModifiersGroup: addCommonModifiersGroup,
            removeCommonModifiersGroups: removeCommonModifiersGroups,
            setActiveState: setActiveState,
            updateCommonModifierGroupsPositions: updateCommonModifierGroupsPositions,
            getCommonModifiersGroupByIdOrFirst: getCommonModifiersGroupByIdOrFirst,
            copyCommonModifiersToMenuItem: copyCommonModifiersToMenuItem
        }
    }
])