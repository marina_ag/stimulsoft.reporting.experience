﻿angular.module('bleu.menuEditor').factory('CommonModifiersService', [
    'CommonModifiersTransport',
    'CommonModifiersMapper',
    'CommonModifiersPositionsMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (commonModifiersTransport, commonModifiersMapper, commonModifiersPositionsMapper, requestHandlerHelper, cacheService) {
        var
            getCommonModifiers = function (filter) {
                return requestHandlerHelper.handleRequest(commonModifiersTransport.getFilteredCommonModifiers(filter)).then(function (response) {
                    return {
                        Results: commonModifiersMapper.mapCollectionToClient(response.Results),
                        Count: response.Count
                    }
                });
            },
            getCommonModifier = function (id) {
                return requestHandlerHelper.handleRequest(commonModifiersTransport.getCommonModifier(id)).then(function (response) {
                    return commonModifiersMapper.mapInstanceToClient(response);
                });
            },
            updateCommonModifier = function (commonModifier) {
                return requestHandlerHelper.handleRequest(commonModifiersTransport.updateCommonModifier(commonModifiersMapper.mapInstanceToServer(commonModifier))).then(function () {
                    cacheService.clear('commonModifiersGroups');
                });
            },
            addCommonModifier = function (commonModifier) {
                return requestHandlerHelper.handleRequest(commonModifiersTransport.addCommonModifier(commonModifiersMapper.mapInstanceToServer(commonModifier))).then(function (commonModifierId) {
                    cacheService.clear('commonModifiersGroups');
                    return commonModifierId;
                });
            },
            removeCommonModifiers = function (commonModifiersIds) {
                return requestHandlerHelper.handleRequest(commonModifiersTransport.removeCommonModifiers(commonModifiersIds));
            },
            duplicateCommonModifier = function (commonModifierId) {
                return getCommonModifier(commonModifierId).then(function (commonModifier) {
                    commonModifier.id = null;
                    commonModifier.itemId = null;
                    return addCommonModifier(commonModifier);
                });
            },
            setActiveState = function (id, isActive) {
                return getCommonModifier(id).then(function (commonModifier) {
                    commonModifier.isActive = isActive;
                    return updateCommonModifier(commonModifier);
                });
            },
            updateCommonModifiersPositions = function (groupId, commonModifiers) {
                return requestHandlerHelper.handleRequest(commonModifiersTransport.updateCommonModifiersPositions(commonModifiersPositionsMapper.mapInstanceToServer(groupId, commonModifiers)));
            };

        return {
            getCommonModifiers: getCommonModifiers,
            getCommonModifier: getCommonModifier,
            updateCommonModifier: updateCommonModifier,
            addCommonModifier: addCommonModifier,
            removeCommonModifiers: removeCommonModifiers,
            duplicateCommonModifier: duplicateCommonModifier,
            setActiveState: setActiveState,
            updateCommonModifiersPositions: updateCommonModifiersPositions
        }
    }
]);