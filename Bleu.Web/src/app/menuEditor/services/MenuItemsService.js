﻿angular.module('bleu.menuEditor').factory('MenuItemsService', [
    '$q',
    'CommonModifiersGroupsService',
    'MenuItemsTransport',
    'MenuItemsMapper',
    'MenuItemsPositionsMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (
        $q,
        commonModifiersGroupsService,
        menuItemsTransport,
        menuItemsMapper,
        menuItemsPositionsMapper,
        requestHandlerHelper,
        cacheService) {
        var
            getMenuItems = function (filter) {
                return requestHandlerHelper.handleRequest(menuItemsTransport.getFilteredMenuItems(filter)).then(function (response) {
                    return {
                        Results: menuItemsMapper.mapCollectionToClient(response.Results),
                        Count: response.Count
                    }
                });
            },
            getMenuItem = function (id) {
                return requestHandlerHelper.handleRequest(menuItemsTransport.getMenuItem(id)).then(function (response) {
                    return menuItemsMapper.mapInstanceToClient(response);
                });
            },
            updateMenuItem = function (menuItem) {
                return requestHandlerHelper.handleRequest(menuItemsTransport.updateMenuItem(menuItemsMapper.mapInstanceToServer(menuItem))).then(function () {
                    cacheService.clear('menuItemsCategories');
                });
            },
            addMenuItem = function (menuItem) {
                return requestHandlerHelper.handleRequest(menuItemsTransport.addMenuItem(menuItemsMapper.mapInstanceToServer(menuItem))).then(function (itemId) {
                    cacheService.clear('menuItemsCategories');
                    return itemId;
                });
            },
            removeMenuItems = function (menuItemsIds) {
                return requestHandlerHelper.handleRequest(menuItemsTransport.removeMenuItems(menuItemsIds)).then(function () {
                    cacheService.clear('menuItemsCategories');
                });
            },
            duplicateMenuItem = function (menuItemId) {
                return getMenuItem(menuItemId).then(function (menuItem) {
                    menuItem.id = null;
                    menuItem.itemId = null;
                    return addMenuItem(menuItem).then(function (newMenuItemId) {
                        menuItem.id = newMenuItemId;
                        return commonModifiersGroupsService.copyCommonModifiersToMenuItem(menuItem).then(function () {
                            return newMenuItemId;
                        });
                    });
                });
            },
            setActiveState = function (id, isActive) {
                return getMenuItem(id).then(function (menuItem) {
                    menuItem.isActive = isActive;
                    return updateMenuItem(menuItem);
                });
            },
            updateMenuItemsPositions = function (categoryId, menuItems) {
                return requestHandlerHelper.handleRequest(menuItemsTransport.updateMenuItemsPositions(menuItemsPositionsMapper.mapInstanceToServer(categoryId, menuItems)));
            },
            getMenuItemCommonModifiers = function (menuItem) {
                var dfd = $q.defer();
                dfd.resolve(menuItemsMapper.mapMenuItemCommonModifiers(menuItem));
                return dfd.promise;
            };

        return {
            getMenuItems: getMenuItems,
            getMenuItem: getMenuItem,
            updateMenuItem: updateMenuItem,
            addMenuItem: addMenuItem,
            removeMenuItems: removeMenuItems,
            duplicateMenuItem: duplicateMenuItem,
            setActiveState: setActiveState,
            updateMenuItemsPositions: updateMenuItemsPositions,
            getMenuItemCommonModifiers: getMenuItemCommonModifiers
        }
    }
]);