﻿angular.module('bleu.messages', [
    'bleu.main',
    'ui.router'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {

    $stateProvider
        .state({
            name: 'root.messages',
            url: '/messages',
            views: {
                'content-view': {
                    templateUrl: 'src/app/messages/views/MessagesView.html',
                    controller: 'MessagesController'
                }
            }
        });
}]).run();