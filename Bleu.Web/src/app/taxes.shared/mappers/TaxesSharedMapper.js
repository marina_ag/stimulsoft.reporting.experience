﻿angular.module('bleu.taxes.shared').factory('TaxesSharedMapper', [
    'TaxSharedViewModel',
    function (taxSharedViewModel) {
        var
            transformToClient = function (response) {
                var tax = new taxSharedViewModel();

                tax.id = response.Id;
                tax.name = response.Name;
                tax.value = response.TaxRate;
                tax.description = response.Description;

                return tax;
            },
            transformToServer = function (request) {
                return {
                    Id: request.id,
                    Name: request.name,
                    TaxRate: request.value,
                    Description: request.description
                }
            },
            mapCollectionToClient = function (response) {
                return response.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (response) {
                return transformToClient(response);
            },
            mapCollectionToServer = function (request) {
                return request.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            }

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);