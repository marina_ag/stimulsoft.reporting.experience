﻿angular.module('bleu.taxes.shared').factory('TaxSharedViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var taxSharedViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.name = '';
            this.value = 0;
            this.description = '';
        }

        taxSharedViewModel.prototype = Object.create(baseViewModel.prototype);

        return taxSharedViewModel;
    }
]);