﻿angular.module('bleu.taxes.shared').factory('TaxesSharedService', [
    'TaxesSharedTransport',
    'TaxesSharedMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (taxesSharedTransport, taxesSharedMapper, requestHandlerHelper, cacheService) {
        var
            getTaxes = function () {
                return taxesSharedTransport.getAllTaxes().then(function (response) {
                    return taxesSharedMapper.mapCollectionToClient(response);
                });
            },
            getAllTaxes = function () {
                return requestHandlerHelper.handleRequest(cacheService.cache('taxes', getTaxes));
            },
            getTaxesByName = function (key) {
                return getAllTaxes().then(function (response) {
                    return response.filter(function (item) {
                        return item.name.toLowerCase().indexOf(key.toLowerCase()) != -1;
                    });
                });
            };

        return {
            getAllTaxes: getAllTaxes,
            getTaxesByName: getTaxesByName
        }
    }
]);