﻿angular.module('bleu.customers', [
    'core.customers',
    'bleu.customers.shared',
    'bleu.stores.shared',
    'bleu.main',
    'ui.router'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {

    $stateProvider
        .state({
            name: 'root.customerslist',
            url: '/customers',
            views: {
                'content-view': {
                    templateUrl: 'src/app/customers/views/CustomersListView.html',
                    controller: 'CustomersListController'
                }
            },
            menu: {
                section: menuSections.customers,
                item: 'Main.LeftMenu_MenuItemCustomers',
                order: 10
            }
        })
        //.state({
        //    name: 'root.customersBest',
        //    url: '/bestcustomers',
        //    views: {
        //        'content-view': {
        //            templateUrl: 'src/app/customers/views/BestCustomersListView.html',
        //            controller: 'BestCustomersListController'
        //        }
        //    },
        //    menu: {
        //        section: menuSections.customers,
        //        item: 'Main.LeftMenu_MenuItemBestCustomers',
        //        order: 20
        //    }
        //})
        .state({
            name: 'root.customersGroups',
            url: '/customersgroups',
            views: {
                'content-view': {
                    templateUrl: 'src/app/customers/views/CustomersGroupsListView.html',
                    controller: 'CustomersGroupsListController'
                }
            },
            menu: {
                section: menuSections.customers,
                item: 'Main.LeftMenu_MenuItemCustomersGroups',
                order: 30
            }
        }).state({
            name: 'root.customersAddCustomer',
            url: '/addcustomer',
            views: {
                'content-view': {
                    templateUrl: 'src/app/customers/views/AddCustomerView.html',
                    controller: 'AddCustomerController'
                }
            }
        }).state({
            name: 'root.customersCustomer',
            url: '/customer/:id',
            views: {
                'content-view': {
                    templateUrl: 'src/app/customers/views/CustomerView.html',
                    controller: 'CustomerController'
                }
            }
        }).state({
            name: 'root.customersAddCustomersGroup',
            url: '/addcustomersgroup',
            views: {
                'content-view': {
                    templateUrl: 'src/app/customers/views/AddCustomersGroupView.html',
                    controller: 'AddCustomersGroupController'
                }
            }
        }).state({
            name: 'root.customersCustromersGroup',
            url: '/customersGroup/:id',
            views: {
                'content-view': {
                    templateUrl: 'src/app/customers/views/CustomersGroupView.html',
                    controller: 'CustomersGroupController'
                }
            }
        });
}]).run();