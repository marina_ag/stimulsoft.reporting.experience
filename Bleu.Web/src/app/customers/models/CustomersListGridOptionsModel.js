﻿angular.module('bleu.customers').factory('CustomersListGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var customersListGridViewModel = function () {
            this.columnDefs = [
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomersGrid_HeaderName'),
                    name: 'fullName',
                    type: 'string',
                    cellTemplate: 'src/app/customers/views/grid/CustomersListGridCustomerNameTemplate.html',
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomersGrid_HeaderLastVisit'),
                    name: 'lastVisit',
                    type: 'string',
                    enableSorting: false,
                    cellTemplate: 'src/app/customers/views/grid/CustomersListGridLastVisitTemplate.html'
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomersGrid_HeaderTotalSpent'),
                    name: 'totalSpent',
                    type: 'string',
                    cellFilter: 'currency',
                    enableSorting: false
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomersGrid_HeaderAvgSpend'),
                    name: 'avgSpend',
                    type: 'string',
                    cellFilter: 'currency',
                    enableSorting: false
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomersGrid_HeaderEmail'),
                    name: 'email',
                    type: 'string'
                }
            ];
        };

        angular.extend(customersListGridViewModel.prototype, new gridDefaultOptions());

        return customersListGridViewModel;
    }
]);