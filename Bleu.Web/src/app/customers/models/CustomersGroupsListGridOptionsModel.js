﻿angular.module('bleu.customers').factory('CustomersGroupsListGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var customersGroupsListGridOptionsModel = function () {
            this.useExternalSorting = false;
            this.columnDefs = [
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomersGroupsGrid_HeaderName'),
                    name: 'name',
                    type: 'string',
                    cellTemplate: 'src/app/customers/views/grid/CustomersGroupsListGridGroupNameTemplate.html',
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomersGrid_HeaderAuto'),
                    name: 'isAuto',
                    type: 'string',
                    width: 50,
                    cellTemplate: 'src/app/customers/views/grid/CustomersGroupsListGridGroupAutoTemplate.html',
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomersGroupsGrid_HeaderCustomersCount'),
                    name: 'customersCount',
                    type: 'string',
                    cellTemplate: 'src/app/customers/views/grid/CustomersGroupsListGridGroupCustomersCountTemplate.html'
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomersGroupsGrid_HeaderTotalSpent'),
                    name: 'totalSpent',
                    type: 'number',
                    cellFilter: 'currency'
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomersGroupsGrid_HeaderAvgSpend'),
                    name: 'avgSpend',
                    type: 'number',
                    cellFilter: 'currency'
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomersGroupsGrid_HeaderTotalOrders'),
                    name: 'totalOrders',
                    type: 'number',
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomersGroupsGrid_HeaderAvgOrders'),
                    name: 'avgOrders',
                    type: 'number',
                    cellFilter: 'currency'
                }
            ];
        };

        angular.extend(customersGroupsListGridOptionsModel.prototype, new gridDefaultOptions());

        return customersGroupsListGridOptionsModel;
    }
]);