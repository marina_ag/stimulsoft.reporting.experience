﻿angular.module('bleu.customers').factory('CustomersSummaryViewModel', [
    function () {
        var customersSummaryViewModel = function () {
            this.total = 0;
            this.avgSpend = 0;
            this.totalSpend = 0;
        };

        return customersSummaryViewModel;
    }
]);