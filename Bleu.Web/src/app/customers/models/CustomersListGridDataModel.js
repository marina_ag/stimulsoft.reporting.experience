﻿angular.module('bleu.customers').value('CustomersListGridDataModel', {
    fields: [
        'CardNo',
        'FirstName',
        'LastName',
        'Email',
        'Photo',
        'TotalSpend',
        'AvgSpend',
        'LastVisit'
    ],
    fieldsForSelectAll: ['CardNo'],
    primaryFields: ['id'],
    sortMapping: [
        { column: 'fullName', data: ['FirstName', 'LastName'] },
        { column: 'lastVisit', data: ['LastVisit'] },
        { column: 'visits', data: ['Visits'] },
        { column: 'totalSpent', data: ['TotalSpend'] },
        { column: 'avgSpend', data: ['AvgSpend'] },
        { column: 'email', data: ['Email'] }
    ]
});