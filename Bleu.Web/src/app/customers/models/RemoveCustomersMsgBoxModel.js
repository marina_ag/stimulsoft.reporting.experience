﻿angular.module('bleu.customers').factory('RemoveCustomersMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeStoresModel = function () {
            this.title = localizationService.getLocalizedString('Customers.CustomersListPageRemoveCustomersDlg_Title');
            this.message = localizationService.getLocalizedString('Customers.CustomersListPageRemoveCustomersDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('Customers.CustomersListPageRemoveCustomersDlg_BtnTextOk'),
                    value: 'ok',
                    icon: 'icon-checkmark'
                },
                {
                    text: localizationService.getLocalizedString('Customers.CustomersListPageRemoveCustomersDlg_BtnTextCancel'),
                    value: 'cancel',
                    icon: 'icon-cross'
                }
            ];
            this.successValue = 'ok';
        };

        return removeStoresModel;
    }
]);