﻿angular.module('bleu.customers').factory('RecentActivityGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var customersListGridViewModel = function () {
            this.infiniteScrollRowsFromEnd = 40;
            this.infiniteScrollUp = true;
            this.infiniteScrollDown = true;
            this.enableInfiniteScroll = true;
            this.columnDefs = [
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomerRecentActivityGrid_HeaderDateTime'),
                    name: 'dateTime',
                    type: 'string',
                    cellFilter: 'date : "short"'
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomerRecentActivityGrid_HeaderOrderNo'),
                    name: 'orderNo',
                    type: 'string',
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomerRecentActivityGrid_HeaderAmount'),
                    name: 'amount',
                    type: 'string',
                    cellFilter: 'currency'
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomerRecentActivityGrid_HeaderStore'),
                    name: 'storeName',
                    type: 'string'
                },
                {
                    displayName: localizationService.getLocalizedString('Customers.CustomerRecentActivityGrid_HeaderAddress'),
                    name: 'storeAddress',
                    type: 'string',
                }
            ];
        };

        angular.extend(customersListGridViewModel.prototype, new gridDefaultOptions());

        return customersListGridViewModel;
    }
]);