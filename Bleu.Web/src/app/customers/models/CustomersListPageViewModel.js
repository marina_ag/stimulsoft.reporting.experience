﻿angular.module('bleu.customers').factory('CustomersListPageViewModel', [
    function () {
        var customersListPageViewModel = function () {
            this.canEmail = false;
            this.canDelete = false;
            this.canExport = false;
            this.canAddToGroup = false;
            this.canMoveToGroup = false;
            this.canDeleteFromGroup = false;
        };

        return customersListPageViewModel;
    }
]);