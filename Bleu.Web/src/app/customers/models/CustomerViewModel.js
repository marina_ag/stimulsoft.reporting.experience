﻿angular.module('bleu.customers').factory('CustomerViewModel', [
    'BaseViewModel',
    'Gender',
    function (baseViewModel, gender) {
        var customerViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.fullName = '';
            this.firstName = '';
            this.LastName = '';
            this.birth = null;
            this.isActive = true;
            this.phone = '';
            this.city = '';
            this.state = '';
            this.country = '';
            this.gender = gender.Unknown;
            this.photoId = null;
            this.email = '';
            this.notes = '';
            this.groups = [];
            this.lastVisit = 0;
            this.totalSpent = 0;
            this.avgSpend = 0;
            this.totalOrders = 0;
            this.avgOrders = 0;
        };

        customerViewModel.prototype = Object.create(baseViewModel.prototype);

        return customerViewModel;
    }
]);