﻿angular.module('bleu.customers').factory('CustomersListGridFilterViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var customersListGridFilterViewModel = function () {
            baseViewModel.call(this);

            this.textFilter = null;
            this.groupId = null;
            this.birth = null;
            this.yearsFrom = null;
            this.yearsTo = null;
            this.isActive = null;
            this.city = null;
            this.totalSpentFrom = null;
            this.totalSpentTo = null;
            this.state = null;
            this.country = null;
            this.gender = null;
        };

        customersListGridFilterViewModel.prototype = Object.create(baseViewModel.prototype);

        return customersListGridFilterViewModel;
    }
]);