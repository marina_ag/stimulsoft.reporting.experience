﻿angular.module('bleu.customers').factory('CustomersGroupsListPageViewModel', [
    function () {
        var customersGroupsListPageViewModel = function () {
            this.textFilter = null;
            this.isAutoFilter = null;
            this.canDeleteGroup = false;
        };

        return customersGroupsListPageViewModel;
    }
]);