﻿angular.module('bleu.customers').value('RecentActivityGridDataModel', {
    fields: [
        'OrderId',
        'Created',
        'Number',
        'Amount',
        'StoreName',
        'StoreAddress'
    ],
    fieldsForSelectAll: ['Id'],
    primaryFields: ['id'],
    sortMapping: [
        { column: 'dateTime', data: ['Created'] },
        { column: 'orderNo', data: ['Number'] },
        { column: 'amount', data: ['Amount'] },
        { column: 'storeName', data: ['StoreName'] },
        { column: 'storeAddress', data: ['StoreAddress'] }
    ]
});