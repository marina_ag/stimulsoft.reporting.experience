﻿angular.module('bleu.customers').factory('RecentActivityViewModel', [
    function () {
        var recentActivityViewModel = function () {
            this.id = null;
            this.dateTime = null;
            this.orderNo = '';
            this.amount = '';
            this.storeName = '';
            this.storeAddress = '';
        };

        return recentActivityViewModel;
    }
]);