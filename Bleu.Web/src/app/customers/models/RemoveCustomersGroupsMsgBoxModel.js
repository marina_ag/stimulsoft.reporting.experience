﻿angular.module('bleu.customers').factory('RemoveCustomersGroupsMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeStoresModel = function () {
            this.title = localizationService.getLocalizedString('Customers.CustomersGroupsListPageRemoveGroupsDlg_Title');
            this.message = localizationService.getLocalizedString('Customers.CustomersGroupsListPageRemoveGroupsDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('Customers.CustomersGroupsListPageRemoveGroupsDlg_BtnTextOk'),
                    value: 'ok',
                    icon: 'icon-checkmark'
                },
                {
                    text: localizationService.getLocalizedString('Customers.CustomersGroupsListPageRemoveGroupsDlg_BtnTextCancel'),
                    value: 'cancel',
                    icon: 'icon-cross'
                }
            ];
            this.successValue = 'ok';
        };

        return removeStoresModel;
    }
]);