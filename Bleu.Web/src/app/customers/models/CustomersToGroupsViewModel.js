﻿angular.module('bleu.customers').factory('CustomersToGroupsViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var customersToGroupsViewModel = function () {
            baseViewModel.call(this);

            this.customersIds = [];
            this.groups = [];
        };

        customersToGroupsViewModel.prototype = Object.create(baseViewModel.prototype);

        return customersToGroupsViewModel;
    }
]);