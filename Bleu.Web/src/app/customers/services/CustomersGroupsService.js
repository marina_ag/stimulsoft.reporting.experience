﻿angular.module('bleu.customers').factory('CustomersGroupsService', [
    '$q',
    'CustomersGroupsSharedService',
    'CustomersGroupsTransport',
    'CustomersGroupsSharedMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (
        $q,
        customersGroupsSharedService,
        customersGroupsTransport,
        customersGroupsSharedMapper,
        requestHandlerHelper,
        cacheService) {
        var
            getNonAutoGroups = function () {
                return customersGroupsSharedService.getAllCustomersGroups().then(function (groups) {
                    return groups.filter(function (group) {
                        return !group.isAuto;
                    });
                });
            },
            getNonAutoGroupsByName = function (key) {
                return getNonAutoGroups().then(function (groups) {
                    return groups.filter(function (group) {
                        return group.name.toLowerCase().indexOf(key.toLowerCase()) != -1;
                    });
                });
            },
            getGroupById = function (id) {
                return customersGroupsSharedService.getAllCustomersGroups().then(function (groups) {
                    var filteredGroups = groups.filter(function (group) {
                        return group.id == id;
                    });
                    if (filteredGroups.length == 0) return null;
                    return filteredGroups[0];
                });
            },
            updateCustomersGroup = function (group) {
                return requestHandlerHelper.handleRequest(customersGroupsTransport.updateCustomersGroup(customersGroupsSharedMapper.mapInstanceToServer(group))).then(function () {
                    cacheService.clear('customersGroups');
                });
            },
            addCustomersToCustomersGroup = function (groupId, customersIds) {
                return requestHandlerHelper.handleRequest(customersGroupsTransport.addCustomersToCustomersGroup(groupId, customersIds));
            },
            removeCustomersFromCustomersGroup = function (groupId, customersIds) {
                return requestHandlerHelper.handleRequest(customersGroupsTransport.removeCustomersFromCustomersGroup(groupId, customersIds));
            },
            removeCustomersFromCustomersGroups = function (model) {
                var promises = [];
                model.groups.forEach(function (group) {
                    promises.push(removeCustomersFromCustomersGroup(group.id, model.customersIds));
                });
                return $q.all(promises).then(function () {
                    cacheService.clear('customersGroups');
                });
            },
            moveCustomersToCustomersGroup = function (groupId, customersIds) {
                return removeCustomersFromCustomersGroup(groupId, customersIds).then(function () {
                    return addCustomersToCustomersGroup(groupId, customersIds);
                });
            },
            moveCustomersToCustomersGroups = function (model) {
                var promises = [];
                model.groups.forEach(function (group) {
                    promises.push(moveCustomersToCustomersGroup(group.id, model.customersIds));
                });
                return $q.all(promises).then(function () {
                    cacheService.clear('customersGroups');
                });
            },
            addCustomersToCustomersGroups = function (model) {
                var promises = [];
                model.groups.forEach(function (group) {
                    promises.push(addCustomersToCustomersGroup(group.id, model.customersIds));
                });
                return $q.all(promises).then(function () {
                    cacheService.clear('customersGroups');
                });
            },
            addCustomersGroup = function (group) {
                return requestHandlerHelper.handleRequest(customersGroupsTransport.addCustomersGroup(customersGroupsSharedMapper.mapInstanceToServer(group))).then(function (groupId) {
                    cacheService.clear('customersGroups');
                    return groupId;
                });
            },
            removeCustomersGroups = function (groupsIds) {
                return requestHandlerHelper.handleRequest(customersGroupsTransport.removeCustomersGroups(groupsIds)).then(function () {
                    cacheService.clear('customersGroups');
                });
            },
            getCustomersGroup = function (groupId) {
                return customersGroupsTransport.getCustomersGroup(groupId).then(function (data) {
                    return customersGroupsSharedMapper.mapInstanceToClient(data);
                });
            };

        return {
            updateCustomersGroup: updateCustomersGroup,
            removeCustomersFromCustomersGroups: removeCustomersFromCustomersGroups,
            moveCustomersToCustomersGroups: moveCustomersToCustomersGroups,
            addCustomersToCustomersGroups: addCustomersToCustomersGroups,
            addCustomersGroup: addCustomersGroup,
            removeCustomersGroups: removeCustomersGroups,
            getCustomersGroup: getCustomersGroup,
            getNonAutoGroups: getNonAutoGroups,
            getNonAutoGroupsByName: getNonAutoGroupsByName,
            getGroupById: getGroupById
        }
    }
]);
