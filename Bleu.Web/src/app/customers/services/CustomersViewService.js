﻿angular.module('bleu.customers').factory('CustomersViewService', [
    'LocalizationService',
    'Gender',
    function (localizationService, gender) {
        var
            validateCustomer = function (validationInfo, model) {
                if (validationInfo != null) {
                    if (validationInfo.HomePhone != null) {
                        model.$setValidity('phone', 'exists', false);
                    }

                    if (validationInfo.Email != null) {
                        model.$setValidity('email', 'exists', false);
                    }
                }
            },
            customersTypes = [
                { id: true, name: localizationService.getLocalizedString('Customers.CustomersTypes_Active') },
                { id: false, name: localizationService.getLocalizedString('Customers.CustomersTypes_Inactive') }
            ],
            isAutoStates = [
                { id: true, name: localizationService.getLocalizedString('Customers.IsAutoStates_Auto') },
                { id: false, name: localizationService.getLocalizedString('Customers.IsAutoStates_NonAuto') }
            ],
            customerGenders = [
                { id: gender.Unknown, name: localizationService.getLocalizedString('Customers.CustomersGenders_Unknown') },
                { id: gender.Male, name: localizationService.getLocalizedString('Customers.CustomersGenders_Male') },
                { id: gender.Female, name: localizationService.getLocalizedString('Customers.CustomersGenders_Female') }
            ],
            getCustomersTypes = function () {
                return customersTypes;
            },
            getIsAutoStates = function () {
                return isAutoStates;
            },
            getCustomersGenders = function () {
                return customerGenders;
            };

        return {
            validateCustomer: validateCustomer,
            getCustomersTypes: getCustomersTypes,
            getIsAutoStates: getIsAutoStates,
            getCustomersGenders: getCustomersGenders
        }
    }
]);
