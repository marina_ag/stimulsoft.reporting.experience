﻿angular.module('bleu.customers').factory('CustomersService', [
    'FakeTransport',
    'CustomersTransport',
    'RequestHandlerHelper',
    'CustomersSummaryMapper',
    'CustomersMapper',
    'DataMapHelper',
    'RecentActivityMapper',
    function (fakeTransport, customersTransport, requestHandlerHelper, customersSummaryMapper, customersMapper, dataMapHelper, recentActivityMapper) {
        var
            getCustomers = function (filter) {
                return requestHandlerHelper.handleRequest(customersTransport.getFilteredCustomers(filter)).then(function (data) {
                    return {
                        Results: customersMapper.mapCollectionToClient(data.Results),
                        Count: data.Count
                    }
                });
            },
            getCustomer = function (id) {
                return requestHandlerHelper.handleRequest(customersTransport.getCustomer(id)).then(function (data) {
                    return customersMapper.mapInstanceToClient(data);
                });
            },
            addCustomer = function (customer) {
                return requestHandlerHelper.handleRequest(customersTransport.addCustomer(customersMapper.mapInstanceToServer(customer)));
            },
            updateCustomer = function (customer) {
                return requestHandlerHelper.handleRequest(customersTransport.updateCustomer(customersMapper.mapInstanceToServer(customer)));
            },
            removeCustomers = function (ids) {
                return requestHandlerHelper.handleRequest(customersTransport.removeCustomers(ids));
            },
            getCustomersSummary = function (filters) {
                return requestHandlerHelper.handleRequest(customersTransport.getFilteredCustomersSummary(filters)).then(function (data) {
                    return customersSummaryMapper.mapInstanceToClient(data);
                });
            },
            getRecentActivity = function (filters) {
                return requestHandlerHelper.handleRequest(customersTransport.getFilteredCustomersOrders(filters)).then(function (data) {
                    return {
                        Results: recentActivityMapper.mapCollectionToClient(data.Results),
                        Count: data.Count
                    }
                });
            };

        return {
            getCustomers: getCustomers,
            getCustomer: getCustomer,
            addCustomer: addCustomer,
            updateCustomer: updateCustomer,
            removeCustomers: removeCustomers,
            getCustomersSummary: getCustomersSummary,
            getRecentActivity: getRecentActivity
        }
    }
]);
