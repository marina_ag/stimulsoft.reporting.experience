﻿angular.module('bleu.customers').controller('CustomersListGridFilterDialogController', [
    '$scope',
    '$modalInstance',
    'SessionService',
    'CustomersListGridFilterViewModel',
    'CustomersGroupsSharedService',
    'CustomersViewService',
    function (
        $scope,
        $modalInstance,
        sessionService,
        customersListGridFilterViewModel,
        customersGroupsSharedService,
        customersViewService) {
        var filters = angular.copy(sessionService.get('customersListGridFilterViewModel')),
            customersGroups = [],
            customersTypes = customersViewService.getCustomersTypes(),
            customersGenders = customersViewService.getCustomersGenders(),
            ok = function () {
                $scope.filters.$validate().then(function () {
                    sessionService.set('customersListGridFilterViewModel', $scope.filters, true);
                    $modalInstance.close();
                });
            },
            cancel = function () {
                $modalInstance.dismiss();
            },
            clear = function () {
                $scope.filters = new customersListGridFilterViewModel();
            },
            init = function () {
                customersGroupsSharedService.getAllCustomersGroups().then(function (groups) {
                    $scope.customersGroups = groups;
                });
            };

        $scope.filters = filters;
        $scope.ok = ok;
        $scope.cancel = cancel;
        $scope.clear = clear;
        $scope.customersGroups = customersGroups;
        $scope.customersTypes = customersTypes;
        $scope.customersGenders = customersGenders;

        init();
    }
]);
