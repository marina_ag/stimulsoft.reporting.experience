﻿angular.module('bleu.customers').controller('CustomerController', [
    '$scope',
    '$state',
    '$stateParams',
    'CustomersService',
    'CustomersGroupsService',
    'CustomersViewService',
    'CustomerViewModel',
    'RecentActivityGridDataModel',
    'RecentActivityGridOptionsModel',
    'GridDataHelper',
    'FileUploadHelper',
    'PathesOfDefaultImages',
    'ImageSizeType',
    'FilterCompareMethod',
    'FilterRelationMethod',
    function (
        $scope,
        $state,
        $stateParams,
        customersService,
        customersGroupsService,
        customersViewService,
        customerViewModel,
        recentActivityGridDataModel,
        recentActivityGridOptionsModel,
        gridDataHelper,
        fileUploadHelper,
        pathesOfDefaultImages,
        imageSizeType,
        filterCompareMethod,
        filterRelationMethod) {
        var customer = new customerViewModel(),
            gridOptions = new recentActivityGridOptionsModel(),
            gridHelper = new gridDataHelper(gridOptions),
            fileUpload = new fileUploadHelper({ placeholder: pathesOfDefaultImages.customer }),
            customersTypes = customersViewService.getCustomersTypes(),
            customersGenders = customersViewService.getCustomersGenders(),
            getGroups = function (key) {
                return customersGroupsService.getNonAutoGroupsByName(key);
            },
            onSuccessImageUpload = function (imageId) {
                $scope.customer.photoId = imageId;
            },
            backToCustomersList = function () {
                $state.goBack('root.customerslist');
            },
            updateCustomer = function () {
                $scope.customer.$validate().then(function () {
                    fileUpload.addPhoto(onSuccessImageUpload).then(function () {
                        customersService.updateCustomer($scope.customer).then(function () {
                            backToCustomersList();
                        }, function (validationInfo) {
                            customersViewService.validateCustomer(validationInfo, $scope.customer);
                        });
                    });
                });
            },
            cancel = function () {
                backToCustomersList();
            },
            init = function () {
                customersService.getCustomer($stateParams.id).then(function (data) {
                    fileUpload.updatePlaceholder(data.photoId, imageSizeType.Medium);
                    $scope.customer = data;
                });
                gridHelper.init({
                    fields: recentActivityGridDataModel.fields,
                    fieldsForSelectAll: recentActivityGridDataModel.fieldsForSelectAll,
                    primaryFields: recentActivityGridDataModel.primaryFields,
                    sortMapping: recentActivityGridDataModel.sortMapping,
                    grid: $scope,
                    getDataMethod: customersService.getRecentActivity,
                    defaultFilters: [
                        { Selector: 'CustomerId', Method: filterCompareMethod.Equals, Value: $stateParams.id, Relation: filterRelationMethod.And }
                    ]
                });
            };

        $scope.fileUpload = fileUpload;
        $scope.customer = customer;
        $scope.updateCustomer = updateCustomer;
        $scope.cancel = cancel;
        $scope.getGroups = getGroups;
        $scope.customersTypes = customersTypes;
        $scope.customersGenders = customersGenders;
        $scope.gridOptions = gridOptions;

        init();
    }
]);