﻿angular.module('bleu.customers').controller('AddCustomerController', [
    '$scope',
    '$state',
    '$stateParams',
    'CustomersService',
    'CustomersGroupsService',
    'CustomersViewService',
    'CustomerViewModel',
    'FileUploadHelper',
    'PathesOfDefaultImages',
    function (
        $scope,
        $state,
        $stateParams,
        customersService,
        customersGroupsService,
        customersViewService,
        customerViewModel,
        fileUploadHelper,
        pathesOfDefaultImages) {
        var customer = new customerViewModel(),
            customersTypes = customersViewService.getCustomersTypes(),
            customersGenders = customersViewService.getCustomersGenders(),
            fileUpload = new fileUploadHelper({ placeholder: pathesOfDefaultImages.customer }),
            getGroups = function (key) {
                return customersGroupsService.getNonAutoGroupsByName(key);
            },
            onSuccessImageUpload = function (imageId) {
                $scope.customer.photoId = imageId;
            },
            backToCustomersList = function () {
                $state.goBack('root.customerslist');
            },
            saveCustomer = function () {
                $scope.customer.$validate().then(function () {
                    fileUpload.addPhoto(onSuccessImageUpload).then(function () {
                        customersService.addCustomer($scope.customer).then(function () {
                            backToCustomersList();
                        }, function (validationInfo) {
                            customersViewService.validateCustomer(validationInfo, $scope.customer);
                        });
                    });
                });
            },
            cancel = function () {
                backToCustomersList();
            };

        $scope.fileUpload = fileUpload;
        $scope.customer = customer;
        $scope.saveCustomer = saveCustomer;
        $scope.cancel = cancel;
        $scope.getGroups = getGroups;
        $scope.customersTypes = customersTypes;
        $scope.customersGenders = customersGenders;
    }
]);