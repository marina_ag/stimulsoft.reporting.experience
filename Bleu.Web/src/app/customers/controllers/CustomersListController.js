﻿angular.module('bleu.customers').controller('CustomersListController', [
    '$scope',
    '$modal',
    '$state',
    'GridDataHelper',
    'CustomersService',
    'CustomersGroupsService',
    'CustomersGroupsSharedService',
    'ImagesService',
    'CustomersListGridOptionsModel',
    'CustomersListGridDataModel',
    'CustomersListGridFilterViewModel',
    'CustomersSummaryViewModel',
    'CustomersListGridFilterDialogOptions',
    'AddCustomersToGroupsDialogOptions',
    'MoveCustomersToGroupsDialogOptions',
    'RemoveCustomersFromGroupsDialogOptions',
    'RemoveCustomersMsgBoxModel',
    'CustomersListPageViewModel',
    'CustomersToGroupsViewModel',
    'DataMapHelper',
    'MessageBoxHelper',
    'SessionService',
    'PathesOfDefaultImages',
    'FilterCompareMethod',
    'FilterRelationMethod',
    function (
        $scope,
        $modal,
        $state,
        gridDataHelper,
        customersService,
        customersGroupsService,
        customersGroupsSharedService,
        imagesService,
        customersListGridOptionsModel,
        customersListGridDataModel,
        customersListGridFilterViewModel,
        customersSummaryViewModel,
        customersListGridFilterDialogOptions,
        addCustomersToGroupsDialogOptions,
        moveCustomersToGroupsDialogOptions,
        removeCustomersFromGroupsDialogOptions,
        removeCustomersMsgBoxModel,
        customersListPageViewModel,
        customersToGroupsViewModel,
        dataMapHelper,
        messageBoxHelper,
        sessionService,
        pathesOfDefaultImages,
        filterCompareMethod,
        filterRelationMethod) {
        var gridOptions = new customersListGridOptionsModel(),
            gridHelper = new gridDataHelper(gridOptions),
            filters = sessionService.set('customersListGridFilterViewModel', new customersListGridFilterViewModel()),
            viewModel = new customersListPageViewModel(),
            customersSummary = new customersSummaryViewModel(),
            customersGroups = [],
            loadCustomersSummary = function () {
                customersService.getCustomersSummary(gridHelper.getFilters()).then(function (summary) {
                    $scope.customersSummary = summary;
                });
            },
            getFilters = function () {
                return [
                    { Selector: 'FirstName', Method: filterCompareMethod.Contains, Value: $scope.filters.textFilter, Relation: filterRelationMethod.Or },
                    { Selector: 'LastName', Method: filterCompareMethod.Contains, Value: $scope.filters.textFilter, Relation: filterRelationMethod.Or },
                    { Selector: 'Email', Method: filterCompareMethod.Contains, Value: $scope.filters.textFilter, Relation: filterRelationMethod.And },
                    { Selector: 'GroupId', Method: filterCompareMethod.Exists, Value: $scope.filters.groupId, Relation: filterRelationMethod.And },
                    { Selector: 'BirthDate', Method: filterCompareMethod.Equals, Value: dataMapHelper.prepareDateForFilter($scope.filters.birth), Relation: filterRelationMethod.And },
                    { Selector: 'BirthDate', Method: filterCompareMethod.LowerThan, Value: dataMapHelper.prepareDateForFilter(dataMapHelper.getBirthFromAge($scope.filters.yearsFrom)), Relation: filterRelationMethod.And },
                    { Selector: 'BirthDate', Method: filterCompareMethod.GreatherThan, Value: dataMapHelper.prepareDateForFilter(dataMapHelper.getBirthFromAge($scope.filters.yearsTo)), Relation: filterRelationMethod.And },
                    { Selector: 'IsActive', Method: filterCompareMethod.Equals, Value: $scope.filters.isActive, Relation: filterRelationMethod.And },
                    { Selector: 'City', Method: filterCompareMethod.Contains, Value: $scope.filters.city, Relation: filterRelationMethod.And },
                    { Selector: 'TotalSpend', Method: filterCompareMethod.GreatherThan, Value: $scope.filters.totalSpentFrom, Relation: filterRelationMethod.And },
                    { Selector: 'TotalSpend', Method: filterCompareMethod.LowerThan, Value: $scope.filters.totalSpentTo, Relation: filterRelationMethod.And },
                    { Selector: 'State', Method: filterCompareMethod.Contains, Value: $scope.filters.state, Relation: filterRelationMethod.And },
                    { Selector: 'Country', Method: filterCompareMethod.Contains, Value: $scope.filters.country, Relation: filterRelationMethod.And },
                    { Selector: 'Sex', Method: filterCompareMethod.Equals, Value: $scope.filters.gender, Relation: filterRelationMethod.And }
                ];
            },
            openFilter = function () {
                $modal.open(customersListGridFilterDialogOptions).result.then(function () {
                    $scope.filters = sessionService.get('customersListGridFilterViewModel');
                    filterChange();
                });
            },
            filterChange = function () {
                gridHelper.updateFilters(getFilters()).then(loadCustomersSummary);
            },
            getCustomersIds = function () {
                return gridHelper.getSelection().map(function (item) {
                    return item.id;
                });
            },
            sendEmails = function () {
                console.log('Send emails to ', getCustomersIds());
            },
            importCustomers = function () {
                console.log('Import customers');
                gridHelper.reload();
            },
            addCustomer = function () {
                $state.go('root.customersAddCustomer');
            },
            removeCustomers = function () {
                messageBoxHelper.openModal(new removeCustomersMsgBoxModel()).then(function () {
                    customersService.removeCustomers(getCustomersIds()).then(function () {
                        gridHelper.reload();
                    });
                });
            },
            exportCustomers = function () {
                console.log('Export customers ', getCustomersIds());
            },
            addCustomersGroup = function () {
                $state.go('root.customersAddCustomersGroup');
            },
            prepareCustomersToGroups = function () {
                var customersToGroups = new customersToGroupsViewModel();
                customersToGroups.customersIds = getCustomersIds();
                sessionService.set('customersToGroupsViewModel', customersToGroups, true);
            },
            prepareDeleteCustomersFromGroups = function () {
                var customersToGroups = new customersToGroupsViewModel();
                return customersGroupsService.getGroupById($scope.filters.groupId).then(function (group) {
                    if (group != null) {
                        customersToGroups.groups.push(group);
                    }
                    customersToGroups.customersIds = getCustomersIds();
                    sessionService.set('customersToGroupsViewModel', customersToGroups, true);
                });
            },
            addToCustomersGroup = function () {
                prepareCustomersToGroups();
                $modal.open(addCustomersToGroupsDialogOptions).result.then(function () {
                    customersGroupsService.addCustomersToCustomersGroups(sessionService.get('customersToGroupsViewModel'));
                });
            },
            moveToCustomersGroup = function () {
                prepareCustomersToGroups();
                $modal.open(moveCustomersToGroupsDialogOptions).result.then(function () {
                    customersGroupsService.moveCustomersToCustomersGroups(sessionService.get('customersToGroupsViewModel')).then(function () {
                        gridHelper.reload();
                    });
                });
            },
            deleteFromCustomersGroup = function () {
                prepareDeleteCustomersFromGroups().then(function () {
                    $modal.open(removeCustomersFromGroupsDialogOptions).result.then(function () {
                        customersGroupsService.removeCustomersFromCustomersGroups(sessionService.get('customersToGroupsViewModel')).then(function () {
                            gridHelper.reload();
                        });
                    });
                });
            },
            onSelectionChanged = function (selection) {
                var hasSelection = selection.length != 0;
                viewModel.canEmail = hasSelection;
                viewModel.canDelete = hasSelection;
                viewModel.canExport = hasSelection;
                viewModel.canAddToGroup = hasSelection;
                viewModel.canMoveToGroup = hasSelection;
                viewModel.canDeleteFromGroup = hasSelection;
            },
            init = function () {
                customersGroupsSharedService.getAllCustomersGroups().then(function (groups) {
                    $scope.customersGroups = groups;
                });
                gridHelper.init({
                    fields: customersListGridDataModel.fields,
                    fieldsForSelectAll: customersListGridDataModel.fieldsForSelectAll,
                    primaryFields: customersListGridDataModel.primaryFields,
                    sortMapping: customersListGridDataModel.sortMapping,
                    grid: $scope,
                    getDataMethod: customersService.getCustomers,
                    onSelectionChanged: onSelectionChanged,
                    initFilters: getFilters()
                });

                loadCustomersSummary();
            };

        $scope.gridOptions = gridOptions;
        $scope.sendEmails = sendEmails;
        $scope.importCustomers = importCustomers;
        $scope.addCustomer = addCustomer;
        $scope.removeCustomers = removeCustomers;
        $scope.exportCustomers = exportCustomers;
        $scope.filterChange = filterChange;
        $scope.openFilter = openFilter;
        $scope.addCustomersGroup = addCustomersGroup;
        $scope.addToCustomersGroup = addToCustomersGroup;
        $scope.moveToCustomersGroup = moveToCustomersGroup;
        $scope.deleteFromCustomersGroup = deleteFromCustomersGroup;
        $scope.customersGroups = customersGroups;
        $scope.viewModel = viewModel;
        $scope.customersSummary = customersSummary;
        $scope.filters = filters;

        init();
    }
]);
