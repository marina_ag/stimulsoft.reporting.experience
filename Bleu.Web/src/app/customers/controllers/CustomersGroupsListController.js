﻿angular.module('bleu.customers').controller('CustomersGroupsListController', [
    '$scope',
    '$state',
    'CustomersService',
    'CustomersGroupsService',
    'CustomersGroupsSharedService',
    'CustomersSummaryViewModel',
    'CustomersGroupsListGridOptionsModel',
    'CustomersGroupsListPageViewModel',
    'RemoveCustomersGroupsMsgBoxModel',
    'ClientGridDataHelper',
    'MessageBoxHelper',
    'FilterCompareMethod',
    'FilterRelationMethod',
    'SessionService',
    'CustomersViewService',
    'CustomersListGridFilterViewModel',
    function (
        $scope,
        $state,
        customersService,
        customersGroupsService,
        customersGroupsSharedService,
        customersSummaryViewModel,
        customersGroupsListGridOptionsModel,
        customersGroupsListPageViewModel,
        removeCustomersGroupsMsgBoxModel,
        clientGridDataHelper,
        messageBoxHelper,
        filterCompareMethod,
        filterRelationMethod,
        sessionService,
        customersViewService,
        customersListGridFilterViewModel) {
        var isAutoStates = [],
            gridOptions = new customersGroupsListGridOptionsModel(),
            gridHelper = new clientGridDataHelper(gridOptions),
            customersSummary = new customersSummaryViewModel(),
            viewModel = new customersGroupsListPageViewModel(),
            addCustomersGroup = function () {
                $state.go('root.customersAddCustomersGroup');
            },
            getGroupsIds = function () {
                return gridHelper.getSelection().map(function (item) {
                    return item.id;
                });
            },
            removeCustomersGroup = function () {
                messageBoxHelper.openModal(new removeCustomersGroupsMsgBoxModel()).then(function () {
                    customersGroupsService.removeCustomersGroups(getGroupsIds()).then(function () {
                        gridHelper.reload();
                    });
                });
            },
            filterChange = function () {
                gridHelper.updateFilters([
                    { Selector: 'name', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.And },
                    { Selector: 'isAuto', Method: filterCompareMethod.Equals, Value: viewModel.isAutoFilter, Relation: filterRelationMethod.And }
                ]);
            },
            onSelectionChanged = function (selection) {
                var hasSelection = selection.length != 0;
                viewModel.canDeleteGroup = hasSelection;
            },
            navigateToCustomers = function (groupId) {
                var customersListGridFilter = sessionService.set('customersListGridFilterViewModel', new customersListGridFilterViewModel(), true);
                customersListGridFilter.groupId = groupId;
                $state.go('root.customerslist');
            },
            init = function () {
                $scope.isAutoStates = customersViewService.getIsAutoStates();
                customersService.getCustomersSummary().then(function (summary) {
                    $scope.customersSummary = summary;
                });
                gridHelper.init({
                    getDataMethod: customersGroupsSharedService.getAllCustomersGroups,
                    grid: $scope,
                    onSelectionChanged: onSelectionChanged
                });
            };

        $scope.gridOptions = gridOptions;
        $scope.viewModel = viewModel;
        $scope.addCustomersGroup = addCustomersGroup;
        $scope.removeCustomersGroup = removeCustomersGroup;
        $scope.customersSummary = customersSummary;
        $scope.filterChange = filterChange;
        $scope.navigateToCustomers = navigateToCustomers;
        $scope.isAutoStates = isAutoStates;

        init();
    }
]);
