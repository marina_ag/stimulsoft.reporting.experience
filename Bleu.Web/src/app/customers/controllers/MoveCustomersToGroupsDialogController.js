﻿angular.module('bleu.customers').controller('MoveCustomersToGroupsDialogController', [
    '$scope',
    '$modalInstance',
    'SessionService',
    'CustomersToGroupsViewModel',
    'CustomersGroupsService',
    function (
        $scope,
        $modalInstance,
        sessionService,
        customersToGroupsViewModel,
        customersGroupsService) {
        var customersToGroups = sessionService.get('customersToGroupsViewModel'),
            getGroups = function (key) {
                return customersGroupsService.getNonAutoGroupsByName(key);
            },
            cancel = function () {
                $modalInstance.dismiss();
            },
            ok = function () {
                $scope.customersToGroups.$validate().then(function () {
                    sessionService.set('customersToGroupsViewModel', $scope.customersToGroups, true);
                    $modalInstance.close();
                });
            };

        $scope.customersToGroups = customersToGroups;
        $scope.getGroups = getGroups;
        $scope.cancel = cancel;
        $scope.ok = ok;
    }
]);
