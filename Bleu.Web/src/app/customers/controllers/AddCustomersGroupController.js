﻿angular.module('bleu.customers').controller('AddCustomersGroupController', [
    '$scope',
    '$state',
    '$stateParams',
    'CustomersGroupsService',
    'CustomersViewService',
    'CustomersGroupSharedViewModel',
    function (
        $scope,
        $state,
        $stateParams,
        customersGroupsService,
        customersViewService,
        customersGroupSharedViewModel) {
        var customersGroup = new customersGroupSharedViewModel(),
            customersGenders = customersViewService.getCustomersGenders(),
            backToCustomersGroupsList = function () {
                $state.goBack('root.customersGroups');
            },
            saveCustomersGroup = function () {
                $scope.group.$validate().then(function () {
                    customersGroupsService.addCustomersGroup($scope.group).then(function () {
                        backToCustomersGroupsList();
                    });
                });
            },
            cancel = function () {
                backToCustomersGroupsList();
            };

        $scope.saveCustomersGroup = saveCustomersGroup;
        $scope.cancel = cancel;
        $scope.group = customersGroup;
        $scope.customersGenders = customersGenders;
    }
]);
