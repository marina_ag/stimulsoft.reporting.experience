﻿angular.module('bleu.customers').controller('CustomersGroupController',[
    '$scope',
    '$state',
    '$stateParams',
    'CustomersGroupsService',
    'CustomersViewService',
    'CustomersGroupSharedViewModel',
    function (
        $scope,
        $state,
        $stateParams,
        customersGroupsService,
        customersViewService,
        customersGroupSharedViewModel) {
        var customersGroup = new customersGroupSharedViewModel(),
            customersGenders = customersViewService.getCustomersGenders(),
            backToCustomersGroupsList = function () {
                $state.goBack('root.customersGroups');
            },
            updateCustomersGroup = function () {
                $scope.group.$validate().then(function() {
                    customersGroupsService.updateCustomersGroup($scope.group).then(function () {
                        backToCustomersGroupsList();
                    });
                });
            },
            cancel = function () {
                backToCustomersGroupsList();
            },
            init = function() {
                customersGroupsService.getCustomersGroup($stateParams.id).then(function (data) {
                    $scope.group = data;
                });
            };

        $scope.updateCustomersGroup = updateCustomersGroup;
        $scope.cancel = cancel;
        $scope.group = customersGroup;
        $scope.customersGenders = customersGenders;

        init();
    }
]);
