﻿angular.module('bleu.customers').constant('RemoveCustomersFromGroupsDialogOptions', {
    animation: true,
    templateUrl: '/src/app/customers/views/RemoveCustomersFromGroupsDialogView.html',
    controller: 'RemoveCustomersFromGroupsDialogController',
    size: 'lg'
});