﻿angular.module('bleu.customers').constant('MoveCustomersToGroupsDialogOptions', {
    animation: true,
    templateUrl: '/src/app/customers/views/MoveCustomersToGroupsDialogView.html',
    controller: 'MoveCustomersToGroupsDialogController',
    size: 'lg'
});