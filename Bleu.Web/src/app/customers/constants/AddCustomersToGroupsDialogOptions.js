﻿angular.module('bleu.customers').constant('AddCustomersToGroupsDialogOptions', {
    animation: true,
    templateUrl: '/src/app/customers/views/AddCustomersToGroupsDialogView.html',
    controller: 'AddCustomersToGroupsDialogController',
    size: 'lg'
});