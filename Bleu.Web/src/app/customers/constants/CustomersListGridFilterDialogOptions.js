﻿angular.module('bleu.customers').constant('CustomersListGridFilterDialogOptions', {
    animation: true,
    templateUrl: '/src/app/customers/views/CustomersListGridFilterDialogView.html',
    controller: 'CustomersListGridFilterDialogController',
    size: 'lg'
});