angular.module('bleu.customers').directive('cdCustomersSummary', function () {
    return {
        scope: {
            summary: '=cdCustomersSummary',
        },
        restrict: 'A',
        replace: true,
        templateUrl: 'src/app/customers/views/directives/CustomersSummary.html'
    };
});