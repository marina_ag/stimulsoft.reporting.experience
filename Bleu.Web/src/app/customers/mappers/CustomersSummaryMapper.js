﻿angular.module('bleu.customers').factory('CustomersSummaryMapper', [
    'CustomersSummaryViewModel',
    function (customersSummaryViewModel) {
        var
            transformToClient = function (data) {
                var summary = new customersSummaryViewModel();

                summary.total = data.Total;
                summary.avgSpend = data.AvgSpend;
                summary.totalSpend = data.TotalSpend;

                return summary;
            },
            mapInstanceToClient = function (data) {
                return transformToClient(data);
            };

        return {
            mapInstanceToClient: mapInstanceToClient
        }
    }
]);