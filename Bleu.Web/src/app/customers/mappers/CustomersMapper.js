﻿angular.module('bleu.customers').factory('CustomersMapper', [
    'ImagesService',
    'CustomerViewModel',
    'DataMapHelper',
    'CustomersGroupsSharedMapper',
    'PathesOfDefaultImages',
    function (imagesService, customerViewModel, dataMapHelper, customersGroupsSharedMapper, pathesOfDefaultImages) {
        var
            transformToClient = function (data) {
                var customer = new customerViewModel();

                customer.id = data.CardNo;
                customer.fullName = data.FirstName + ' ' + data.LastName;
                customer.firstName = data.FirstName;
                customer.lastName = data.LastName;
                customer.birth = dataMapHelper.serverDateToClientDate(data.BirthDate);
                customer.isActive = data.IsActive;
                customer.phone = data.HomePhone;
                customer.city = data.City;
                customer.state = data.State;
                customer.gender = data.Sex;
                customer.photoId = data.Photo;
                customer.photo = data.Photo != null ? imagesService.getImageSmallUrl(data.Photo) : pathesOfDefaultImages.customer;
                customer.email = data.Email;
                customer.notes = data.Description;
                customer.groups = data.Groups != null ? customersGroupsSharedMapper.mapCollectionToClient(data.Groups) : [];
                customer.country = data.Country;
                customer.lastVisit = dataMapHelper.getDaysAgo(dataMapHelper.serverDateToClientDate(data.LastVisit));
                customer.totalSpent = data.TotalSpend;
                customer.avgSpend = data.AvgSpend;
                customer.totalOrders = data.TotalOrders;

                return customer;
            },
            transformToServer = function (customer) {
                return {
                    CardNo: customer.id,
                    FirstName: customer.firstName,
                    LastName: customer.lastName,
                    BirthDate: dataMapHelper.clientDateToServerDate(customer.birth),
                    IsActive: customer.isActive,
                    HomePhone: customer.phone,
                    City: customer.city,
                    State: customer.state,
                    Country: customer.country,
                    Sex: customer.gender,
                    Photo: customer.photoId,
                    Email: customer.email,
                    Description: customer.notes,
                    Groups: customer.groups != null ? customersGroupsSharedMapper.mapCollectionToServer(customer.groups) : null
                };
            },
            mapCollectionToClient = function (data) {
                return data.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (data) {
                return transformToClient(data);
            },
            mapCollectionToServer = function (data) {
                return data.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (data) {
                return transformToServer(data);
            };

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);