﻿angular.module('bleu.customers').factory('RecentActivityMapper', [
    'RecentActivityViewModel',
    'DataMapHelper',
    function (recentActivityViewModel, dataMapHelper) {
        var
            transformToClient = function (data) {
                var recentActivity = new recentActivityViewModel();

                recentActivity.id = data.OrderId;
                recentActivity.dateTime = dataMapHelper.serverDateToClientDate(data.Created);
                recentActivity.orderNo = data.Number;
                recentActivity.amount = data.Amount;
                recentActivity.storeName = data.StoreName;
                recentActivity.storeAddress = data.StoreAddress;

                return recentActivity;
            },
            mapCollectionToClient = function (data) {
                return data.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (data) {
                return transformToClient(data);
            };

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient
        }
    }
]);