﻿angular.module('bleu.summary').controller('SummaryController', [
    '$scope',
    'SalesSummaryViewModel',
    'SummaryService',
    'SalesSummaryFilterViewModel',
    'SessionService',
    'DataMapHelper',
    'SalesSummaryParamsViewModel',
    'MostPopularItemsGridDataModel',
    'MostPopularItemsGridOptionsModel',
    'GridDataHelper',
    function(
        $scope,
        salesSummaryViewModel,
        summaryService,
        salesSummaryFilterViewModel,
        sessionService,
        dataMapHelper,
        salesSummaryParamsViewModel,
        mostPopularItemsGridDataModel,
        mostPopularItemsGridOptionsModel,
        gridDataHelper) {
        var
            salesSummary = new salesSummaryViewModel(),
            summaryFilter = sessionService.set('salesSummaryFilterViewModel', new salesSummaryFilterViewModel()),
            summaryParams = new salesSummaryParamsViewModel(),
            gridOptions = new mostPopularItemsGridOptionsModel(),
            gridHelper = new gridDataHelper(gridOptions),
            loadCustomersSummary = function() {
                //filter?
                summaryService.getFilteredSalesSummary(null).then(function(summary) {
                    $scope.salesSummary = summary;
                });
            },
            onSummaryParamsChange = function () {
                $scope.summaryFilter.$validate().then(function () {
                    $scope.summaryParams.startdate = dataMapHelper.prepareDateForReports($scope.summaryFilter.startDate);
                    $scope.summaryParams.enddate = dataMapHelper.prepareDateForReports($scope.summaryFilter.endDate);
                });
            },
            applySummaryFilter = function() {
                if ($scope.summaryParams.startdate != null && $scope.summaryParams.enddate != null) {
                    var values = [];
                    if ($scope.summaryParams.startdate == $scope.summaryParams.enddate) {
                        summaryService.getSalesSummaryByHours($scope.summaryParams.startdate).then(function (summary) {
                            for (var i in summary) {
                                values.push([dataMapHelper.serverDateToClientDate(summary[i].Date).getTime(), summary[i].Sales]);
                            }
                            $scope.salesData = [
                               {
                                   "key": " ",
                                   "values": values
                               }
                            ];
                        });
                    }
                    else {
                        summaryService.getSalesSummaryByDays($scope.summaryParams.startdate, $scope.summaryParams.enddate).then(function (summary) {
                            for (var i in summary) {
                                values.push([dataMapHelper.serverDateToClientDate(summary[i].Date).getTime(), summary[i].Sales]);
                            }
                            $scope.salesData = [
                                {
                                    "key": " ",
                                    "values": values
                                }
                            ];
                        });
                    }
                }
            },
            xAxisTickFormatFunction = function () {
                return function (d) {
                    return d3.time.format('%m-%d-%Y')(new Date(d));
                };
            },
            yAxisTickFormatFunction = function () {
                return function (d) {
                    return '$'+d;
                };
            },
            colorFunction = function () {
                return function (d, i) {
                    return '#FF6800';
                };
            },
            init = function() {
                loadCustomersSummary();
                gridHelper.init({
                    fields: mostPopularItemsGridDataModel.fields,
                    fieldsForSelectAll: mostPopularItemsGridDataModel.fieldsForSelectAll,
                    primaryFields: mostPopularItemsGridDataModel.primaryFields,
                    sortMapping: mostPopularItemsGridDataModel.sortMapping,
                    grid: $scope,
                    getDataMethod: summaryService.getFilteredMenuItemSales,
                    //defaultFilters: []
                });
            };

        $scope.salesSummary = salesSummary;
        $scope.summaryFilter = summaryFilter;
        $scope.summaryParams = summaryParams;
        $scope.onSummaryParamsChange = onSummaryParamsChange;
        $scope.applySummaryFilter = applySummaryFilter;
        $scope.xAxisTickFormatFunction = xAxisTickFormatFunction;
        $scope.yAxisTickFormatFunction = yAxisTickFormatFunction;
        $scope.colorFunction = colorFunction;
        $scope.gridOptions = gridOptions;

        init();
    }
]);
