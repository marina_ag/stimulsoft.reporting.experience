﻿angular.module('bleu.summary').factory('SummaryMapper', [
    'SalesSummaryViewModel',
    function (salesSummaryViewModel) {
        var
            transformToClient = function (data) {
                var summary = new salesSummaryViewModel();

                summary.totalSales = data.TotalSales;
                summary.totalOrders = data.TotalOrders;
                summary.avgOrder = data.AvgOrder;
                summary.totalCustomers = data.TotalCustomers;

                return summary;

            },
            mapInstanceToClient = function (data) {
                return transformToClient(data);
            };

        return {
            mapInstanceToClient: mapInstanceToClient
        };
    }
]);