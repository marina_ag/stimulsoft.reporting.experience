﻿angular.module('bleu.summary').factory('PopularItemsMapper', [
    'MostPopularItemsViewModel',
    function (mostPopularItemsViewModel) {
        var
            transformToClient = function (data) {
                var item = new mostPopularItemsViewModel();

                item.id = data.MenuItemId;
                item.amount = data.Amount;
                item.sales = data.Sales;
                item.name = data.Languages.length > 0 ? data.Languages[0].Name : '';

                return item;
            },
            mapCollectionToClient = function (data) {
                return data.map(function (item) {
                    return transformToClient(item);
                });
            };

        return {
            mapCollectionToClient: mapCollectionToClient
        };
    }
]);