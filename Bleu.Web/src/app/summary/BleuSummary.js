﻿angular.module('bleu.summary', [
    'bleu.main',
    'ui.router',
    'nvd3ChartDirectives',
    'core.summary'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {

    $stateProvider
        .state({
            name: 'root.summary',
            url: '/summary',
            views: {
                'content-view': {
                    templateUrl: 'src/app/summary/views/SummaryView.html',
                    controller: 'SummaryController'
                }
            },
            menu: {
                section: menuSections.summary,
                item: 'Main.LeftMenu_MenuItemSummary',
                order: 10
            }
        });
}]).run();