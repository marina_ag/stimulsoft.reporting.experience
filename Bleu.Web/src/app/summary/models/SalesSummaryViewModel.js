﻿angular.module('bleu.summary').factory('SalesSummaryViewModel', [
    function () {
        var salesSummaryViewModel = function () {
            this.totalSales = 0;
            this.totalOrders = 0;
            this.avgOrder = 0;
            this.totalCustomers = 0;

        };
        return salesSummaryViewModel;
    }
]);