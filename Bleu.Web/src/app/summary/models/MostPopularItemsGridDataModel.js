﻿angular.module('bleu.summary').value('MostPopularItemsGridDataModel', {
    fields: [
        'MenuItemId',
        'Amount',
        'Sales',
        'Languages'
    ],
    fieldsForSelectAll: ['MenuItemId'],
    primaryFields: ['id'],
    sortMapping: [
        { column: 'amount', data: ['Amount'] },
        { column: 'sales', data: ['Sales'] }
    ]
});