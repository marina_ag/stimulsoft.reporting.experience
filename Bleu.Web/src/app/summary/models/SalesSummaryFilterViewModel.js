﻿angular.module('bleu.summary').factory('SalesSummaryFilterViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var salesSummaryFilterViewModel = function () {
            baseViewModel.call(this);

            this.startDate = null;
            this.endDate = null;
        };

        salesSummaryFilterViewModel.prototype = Object.create(baseViewModel.prototype);

        return salesSummaryFilterViewModel;
    }
]);