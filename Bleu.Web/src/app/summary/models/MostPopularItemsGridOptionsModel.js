﻿angular.module('bleu.summary').factory('MostPopularItemsGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var mostPopularItemsGridViewModel = function () {
            this.paginationPageSize = 10;
            this.columnDefs = [
                {
                    displayName: localizationService.getLocalizedString('Summary.SummaryPage_ItemsGridItemName'),
                    name: 'name',
                    type: 'string',
                },
                {
                    displayName: localizationService.getLocalizedString('Summary.SummaryPage_ItemsGridAmount'),
                    name: 'amount',
                    type: 'string',
                },
                {
                    displayName: localizationService.getLocalizedString('Summary.SummaryPage_ItemsGridSales'),
                    name: 'sales',
                    type: 'string',
                    cellFilter: 'currency'
                }
            ];
        };

        angular.extend(mostPopularItemsGridViewModel.prototype, new gridDefaultOptions());

        return mostPopularItemsGridViewModel;
    }
]);