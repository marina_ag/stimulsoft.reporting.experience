﻿angular.module('bleu.summary').factory('MostPopularItemsViewModel', [
    function () {
        var mostPopularItemsViewModel = function () {
            this.id = null;
            this.amount = '';
            this.sales = '';
            this.name = '';
        };

        return mostPopularItemsViewModel;
    }
]);