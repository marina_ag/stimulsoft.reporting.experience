﻿angular.module('bleu.summary').factory('SalesSummaryParamsViewModel', [
    function () {
        var salesSummaryParamsViewModel = function () {
            this.startdate = null;
            this.enddate = null;
        };

        return salesSummaryParamsViewModel;
    }
]);