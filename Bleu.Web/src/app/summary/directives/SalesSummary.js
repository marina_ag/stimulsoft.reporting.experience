angular.module('bleu.summary').directive('cdSalesSummary', function () {
    return {
        scope: {
            summary: '=cdSalesSummary',
        },
        restrict: 'A',
        replace: true,
        templateUrl: 'src/app/summary/views/directives/SalesSummary.html'
    };
});