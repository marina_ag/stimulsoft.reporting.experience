﻿angular.module('bleu.summary').factory('SummaryService', [
    'FakeTransport',
    'RequestHandlerHelper',
    'SummaryMapper',
    'SummaryTransport',
    'PopularItemsMapper',
    function (fakeTransport, requestHandlerHelper, summaryMapper, summaryTransport, popularItemsMapper) {
        var
            getFilteredSalesSummary = function(filter) {
                return requestHandlerHelper.handleRequest(summaryTransport.getFilteredSalesSummary(filter)).then(function(data) {
                    return summaryMapper.mapInstanceToClient(data);
                });
            },
            getSalesSummaryByHours = function(day) {
                return requestHandlerHelper.handleRequest(summaryTransport.getSalesSummaryByHours(day)).then(function (data) {
                    return data;
                });
            },
            getSalesSummaryByDays = function (startDay, endDay) {
                return requestHandlerHelper.handleRequest(summaryTransport.getSalesSummaryByDays(startDay, endDay)).then(function (data) {
                    return data;
                });
            },
            getFilteredMenuItemSales = function (filter) {
                if (filter.SortFilters.length == 0) {
                    filter.SortFilters = [{ Selector: "Amount", Desc: true }];
                }

                return requestHandlerHelper.handleRequest(summaryTransport.getFilteredMenuItemSales(filter)).then(function (data) {
                    return {
                        Results: popularItemsMapper.mapCollectionToClient(data.Results),
                        Count: data.Count
                    };
                });
            };


        return {
            getFilteredSalesSummary: getFilteredSalesSummary,
            getSalesSummaryByHours: getSalesSummaryByHours,
            getSalesSummaryByDays: getSalesSummaryByDays,
            getFilteredMenuItemSales: getFilteredMenuItemSales
        };
    }
]);
