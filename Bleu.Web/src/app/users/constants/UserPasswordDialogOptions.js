﻿angular.module('bleu.users').constant('UserPasswordDialogOptions', {
    animation: true,
    templateUrl: '/src/app/users/views/UserPasswordDialogView.html',
    controller: 'UserPasswordDialogController',
    size: 'lg'
});