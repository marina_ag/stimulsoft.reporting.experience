﻿angular.module('bleu.users').controller('UsersListController', [
    '$scope',
    '$state',
    'RolesSharedService',
    'StoresSharedService',
    'SessionService',
    'UsersService',
    'UsersGridOptionsModel',
    'UsersListGridDataModel',
    'UsersListPageViewModel',
    'RemoveUsersMsgBoxModel',
    'GridDataHelper',
    'MessageBoxHelper',
    'FilterCompareMethod',
    'FilterRelationMethod',
    function(
        $scope,
        $state,
        rolesSharedService,
        storesSharedService,
        sessionService,
        usersService,
        usersGridOptionsModel,
        usersListGridDataModel,
        usersListPageViewModel,
        removeUsersMsgBoxModel,
        gridDataHelper,
        messageBoxHelper,
        filterCompareMethod,
        filterRelationMethod) {
        var
            roles = [],
            stores = [],
            gridOptions = new usersGridOptionsModel(),
            viewModel = sessionService.set('usersListPageViewModel', new usersListPageViewModel()),
            gridHelper = new gridDataHelper(gridOptions),
            onSelectionChanged = function (selection) {
                var hasSelection = selection.length != 0;
                viewModel.canDelete = hasSelection;
            },
            getUsersIds = function () {
                return gridHelper.getSelection().map(function (item) {
                    return item.id;
                });
            },
            getFilters = function () {
                return [
                    { Selector: 'FirstName', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.Or },
                    { Selector: 'LastName', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.Or },
                    { Selector: 'Email', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.And },
                    { Selector: 'RoleId', Method: filterCompareMethod.Exists, Value: viewModel.roleFilter, Relation: filterRelationMethod.And },
                    { Selector: 'StoreId', Method: filterCompareMethod.Exists, Value: viewModel.storeFilter, Relation: filterRelationMethod.And }
                ];
            },
            filterChange = function () {
                gridHelper.updateFilters(getFilters());
            },
            addUser = function () {
                $state.go('root.settingsAddUser');
            },
            removeUsers = function () {
                messageBoxHelper.openModal(new removeUsersMsgBoxModel()).then(function () {
                    usersService.removeUsers(getUsersIds()).then(function () {
                        gridHelper.reload();
                    });
                });
            },
            setActiveState = function (id, isActive) {
                usersService.setActiveState(id, isActive).then(function () {
                    gridHelper.reload();
                });
            },
            init = function () {
                rolesSharedService.getAllRoles().then(function (r) {
                    $scope.roles = r;
                });
                storesSharedService.getAllStores().then(function (s) {
                    $scope.stores = s;
                });

                gridHelper.init({
                    fields: usersListGridDataModel.fields,
                    fieldsForSelectAll: usersListGridDataModel.fieldsForSelectAll,
                    primaryFields: usersListGridDataModel.primaryFields,
                    sortMapping: usersListGridDataModel.sortMapping,
                    grid: $scope,
                    getDataMethod: usersService.getUsers,
                    onSelectionChanged: onSelectionChanged,
                    initFilters: getFilters()
                });
            };

        $scope.roles = roles;
        $scope.stores = stores;
        $scope.addUser = addUser;
        $scope.removeUsers = removeUsers;
        $scope.setActiveState = setActiveState;
        $scope.gridOptions = gridOptions;
        $scope.filterChange = filterChange;
        $scope.viewModel = viewModel;

        init();
    }
]);