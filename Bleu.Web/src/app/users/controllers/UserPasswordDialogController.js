﻿angular.module('bleu.users').controller('UserPasswordDialogController', [
    '$scope',
    '$modalInstance',
    'UsersService',
    'SessionService',
    'UserPasswordViewModel',
    function (
        $scope,
        $modalInstance,
        usersService,
        sessionService,
        userPasswordViewModel) {
        var userPassword = new userPasswordViewModel(),
            user = sessionService.get('userViewModel'),
            cancel = function () {
                $modalInstance.dismiss();
            },
            ok = function () {
                $scope.userPassword.$validate().then(function () {
                    usersService.changeUserPassword($scope.userPassword).then(function () {
                        $modalInstance.close();
                    });
                });
            },
            init = function () {
                $scope.userPassword.userId = user.id;
            };

        $scope.userPassword = userPassword;
        $scope.cancel = cancel;
        $scope.ok = ok;

        init();
    }
]);