﻿angular.module('bleu.users').controller('UserController', [
    '$scope',
    '$state',
    '$stateParams',
    '$modal',
    'UsersService',
    'UsersViewService',
    'RolesSharedService',
    'StoresSharedService',
    'SessionService',
    'UserSharedViewModel',
    'UserPasswordDialogOptions',
    function (
        $scope,
        $state,
        $stateParams,
        $modal,
        usersService,
        usersViewService,
        rolesSharedService,
        storesSharedService,
        sessionService,
        userSharedViewModel,
        userPasswordDialogOptions) {
        var user = new userSharedViewModel(),
            getRoles = function (query) {
                return rolesSharedService.getRolesByName(query);
            },
            getStores = function (query) {
                return storesSharedService.getStoresByName(query);
            },
            updateUser = function () {
                $scope.user.$validate().then(function () {
                    usersService.updateUser($scope.user).then(function () {
                        $state.goBack('root.settingsUsersList');
                    }, function (validationInfo) {
                        usersViewService.validateUser(validationInfo, $scope.user);
                    });
                });
            },
            cancel = function () {
                $state.goBack('root.settingsUsersList');
            },
            openChangePasswordDialog = function () {
                sessionService.set('userViewModel', $scope.user, true);
                $modal.open(userPasswordDialogOptions);
            },
            init = function () {
                usersService.getUser($stateParams.id).then(function (u) {
                    $scope.user = u;
                });
            };

        $scope.getRoles = getRoles;
        $scope.getStores = getStores;
        $scope.user = user;
        $scope.updateUser = updateUser;
        $scope.cancel = cancel;
        $scope.openChangePasswordDialog = openChangePasswordDialog;

        init();
    }
]);