﻿angular.module('bleu.users').controller('AddUserController', [
    '$scope',
    '$state',
    'UsersService',
    'UsersViewService',
    'RolesSharedService',
    'StoresSharedService',
    'UserSharedViewModel',
    'UserPasswordViewModel',
    function (
        $scope,
        $state,
        usersService,
        usersViewService,
        rolesSharedService,
        storesSharedService,
        userSharedViewModel,
        userPasswordViewModel) {
        var user = new userSharedViewModel(),
            userPassword = new userPasswordViewModel(),
            getRoles = function (query) {
                return rolesSharedService.getRolesByName(query);
            },
            getStores = function (query) {
                return storesSharedService.getStoresByName(query);
            },
            saveUser = function () {
                $scope.user.$validate().then(function () {
                    usersService.addUser($scope.user).then(function (userId) {
                        $scope.userPassword.userId = userId;
                        usersService.changeUserPassword($scope.userPassword).then(function () {
                            $state.goBack('root.settingsUsersList');
                        });
                    }, function (validationInfo) {
                        usersViewService.validateUser(validationInfo, $scope.user);
                    });
                });
            },
            cancel = function () {
                $state.goBack('root.settingsUsersList');
            };

        $scope.getRoles = getRoles;
        $scope.getStores = getStores;
        $scope.user = user;
        $scope.userPassword = userPassword;
        $scope.saveUser = saveUser;
        $scope.cancel = cancel;
    }
]);