﻿angular.module('bleu.users').factory('UsersGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var usersGridOptionsModel = function () {
            this.columnDefs = [
                {
                    field: 'name',
                    displayName: localizationService.getLocalizedString('Users.UsersGrid_HeaderName'),
                    type: 'string',
                    cellTemplate: 'src/app/users/views/grid/UsersGridNameTemplate.html'
                },
                {
                    field: 'roles',
                    displayName: localizationService.getLocalizedString('Users.UsersGrid_HeaderRole'),
                    type: 'string',
                    cellTemplate: 'src/app/users/views/grid/UsersGridRolesTemplate.html'
                },
                {
                    field: 'isActive',
                    displayName: localizationService.getLocalizedString('Users.UsersGrid_HeaderActive'),
                    type: 'string',
                    cellTemplate: 'src/app/users/views/grid/UsersGridIsActiveTemplate.html'
                },
                {
                    field: 'email',
                    type: 'string',
                    displayName: localizationService.getLocalizedString('Users.UsersGrid_HeaderEmail')
                }
            ];
        };

        angular.extend(usersGridOptionsModel.prototype, new gridDefaultOptions());

        return usersGridOptionsModel;
    }
]);