﻿angular.module('bleu.users').factory('RemoveUsersMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeStoresModel = function () {
            this.title = localizationService.getLocalizedString('Users.UsersListPageRemoveUsersDlg_Title');
            this.message = localizationService.getLocalizedString('Users.UsersListPageRemoveUsersDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('Users.UsersListPageRemoveUsersDlg_BtnTextOk'),
                    value: 'ok'
                },
                {
                    text: localizationService.getLocalizedString('Users.UsersListPageRemoveUsersDlg_BtnTextCancel'),
                    value: 'cancel'
                }
            ];
            this.successValue = 'ok';
        };

        return removeStoresModel;
    }
]);