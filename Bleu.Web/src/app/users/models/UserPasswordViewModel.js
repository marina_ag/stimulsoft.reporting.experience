﻿angular.module('bleu.users').factory('UserPasswordViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var userPasswordViewModel = function () {
            baseViewModel.call(this);

            this.userId = null;
            this.password = '';
            this.confirmPassword = '';
        };

        userPasswordViewModel.prototype = Object.create(baseViewModel.prototype);

        return userPasswordViewModel;
    }
]);