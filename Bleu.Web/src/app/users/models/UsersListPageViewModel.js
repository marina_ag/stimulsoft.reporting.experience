﻿angular.module('bleu.users').factory('UsersListPageViewModel', [
    function () {
        var usersListPageViewModel = function () {
            this.textFilter = null;
            this.roleFilter = null;
            this.storeFilter = null;
            this.canDelete = false;
        };

        return usersListPageViewModel;
    }
]);