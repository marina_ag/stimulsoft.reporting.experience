﻿angular.module('bleu.users').value('UsersListGridDataModel', {
    fields: [
        'Id',
        'FirstName',
        'LastName',
        'IsActive',
        'Email'
    ],
    fieldsForSelectAll: ['Id'],
    primaryFields: ['id'],
    sortMapping: [
        { column: 'name', data: ['FirstName', 'LastName'] },
        { column: 'roles', data: ['Roles'] },
        { column: 'isActive', data: ['IsActive'] },
        { column: 'email', data: ['Email'] }
    ]
});