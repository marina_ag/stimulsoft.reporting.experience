﻿angular.module('bleu.users', [
    'core.users',
    'bleu.users.shared',
    'bleu.roles.shared',
    'bleu.stores.shared',
    'bleu.main',
    'ui.router'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {

    $stateProvider
        .state({
            name: 'root.settingsUsersList',
            url: '/users',
            views: {
                'content-view': {
                    templateUrl: 'src/app/users/views/UsersListView.html',
                    controller: 'UsersListController'
                }
            },
            menu: {
                section: menuSections.settings,
                item: 'Main.LeftMenu_MenuItemUsers',
                order: 20
            }
        })
        .state({
            name: 'root.settingsAddUser',
            url: '/adduser',
            views: {
                'content-view': {
                    templateUrl: 'src/app/users/views/AddUserView.html',
                    controller: 'AddUserController'
                }
            }
        })
        .state({
            name: 'root.settingsUser',
            url: '/user/:id',
            views: {
                'content-view': {
                    templateUrl: 'src/app/users/views/UserView.html',
                    controller: 'UserController'
                }
            }
        });
}]).run();