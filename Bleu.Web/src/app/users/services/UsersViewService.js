﻿angular.module('bleu.users').factory('UsersViewService', [
    function () {
        var
            validateUser = function (validationInfo, model) {
                if (validationInfo != null) {
                    if (validationInfo.Login != null) {
                        model.$setValidity('login', 'exists', false);
                    }

                    if (validationInfo.CashierPassword != null) {
                        model.$setValidity('pin', 'exists', false);
                    }
                }
            };

        return {
            validateUser: validateUser
        }
    }
]);