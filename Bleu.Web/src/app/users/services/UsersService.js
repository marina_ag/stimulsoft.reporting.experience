﻿angular.module('bleu.users').factory('UsersService', [
    'EmployeesTransport',
    'UsersSharedMapper',
    'UsersPasswordsMapper',
    'StoresSharedService',
    'RequestHandlerHelper',
    function (employeesTransport, usersSharedMapper, usersPasswordsMapper, storesSharedService, requestHandlerHelper) {
        var
            isStoreAlreadyExistInUserStores = function (stores, storeId) {
                return stores.some(function (item) {
                    return item.id == storeId;
                });
            },
            getUserStore = function (allStores, storeId) {
                var stores = allStores.filter(function (item) {
                    return item.id == storeId;
                });

                return stores.length > 0 ? stores[0] : null;
            },
            getUserStores = function (user) {
                return storesSharedService.getAllStores().then(function (allStores) {
                    var userStores = [];

                    user.roles.forEach(function (role) {
                        role.storesIds.forEach(function (storeId) {
                            if (!isStoreAlreadyExistInUserStores(userStores, storeId)) {
                                userStores.push(getUserStore(allStores, storeId));
                            }
                        });
                    });

                    return userStores;
                });
            },
            getUsers = function(filter) {
                return requestHandlerHelper.handleRequest(employeesTransport.getFilteredEmployees(filter)).then(function (response) {
                    return {
                        Results: usersSharedMapper.mapCollectionToClient(response.Results),
                        Count: response.Count
                    }
                });
            },
            getUser = function (id) {
                return requestHandlerHelper.handleRequest(employeesTransport.getEmployee(id)).then(function (response) {
                    var user = usersSharedMapper.mapInstanceToClient(response);
                    return getUserStores(user).then(function (stores) {
                        user.stores = stores;
                        return user;
                    });
                });
            },
            updateUser = function (user) {
                return requestHandlerHelper.handleRequest(employeesTransport.updateEmployee(usersSharedMapper.mapInstanceToServer(user)));
            },
            addUser = function(user) {
                return requestHandlerHelper.handleRequest(employeesTransport.addEmployee(usersSharedMapper.mapInstanceToServer(user)).then(function (id) {
                    return id;
                }));
            },
            removeUsers = function (usersIds) {
                return requestHandlerHelper.handleRequest(employeesTransport.removeEmployees(usersIds));
            },
            setActiveState = function (id, isActive) {
                return getUser(id).then(function (user) {
                    user.isActive = isActive;
                    return updateUser(user);
                });
            },
            changeUserPassword = function (userPassword) {
                return requestHandlerHelper.handleRequest(employeesTransport.changeEmployeePassword(usersPasswordsMapper.mapInstanceToServer(userPassword)));
            };

        return {
            getUsers: getUsers,
            getUser: getUser,
            updateUser: updateUser,
            addUser: addUser,
            removeUsers: removeUsers,
            setActiveState: setActiveState,
            changeUserPassword: changeUserPassword
        }
    }
]);