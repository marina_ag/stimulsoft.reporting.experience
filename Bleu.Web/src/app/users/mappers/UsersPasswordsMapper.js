﻿angular.module('bleu.users').factory('UsersPasswordsMapper', [
    function () {
        var
            transformToServer = function (request) {
                return {
                    EmployeeId: request.userId,
                    Password: request.password
                }
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            }

        return {
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);