﻿angular.module('bleu', [
    'ui.router',
    'bleu.summary',
    'bleu.customers',
    'bleu.menuEditor',
    'bleu.settings',
    'bleu.users',
    'bleu.taxes',
    'bleu.roles',
    //'bleu.beacons',
    'bleu.devices',
    'bleu.receipts',
    'bleu.stores',
    'bleu.discounts',
    'bleu.reports',
    'bleu.profile',
    'bleu.messages'
]).config(['$urlRouterProvider', '$logProvider', function ($urlRouterProvider, $logProvider) {
    $logProvider.debugEnabled(true);
    //default route here
    $urlRouterProvider.otherwise('/summary');
}]).run(['$rootScope',function ($rootScope) {
    $rootScope.$defaultRouteStateName = 'root.summary';
}]);