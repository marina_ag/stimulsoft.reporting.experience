﻿angular.module('bleu.menuEditor.shared').factory('MenuItemsCategoriesSharedMapper', [
    'MenuItemsCategorySharedViewModel',
    'StoresSharedMapper',
    'ImagesService',
    'PathesOfDefaultImages',
    function (menuItemsCategorySharedViewModel, storesSharedMapper, imagesService, pathesOfDefaultImages) {
        var
            transformToClient = function (response) {
                var menuItemCategory = new menuItemsCategorySharedViewModel();

                menuItemCategory.id = response.CategoryId;
                menuItemCategory.name = response.Languages.length > 0 ? response.Languages[0].Name : response.SalesCategory;//response.SalesCategory;
                menuItemCategory.imageId = response.Image != null ? response.Image.ImageId : null;
                menuItemCategory.image = menuItemCategory.imageId != null ? imagesService.getImageSmallUrl(menuItemCategory.imageId) : pathesOfDefaultImages.placeholder;
                menuItemCategory.isActive = response.IsActive;
                menuItemCategory.itemsAssigned = response.ItemsAssigned != null ? response.ItemsAssigned : 0;
                menuItemCategory.position = response.DisplayOrder;
                menuItemCategory.stores = response.Stores != null ? storesSharedMapper.mapCollectionToClient(response.Stores) : [];
                menuItemCategory.sorting = response.Sorting;

                return menuItemCategory;
            },
            transformToServer = function (request) {
                return {
                    CategoryId: request.id,
                    SalesCategory: request.name,
                    Image: {
                        ImageId: request.imageId,
                        Picture: [],
                        ImageType: 3
                    },
                    IsActive: request.isActive,
                    DisplayOrder: request.position,
                    Stores: storesSharedMapper.mapCollectionToServer(request.stores),
                    Sorting: request.sorting
                }
            },
            mapCollectionToClient = function (response) {
                return response.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (response) {
                return transformToClient(response);
            },
            mapCollectionToServer = function (request) {
                return request.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            };

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);