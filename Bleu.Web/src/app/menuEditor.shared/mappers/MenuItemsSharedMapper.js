﻿angular.module('bleu.menuEditor.shared').factory('MenuItemsSharedMapper', [
    'MenuItemViewModel',
    'LanguagesMapper',
    function (
        menuItemViewModel,
        languagesMapper) {
        var
            transformToClient = function(response) {
                var menuItem = new menuItemViewModel();

                menuItem.id = response.MenuItemId;
                menuItem.languages = response.Languages != null ? languagesMapper.mapCollectionToClient(response.Languages) : [];
                menuItem.name = menuItem.languages.length > 0 ? menuItem.languages[0].name : '';

                return menuItem;
            },
            transformToServer = function(request) {
                var languages = request.languages.map(function(item, key) {
                    if (key == 0) {
                        item.name = request.name;
                        item.description = request.description;
                    }

                    return item;
                });
                return {
                    MenuItemId: request.id,
                    Languages: languagesMapper.mapCollectionToServer(languages)
                }
            },
            mapCollectionToClient = function(response) {
                return response.map(function(item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function(response) {
                return transformToClient(response);
            },
            mapCollectionToServer = function(request) {
                return request.map(function(item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function(request) {
                return transformToServer(request);
            };

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);