﻿angular.module('bleu.menuEditor.shared').factory('MenuItemsCategorySharedViewModel', [
    'BaseViewModel',
    'MenuSortingType',
    'PathesOfDefaultImages',
    function (baseViewModel, menuSortingType, pathesOfDefaultImages) {
        var menuItemsCategorySharedViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.name = '';
            this.imageId = null;
            this.isActive = true;
            this.itemsAssigned = 0;
            this.position = null;
            this.stores = [];
            this.menuItems = [];
            this.sorting = menuSortingType.Custom;
            this.image = pathesOfDefaultImages.placeholder;
        };

        menuItemsCategorySharedViewModel.prototype = Object.create(baseViewModel.prototype);

        return menuItemsCategorySharedViewModel;
    }
]);