﻿angular.module('bleu.menuEditor.shared').factory('MenuItemSharedViewModel', [
    'BaseViewModel',
    'LanguageViewModel',
    function (baseViewModel, languageViewModel) {
        var menuItemViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.name = '';
            this.languages = [new languageViewModel()];
        };

        menuItemViewModel.prototype = Object.create(baseViewModel.prototype);

        return menuItemViewModel;
    }
]);