﻿angular.module('bleu.menuEditor.shared', [
    'core.menuEditor.shared',
    'bleu.stores.shared',
    'bleu.main'
]).run();