﻿angular.module('bleu.menuEditor.shared').factory('MenuItemsCategoriesSharedService', [
    'MenuCategoriesSharedTransport',
    'MenuItemsCategoriesSharedMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (
        menuCategoriesSharedTransport,
        menuItemsCategoriesSharedMapper,
        requestHandlerHelper,
        cacheService) {
        var
            getMenuItemsCategories = function () {
                return menuCategoriesSharedTransport.getAllMenuCategories().then(function (response) {
                    return menuItemsCategoriesSharedMapper.mapCollectionToClient(response);
                });
            },
            getAllMenuItemsCategories = function () {
                return requestHandlerHelper.handleRequest(cacheService.cache('menuItemsCategories', getMenuItemsCategories));
            },
            getMenuItemsCategoriesByName = function (key) {
                return getAllMenuItemsCategories().then(function (response) {
                    return response.filter(function (item) {
                        return item.name.toLowerCase().indexOf(key.toLowerCase()) != -1;
                    });;
                });
            };

        return {
            getAllMenuItemsCategories: getAllMenuItemsCategories,
            getMenuItemsCategoriesByName: getMenuItemsCategoriesByName
        }
    }
])