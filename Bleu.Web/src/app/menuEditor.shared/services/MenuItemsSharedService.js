﻿angular.module('bleu.menuEditor.shared').factory('MenuItemsSharedService', [
    'MenuItemsTransport',
    'MenuItemsSharedMapper',
    'RequestHandlerHelper',
    function (
        menuItemsTransport,
        menuItemsMapper,
        requestHandlerHelper) {
        var
            getMenuItems = function(filter) {
                return requestHandlerHelper.handleRequest(menuItemsTransport.getFilteredMenuItems(filter)).then(function(response) {
                    return menuItemsMapper.mapCollectionToClient(response.Results);
                });
            },
            getMenuItemsByName = function (filter, key) {
                return getMenuItems(filter).then(function (response) {
                    return response.filter(function (item) {
                        return item.name.toLowerCase().indexOf(key.toLowerCase()) != -1;
                    });
                });
            };
            

        return {
            getMenuItems: getMenuItems,
            getMenuItemsByName: getMenuItemsByName
        }
    }
]);