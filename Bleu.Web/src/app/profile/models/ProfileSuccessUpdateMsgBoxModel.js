﻿angular.module('bleu.profile').factory('ProfileSuccessUpdateMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var profileSuccessUpdateModel = function () {
            this.title = localizationService.getLocalizedString('Profile.ProfilePageProfileSuccessUpdateDlg_Title');
            this.message = localizationService.getLocalizedString('Profile.ProfilePageProfileSuccessUpdateDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('Profile.ProfilePageProfileSuccessUpdateDlg_BtnTextOk'),
                    value: 'ok'
                }
            ];
            this.successValue = 'ok';
        };

        return profileSuccessUpdateModel;
    }
]);