﻿angular.module('bleu.profile').factory('ProfilePasswordViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var profilePasswordViewModel = function () {
            baseViewModel.call(this);

            this.oldPassword = '';
            this.password = '';
            this.confirmPassword = '';
        };

        profilePasswordViewModel.prototype = Object.create(baseViewModel.prototype);

        return profilePasswordViewModel;
    }
]);