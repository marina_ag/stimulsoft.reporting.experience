﻿angular.module('bleu.profile').factory('ProfileViewService', [
    function () {
        var
            validateProfilePassword = function (validationInfo, model) {
                if (validationInfo != null && validationInfo.OldPassword != null) {
                    model.$setValidity('oldPassword', 'wrong', false);
                }
            };

        return {
            validateProfilePassword: validateProfilePassword
        }
    }
]);