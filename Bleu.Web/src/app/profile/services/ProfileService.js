﻿angular.module('bleu.profile').factory('ProfileService', [
    'ProfileTransport',
    'UsersSharedMapper',
    'ProfilesPasswordsMapper',
    'RequestHandlerHelper',
    function (profileTransport, usersSharedMapper, profilesPasswordsMapper, requestHandlerHelper) {
        var
            getProfile = function () {
                return requestHandlerHelper.handleRequest(profileTransport.getProfile()).then(function (response) {
                    return usersSharedMapper.mapInstanceToClient(response);
                });
            },
            updateProfile = function (profile) {
                return requestHandlerHelper.handleRequest(profileTransport.updateProfile(usersSharedMapper.mapInstanceToServer(profile)));
            },
            changeProfilePassword = function (profilePassword) {
                return requestHandlerHelper.handleRequest(profileTransport.changePassword(profilesPasswordsMapper.mapInstanceToServer(profilePassword)));
            };

        return {
            getProfile: getProfile,
            updateProfile: updateProfile,
            changeProfilePassword: changeProfilePassword
        }
    }
]);