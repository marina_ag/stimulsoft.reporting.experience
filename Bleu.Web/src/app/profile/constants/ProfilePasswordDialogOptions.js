﻿angular.module('bleu.profile').constant('ProfilePasswordDialogOptions', {
    animation: true,
    templateUrl: '/src/app/profile/views/ProfilePasswordDialogView.html',
    controller: 'ProfilePasswordDialogController',
    size: 'lg'
});