﻿angular.module('bleu.profile', [
    'core.profile',
    'bleu.users.shared',
    'bleu.main',
    'ui.router'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {

    $stateProvider
        .state({
            name: 'root.profile',
            url: '/profile',
            views: {
                'content-view': {
                    templateUrl: 'src/app/profile/views/ProfileView.html',
                    controller: 'ProfileController'
                }
            }
        });
}]).run();