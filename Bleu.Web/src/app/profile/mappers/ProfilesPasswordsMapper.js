﻿angular.module('bleu.profile').factory('ProfilesPasswordsMapper', [
    function () {
        var
            transformToServer = function (request) {
                return {
                    OldPassword: request.oldPassword,
                    NewPassword: request.password
                }
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            };

        return {
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);