﻿angular.module('bleu.profile').controller('ProfileController', [
    '$scope',
    '$modal',
    'ProfileService',
    'UserSharedViewModel',
    'ProfileSuccessUpdateMsgBoxModel',
    'MessageBoxHelper',
    'ProfilePasswordDialogOptions',
    function (
        $scope,
        $modal,
        profileService,
        userSharedViewModel,
        profileSuccessUpdateMsgBoxModel,
        messageBoxHelper,
        profilePasswordDialogOptions) {
        var profile = new userSharedViewModel(),
            openChangePasswordDialog = function () {
                $modal.open(profilePasswordDialogOptions);
            },
            updateProfile = function () {
                $scope.profile.$validate().then(function () {
                    profileService.updateProfile($scope.profile).then(function () {
                        messageBoxHelper.openModal(new profileSuccessUpdateMsgBoxModel());
                    });
                });
            },
            init = function () {
                profileService.getProfile().then(function (p) {
                    $scope.profile = p;
                });
            };

        $scope.profile = profile;
        $scope.openChangePasswordDialog = openChangePasswordDialog;
        $scope.updateProfile = updateProfile;

        init();
    }
]);