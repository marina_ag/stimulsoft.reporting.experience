﻿angular.module('bleu.profile').controller('ProfilePasswordDialogController', [
    '$scope',
    '$modalInstance',
    'ProfileService',
    'ProfileViewService',
    'ProfilePasswordViewModel',
    function (
        $scope,
        $modalInstance,
        profileService,
        profileViewService,
        profilePasswordViewModel) {
        var profilePassword = new profilePasswordViewModel(),
            cancel = function () {
                $modalInstance.dismiss();
            },
            ok = function () {
                $scope.profilePassword.$validate().then(function () {
                    profileService.changeProfilePassword($scope.profilePassword).then(function () {
                        $modalInstance.close();
                    }, function (validationInfo) {
                        profileViewService.validateProfilePassword(validationInfo, $scope.profilePassword);
                    });
                });
            };

        $scope.profilePassword = profilePassword;
        $scope.cancel = cancel;
        $scope.ok = ok;
    }
]);