﻿angular.module('bleu.main').service('GridDefaultOptions', ['uiGridConstants', function (uiGridConstants) {
    var gridDefaultOptions = function () {
        this.enableRowSelection = true;
        this.enableSelectAll = true;
        this.multiSelect = true;

        this.enableSorting = true;
        this.useExternalSorting = true;
        this.enableColumnMenus = false;

        this.enableHorizontalScrollbar = uiGridConstants.scrollbars.NEVER;

        this.headerRowHeight = 60;
        this.rowHeight = 60;
        this.minRowsToShow = 15;

        this.useExternalFiltering = true;

        this.enablePaginationControls = true;
        this.paginationPageSizes = [15, 25, 50, 100, 250, 500];
        this.paginationPageSize = 15;
        this.paginationCurrentPage = 1;
        this.useExternalPagination = true;

        this.infiniteScrollCurrentPage = 1;
        this.infiniteScrollPageSize = 100;
    };

    return gridDefaultOptions;
}]);