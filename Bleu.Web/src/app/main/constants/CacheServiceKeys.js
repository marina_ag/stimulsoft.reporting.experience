angular.module('bleu.main').constant('CacheServiceKeys', {
    customersGroups: null,
    stores: null,
    roles: null,
    menuItemsCategories: null,
    commonModifiersGroups: null,
    taxes: null,
    discounts: null,
    summary: null,
    devices: null,
    customers: null,
    employees: null
});
