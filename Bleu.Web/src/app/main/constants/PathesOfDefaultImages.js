﻿angular.module('bleu.main').constant('PathesOfDefaultImages', {
    customer: 'src/assets/images/user.png',
    placeholder: 'src/assets/images/placeholder.png'
});