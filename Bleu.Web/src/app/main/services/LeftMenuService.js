﻿angular.module('bleu.main').factory('LeftMenuService', [
    'MenuSections',
    function (menuSections) {
        var
            setMenuSection = function (section, sectionLabel, menu) {
                var
                    menuSectionItems = menu.filter(function (menuItem) {
                        return menuItem.section == section;
                    }),
                    menuSectionItem = null;

                if (menuSectionItems.length === 0) {
                    menuSectionItem = {
                        section: section,
                        label: sectionLabel,
                        items: []
                    };
                    menu.push(menuSectionItem);
                } else {
                    menuSectionItem = menuSectionItems[0];
                }

                return menuSectionItem;
            },
            addMenuItem = function (section, sectionLabel, stateItem, menu) {
                var menuSectionItem = setMenuSection(section, sectionLabel, menu);
                menuSectionItem.items.push({
                    name: stateItem.name,
                    label: stateItem.menu.item,
                    order: stateItem.menu.order
                });
            },
            sortMenuItems = function(menu) {
                menu.forEach(function(section) {
                    section.items.sort(function(a, b) {
                        return a.order - b.order;
                    });
                });
                menu.sort(function (a, b) {
                    return a.section - b.section;
                });
            },
            buildMenu = function (states) {
                var menu = [];
                states.forEach(function (stateItem) {
                    if (stateItem.menu != null) {
                        switch (stateItem.menu.section) {
                            case menuSections.summary:
                                addMenuItem(menuSections.summary, 'Main.LeftMenu_SectionLabelSummary', stateItem, menu);
                                break;
                            case menuSections.customers:
                                addMenuItem(menuSections.customers, 'Main.LeftMenu_SectionLabelCustomers', stateItem, menu);
                                break;
                            case menuSections.itemsList:
                                addMenuItem(menuSections.itemsList, 'Main.LeftMenu_SectionLabelItemsList', stateItem, menu);
                                break;
                            case menuSections.settings:
                                addMenuItem(menuSections.settings, 'Main.LeftMenu_SectionLabelSettings', stateItem, menu);
                                break;
                            case menuSections.reports:
                                addMenuItem(menuSections.reports, 'Main.LeftMenu_SectionLabelReports', stateItem, menu);
                                break;
                            default:
                                throw new Error('Unknown menu section');
                        }
                    }
                });
                sortMenuItems(menu);
                return menu;
            };
        return {
            buildMenu: buildMenu
        }
    }
]);
