﻿angular.module('bleu.main').constant('MenuSections', {
    summary: 0,
    customers: 1,
    itemsList: 2,
    settings: 3,
    reports: 4
});