angular.module('bleu.main').factory('ErrorStatusHandlerHelper', [
    '$rootScope',
    '$state',
    'localStorageService',
    'MessageBoxHelper',
    'NotFoundMsgBoxModel',
    function ($rootScope, $state, localStorageService, messageBoxHelper, notFoundMsgBoxModel) {
        var handleErrorStatus = function (status) {
            switch (status) {
                case 401:
                    localStorageService.cookie.remove('Employee');
                    window.location = '/Account/Login?ReturnUrl=' + window.location.hash;
                case 403:
                    break;
                case 404:
                    messageBoxHelper.openModal(new notFoundMsgBoxModel()).then(function (value) {
                        if (value == 'back') {
                            $state.goBack();
                        } else if (value == 'home') {
                            $state.goHome();
                        }
                    });
                    break;
                default:
            }
        };

        return {
            handleErrorStatus: handleErrorStatus
        }
    }
]);
