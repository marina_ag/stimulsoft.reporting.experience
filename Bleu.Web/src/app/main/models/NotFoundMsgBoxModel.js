﻿angular.module('bleu.main').factory('NotFoundMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var notFoundMsgBoxModel = function () {
            this.title = localizationService.getLocalizedString('Main.NotFoundDlg_Title');
            this.message = localizationService.getLocalizedString('Main.NotFoundDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('Main.NotFoundDlg_BtnTextBack'),
                    value: 'back',
                    icon: 'icon-arrow'
                },
                {
                    text: localizationService.getLocalizedString('Main.NotFoundDlg_BtnTextHome'),
                    value: 'home',
                    icon: 'icon-checkmark'
                }
            ];
            this.notCloseable = true;
        };

        return notFoundMsgBoxModel;
    }
]);