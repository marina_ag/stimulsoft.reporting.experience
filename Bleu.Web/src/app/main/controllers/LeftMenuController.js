﻿angular.module('bleu.main').controller('LeftMenuController', [
    '$scope',
    '$state',
    'LeftMenuService',
    function ($scope, $state, leftMenuService) {
        var
            init = function () {
                $scope.menu = leftMenuService.buildMenu($state.get());
            };

        $scope.menu = [];

        init();
    }
]);
