﻿angular.module('bleu.main', [
    'core.main',
    'ui.router',
    'ui.grid',
    'ui.grid.pagination',
    'ui.grid.selection',
    'ui.grid.resizeColumns',
    'ui.grid.infiniteScroll',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker',
    'ui.sortable',
    'ui.tree',
    'ngTagsInput',
    'ngFileUpload',
    'LocalStorageModule'
]).config(['$stateProvider', 'localStorageServiceProvider', 'tagsInputConfigProvider', function ($stateProvider, localStorageServiceProvider, tagsInputConfigProvider) {
    localStorageServiceProvider.setPrefix('bleu');

    tagsInputConfigProvider.setDefaults('tagsInput', {
        addFromAutocompleteOnly: true
    });
    tagsInputConfigProvider.setDefaults('autoComplete', {
        minLength: 1,
        loadOnFocus: true,
        loadOnEmpty: true
    });

    $stateProvider
        .state({
            name: 'root',
            abstract: true,
            views: {
                'header-view': {
                    controller: 'HeaderController',
                    templateUrl: 'src/app/main/views/HeaderView.html'
                },
                'menu-view': {
                    controller: 'LeftMenuController',
                    templateUrl: 'src/app/main/views/LeftMenuView.html'
                },
                'content-wrapper-view': {
                    templateUrl: 'src/app/main/views/ContentWrapperView.html'
                }
            }
        });
}]).run(['StateExtension', function (stateExtension) {
    stateExtension.init();
}]);
