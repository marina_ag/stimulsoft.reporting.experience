﻿angular.module('bleu.settings', [
    'bleu.main',
    'ui.router'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {

    $stateProvider
        .state({
            name: 'root.settingsSystem',
            url: '/systemsettings',
            views: {
                'content-view': {
                    templateUrl: 'src/app/settings/views/SystemSettingsView.html',
                    controller: 'SystemSettingsController'
                }
            },
            menu: {
                section: menuSections.settings,
                item: 'Main.LeftMenu_MenuItemSystemSettings',
                order: 80
            }
        });
}]).run();