﻿angular.module('bleu.roles.shared').factory('RolesSharedService', [
    'RolesSharedTransport',
    'RolesSharedMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (rolesSharedTransport, rolesSharedMapper, requestHandlerHelper, cacheService) {
        var
            getRoles = function () {
                return rolesSharedTransport.getAllRoles().then(function (response) {
                    return rolesSharedMapper.mapCollectionToClient(response);
                });
            },
            getAllRoles = function () {
                return requestHandlerHelper.handleRequest(cacheService.cache('roles', getRoles));
            },
            getRolesByName = function (key) {
                return getAllRoles().then(function (response) {
                    return response.filter(function (item) {
                        return item.name.toLowerCase().indexOf(key.toLowerCase()) != -1;
                    });
                });
            };

        return {
            getAllRoles: getAllRoles,
            getRolesByName: getRolesByName
        }
    }
]);