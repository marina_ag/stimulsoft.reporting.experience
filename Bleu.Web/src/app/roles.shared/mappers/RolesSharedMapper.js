﻿angular.module('bleu.roles.shared').factory('RolesSharedMapper', [
    'RoleSharedViewModel',
    function (roleSharedViewModel) {
        var
            transformToClient = function(response) {
                var role = new roleSharedViewModel();

                role.id = response.Id;
                role.name = response.Name;
                role.usersQty = response.EmployeesReferences;
                role.storesIds = response.StoresId;

                return role;
            },
            transformToServer = function (request) {
                return {
                    Id: request.id,
                    Name: request.name,
                    StoresId: request.storesIds
                }
            },
            mapCollectionToClient = function (response) {
                return response.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (response) {
                return transformToClient(response);
            },
            mapCollectionToServer = function (request) {
                return request.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            }

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);