﻿angular.module('bleu.roles.shared').factory('RoleSharedViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var roleSharedViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.name = '';
            this.usersQty = 0;
            this.storesIds = [];
        }

        roleSharedViewModel.prototype = Object.create(baseViewModel.prototype);

        return roleSharedViewModel;
    }
]);