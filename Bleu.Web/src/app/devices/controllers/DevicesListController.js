﻿angular.module('bleu.devices').controller('DevicesListController', [
    '$scope',
    '$state',
    'DevicesGridDataModel',
    'DevicesGridOptionsModel',
    'GridDataHelper',
    'DevicesService',
    function (
        $scope,
        $state,
        devicesGridDataModel,
        devicesGridOptionsModel,
        gridDataHelper,
        devicesService) {
        var 
            gridOptions = new devicesGridOptionsModel(),
            gridHelper = new gridDataHelper(gridOptions),
            addDevice = function () {
                $state.go('root.settingsAddDevice');
            },
            setActiveState = function (device) {
                devicesService.updateDevice(device);
            },
            init = function () {
                
                gridHelper.init({
                    fields: devicesGridDataModel.fields,
                    fieldsForSelectAll: devicesGridDataModel.fieldsForSelectAll,
                    primaryFields: devicesGridDataModel.primaryFields,
                    sortMapping: devicesGridDataModel.sortMapping,
                    grid: $scope,
                    getDataMethod: devicesService.getDevices,
                });
            };

        $scope.gridOptions = gridOptions;
        $scope.addDevice = addDevice;
        $scope.setActiveState = setActiveState;

        init();
    }
]);
