﻿angular.module('bleu.devices').controller('DeviceController', [
    '$scope',
    '$state',
    '$stateParams',
    'DevicesService',
    'DeviceViewModel',
    'DevicesViewService',
    'StoresSharedService',
    function ($scope, $state, $stateParams, devicesService, deviceViewModel, devicesViewService, storesSharedService) {
        var
            stores = [],
            device = new deviceViewModel(),
            updateDevice = function () {
                $scope.device.$validate().then(function () {
                    devicesService.updateDevice($scope.device).then(function () {
                        $state.goBack('root.settingsDevicesList');
                    }, function (validationInfo) {
                        devicesViewService.validateDevice(validationInfo, $scope.device);
                    });
                });
            },
            cancel = function () {
                $state.goBack('root.settingsDevicesList');
            },
            init = function () {
                storesSharedService.getAllStores().then(function (s) {
                    $scope.stores = s;
                });

                devicesService.getDevice($stateParams.id).then(function (s) {
                    $scope.device = s;
                });
            };

        $scope.device = device;
        $scope.stores = stores;
        $scope.updateDevice = updateDevice;
        $scope.cancel = cancel;

        init();
    }
]);