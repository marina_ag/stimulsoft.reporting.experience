﻿angular.module('bleu.devices').controller('AddDeviceController', [
    '$scope',
    '$state',
    'DevicesService',
    'DeviceViewModel',
    'DevicesViewService',
    'StoresSharedService',
    function ($scope, $state, devicesService, deviceViewModel, devicesViewService, storesSharedService) {
        var
            stores = [],
            saveDevice = function () {
                $scope.device.$validate().then(function () {
                    devicesService.addDevice($scope.device).then(function () {
                        $state.goBack('root.settingsDevicesList');
                    },
                    function (validationInfo) {
                        devicesViewService.validateDevice(validationInfo, $scope.device);
                    });
                });
            },
            cancel = function () {
                $state.goBack('root.settingsDevicesList');
            },
            init = function () {
                storesSharedService.getAllStores().then(function (s) {
                    $scope.stores = s;
                });
            };

        

        $scope.device = new deviceViewModel();
        $scope.saveDevice = saveDevice;
        $scope.cancel = cancel;
        $scope.stores = stores;
        $scope.init = init;

        init();
    }
]);
