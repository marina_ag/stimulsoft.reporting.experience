﻿angular.module('bleu.devices').factory('DevicesMapper', [
    'DeviceViewModel',
    function (deviceViewModel) {
        var
            transformToClient = function (data) {
                var item = new deviceViewModel();

                item.id = data.TerminalId;
                item.storeId = data.StoreId;
                item.name = data.Name;
                item.storeName = data.StoreName;
                item.isConfirmed = data.IsConfirmed;

                return item;
            },
            transformToServer = function (request) {
                return {
                    TerminalId: request.id,
                    Name: request.name,
                    StoreId: request.storeId,
                    IsConfirmed: request.isConfirmed
                };
            },
            mapCollectionToClient = function (data) {
                return data.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            },
            mapInstanceToClient = function (response) {
                return transformToClient(response);
            };

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToServer: mapInstanceToServer,
            mapInstanceToClient: mapInstanceToClient
        };
    }
]);