﻿angular.module('bleu.devices').factory('DevicesGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var devicesGridViewModel = function () {
            this.paginationPageSize = 500;
            this.columnDefs = [
                //{
                //    field: 'id',
                //    displayName: localizationService.getLocalizedString('Devices.DevicesListPage_DevicesGridId'),
                //    type: 'string',
                //    cellTemplate: 'src/app/devices/views/grid/DevicesGridIdTemplate.html'
                //},
                {
                    field: 'name',
                    displayName: localizationService.getLocalizedString('Devices.DevicesListPage_DevicesGridName'),
                    type: 'string',
                    cellTemplate: 'src/app/devices/views/grid/DevicesGridIdTemplate.html'
                },
                {
                    field: 'storeName',
                    displayName: localizationService.getLocalizedString('Devices.DevicesListPage_DevicesGridStore'),
                    type: 'string',
                },
                {
                    field: 'isConfirmed',
                    displayName: localizationService.getLocalizedString('Devices.DevicesListPage_DevicesGridIsConfirmed'),
                    type: 'string',
                    cellTemplate: 'src/app/devices/views/grid/DevicesGridIsConfirmedTemplate.html'
                }
            ];
        };

        angular.extend(devicesGridViewModel.prototype, new gridDefaultOptions());

        return devicesGridViewModel;
    }
]);