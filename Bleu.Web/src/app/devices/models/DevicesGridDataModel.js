﻿angular.module('bleu.devices').value('DevicesGridDataModel', {
    fields: [
        //'TerminalId',
        'Name',
        'StoreId',
        'StoreName',
        'IsConfirmed'
    ],
    fieldsForSelectAll: ['TerminalId'],
    //primaryFields: ['id'],
    sortMapping: [
        { column: 'name', data: ['Name'] },
        { column: 'storeName', data: ['StoreName'] }
    ]
});