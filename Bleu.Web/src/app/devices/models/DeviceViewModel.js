﻿angular.module('bleu.devices').factory('DeviceViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var deviceViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.storeId = null;
            this.name = '';
            this.storeName = '';
            this.isConfirmed = false;
        };

        deviceViewModel.prototype = Object.create(baseViewModel.prototype);

        return deviceViewModel;
    }
]);