﻿angular.module('bleu.devices').factory('DevicesService', [
    'FakeTransport',
    'RequestHandlerHelper',
    'DevicesMapper',
    'DevicesTransport',
    'CacheService',
    function (fakeTransport, requestHandlerHelper, devicesMapper, devicesTransport,cacheService) {
        var
            getDevices = function(/*filter*/) {
                //TO DO: add filters
                return requestHandlerHelper.handleRequest(devicesTransport.getTerminals()).then(function (data) {
                    return {
                        Results: devicesMapper.mapCollectionToClient(data),
                        Count: data.length
                    };
                });
            },
            addDevice = function (device) {
                return requestHandlerHelper.handleRequest(devicesTransport.addTerminalToStore(devicesMapper.mapInstanceToServer(device))).then(function () {
                    cacheService.clear('devices');
                });
            },
            updateDevice = function (device) {
                return requestHandlerHelper.handleRequest(devicesTransport.updateTerminal(devicesMapper.mapInstanceToServer(device))).then(function () {
                    cacheService.clear('devices');
                });
            },
            getDevice = function (id) {
                return requestHandlerHelper.handleRequest(devicesTransport.getTerminal(id)).then(function (response) {
                    return devicesMapper.mapInstanceToClient(response);
                });
            },
            getDevicesByName = function (key) {
                return getDevices().then(function (response) {
                    return response.Results.filter(function (item) {
                        if (item.name != null) return item.name.toLowerCase().indexOf(key.toLowerCase()) != -1;
                        return '';
                    });
                });
            };


        return {
            getDevices: getDevices,
            addDevice: addDevice,
            updateDevice: updateDevice,
            getDevice: getDevice,
            getDevicesByName: getDevicesByName
        };
    }
]);
