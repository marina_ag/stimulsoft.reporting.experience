﻿angular.module('bleu.devices').factory('DevicesViewService', [
    function () {
        var
           validateDevice = function (validationInfo, model) {
               if (validationInfo != null && validationInfo.id != null) {
                   model.$setValidity('id', 'exists', false);
               }
               if (validationInfo != null && validationInfo.storeId != null) {
                   model.$setValidity('storeId', 'exists', false);
               }
           };

        return {
            validateDevice: validateDevice
        };
    }
]);