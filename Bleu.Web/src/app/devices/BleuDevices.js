﻿angular.module('bleu.devices', [
    'core.devices',
    'bleu.main',
    'ui.router'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {

    $stateProvider
        .state({
            name: 'root.settingsDevicesList',
            url: '/devices',
            views: {
                'content-view': {
                    templateUrl: 'src/app/devices/views/DevicesListView.html',
                    controller: 'DevicesListController'
                }
            },
            menu: {
                section: menuSections.settings,
                item: 'Main.LeftMenu_MenuItemDevices',
                order: 40
            }
        })
        .state({
            name: 'root.settingsAddDevice',
            url: '/adddevice',
            views: {
                'content-view': {
                    templateUrl: 'src/app/devices/views/AddDeviceView.html',
                    controller: 'AddDeviceController'
                }
            }
        })
            .state({
                name: 'root.settingsDevice',
                url: '/device/:id',
                views: {
                    'content-view': {
                        templateUrl: 'src/app/devices/views/DeviceView.html',
                        controller: 'DeviceController'
                    }
                }
            });
}]).run();