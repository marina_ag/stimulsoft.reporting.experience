﻿angular.module('bleu.roles').factory('RemoveRolesMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeStoresModel = function () {
            this.title = localizationService.getLocalizedString('Roles.RolesListPageRemoveRolesDlg_Title');
            this.message = localizationService.getLocalizedString('Roles.RolesListPageRemoveRolesDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('Roles.RolesListPageRemoveRolesDlg_BtnTextOk'),
                    value: 'ok'
                },
                {
                    text: localizationService.getLocalizedString('Roles.RolesListPageRemoveRolesDlg_BtnTextCancel'),
                    value: 'cancel'
                }
            ];
            this.successValue = 'ok';
        };

        return removeStoresModel;
    }
]);