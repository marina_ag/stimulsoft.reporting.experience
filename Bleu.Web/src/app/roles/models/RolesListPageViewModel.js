﻿angular.module('bleu.roles').factory('RolesListPageViewModel', [
    function () {
        var rolesListPageViewModel = function () {
            this.canDelete = false;
        };

        return rolesListPageViewModel;
    }
]);