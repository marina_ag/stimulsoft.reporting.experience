﻿angular.module('bleu.roles').factory('RolesGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var rolesGridOptionsModel = function () {
            this.useExternalSorting = false;
            this.columnDefs = [
                {
                    field: 'name',
                    displayName: localizationService.getLocalizedString('Roles.RolesGrid_HeaderName'),
                    type: 'string',
                    cellTemplate: 'src/app/roles/views/grid/RolesGridNameTemplate.html'
                },
                {
                    field: 'usersQty',
                    displayName: localizationService.getLocalizedString('Roles.RolesGrid_HeaderUsersQty'),
                    type: 'string',
                    cellTemplate: 'src/app/roles/views/grid/RolesGridUsersQtyTemplate.html'
                }
            ];
        };

        angular.extend(rolesGridOptionsModel.prototype, new gridDefaultOptions());

        return rolesGridOptionsModel;
    }
]);