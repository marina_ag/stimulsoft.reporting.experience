﻿angular.module('bleu.roles', [
    'core.roles',
    'bleu.roles.shared',
    'bleu.main',
    'ui.router'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {

    $stateProvider
        .state({
            name: 'root.settingsRolesList',
            url: '/roles',
            views: {
                'content-view': {
                    templateUrl: 'src/app/roles/views/RolesListView.html',
                    controller: 'RolesListController'
                }
            },
            menu: {
                section: menuSections.settings,
                item: 'Main.LeftMenu_MenuItemRoles',
                order: 30
            }
        })
        .state({
            name: 'root.settingsAddRole',
            url: '/addrole',
            views: {
                'content-view': {
                    templateUrl: 'src/app/roles/views/AddRoleView.html',
                    controller: 'AddRoleController'
                }
            }
        })
        .state({
            name: 'root.settingsRole',
            url: '/role/:id',
            views: {
                'content-view': {
                    templateUrl: 'src/app/roles/views/RoleView.html',
                    controller: 'RoleController'
                }
            }
        });
}]).run();