﻿angular.module('bleu.roles').factory('RolesService', [
    'RolesTransport',
    'RolesSharedMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (rolesTransport, rolesSharedMapper, requestHandlerHelper, cacheService) {
        var
            getRole = function (id) {
                return requestHandlerHelper.handleRequest(rolesTransport.getRole(id)).then(function (response) {
                    return rolesSharedMapper.mapInstanceToClient(response);
                });
            },
            updateRole = function (role) {
                return requestHandlerHelper.handleRequest(rolesTransport.updateRole(rolesSharedMapper.mapInstanceToServer(role))).then(function () {
                    cacheService.clear('roles');
                });
            },
            addRole = function (role) {
                return requestHandlerHelper.handleRequest(rolesTransport.addRole(rolesSharedMapper.mapInstanceToServer(role))).then(function (roleId) {
                    cacheService.clear('roles');
                    return roleId;
                });
            },
            removeRoles = function (rolesIds) {
                return requestHandlerHelper.handleRequest(rolesTransport.removeRoles(rolesIds)).then(function () {
                    cacheService.clear('roles');
                });
            };

        return {
            getRole: getRole,
            updateRole: updateRole,
            addRole: addRole,
            removeRoles: removeRoles
        }
    }
]);