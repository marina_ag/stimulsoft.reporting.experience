﻿angular.module('bleu.roles').factory('RolesViewService', [
    function () {
        var
            validateRole = function (validationInfo, model) {
                if (validationInfo != null && validationInfo.Name != null) {
                    model.$setValidity('name', 'exists', false);
                }
            };

        return {
            validateRole: validateRole
        }
    }
]);