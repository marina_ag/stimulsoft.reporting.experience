﻿angular.module('bleu.roles').factory('PermissionsService', [
    'FakeTransport',
    'RequestHandlerHelper',
    function (fakeTransport, requestHandlerHelper) {
        var
            fakePermissions = [
                {
                    title: 'POS Terminal',
                    modules: [
                        {
                            title: 'Ordering'
                        },
                        {
                            title: 'Checkout'
                        },
                        {
                            title: 'Orders list'
                        },
                        {
                            title: 'Closed Orders'
                        },
                        {
                            title: 'Cash Drawer'
                        },
                        {
                            title: 'Customers'
                        },
                        {
                            title: 'Reports'
                        },
                        {
                            title: 'Settings'
                        }
                    ]
                },
                {
                    title: 'Back Office',
                    modules: [
                        {
                            title: 'Items editor'
                        },
                        {
                            title: 'Customers'
                        },
                        {
                            title: 'Settings'
                        },
                        {
                            title: 'Reports'
                        }
                    ]
                }
            ],
            getPermissions = function () {
                return requestHandlerHelper.handleRequest(fakeTransport.request('Get permissions', fakePermissions)).then(function (response) {
                    return response;
                });
            };

        return {
            getPermissions: getPermissions
        }
    }
]);