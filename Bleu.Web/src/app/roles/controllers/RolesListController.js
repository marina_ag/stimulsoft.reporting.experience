﻿angular.module('bleu.roles').controller('RolesListController', [
    '$scope',
    '$state',
    'RolesService',
    'RolesSharedService',
    'SessionService',
    'RolesGridOptionsModel',
    'RolesListPageViewModel',
    'RemoveRolesMsgBoxModel',
    'UsersListPageViewModel',
    'ClientGridDataHelper',
    'MessageBoxHelper',
    function (
        $scope,
        $state,
        rolesService,
        rolesSharedService,
        sessionService,
        rolesGridOptionsModel,
        rolesListPageViewModel,
        removeRolesMsgBoxModel,
        usersListPageViewModel,
        clientGridDataHelper,
        messageBoxHelper) {
        var
            gridOptions = new rolesGridOptionsModel(),
            viewModel = new rolesListPageViewModel(),
            gridHelper = new clientGridDataHelper(gridOptions),
            onSelectionChanged = function (selection) {
                var hasSelection = selection.length != 0;
                viewModel.canDelete = hasSelection;
            },
            getRolesIds = function () {
                return gridHelper.getSelection().map(function (item) {
                    return item.id;
                });
            },
            addRole = function () {
                $state.go('root.settingsAddRole');
            },
            removeRoles = function () {
                messageBoxHelper.openModal(new removeRolesMsgBoxModel()).then(function () {
                    rolesService.removeRoles(getRolesIds()).then(function () {
                        gridHelper.reload();
                    });
                });
            },
            navigateToUsers = function (roleId) {
                var usersListPage = sessionService.set('usersListPageViewModel', new usersListPageViewModel(), true);
                usersListPage.roleFilter = roleId;
                $state.go('root.settingsUsersList');
            },
            init = function () {
                gridHelper.init({
                    grid: $scope,
                    getDataMethod: rolesSharedService.getAllRoles,
                    onSelectionChanged: onSelectionChanged
                });
            };

        $scope.addRole = addRole;
        $scope.removeRoles = removeRoles;
        $scope.gridOptions = gridOptions;
        $scope.viewModel = viewModel;
        $scope.navigateToUsers = navigateToUsers;

        init();
    }
]);