﻿angular.module('bleu.roles').controller('RoleController', [
    '$scope',
    '$state',
    '$stateParams',
    'RolesService',
    'RolesViewService',
    'PermissionsService',
    'RoleSharedViewModel',
    function ($scope, $state, $stateParams, rolesService, rolesViewService, permissionsService, roleSharedViewModel) {
        var role = new roleSharedViewModel(),
            permissions = [],
            updateRole = function () {
                $scope.role.$validate().then(function () {
                    rolesService.updateRole($scope.role).then(function () {
                        $state.goBack('root.settingsRolesList');
                    }, function (validationInfo) {
                        rolesViewService.validateRole(validationInfo, $scope.role);
                    });
                });
            },
            cancel = function () {
                $state.goBack('root.settingsRolesList');
            },
            init = function () {
                rolesService.getRole($stateParams.id).then(function (r) {
                    $scope.role = r;
                });

                permissionsService.getPermissions().then(function (permissionsList) {
                    $scope.permissions = permissionsList;
                });
            };

        $scope.role = role;
        $scope.permissions = permissions;
        $scope.updateRole = updateRole;
        $scope.cancel = cancel;

        init();
    }
]);