﻿angular.module('bleu.roles').controller('AddRoleController', [
    '$scope',
    '$state',
    'RolesService',
    'RolesViewService',
    'PermissionsService',
    'RoleSharedViewModel',
    function ($scope, $state, rolesService, rolesViewService , permissionsService, roleSharedViewModel) {
        var permissions = [],
            role = new roleSharedViewModel(),
            saveRole = function () {
                $scope.role.$validate().then(function () {
                    rolesService.addRole($scope.role).then(function () {
                        $state.goBack('root.settingsRolesList');
                    }, function (validationInfo) {
                        rolesViewService.validateRole(validationInfo, $scope.role);
                    });
                });
            },
            cancel = function () {
                $state.goBack('root.settingsRolesList');
            },
            init = function () {
                permissionsService.getPermissions().then(function (permissionsList) {
                    $scope.permissions = permissionsList;
                });
            };

        $scope.role = role;
        $scope.permissions = permissions;
        $scope.saveRole = saveRole;
        $scope.cancel = cancel;

        init();
    }
]);