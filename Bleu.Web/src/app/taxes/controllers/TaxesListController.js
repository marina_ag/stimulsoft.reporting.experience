﻿angular.module('bleu.taxes').controller('TaxesListController', [
    '$scope',
    '$state',
    'TaxesService',
    'TaxesSharedService',
    'TaxesGridOptionsModel',
    'TaxesListPageViewModel',
    'RemoveTaxesMsgBoxModel',
    'ClientGridDataHelper',
    'MessageBoxHelper',
    function (
        $scope,
        $state,
        taxesService,
        taxesSharedService,
        taxesGridOptionsModel,
        taxesListPageViewModel,
        removeTaxesMsgBoxModel,
        clientGridDataHelper,
        messageBoxHelper) {
        var
            gridOptions = new taxesGridOptionsModel(),
            viewModel = new taxesListPageViewModel(),
            gridHelper = new clientGridDataHelper(gridOptions),
            onSelectionChanged = function (selection) {
                var hasSelection = selection.length != 0;
                viewModel.canDelete = hasSelection;
            },
            getTaxesIds = function () {
                return gridHelper.getSelection().map(function (item) {
                    return item.id;
                });
            },
            addTax = function () {
                $state.go('root.settingsAddTax');
            },
            removeTaxes = function () {
                messageBoxHelper.openModal(new removeTaxesMsgBoxModel()).then(function () {
                    taxesService.removeTaxes(getTaxesIds()).then(function () {
                        gridHelper.reload();
                    });
                });
            },
            init = function () {
                gridHelper.init({
                    grid: $scope,
                    getDataMethod: taxesSharedService.getAllTaxes,
                    onSelectionChanged: onSelectionChanged
                });
            };

        $scope.addTax = addTax;
        $scope.removeTaxes = removeTaxes;
        $scope.gridOptions = gridOptions;
        $scope.viewModel = viewModel;

        init();
    }
]);