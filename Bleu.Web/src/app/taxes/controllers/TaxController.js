﻿angular.module('bleu.taxes').controller('TaxController', [
    '$scope',
    '$state',
    '$stateParams',
    'TaxesService',
    'TaxesViewService',
    'TaxSharedViewModel',
    function ($scope, $state, $stateParams, taxesService, taxesViewService, taxSharedViewModel) {
        var tax = new taxSharedViewModel(),
            updateTax = function () {
                $scope.tax.$validate().then(function () {
                    taxesService.updateTax($scope.tax).then(function () {
                        $state.goBack('root.settingsTaxesList');
                    }, function (validationInfo) {
                        taxesViewService.validateTax(validationInfo, $scope.tax);
                    });
                });
            },
            cancel = function () {
                $state.goBack('root.settingsTaxesList');
            },
            init = function () {
                taxesService.getTax($stateParams.id).then(function (t) {
                    $scope.tax = t;
                });
            };

        $scope.tax = tax;
        $scope.updateTax = updateTax;
        $scope.cancel = cancel;

        init();
    }
]);