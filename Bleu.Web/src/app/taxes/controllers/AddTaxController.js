﻿angular.module('bleu.taxes').controller('AddTaxController', [
    '$scope',
    '$state',
    'TaxesService',
    'TaxesViewService',
    'TaxSharedViewModel',
    function ($scope, $state, taxesService, taxesViewService, taxSharedViewModel) {
        var tax = new taxSharedViewModel(),
            saveTax = function () {
                $scope.tax.$validate().then(function () {
                    taxesService.addTax($scope.tax).then(function () {
                        $state.goBack('root.settingsTaxesList');
                    }, function (validationInfo) {
                        taxesViewService.validateTax(validationInfo, $scope.tax);
                    });
                });
            },
            cancel = function () {
                $state.goBack('root.settingsTaxesList');
            };

        $scope.tax = tax;
        $scope.saveTax = saveTax;
        $scope.cancel = cancel;
    }
]);