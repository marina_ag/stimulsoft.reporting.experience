﻿angular.module('bleu.taxes', [
    'core.taxes',
    'bleu.taxes.shared',
    'bleu.main',
    'ui.router'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {

    $stateProvider
        .state({
            name: 'root.settingsTaxesList',
            url: '/taxes',
            views: {
                'content-view': {
                    templateUrl: 'src/app/taxes/views/TaxesListView.html',
                    controller: 'TaxesListController'
                }
            },
            menu: {
                section: menuSections.settings,
                item: 'Main.LeftMenu_MenuItemTaxes',
                order: 60
            }
        })
        .state({
            name: 'root.settingsAddTax',
            url: '/addtax',
            views: {
                'content-view': {
                    templateUrl: 'src/app/taxes/views/AddTaxView.html',
                    controller: 'AddTaxController'
                }
            }
        })
        .state({
            name: 'root.settingsTax',
            url: '/tax/:id',
            views: {
                'content-view': {
                    templateUrl: 'src/app/taxes/views/TaxView.html',
                    controller: 'TaxController'
                }
            }
        });
}]).run();