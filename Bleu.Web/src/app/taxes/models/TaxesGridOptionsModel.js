﻿angular.module('bleu.settings').factory('TaxesGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var taxesGridOptionsModel = function () {
            this.useExternalSorting = false;
            this.columnDefs = [
                {
                    field: 'name',
                    displayName: localizationService.getLocalizedString('Taxes.TaxesGrid_HeaderName'),
                    type: 'string',
                    cellTemplate: 'src/app/taxes/views/grid/TaxesGridNameTemplate.html'
                }, {
                    field: 'value',
                    displayName: localizationService.getLocalizedString('Taxes.TaxesGrid_HeaderValue'),
                    type: 'number',
                    cellFilter: 'percentage'
                }, {
                    field: 'description',
                    displayName: localizationService.getLocalizedString('Taxes.TaxesGrid_HeaderDescription'),
                    type: 'string'
                }
            ];
        };

        angular.extend(taxesGridOptionsModel.prototype, new gridDefaultOptions());

        return taxesGridOptionsModel;
    }
]);