﻿angular.module('bleu.settings').factory('TaxesListPageViewModel', [
    function () {
        var taxesListPageViewModel = function () {
            this.canDelete = false;
        };

        return taxesListPageViewModel;
    }
]);