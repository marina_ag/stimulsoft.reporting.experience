﻿angular.module('bleu.settings').factory('RemoveTaxesMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeTaxesModel = function () {
            this.title = localizationService.getLocalizedString('Taxes.TaxesListPageRemoveTaxesDlg_Title');
            this.message = localizationService.getLocalizedString('Taxes.TaxesListPageRemoveTaxesDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('Taxes.TaxesListPageRemoveTaxesDlg_BtnTextOk'),
                    value: 'ok'
                },
                {
                    text: localizationService.getLocalizedString('Taxes.TaxesListPageRemoveTaxesDlg_BtnTextCancel'),
                    value: 'cancel'
                }
            ];
            this.successValue = 'ok';
        };

        return removeTaxesModel;
    }
]);