﻿angular.module('bleu.taxes').factory('TaxesService', [
    'TaxesTransport',
    'TaxesSharedMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (taxesTransport, taxesSharedMapper, requestHandlerHelper, cacheService) {
        var
            getTax = function (id) {
                return requestHandlerHelper.handleRequest(taxesTransport.getTax(id)).then(function (response) {
                    return taxesSharedMapper.mapInstanceToClient(response);
                });
            },
            updateTax = function (tax) {
                return requestHandlerHelper.handleRequest(taxesTransport.updateTax(taxesSharedMapper.mapInstanceToServer(tax))).then(function () {
                    cacheService.clear('taxes');
                });
            },
            addTax = function (tax) {
                return requestHandlerHelper.handleRequest(taxesTransport.addTax(taxesSharedMapper.mapInstanceToServer(tax))).then(function (taxId) {
                    cacheService.clear('taxes');
                    return taxId;
                });
            },
            removeTaxes = function (taxesIds) {
                return requestHandlerHelper.handleRequest(taxesTransport.removeTaxes(taxesIds)).then(function () {
                    cacheService.clear('taxes');
                });
            };

        return {
            getTax: getTax,
            updateTax: updateTax,
            addTax: addTax,
            removeTaxes: removeTaxes
        }
    }
]);