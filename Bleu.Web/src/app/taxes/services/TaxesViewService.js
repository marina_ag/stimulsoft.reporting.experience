﻿angular.module('bleu.taxes').factory('TaxesViewService', [
    function () {
        var
            validateTax = function (validationInfo, model) {
                if (validationInfo != null && validationInfo.Name != null) {
                    model.$setValidity('name', 'exists', false);
                }
            };

        return {
            validateTax: validateTax
        }
    }
]);