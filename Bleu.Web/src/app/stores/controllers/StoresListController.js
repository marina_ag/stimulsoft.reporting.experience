﻿angular.module('bleu.stores').controller('StoresListController', [
    '$scope',
    '$state',
    'StoresService',
    'StoresSharedService',
    'StoresGridOptionsModel',
    'StoresListPageViewModel',
    'RemoveStoresMsgBoxModel',
    'ClientGridDataHelper',
    'MessageBoxHelper',
    'FilterCompareMethod',
    'FilterRelationMethod',
    'StoresViewService',
    function (
        $scope,
        $state,
        storesService,
        storesSharedService,
        storesGridOptionsModel,
        storesListPageViewModel,
        removeStoresMsgBoxModel,
        clientGridDataHelper,
        messageBoxHelper,
        filterCompareMethod,
        filterRelationMethod,
        storesViewService) {
        var
            gridOptions = new storesGridOptionsModel(),
            viewModel = new storesListPageViewModel(),
            gridHelper = new clientGridDataHelper(gridOptions),
            onSelectionChanged = function (selection) {
                var hasSelection = selection.length != 0;
                viewModel.canDelete = hasSelection;
            },
            getStoresIds = function () {
                return gridHelper.getSelection().map(function (item) {
                    return item.id;
                });
            },
            filterChange = function () {
                gridHelper.updateFilters([
                    { Selector: 'name', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.Or },
                    { Selector: 'state', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.Or },
                    { Selector: 'city', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.Or },
                    { Selector: 'address', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.Or },
                    { Selector: 'phoneNumber', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.And }
                ]);
            },
            addStore = function () {
                $state.go('root.settingsAddStore');
            },
            removeStores = function () {
                messageBoxHelper.openModal(new removeStoresMsgBoxModel()).then(function () {
                    storesService.removeStores(getStoresIds()).then(function () {
                        gridHelper.reload();
                    });
                });
            },
            setActiveState = function (id, isActive) {
                storesService.setActiveState(id, isActive).then(function () {
                    gridHelper.reload();
                });
            },
            getBusinessTypeName = function (id) {
                return storesViewService.getBusinessTypeName(id);
            },
            init = function () {
                gridHelper.init({
                    grid: $scope,
                    getDataMethod: storesSharedService.getAllStores,
                    onSelectionChanged: onSelectionChanged
                });
            };

        $scope.addStore = addStore;
        $scope.removeStores = removeStores;
        $scope.setActiveState = setActiveState;
        $scope.gridOptions = gridOptions;
        $scope.filterChange = filterChange;
        $scope.viewModel = viewModel;
        $scope.getBusinessTypeName = getBusinessTypeName;

        init();
    }
]);