﻿angular.module('bleu.stores').controller('AddStoreController', [
    '$scope',
    '$state',
    '$modal',
    'SessionService',
    'StoresService',
    'StoresViewService',
    'StoreSharedViewModel',
    'OpeningTimeDialogOptions',
    'OpeningTimeSharedViewModel',
    'BusinessType',
    function ($scope,
        $state,
        $modal,
        sessionService,
        storesService,
        storesViewService,
        storeSharedViewModel,
        openingTimeDialogOptions,
        openingTimeSharedViewModel,
        businessType) {
        var
            store = new storeSharedViewModel(),
            localizedBusinessTypes = storesViewService.getBusinessTypes(),
            openingTimeString = 'Opening time',
            saveStore = function () {
                $scope.store.$validate().then(function () {
                    storesService.addStore($scope.store).then(function () {
                        $state.goBack('root.settingsStoresList');
                    }, function (validationInfo) {
                        storesViewService.validateStore(validationInfo, $scope.store);
                    });
                });
            },
            showOpeningTime = function() {
                $modal.open(openingTimeDialogOptions).result.then(function () {
                    $scope.store.openingTime = angular.copy(sessionService.get('openingTimeSharedViewModel'));
                    $scope.openingTimeString = constructViewValueFor($scope.store.openingTime);
                });
            },
            cancel = function () {
                $state.goBack('root.settingsStoresList');
            },
            constructViewValueFor = function (openingTime) {
                var viewValue = '';
                viewValue += openingTime.mondayIsAvailable ? 'Mon. ' : '';
                viewValue += openingTime.tuesdayIsAvailable ? 'Tue. ' : '';
                viewValue += openingTime.wednesdayIsAvailable ? 'Wed. ' : '';
                viewValue += openingTime.thursdayIsAvailable ? 'Thu. ' : '';
                viewValue += openingTime.fridayIsAvailable ? 'Fri. ' : '';
                viewValue += openingTime.saturdayIsAvailable ? 'Sat. ' : '';
                viewValue += openingTime.sundayIsAvailable ? 'Sun. ' : '';
                return viewValue == '' ? 'Opening Time' : viewValue;
            },
            init = function () {
                $scope.store = new storeSharedViewModel();
                sessionService.set('openingTimeSharedViewModel', new openingTimeSharedViewModel(),true);
                $scope.openingTimeString = constructViewValueFor($scope.store.openingTime);
            };

        $scope.store = store;
        $scope.saveStore = saveStore;
        $scope.cancel = cancel;
        $scope.showOpeningTime = showOpeningTime;
        $scope.openingTimeString = openingTimeString;
        $scope.businessTypes = businessType;
        $scope.localizedBusinessTypes = localizedBusinessTypes;

        init();
    }
]);
