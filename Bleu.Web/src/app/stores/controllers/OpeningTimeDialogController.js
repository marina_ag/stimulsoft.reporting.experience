﻿angular.module('bleu.customers').controller('OpeningTimeDialogController', [
    '$scope',
    '$modalInstance',
    'SessionService',
    'OpeningTimeSharedViewModel',
    function (
        $scope,
        $modalInstance,
        sessionService,
        openingTimeSharedViewModel) {
        var
            openingTime = new openingTimeSharedViewModel(),

            ok = function () {
                $scope.openingTime.$validate().then(function () {
                    sessionService.set('openingTimeSharedViewModel', $scope.openingTime, true);
                    $modalInstance.close();
                });
            },
            cancel = function () {
                $modalInstance.dismiss();
            },
            init = function() {
                $scope.openingTime = angular.copy(sessionService.get('openingTimeSharedViewModel'));
            };

        $scope.openingTime = openingTime;
        $scope.ok = ok;
        $scope.cancel = cancel;

        init();
    }
]);
