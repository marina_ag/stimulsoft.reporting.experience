﻿angular.module('bleu.stores').controller('StoreController', [
    '$scope',
    '$state',
    '$stateParams',
    '$modal',
    'StoresService',
    'StoresViewService',
    'SessionService',
    'StoreSharedViewModel',
    'OpeningTimeSharedViewModel',
    'OpeningTimeDialogOptions',
    'BusinessType',
    function ($scope,
        $state,
        $stateParams,
        $modal,
        storesService,
        storesViewService,
        sessionService,
        storeSharedViewModel,
        openingTimeSharedViewModel,
        openingTimeDialogOptions,
        businessType) {
        var store = new storeSharedViewModel(),
            localizedBusinessTypes = storesViewService.getBusinessTypes(),
            openingTimeString = 'Opening time',
            updateStore = function () {
                $scope.store.$validate().then(function () {
                    storesService.updateStore($scope.store).then(function () {
                        $state.goBack('root.settingsStoresList');
                    }, function (validationInfo) {
                        storesViewService.validateStore(validationInfo, $scope.store);
                    });
                });
            },
            cancel = function () {
                $state.goBack('root.settingsStoresList');
            },
            showOpeningTime = function() {
                $modal.open(openingTimeDialogOptions).result.then(function () {
                    $scope.store.openingTime = angular.copy(sessionService.get('openingTimeSharedViewModel'));
                    $scope.openingTimeString = constructViewValueFor($scope.store.openingTime);
                });
            },
            init = function () {
                storesService.getStore($stateParams.id).then(function (s) {
                    $scope.store = s;
                    sessionService.set('openingTimeSharedViewModel', s.openingTime,true);
                    $scope.openingTimeString = constructViewValueFor($scope.store.openingTime);
                });
            },
            constructViewValueFor = function (openingTime) {
                var viewValue = '';
                viewValue += openingTime.mondayIsAvailable ? 'Mon. ' : '';
                viewValue += openingTime.tuesdayIsAvailable ? 'Tue. ' : '';
                viewValue += openingTime.wednesdayIsAvailable ? 'Wed. ' : '';
                viewValue += openingTime.thursdayIsAvailable ? 'Thu. ' : '';
                viewValue += openingTime.fridayIsAvailable ? 'Fri. ' : '';
                viewValue += openingTime.saturdayIsAvailable ? 'Sat. ' : '';
                viewValue += openingTime.sundayIsAvailable ? 'Sun. ' : '';
                return viewValue == '' ? 'Opening Time' : viewValue;
            };

        $scope.store = store;
        $scope.updateStore = updateStore;
        $scope.cancel = cancel;
        $scope.showOpeningTime = showOpeningTime;
        $scope.openingTimeString = openingTimeString;
        $scope.businessTypes = businessType;
        $scope.localizedBusinessTypes = localizedBusinessTypes;

        init();
    }
]);