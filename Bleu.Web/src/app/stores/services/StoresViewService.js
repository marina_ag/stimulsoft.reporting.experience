﻿angular.module('bleu.stores').factory('StoresViewService', [
    'LocalizationService',
    'BusinessType',
    function (localizationService, businessType) {
        var
           validateStore = function (validationInfo, model) {
               if (validationInfo != null && validationInfo.Name != null) {
                   model.$setValidity('name', 'exists', false);
               }
           },
           businessTypes = [
                        { id: businessType.Store, name: localizationService.getLocalizedString('Stores.BusinessTypes_Store') },
                        { id: businessType.Restaurant, name: localizationService.getLocalizedString('Stores.BusinessTypes_Restaurant') },
                        { id: businessType.Coffeeshop, name: localizationService.getLocalizedString('Stores.BusinessTypes_Coffeeshop') },
                        { id: businessType.Bar, name: localizationService.getLocalizedString('Stores.BusinessTypes_Bar') },
                        { id: businessType.FastFood, name: localizationService.getLocalizedString('Stores.BusinessTypes_FastFood') }
            ],
            getBusinessTypes = function () {
                return businessTypes;
            },
            getBusinessTypeName = function (businessTypeId) {
                var type = businessTypes.filter(function (item) {
                    return item.id == businessTypeId;
                });
                return type.length != 0 ? type[0].name : null;
            };

        return {
            validateStore: validateStore,
            getBusinessTypes: getBusinessTypes,
            getBusinessTypeName: getBusinessTypeName
        }
    }
]);