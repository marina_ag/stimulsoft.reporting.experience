﻿angular.module('bleu.stores').factory('StoresService', [
    'StoresTransport',
    'StoresSharedMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (storesTransport, storesSharedMapper, requestHandlerHelper, cacheService) {
        var
            getStore = function (id) {
                return requestHandlerHelper.handleRequest(storesTransport.getStore(id)).then(function (response) {
                    return storesSharedMapper.mapInstanceToClient(response);
                });
            },
            updateStore = function (store) {
                return requestHandlerHelper.handleRequest(storesTransport.updateStore(storesSharedMapper.mapInstanceToServer(store))).then(function () {
                    cacheService.clear('stores');
                });
            },
            addStore = function (store) {
                return requestHandlerHelper.handleRequest(storesTransport.addStore(storesSharedMapper.mapInstanceToServer(store))).then(function (storeId) {
                    cacheService.clear('stores');
                    return storeId;
                });
            },
            removeStores = function (storesIds) {
                return requestHandlerHelper.handleRequest(storesTransport.removeStores(storesIds)).then(function () {
                    cacheService.clear('stores');
                });
            },
            setActiveState = function (id, isActive) {
                return getStore(id).then(function (store) {
                    store.isActive = isActive;
                    return updateStore(store);
                });
            };

        return {
            getStore: getStore,
            updateStore: updateStore,
            addStore: addStore,
            removeStores: removeStores,
            setActiveState: setActiveState
        }
    }
]);