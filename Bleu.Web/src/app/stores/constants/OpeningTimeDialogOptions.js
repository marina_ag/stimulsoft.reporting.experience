﻿angular.module('bleu.stores').constant('OpeningTimeDialogOptions', {
    animation: true,
    templateUrl: '/src/app/stores/views/OpeningTimeDialogView.html',
    controller: 'OpeningTimeDialogController',
    size: 'lg'
});