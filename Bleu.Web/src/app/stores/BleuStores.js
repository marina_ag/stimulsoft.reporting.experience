﻿angular.module('bleu.stores', [
    'core.stores',
    'bleu.stores.shared',
    'bleu.main',
    'ui.router'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {

    $stateProvider
        .state({
            name: 'root.settingsStoresList',
            url: '/stores',
            views: {
                'content-view': {
                    templateUrl: 'src/app/stores/views/StoresListView.html',
                    controller: 'StoresListController'
                }
            },
            menu: {
                section: menuSections.settings,
                item: 'Main.LeftMenu_MenuItemStores',
                order: 10
            }
        })
        .state({
            name: 'root.settingsAddStore',
            url: '/addstore',
            views: {
                'content-view': {
                    templateUrl: 'src/app/stores/views/AddStoreView.html',
                    controller: 'AddStoreController'
                }
            }
        })
        .state({
            name: 'root.settingsStore',
            url: '/store/:id',
            views: {
                'content-view': {
                    templateUrl: 'src/app/stores/views/StoreView.html',
                    controller: 'StoreController'
                }
            }
        });
}]).run();