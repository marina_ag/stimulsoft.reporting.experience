﻿angular.module('bleu.stores').factory('StoresListPageViewModel', [
    function () {
        var storesListPageViewModel = function () {
            this.textFilter = null;
            this.canDelete = false;
        };

        return storesListPageViewModel;
    }
]);