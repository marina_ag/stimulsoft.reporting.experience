﻿angular.module('bleu.stores').factory('StoresGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var storesGridOptionsModel = function () {
            this.useExternalSorting = false;
            this.columnDefs = [
                {
                    field: 'name',
                    displayName: localizationService.getLocalizedString('Stores.StoresGrid_HeaderName'),
                    type: 'string',
                    cellTemplate: 'src/app/stores/views/grid/StoresGridNameTemplate.html'
                },
                {
                    field: 'state',
                    displayName: localizationService.getLocalizedString('Stores.StoresGrid_HeaderState'),
                    type: 'string'
                },
                {
                    field: 'city',
                    displayName: localizationService.getLocalizedString('Stores.StoresGrid_HeaderCity'),
                    type: 'string'
                },
                {
                    field: 'address',
                    displayName: localizationService.getLocalizedString('Stores.StoresGrid_HeaderAddress'),
                    type: 'string'
                },
                {
                    field: 'phoneNumber',
                    displayName: localizationService.getLocalizedString('Stores.StoresGrid_HeaderPhoneNumber'),
                    type: 'string'
                },
                {
                    field: 'isActive',
                    displayName: localizationService.getLocalizedString('Stores.StoresGrid_HeaderActive'),
                    type: 'string',
                    cellTemplate: 'src/app/stores/views/grid/StoresGridIsActiveTemplate.html'
                },
                {
                    field: 'businessType',
                    displayName: localizationService.getLocalizedString('Stores.StoresGrid_HeaderBusinessType'),
                    type: 'string',
                    cellTemplate: 'src/app/stores/views/grid/StoresGridBusinessTypeTemplate.html'
                }
            ];
        };

        angular.extend(storesGridOptionsModel.prototype, new gridDefaultOptions());

        return storesGridOptionsModel;
    }
]);