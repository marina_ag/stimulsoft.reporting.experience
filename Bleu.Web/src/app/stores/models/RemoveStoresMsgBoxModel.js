﻿angular.module('bleu.stores').factory('RemoveStoresMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeStoresModel = function () {
            this.title = localizationService.getLocalizedString('Stores.StoresListPageRemoveStoresDlg_Title');
            this.message = localizationService.getLocalizedString('Stores.StoresListPageRemoveStoresDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('Stores.StoresListPageRemoveStoresDlg_BtnTextOk'),
                    value: 'ok'
                },
                {
                    text: localizationService.getLocalizedString('Stores.StoresListPageRemoveStoresDlg_BtnTextCancel'),
                    value: 'cancel'
                }
            ];
            this.successValue = 'ok';
        };

        return removeStoresModel;
    }
]);