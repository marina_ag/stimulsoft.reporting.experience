﻿angular.module('bleu.receipts').factory('ReceiptSettingsMapper', [
    'ReceiptSettingsViewModel',
    function (receiptSettingsViewModel) {
        var
            transformToClient = function (response) {
                var receiptSettings = new receiptSettingsViewModel();

                receiptSettings.imageId = response.Image;
                receiptSettings.name = response.Name;
                receiptSettings.address = response.Address;
                receiptSettings.endingPhrase = response.EndingPhrase;

                return receiptSettings;
            },
            transformToServer = function (request) {
                return {
                    Image: request.imageId,
                    Name: request.name,
                    Address: request.address,
                    EndingPhrase: request.endingPhrase
                }
            },
            mapInstanceToClient = function (response) {
                return transformToClient(response);
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            };

        return {
            mapInstanceToClient: mapInstanceToClient,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);