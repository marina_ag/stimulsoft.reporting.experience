﻿angular.module('bleu.receipts').controller('ReceiptController', [
    '$scope',
    'ReceiptSettingsService',
    'ReceiptSettingsViewModel',
    'ReceiptSuccessUpdateMsgBoxModel',
    'FileUploadHelper',
    'MessageBoxHelper',
    'PathesOfDefaultImages',
    'ImageSizeType',
    function (
        $scope,
        receiptSettingsService,
        receiptSettingsViewModel,
        receiptSuccessUpdateMsgBoxModel,
        fileUploadHelper,
        messageBoxHelper,
        pathesOfDefaultImages,
        imageSizeType) {
        var receiptSettings = new receiptSettingsViewModel(),
            receiptLogoImageUploader = new fileUploadHelper({ placeholder: pathesOfDefaultImages.placeholder }),
            onSuccessReceiptSettingsImageUpload = function (imageId) {
                $scope.receiptSettings.imageId = imageId;
            },
            updateReceiptSettings = function () {
                receiptLogoImageUploader.addPhoto(onSuccessReceiptSettingsImageUpload).then(function () {
                    receiptSettingsService.updateReceiptSettings($scope.receiptSettings).then(function () {
                        messageBoxHelper.openModal(new receiptSuccessUpdateMsgBoxModel());
                    });
                });
            },
            init = function () {
                receiptSettingsService.getReceiptSettings().then(function (receiptSet) {
                    receiptLogoImageUploader.updatePlaceholder(receiptSet.imageId, imageSizeType.Medium);
                    $scope.receiptSettings = receiptSet;
                });
            };

        $scope.receiptLogoImageUploader = receiptLogoImageUploader;
        $scope.receiptSettings = receiptSettings;
        $scope.updateReceiptSettings = updateReceiptSettings;

        init();
    }
]);