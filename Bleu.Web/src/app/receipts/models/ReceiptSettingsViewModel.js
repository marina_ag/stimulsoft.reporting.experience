﻿angular.module('bleu.receipts').factory('ReceiptSettingsViewModel', [
    function () {
        var receiptSettingsViewModel = function () {
            this.imageId = '';
            this.name = '';
            this.address = '';
            this.endingPhrase = '';
        };

        return receiptSettingsViewModel;
    }
]);