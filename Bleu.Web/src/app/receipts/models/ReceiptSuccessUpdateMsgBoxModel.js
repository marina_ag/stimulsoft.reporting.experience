﻿angular.module('bleu.receipts').factory('ReceiptSuccessUpdateMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var successReceiptUpdateModel = function () {
            this.title = localizationService.getLocalizedString('Receipts.ReceiptPageReceiptSuccessUpdateDlg_Title');
            this.message = localizationService.getLocalizedString('Receipts.ReceiptPageReceiptSuccessUpdateDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('Receipts.ReceiptPageReceiptSuccessUpdateDlg_BtnTextOk'),
                    value: 'ok'
                }
            ];
            this.successValue = 'ok';
        };

        return successReceiptUpdateModel;
    }
]);