﻿angular.module('bleu.receipts').factory('ReceiptSettingsService', [
    'ReceiptTransport',
    'ReceiptSettingsMapper',
    'RequestHandlerHelper',
    function (receiptTransport, receiptSettingsMapper, requestHandlerHelper) {
        var
            getReceiptSettings = function () {
                return requestHandlerHelper.handleRequest(receiptTransport.getReceiptSettings()).then(function (response) {
                    return receiptSettingsMapper.mapInstanceToClient(response);
                });
            },
            updateReceiptSettings = function (receiptSettings) {
                return requestHandlerHelper.handleRequest(receiptTransport.updateReceiptSettings(receiptSettingsMapper.mapInstanceToServer(receiptSettings)));
            };

        return {
            getReceiptSettings: getReceiptSettings,
            updateReceiptSettings: updateReceiptSettings
        }
    }
]);