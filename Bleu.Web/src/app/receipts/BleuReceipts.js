﻿angular.module('bleu.receipts', [
    'core.receipts',
    'bleu.main',
    'ui.router'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {

    $stateProvider
        .state({
            name: 'root.settingsReceipt',
            url: '/receipt',
            views: {
                'content-view': {
                    templateUrl: 'src/app/receipts/views/ReceiptView.html',
                    controller: 'ReceiptController'
                }
            },
            menu: {
                section: menuSections.settings,
                item: 'Main.LeftMenu_MenuItemReceipt',
                order: 70
            }
        });
}]).run();