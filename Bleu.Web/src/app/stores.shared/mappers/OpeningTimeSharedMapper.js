﻿angular.module('bleu.stores.shared').factory('OpeningTimeSharedMapper', [
    'OpeningTimeSharedViewModel',
    function (openingTimeSharedViewModel) {
        var
            transformToClient = function (response) {
                var openingTime = new openingTimeSharedViewModel();

                if (response == null) {
                    return openingTime;
                }

                openingTime.storeId = response.StoreId;

                openingTime.mondayIsAvailable = response.MondayIsAvailable;
                openingTime.tuesdayIsAvailable = response.TuesdayIsAvailable;
                openingTime.wednesdayIsAvailable = response.WednesdayIsAvailable;
                openingTime.thursdayIsAvailable = response.ThursdayIsAvailable;
                openingTime.fridayIsAvailable = response.FridayIsAvailable;
                openingTime.saturdayIsAvailable = response.SaturdayIsAvailable;
                openingTime.sundayIsAvailable = response.SundayIsAvailable;

                openingTime.mondayTimeBegin = response.Mon.split('-')[0];
                openingTime.mondayTimeEnd = response.Mon.split('-')[1];
                openingTime.tuesdayTimeBegin = response.Tue.split('-')[0];
                openingTime.tuesdayTimeEnd = response.Tue.split('-')[1];
                openingTime.wednesdayTimeBegin = response.Wed.split('-')[0];
                openingTime.wednesdayTimeEnd = response.Wed.split('-')[1];
                openingTime.thursdayTimeBegin = response.Thu.split('-')[0];
                openingTime.thursdayTimeEnd = response.Thu.split('-')[1];
                openingTime.fridayTimeBegin = response.Fri.split('-')[0];
                openingTime.fridayTimeEnd = response.Fri.split('-')[1];
                openingTime.saturdayTimeBegin = response.Sat.split('-')[0];
                openingTime.saturdayTimeEnd = response.Sat.split('-')[1];
                openingTime.sundayTimeBegin = response.Sun.split('-')[0];
                openingTime.sundayTimeEnd = response.Sun.split('-')[1];

                return openingTime;
            },
            transformToServer = function (request) {
                return {
                    StoreId: request.storeId,
                    MondayIsAvailable: request.mondayIsAvailable,
                    TuesdayIsAvailable: request.tuesdayIsAvailable,
                    WednesdayIsAvailable: request.wednesdayIsAvailable,
                    ThursdayIsAvailable: request.thursdayIsAvailable,
                    FridayIsAvailable: request.fridayIsAvailable,
                    SaturdayIsAvailable: request.saturdayIsAvailable,
                    SundayIsAvailable: request.sundayIsAvailable,
                    Mon: (request.mondayTimeBegin + '-' + request.mondayTimeEnd).replace(/null/g, ''),
                    Tue: (request.tuesdayTimeBegin + '-' + request.tuesdayTimeEnd).replace(/null/g, ''),
                    Wed: (request.wednesdayTimeBegin + '-' + request.wednesdayTimeEnd).replace(/null/g, ''),
                    Thu: (request.thursdayTimeBegin + '-' + request.thursdayTimeEnd).replace(/null/g, ''),
                    Fri: (request.fridayTimeBegin + '-' + request.fridayTimeEnd).replace(/null/g, ''),
                    Sat: (request.saturdayTimeBegin + '-' + request.saturdayTimeEnd).replace(/null/g, ''),
                    Sun: (request.sundayTimeBegin + '-' + request.sundayTimeEnd).replace(/null/g, '')
                }
            },

            mapInstanceToClient = function (response) {
                return transformToClient(response);
            },

            mapInstanceToServer = function (request) {
                return transformToServer(request);
            };

        return {
            mapInstanceToClient: mapInstanceToClient,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);