﻿angular.module('bleu.stores.shared').factory('StoresSharedMapper', [
    'StoreSharedViewModel',
    'OpeningTimeSharedMapper',
    'DataMapHelper',
    function (storeSharedViewModel, openingTimeSharedMapper, dataMapHelper) {
        var
            transformToClient = function (response) {
                var store = new storeSharedViewModel();

                var address = response.AddressDto != null ? response.AddressDto : {
                    Id: dataMapHelper.getGuidEmpty(),
                    Country: "",
                    City:"",
                    State: "",
                    Street: "",
                    ZipCode: ""
                };

                store.id = response.Id;
                store.name = response.Name;
                store.phoneNumber = response.Phone;
                store.email = response.Email;
                store.country = address.Country;
                store.state = address.State;
                store.city = address.City;
                store.address = address.Street;
                store.zip = address.ZipCode;
                store.isActive = response.IsActive;
                store.description = response.Description;
                store.openingTime = openingTimeSharedMapper.mapInstanceToClient(response.OpeningTime);
                store.businessType = response.BusinessType;
                store.addressId = address.Id;

                return store;
            },
            transformToServer = function (request) {

                var addressDto = {
                    Id: request.addressId,
                    Country: request.country,
                    City: request.city,
                    State: request.state,
                    Street: request.address,
                    ZipCode: request.zip
                };


                return {
                    Id: request.id,
                    Name: request.name,
                    Phone: request.phoneNumber,
                    Email: request.email,
                    IsActive: request.isActive,
                    Description: request.description,
                    OpeningTime: openingTimeSharedMapper.mapInstanceToServer(request.openingTime),
                    BusinessType: request.businessType,
                    AddressDto: addressDto
                };
            },
            mapCollectionToClient = function (response) {
                return response.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (response) {
                return transformToClient(response);
            },
            mapCollectionToServer = function (request) {
                return request.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            };

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);