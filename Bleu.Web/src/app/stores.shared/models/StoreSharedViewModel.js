﻿angular.module('bleu.stores.shared').factory('StoreSharedViewModel', [
    'BaseViewModel',
    'OpeningTimeSharedViewModel',
    'BusinessType',
    function (baseViewModel,openingTimeSharedViewModel, businessType) {
        var storeSharedViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.name = '';
            this.phoneNumber = '';
            this.email = '';
            this.country = '';
            this.state = '';
            this.city = '';
            this.address = '';
            this.zip = '';
            this.isActive = true;
            this.description = '';
            this.openingTime = new openingTimeSharedViewModel();
            this.businessType = businessType.Store;
        };

        storeSharedViewModel.prototype = Object.create(baseViewModel.prototype);

        return storeSharedViewModel;
    }
]);