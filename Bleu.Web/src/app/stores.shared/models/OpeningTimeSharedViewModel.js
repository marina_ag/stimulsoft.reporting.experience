﻿angular.module('bleu.stores.shared').factory('OpeningTimeSharedViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var openingTimeSharedViewModel = function () {
            baseViewModel.call(this);

            this.storeId = null;

            this.mondayIsAvailable = false;
            this.tuesdayIsAvailable = false;
            this.wednesdayIsAvailable = false;
            this.thursdayIsAvailable = false;
            this.fridayIsAvailable = false;
            this.saturdayIsAvailable = false;
            this.sundayIsAvailable = false;

            this.mondayTimeBegin = '';
            this.mondayTimeEnd = '';
            this.tuesdayTimeBegin = '';
            this.tuesdayTimeEnd = '';
            this.wednesdayTimeBegin = '';
            this.wednesdayTimeEnd = '';
            this.thursdayTimeBegin = '';
            this.thursdayTimeEnd = '';
            this.fridayTimeBegin = '';
            this.fridayTimeEnd = '';
            this.saturdayTimeBegin = '';
            this.saturdayTimeEnd = '';
            this.sundayTimeBegin = '';
            this.sundayTimeEnd = '';
        };

        openingTimeSharedViewModel.prototype = Object.create(baseViewModel.prototype);

        return openingTimeSharedViewModel;
    }
]);