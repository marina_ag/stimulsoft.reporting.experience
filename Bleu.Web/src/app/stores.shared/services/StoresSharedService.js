﻿angular.module('bleu.stores.shared').factory('StoresSharedService', [
    'StoresSharedTransport',
    'StoresSharedMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (storesSharedTransport, storesSharedMapper, requestHandlerHelper, cacheService) {
        var
            getStores = function () {
                return storesSharedTransport.getAllStores().then(function (response) {
                    return storesSharedMapper.mapCollectionToClient(response);
                });
            },
            getAllStores = function () {
                return requestHandlerHelper.handleRequest(cacheService.cache('stores', getStores));
            },
            getStoresByName = function (key) {
                return getAllStores().then(function (response) {
                    return response.filter(function (item) {
                        return item.name.toLowerCase().indexOf(key.toLowerCase()) != -1;
                    });
                });
            };

        return {
            getAllStores: getAllStores,
            getStoresByName: getStoresByName
        }
    }
]);