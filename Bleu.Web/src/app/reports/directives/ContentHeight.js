﻿angular.module('bleu.reports').directive('cdContentHeight', ['$window', function ($window) {
    return {
        restrict: "A",
        link: function (scope, elem) {

            scope.$watch(function () {
                scope.reportFrameTopPosition = elem.height() + 30;
            });

            angular.element($window).bind('resize', function () {
                scope.$apply();
            });
        }
    }
}]);