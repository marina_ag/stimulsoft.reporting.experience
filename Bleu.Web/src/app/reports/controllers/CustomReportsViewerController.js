﻿angular.module('bleu.reports').controller('CustomReportsViewerController', [
    '$scope',
    '$state',
    '$stateParams',
    function ($scope, $state, $stateParams) {
        var reportUri = $state.current.reportUrl + '?' + $.param($stateParams);

        $scope.reportUri = reportUri;
    }
]);