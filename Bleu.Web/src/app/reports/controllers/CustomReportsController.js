﻿angular.module('bleu.reports').controller('CustomReportsController', [
    '$scope',
    '$filter',
    'CustomersGroupsSharedService',
    'StoresSharedService',
    'MenuItemsCategoriesSharedService',
    'SessionService',
    'CustomReportsFilterViewModel',
    'CustomReportsParamsViewModel',
    'CustomReportsPageViewModel',
    'DataMapHelper',
    function (
        $scope,
        $filter,
        customersGroupsSharedService,
        storesSharedService,
        menuItemsCategoriesSharedService,
        sessionService,
        customReportsFilterViewModel,
        customReportsParamsViewModel,
        customReportsPageViewModel,
        dataMapHelper) {
        var
            viewModel = sessionService.set('customReportsPageViewModel', new customReportsPageViewModel()),
            customersGroups = [],
            stores = [],
            menuItemsCategories = [],
            reportsFilter = sessionService.set('customReportsFilterViewModel', new customReportsFilterViewModel()),
            reportsParams = new customReportsParamsViewModel(),
            clearReportsFilter = function () {
                $scope.reportsFilter = new customReportsFilterViewModel();
            },
            hideReportsFilters = function () {
                viewModel.isFilterShow = false;
            },
            showReportsFilters = function () {
                viewModel.isFilterShow = true;
            },
            onReportParamsChange = function () {
                $scope.reportsFilter.$validate().then(function () {
                    $scope.reportsParams.startdate = dataMapHelper.prepareDateForReports($scope.reportsFilter.startDate);
                    $scope.reportsParams.enddate = dataMapHelper.prepareDateForReports($scope.reportsFilter.endDate);
                    $scope.reportsParams.customer = $scope.reportsFilter.customerName;
                    $scope.reportsParams.group = $scope.reportsFilter.customerGroupId;
                    $scope.reportsParams.employee = $scope.reportsFilter.employeeId;
                    $scope.reportsParams.store = $scope.reportsFilter.storeId;
                    $scope.reportsParams.item = $scope.reportsFilter.menuItem;
                    $scope.reportsParams.terminal = $scope.reportsFilter.terminalId;
                    $scope.reportsParams.category = $scope.reportsFilter.menuItemCategoryId;
                    $scope.reportsParams.iscustom = $scope.reportsFilter.isCustom;
                });
            },
            init = function () {
                customersGroupsSharedService.getAllCustomersGroups().then(function (groups) {
                    $scope.customersGroups = groups;
                });

                storesSharedService.getAllStores().then(function (s) {
                    $scope.stores = s;
                });

                menuItemsCategoriesSharedService.getAllMenuItemsCategories().then(function (categories) {
                    $scope.menuItemsCategories = categories;
                });
            };

        $scope.customersGroups = customersGroups;
        $scope.stores = stores;
        $scope.menuItemsCategories = menuItemsCategories;
        $scope.reportsFilter = reportsFilter;
        $scope.reportsParams = reportsParams;
        $scope.viewModel = viewModel;
        $scope.clearReportsFilter = clearReportsFilter;
        $scope.hideReportsFilters = hideReportsFilters;
        $scope.showReportsFilters = showReportsFilters;
        $scope.onReportParamsChange = onReportParamsChange;

        init();
    }
]);