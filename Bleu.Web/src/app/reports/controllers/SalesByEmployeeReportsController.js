﻿angular.module('bleu.reports').controller('SalesByEmployeeReportsController', [
    '$scope',
    '$state',
    '$filter',
    'StoresSharedService',
    'SessionService',
    'SalesByEmployeeFilterViewModel',
    'SalesByEmployeeParamsViewModel',
    'SalesByEmployeePageViewModel',
    'UsersSharedService',
    'DataMapHelper',
    function (
        $scope,
        $state,
        $filter,
        storesSharedService,
        sessionService,
        salesByEmployeeFilterViewModel,
        salesByEmployeeParamsViewModel,
        salesByEmployeePageViewModel,
        usersSharedService,
        dataMapHelper) {
        var
            viewModel = sessionService.set('salesByEmployeePageViewModel', new salesByEmployeePageViewModel()),
            reportsFilter = sessionService.set('salesByEmployeeFilterViewModel', new salesByEmployeeFilterViewModel()),
            reportsParams = new salesByEmployeeParamsViewModel(),
            hideReportsFilters = function () {
                viewModel.isFilterShow = false;
            },
            showReportsFilters = function () {
                viewModel.isFilterShow = true;
            },
            onReportParamsChange = function () {
                $scope.reportsFilter.$validate().then(function () {
                    $scope.reportsParams.startdate = dataMapHelper.prepareDateForReports($scope.reportsFilter.startDate);
                    $scope.reportsParams.enddate = dataMapHelper.prepareDateForReports($scope.reportsFilter.endDate);
                    $scope.reportsParams.iscustom = $scope.reportsFilter.isCustom;
                });
            },
            clearReportsFilter = function () {
                $scope.reportsFilter.startDate = null;
                $scope.reportsFilter.endDate = null;
                $scope.reportsFilter.stores = [];
                $scope.reportsFilter.employees = null;
                onReportParamsChange();
            },
            getStores = function (query) {
                return storesSharedService.getStoresByName(query);
            },
            getEmployees = function (query) {
                return usersSharedService.getEmployeesByName(query);
            },
            onGenerateReportClick = function () {
                var storesUri = '';
                var storesNames = [];
                angular.forEach($scope.reportsFilter.stores, function (value) {
                    storesUri = storesUri + "&store=" + value.id;
                    storesNames.push(value.name);
                });

                var employeesUri = '';
                var employeesNames = [];
                angular.forEach($scope.reportsFilter.employees, function (value) {
                    employeesUri = employeesUri + "&employee=" + value.id;
                    employeesNames.push(value.fullName);
                });

                $scope.reportsParams.storename = storesNames.join(", ");
                $scope.reportsParams.employeename = employeesNames.join(", ");
                var reportUri = $state.current.reportUrl + '?' + $.param($scope.reportsParams) + storesUri + employeesUri;
                $scope.reportUri = reportUri;
            };

        $scope.reportsFilter = reportsFilter;
        $scope.reportsParams = reportsParams;
        $scope.viewModel = viewModel;
        $scope.clearReportsFilter = clearReportsFilter;
        $scope.hideReportsFilters = hideReportsFilters;
        $scope.showReportsFilters = showReportsFilters;
        $scope.onReportParamsChange = onReportParamsChange;
        $scope.onGenerateReportClick = onGenerateReportClick;
        $scope.getStores = getStores;
        $scope.getEmployees = getEmployees;
        $scope.reportFrameTopPosition = 0;
        $scope.reportUri = $state.current.reportUrl;

    }
]);