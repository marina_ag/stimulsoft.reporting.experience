﻿angular.module("bleu.reports").controller("SalesByCustomerReportsController", [
    "$scope",
    "$state",
    "$filter",
    "CustomersGroupsSharedService",
    "CustomersSharedService",
    "StoresSharedService",
    "SessionService",
    "SalesByCustomerFilterViewModel",
    "SalesByCustomerParamsViewModel",
    "SalesByCustomerPageViewModel",
    "DataMapHelper",
    "FilterCompareMethod",
    "FilterRelationMethod",
    function(
        $scope,
        $state,
        $filter,
        customersGroupsSharedService,
        customersSharedService,
        storesSharedService,
        sessionService,
        salesByCustomerFilterViewModel,
        salesByCustomerParamsViewModel,
        salesByCustomerPageViewModel,
        dataMapHelper,
        filterCompareMethod,
        filterRelationMethod) {
        var
            viewModel = sessionService.set("salesByCustomerPageViewModel", new salesByCustomerPageViewModel()),
            reportsFilter = sessionService.set("salesByCustomerFilterViewModel", new salesByCustomerFilterViewModel()),
            reportsParams = new salesByCustomerParamsViewModel(),
            hideReportsFilters = function() {
                viewModel.isFilterShow = false;
            },
            showReportsFilters = function() {
                viewModel.isFilterShow = true;
            },
            onReportParamsChange = function() {
                $scope.reportsFilter.$validate().then(function() {
                    $scope.reportsParams.startdate = dataMapHelper.prepareDateForReports($scope.reportsFilter.startDate);
                    $scope.reportsParams.enddate = dataMapHelper.prepareDateForReports($scope.reportsFilter.endDate);
                    $scope.reportsParams.iscustom = $scope.reportsFilter.isCustom;
                });
            },
            clearReportsFilter = function () {
                $scope.reportsFilter.startDate = null;
                $scope.reportsFilter.endDate = null;
                $scope.reportsFilter.stores = [];
                $scope.reportsFilter.customerGroups = [];
                $scope.reportsFilter.customers = [];
                onReportParamsChange();
            },
            onGenerateReportClick = function() {
                var storesUri = '';
                var storesNames = [];
                angular.forEach($scope.reportsFilter.stores, function(value) {
                    storesUri = storesUri + "&store=" + value.id;
                    storesNames.push(value.name);
                });
                var groupsUri = '';
                var groupsNames = [];
                angular.forEach($scope.reportsFilter.customerGroups, function(value) {
                    groupsUri = groupsUri + "&group=" + value.id;
                    groupsNames.push(value.name);
                });
                var customersUri = '';
                var customersNames = [];
                angular.forEach($scope.reportsFilter.customers, function(value) {
                    customersUri = customersUri + "&customer=" + value.id;
                    customersNames.push(value.fullName);
                });

                $scope.reportsParams.storename = storesNames.join(", ");
                $scope.reportsParams.groupname = groupsNames.join(", ");
                $scope.reportsParams.customername = customersNames.join(", ");
                var reportUri = $state.current.reportUrl + "?" + $.param($scope.reportsParams) + storesUri + groupsUri + customersUri;
                $scope.reportUri = reportUri;
            },
            getStores = function(query) {
                return storesSharedService.getStoresByName(query);
            },
            getCustomersGroups = function(query) {
                return customersGroupsSharedService.getCustomersGroupsByName(query);
            },
            getFilters = function() {
                return [
                    { Selector: "FirstName", Method: filterCompareMethod.Contains, Value: $scope.filterStr, Relation: filterRelationMethod.Or },
                    { Selector: "LastName", Method: filterCompareMethod.Contains, Value: $scope.filterStr, Relation: filterRelationMethod.Or }
                ];
            },
            getCustomers = function(query) {
                $scope.filterStr = query;
                var requestOptions = {
                    Skip: 0,
                    Take: 20,
                    Count: true,
                    SortFilters: [],
                    Filters: [],
                    Fields: []
                };
                var filters = getFilters();
                filters.forEach(angular.bind(this, function(filter) {
                    if (filter.Value != null && filter.Value !== "") {
                        requestOptions.Filters.push({
                            Selector: filter.Selector,
                            Value: filter.Value,
                            Method: filter.Method,
                            Relation: filter.Relation
                        });
                    }
                }));
                requestOptions.Fields.push("FirstName", "LastName", "CardNo");
                return customersSharedService.getCustomersByName(requestOptions, $scope.filterStr);
            };

        $scope.reportsFilter = reportsFilter;
        $scope.reportsParams = reportsParams;
        $scope.viewModel = viewModel;
        $scope.clearReportsFilter = clearReportsFilter;
        $scope.hideReportsFilters = hideReportsFilters;
        $scope.showReportsFilters = showReportsFilters;
        $scope.onReportParamsChange = onReportParamsChange;
        $scope.onGenerateReportClick = onGenerateReportClick;
        $scope.getStores = getStores;
        $scope.getCustomersGroups = getCustomersGroups;
        $scope.getCustomers = getCustomers;
        $scope.reportFrameTopPosition = 0;
        $scope.reportUri = $state.current.reportUrl;

    }
]);