﻿angular.module('bleu.reports').controller('SalesByMenuItemReportsController', [
    '$scope',
    '$state',
    '$filter',
    'StoresSharedService',
    'MenuItemsCategoriesSharedService',
    'MenuItemsSharedService',
    'SessionService',
    'SalesByMenuItemFilterViewModel',
    'SalesByMenuItemParamsViewModel',
    'SalesByMenuItemPageViewModel',
    'DataMapHelper',
    "FilterCompareMethod",
    "FilterRelationMethod",
    function (
        $scope,
        $state,
        $filter,
        storesSharedService,
        menuItemsCategoriesSharedService,
        menuItemsSharedService,
        sessionService,
        salesByMenuItemFilterViewModel,
        salesByMenuItemParamsViewModel,
        salesByMenuItemPageViewModel,
        dataMapHelper,
        filterCompareMethod,
        filterRelationMethod) {
        var
            viewModel = sessionService.set('salesByMenuItemPageViewModel', new salesByMenuItemPageViewModel()),
            reportsFilter = sessionService.set('salesByMenuItemFilterViewModel', new salesByMenuItemFilterViewModel()),
            reportsParams = new salesByMenuItemParamsViewModel(),
            
            hideReportsFilters = function () {
                viewModel.isFilterShow = false;
            },
            showReportsFilters = function () {
                viewModel.isFilterShow = true;
            },
            onReportParamsChange = function () {
                $scope.reportsFilter.$validate().then(function () {
                    $scope.reportsParams.startdate = dataMapHelper.prepareDateForReports($scope.reportsFilter.startDate);
                    $scope.reportsParams.enddate = dataMapHelper.prepareDateForReports($scope.reportsFilter.endDate);
                    $scope.reportsParams.iscustom = $scope.reportsFilter.isCustom;
                });
            },
            clearReportsFilter = function () {
                $scope.reportsFilter.startDate = null;
                $scope.reportsFilter.endDate = null;
                $scope.reportsFilter.stores = [];
                $scope.reportsFilter.menuItemCategories = [];
                $scope.reportsFilter.menuItems = [];
                onReportParamsChange();
            },
             getStores = function (query) {
                 return storesSharedService.getStoresByName(query);
             },
           getMenuItemsCategories = function (query) {
               return menuItemsCategoriesSharedService.getMenuItemsCategoriesByName(query);
           },
            onGenerateReportClick = function () {
                var storesUri = '';
                var storesNames = [];
                angular.forEach($scope.reportsFilter.stores, function (value) {
                    storesUri = storesUri + "&store=" + value.id;
                    storesNames.push(value.name);
                });
                var categoriesUri = '';
                var categoriesNames = [];
                angular.forEach($scope.reportsFilter.menuItemCategories, function (value) {
                    categoriesUri = categoriesUri + "&category=" + value.id;
                    categoriesNames.push(value.name);
                });
                var menuItemsUri = '';
                var itemsNames = [];
                angular.forEach($scope.reportsFilter.menuItems, function (value) {
                    menuItemsUri = menuItemsUri + "&item=" + value.id;
                    itemsNames.push(value.name);
                });

                $scope.reportsParams.storename = storesNames.join(", ");
                $scope.reportsParams.categoryname = categoriesNames.join(", ");
                $scope.reportsParams.itemname = itemsNames.join(", ");
                var reportUri = $state.current.reportUrl + '?' + $.param($scope.reportsParams) + storesUri + categoriesUri + menuItemsUri;
                $scope.reportUri = reportUri;
            },
            getFilters = function () {
                return [
                    { Selector: 'Name', Method: filterCompareMethod.Contains, Value: viewModel.textFilter, Relation: filterRelationMethod.Or }
                ];
            },
            getMenuItems = function (query) {
                $scope.filterStr = query;
                var requestOptions = {
                    Skip: 0,
                    Take: 20,
                    Count: true,
                    SortFilters: [],
                    Filters: [],
                    Fields: []
                };
                var filters = getFilters();
                filters.forEach(angular.bind(this, function (filter) {
                    if (filter.Value != null && filter.Value !== "") {
                        requestOptions.Filters.push({
                            Selector: filter.Selector,
                            Value: filter.Value,
                            Method: filter.Method,
                            Relation: filter.Relation
                        });
                    }
                }));
                requestOptions.Fields.push('MenuItemId','Languages');
                return menuItemsSharedService.getMenuItemsByName(requestOptions, $scope.filterStr);
            };

        $scope.reportsFilter = reportsFilter;
        $scope.reportsParams = reportsParams;
        $scope.viewModel = viewModel;
        $scope.clearReportsFilter = clearReportsFilter;
        $scope.hideReportsFilters = hideReportsFilters;
        $scope.showReportsFilters = showReportsFilters;
        $scope.onReportParamsChange = onReportParamsChange;
        $scope.onGenerateReportClick = onGenerateReportClick;
        $scope.getMenuItemsCategories = getMenuItemsCategories;
        $scope.getMenuItems = getMenuItems;
        $scope.getStores = getStores;
        $scope.reportFrameTopPosition = 0;
        $scope.reportUri = $state.current.reportUrl;

    }
]);