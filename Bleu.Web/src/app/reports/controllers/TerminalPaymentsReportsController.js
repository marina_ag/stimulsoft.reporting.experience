﻿angular.module("bleu.reports").controller("TerminalPaymentsReportsController", [
    "$scope",
    "$state",
    "$filter",
    "StoresSharedService",
    "SessionService",
    "TerminalPaymentsFilterViewModel",
    "TerminalPaymentsParamsViewModel",
    "TerminalPaymentsPageViewModel",
    "DevicesService",
    "DataMapHelper",
    function(
        $scope,
        $state,
        $filter,
        storesSharedService,
        sessionService,
        terminalPaymentsFilterViewModel,
        terminalPaymentsParamsViewModel,
        terminalPaymentsPageViewModel,
        devicesService,
        dataMapHelper) {
        var
            viewModel = sessionService.set("terminalPaymentsPageViewModel", new terminalPaymentsPageViewModel()),
            reportsFilter = sessionService.set("terminalPaymentsFilterViewModel", new terminalPaymentsFilterViewModel()),
            reportsParams = new terminalPaymentsParamsViewModel(),
            hideReportsFilters = function() {
                viewModel.isFilterShow = false;
            },
            showReportsFilters = function() {
                viewModel.isFilterShow = true;
            },
            onReportParamsChange = function() {
                $scope.reportsFilter.$validate().then(function() {
                    $scope.reportsParams.startdate = dataMapHelper.prepareDateForReports($scope.reportsFilter.startDate);
                    $scope.reportsParams.enddate = dataMapHelper.prepareDateForReports($scope.reportsFilter.endDate);
                    $scope.reportsParams.iscustom = $scope.reportsFilter.isCustom;
                });
            },
            clearReportsFilter = function() {
                $scope.reportsFilter.startDate = null;
                $scope.reportsFilter.endDate = null;
                $scope.reportsFilter.stores = null;
                $scope.reportsFilter.terminals = null;
                onReportParamsChange();
            },
            getStores = function(query) {
                return storesSharedService.getStoresByName(query);
            },
            getTerminals = function(query) {
                return devicesService.getDevicesByName(query);
            },
            onGenerateReportClick = function () {
                var storesUri = '';
                var storesNames = [];
                angular.forEach($scope.reportsFilter.stores, function(value) {
                    storesUri = storesUri + "&store=" + value.id;
                    storesNames.push(value.name);
                });

                var terminalsUri = '';
                var terminalsNames = [];
                angular.forEach($scope.reportsFilter.terminals, function (value) {
                    terminalsUri = terminalsUri + "&terminal=" + value.id;
                    terminalsNames.push(value.name);
                });

                $scope.reportsParams.storename = storesNames.join(", ");
                $scope.reportsParams.terminalname = terminalsNames.join(", ");
                var reportUri = $state.current.reportUrl + "?" + $.param($scope.reportsParams) + storesUri + terminalsUri;
                $scope.reportUri = reportUri;
            };

        $scope.reportsFilter = reportsFilter;
        $scope.reportsParams = reportsParams;
        $scope.viewModel = viewModel;
        $scope.clearReportsFilter = clearReportsFilter;
        $scope.hideReportsFilters = hideReportsFilters;
        $scope.showReportsFilters = showReportsFilters;
        $scope.onReportParamsChange = onReportParamsChange;
        $scope.onGenerateReportClick = onGenerateReportClick;
        $scope.getStores = getStores;
        $scope.getTerminals = getTerminals;
        $scope.reportFrameTopPosition = 0;
        $scope.reportUri = $state.current.reportUrl;

        clearReportsFilter();
        showReportsFilters();
    }
]);