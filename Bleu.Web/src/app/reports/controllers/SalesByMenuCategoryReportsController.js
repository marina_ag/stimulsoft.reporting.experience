﻿angular.module('bleu.reports').controller('SalesByMenuCategoryReportsController', [
    '$scope',
    '$state',
    '$filter',
    'StoresSharedService',
    'MenuItemsCategoriesSharedService',
    'SessionService',
    'SalesByMenuCategoryFilterViewModel',
    'SalesByMenuCategoryParamsViewModel',
    'SalesByMenuCategoryPageViewModel',
    'DataMapHelper',
    function (
        $scope,
        $state,
        $filter,
        storesSharedService,
        menuItemsCategoriesSharedService,
        sessionService,
        salesByMenuCategoryFilterViewModel,
        salesByMenuCategoryParamsViewModel,
        salesByMenuCategoryPageViewModel,
        dataMapHelper) {
        var
            viewModel = sessionService.set('salesByMenuCategoryPageViewModel', new salesByMenuCategoryPageViewModel()),
            reportsFilter = sessionService.set('salesByMenuCategoryFilterViewModel', new salesByMenuCategoryFilterViewModel()),
            reportsParams = new salesByMenuCategoryParamsViewModel(),
            hideReportsFilters = function () {
                viewModel.isFilterShow = false;
            },
            showReportsFilters = function () {
                viewModel.isFilterShow = true;
            },
            onReportParamsChange = function () {
                $scope.reportsFilter.$validate().then(function () {
                    $scope.reportsParams.startdate = dataMapHelper.prepareDateForReports($scope.reportsFilter.startDate);
                    $scope.reportsParams.enddate = dataMapHelper.prepareDateForReports($scope.reportsFilter.endDate);
                    $scope.reportsParams.iscustom = $scope.reportsFilter.isCustom;
                });
            },
            clearReportsFilter = function () {
                $scope.reportsFilter.startDate = null;
                $scope.reportsFilter.endDate = null;
                $scope.reportsFilter.stores = [];
                $scope.reportsFilter.menuItemCategories = [];
                onReportParamsChange();
            },
            getStores = function (query) {
                return storesSharedService.getStoresByName(query);
            },
            getMenuItemsCategories = function (query) {
                return menuItemsCategoriesSharedService.getMenuItemsCategoriesByName(query);
            },
            onGenerateReportClick = function () {
                var storesUri = '';
                var storesNames = [];
                angular.forEach($scope.reportsFilter.stores, function (value) {
                    storesUri = storesUri + "&store=" + value.id;
                    storesNames.push(value.name);
                });
                var categoriesUri = '';
                var categoriesNames = [];
                angular.forEach($scope.reportsFilter.menuItemCategories, function (value) {
                    categoriesUri = categoriesUri + "&category=" + value.id;
                    categoriesNames.push(value.name);
                });

                $scope.reportsParams.storename = storesNames.join(", ");
                $scope.reportsParams.categoryname = categoriesNames.join(", ");
                var reportUri = $state.current.reportUrl + '?' + $.param($scope.reportsParams) + storesUri + categoriesUri;
                $scope.reportUri = reportUri;
             };

        $scope.reportsFilter = reportsFilter;
        $scope.reportsParams = reportsParams;
        $scope.viewModel = viewModel;
        $scope.clearReportsFilter = clearReportsFilter;
        $scope.hideReportsFilters = hideReportsFilters;
        $scope.showReportsFilters = showReportsFilters;
        $scope.onReportParamsChange = onReportParamsChange;
        $scope.onGenerateReportClick = onGenerateReportClick;
        $scope.getStores = getStores;
        $scope.getMenuItemsCategories = getMenuItemsCategories;
        $scope.reportFrameTopPosition = 0;
        $scope.reportUri = $state.current.reportUrl;

    }
]);