﻿angular.module('bleu.reports').controller('SummariesReportsController', [
    '$scope',
    '$filter',
    '$state',
    'StoresSharedService',
    'SessionService',
    'SummariesFilterViewModel',
    'SummariesParamsViewModel',
    'SummariesPageViewModel',
    'DataMapHelper',
    function(
        $scope,
        $filter,
        $state,
        storesSharedService,
        sessionService,
        summariesFilterViewModel,
        summariesParamsViewModel,
        summariesPageViewModel,
        dataMapHelper) {
        var
            viewModel = sessionService.set('salesSummariesPageViewModel', new summariesPageViewModel()),
            reportsFilter = sessionService.set('salesSummariesFilterViewModel', new summariesFilterViewModel()),
            reportsParams = new summariesParamsViewModel(),
            hideReportsFilters = function() {
                viewModel.isFilterShow = false;
            },
            showReportsFilters = function() {
                viewModel.isFilterShow = true;
            },
            onReportParamsChange = function() {
                $scope.reportsFilter.$validate().then(function() {
                    $scope.reportsParams.startdate = dataMapHelper.prepareDateForReports($scope.reportsFilter.startDate);
                    $scope.reportsParams.enddate = dataMapHelper.prepareDateForReports($scope.reportsFilter.endDate);
                    $scope.reportsParams.iscustom = $scope.reportsFilter.isCustom;
                });
            },
            clearReportsFilter = function () {
                $scope.reportsFilter.startDate = null;
                $scope.reportsFilter.endDate = null;
                $scope.reportsFilter.stores = null;
                onReportParamsChange();
            },
            getStores = function (query) {
                return storesSharedService.getStoresByName(query);
            },
            onGenerateReportClick = function () {
                var storesUri = '';
                var storesNames = [];
                angular.forEach($scope.reportsFilter.stores, function (value) {
                    storesUri = storesUri + "&store=" + value.id;
                    storesNames.push(value.name);
                });

                $scope.reportsParams.storename = storesNames.join(", ");
                var reportUri = $state.current.reportUrl + '?' + $.param($scope.reportsParams) + storesUri;
                $scope.reportUri = reportUri;
            };
           
        $scope.reportsFilter = reportsFilter;
        $scope.reportsParams = reportsParams;
        $scope.viewModel = viewModel;
        $scope.clearReportsFilter = clearReportsFilter;
        $scope.hideReportsFilters = hideReportsFilters;
        $scope.showReportsFilters = showReportsFilters;
        $scope.onReportParamsChange = onReportParamsChange;
        $scope.onGenerateReportClick = onGenerateReportClick;
        $scope.getStores = getStores;
        $scope.reportFrameTopPosition = 0;
        $scope.reportUri = $state.current.reportUrl;

        clearReportsFilter();
        showReportsFilters();
    }
]);
    