﻿angular.module('bleu.reports').controller('CustomersReportsController', [
    '$scope',
    '$filter',
    '$state',
    'CustomersGroupsSharedService',
    'SessionService',
    'CustomersReportsFilterViewModel',
    'CustomersReportsParamsViewModel',
    'CustomersReportsPageViewModel',
    function (
        $scope,
        $filter,
        $state,
        customersGroupsSharedService,
        sessionService,
        customersReportsFilterViewModel,
        customersReportsParamsViewModel,
        customersReportsPageViewModel) {
        var
            viewModel = sessionService.set('customersReportsPageViewModel', new customersReportsPageViewModel()),
            reportsFilter = sessionService.set('customersReportsFilterViewModel', new customersReportsFilterViewModel()),
            reportsParams = new customersReportsParamsViewModel(),
            clearReportsFilter = function () {
                $scope.reportsFilter = new customersReportsFilterViewModel();
            },
            hideReportsFilters = function () {
                viewModel.isFilterShow = false;
            },
            showReportsFilters = function () {
                viewModel.isFilterShow = true;
            },
            onGenerateReportClick = function () {
                var groupsUri = ' ';
                var groupsNames = [];
                angular.forEach($scope.reportsFilter.customerGroups, function (value) {
                    groupsUri = groupsUri + "&customergroup=" + value.id;
                    groupsNames.push(value.name);
                });

                $scope.reportsParams.groupname = groupsNames.join(", ");
                var reportUri = $state.current.reportUrl + '?' + $.param($scope.reportsParams) + groupsUri;
                $scope.reportUri = reportUri;
            },
            getCustomersGroups = function (query) {
                return customersGroupsSharedService.getCustomersGroupsByName(query);
            };

        $scope.reportsFilter = reportsFilter;
        $scope.reportsParams = reportsParams;
        $scope.viewModel = viewModel;
        $scope.clearReportsFilter = clearReportsFilter;
        $scope.hideReportsFilters = hideReportsFilters;
        $scope.showReportsFilters = showReportsFilters;
        $scope.onGenerateReportClick = onGenerateReportClick;
        $scope.getCustomersGroups = getCustomersGroups;
        $scope.reportFrameTopPosition = 0;
        $scope.reportUri = $state.current.reportUrl;

        clearReportsFilter();
        showReportsFilters();
    }
]);