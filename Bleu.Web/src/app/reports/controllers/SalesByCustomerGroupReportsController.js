﻿angular.module('bleu.reports').controller('SalesByCustomerGroupReportsController', [
    '$scope',
    '$state',
    '$filter',
    'CustomersGroupsSharedService',
    'StoresSharedService',
    'SessionService',
    'SalesByCustomerGroupFilterViewModel',
    'SalesByCustomerGroupParamsViewModel',
    'SalesByCustomerGroupPageViewModel',
    'DataMapHelper',
    function (
        $scope,
        $state,
        $filter,
        customersGroupsSharedService,
        storesSharedService,
        sessionService,
        salesByCustomerGroupFilterViewModel,
        salesByCustomerGroupParamsViewModel,
        salesByCustomerGroupPageViewModel,
        dataMapHelper) {
        var
            viewModel = sessionService.set('salesByCustomerGroupPageViewModel', new salesByCustomerGroupPageViewModel()),
            reportsFilter = sessionService.set('salesByCustomerGroupFilterViewModel', new salesByCustomerGroupFilterViewModel()),
            reportsParams = new salesByCustomerGroupParamsViewModel(),
            hideReportsFilters = function () {
                viewModel.isFilterShow = false;
            },
            showReportsFilters = function () {
                viewModel.isFilterShow = true;
            },
            onReportParamsChange = function () {
                $scope.reportsFilter.$validate().then(function () {
                    $scope.reportsParams.startdate = dataMapHelper.prepareDateForReports($scope.reportsFilter.startDate);
                    $scope.reportsParams.enddate = dataMapHelper.prepareDateForReports($scope.reportsFilter.endDate);
                    $scope.reportsParams.iscustom = $scope.reportsFilter.isCustom;
                });
            },
             clearReportsFilter = function () {
                 $scope.reportsFilter.startDate = null;
                 $scope.reportsFilter.endDate = null;
                 $scope.reportsFilter.stores = [];
                 $scope.reportsFilter.customerGroups = [];
                 onReportParamsChange();
             },
            onGenerateReportClick = function () {
                var storesUri = '';
                var storesNames = [];
                angular.forEach($scope.reportsFilter.stores, function (value) {
                    storesUri = storesUri + "&store=" + value.id;
                    storesNames.push(value.name);
                });
                var groupsUri = '';
                var groupsNames = [];
                angular.forEach($scope.reportsFilter.customerGroups, function (value) {
                    groupsUri = groupsUri + "&group=" + value.id;
                    groupsNames.push(value.name);
                });

                $scope.reportsParams.storename = storesNames.join(", ");
                $scope.reportsParams.groupname = groupsNames.join(", ");
                var reportUri = $state.current.reportUrl + '?' + $.param($scope.reportsParams) + storesUri + groupsUri;
                $scope.reportUri = reportUri;
            },
             getStores = function (query) {
                 return storesSharedService.getStoresByName(query);
             },
             getCustomersGroups = function (query) {
                 return customersGroupsSharedService.getCustomersGroupsByName(query);
             };

        $scope.reportsFilter = reportsFilter;
        $scope.reportsParams = reportsParams;
        $scope.viewModel = viewModel;
        $scope.clearReportsFilter = clearReportsFilter;
        $scope.hideReportsFilters = hideReportsFilters;
        $scope.showReportsFilters = showReportsFilters;
        $scope.onReportParamsChange = onReportParamsChange;
        $scope.onGenerateReportClick = onGenerateReportClick;
        $scope.getCustomersGroups = getCustomersGroups;
        $scope.getStores = getStores;
        $scope.reportFrameTopPosition = 0;
        $scope.reportUri = $state.current.reportUrl;

    }
]);