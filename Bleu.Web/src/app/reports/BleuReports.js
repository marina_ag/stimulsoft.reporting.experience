﻿angular.module("bleu.reports", [
    "bleu.stores.shared",
    "bleu.customers.shared",
    "bleu.menuEditor.shared",
    "bleu.devices",
    "bleu.users.shared",
    "bleu.main",
    "ui.router"
]).config([
    "$stateProvider", "MenuSections", function($stateProvider, menuSections) {
        var customReportsParams = "?startdate&enddate&customer&employee&item&group&category&store&terminal&iscustom";
        $stateProvider
            // todo: temporally hide custom reports section
            //#region Custom Reports
            //.state({
            //    name: 'root.customReports',
            //    url: '/customreports',
            //    views: {
            //        'content-view': {
            //            templateUrl: 'src/app/reports/views/CustomReportsView.html',
            //            controller: 'CustomReportsController'
            //        }
            //    },
            //    menu: {
            //        section: menuSections.reports,
            //        item: 'Main.LeftMenu_MenuItemCustomReports'
            //    }
            //})
            // .state({
            //     name: 'root.customSalesReportsSummary',
            //     url: '/customsalesreportssummary' + customReportsParams,
            //     views: {
            //         'content-view': {
            //             templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //             controller: 'CustomReportsViewerController'
            //         }
            //     },
            //     reportUrl: '/SalesSummary'
            // })
            // .state({
            //     name: 'root.customSalesReportsByDay',
            //     url: '/customsalesreportsbyday' + customReportsParams,
            //     views: {
            //         'content-view': {
            //             templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //             controller: 'CustomReportsViewerController'
            //         }
            //     },
            //     reportUrl: '/SalesByDay'
            // })
            // .state({
            //     name: 'root.customSalesReportsByWeekdays',
            //     url: '/customsalesreportsbyweekdays' + customReportsParams,
            //     views: {
            //         'content-view': {
            //             templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //             controller: 'CustomReportsViewerController'
            //         }
            //     },
            //     reportUrl: '/SalesByWeekdays'
            // })
            // .state({
            //     name: 'root.customSalesReportsByHours',
            //     url: '/customsalesreportsbyhours' + customReportsParams,
            //     views: {
            //         'content-view': {
            //             templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //             controller: 'CustomReportsViewerController'
            //         }
            //     },
            //     reportUrl: '/SalesByHours'
            // })
            // .state({
            //     name: 'root.customsalesReportsByEmployee',
            //     url: '/customsalesreportsbyemployee' + customReportsParams,
            //     views: {
            //         'content-view': {
            //             templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //             controller: 'CustomReportsViewerController'
            //         }
            //     },
            //     reportUrl: '/SalesByEmployee'
            // })
            // .state({
            //     name: 'root.customSalesReportsByCustomer',
            //     url: '/customsalesreportsbycustomer' + customReportsParams,
            //     views: {
            //         'content-view': {
            //             templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //             controller: 'CustomReportsViewerController'
            //         }
            //     },
            //     reportUrl: '/SalesByCustomer'
            // })
            // .state({
            //     name: 'root.customSalesReportsByCustomerGroup',
            //     url: '/customsalesreportsbycustomergroup' + customReportsParams,
            //     views: {
            //         'content-view': {
            //             templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //             controller: 'CustomReportsViewerController'
            //         }
            //     },
            //     reportUrl: '/SalesByCustomerGroup'
            // })
            // .state({
            //     name: 'root.customSalesReportsByStore',
            //     url: '/customsalesreportsbystore' + customReportsParams,
            //     views: {
            //         'content-view': {
            //             templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //             controller: 'CustomReportsViewerController'
            //         }
            //     },
            //     reportUrl: '/SalesByStore'
            // })
            // .state({
            //     name: 'root.customSalesReportsByTerminal',
            //     url: '/customsalesreportsbyterminal' + customReportsParams,
            //     views: {
            //         'content-view': {
            //             templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //             controller: 'CustomReportsViewerController'
            //         }
            //     },
            //     reportUrl: '/SalesByTerminal'
            // })
            // .state({
            //     name: 'root.customSalesReportsByItem',
            //     url: '/customsalesreportsbyitem' + customReportsParams,
            //     views: {
            //         'content-view': {
            //             templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //             controller: 'CustomReportsViewerController'
            //         }
            //     },
            //     reportUrl: '/SalesByItem'
            // })
            // .state({
            //     name: 'root.customSalesReportsByCategory',
            //     url: '/customsalesreportsbycategory' + customReportsParams,
            //     views: {
            //         'content-view': {
            //             templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //             controller: 'CustomReportsViewerController'
            //         }
            //     },
            //     reportUrl: '/SalesByCategory'
            // })
            //.state({
            //    name: 'root.customZReport',
            //    url: '/customzreport' + customReportsParams,
            //    views: {
            //        'content-view': {
            //            templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //            controller: 'CustomReportsViewerController'
            //        }
            //    },
            //    reportUrl: '/ZReport'
            //})
            // .state({
            //     name: 'root.customPaymentsSummaryReport',
            //     url: '/custompaymentssummaryreport' + customReportsParams,
            //     views: {
            //         'content-view': {
            //             templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //             controller: 'CustomReportsViewerController'
            //         }
            //     },
            //     reportUrl: '/PaymentsSummary'
            // })
            // .state({
            //     name: 'root.customPaymentsByDateReport',
            //     url: '/custompaymentsbydatereport' + customReportsParams,
            //     views: {
            //         'content-view': {
            //             templateUrl: 'src/app/reports/views/ReportsViewerView.html',
            //             controller: 'CustomReportsViewerController'
            //         }
            //     },
            //     reportUrl: '/PaymentsByDate'
            // })
            //#endregion
            //#region Fast Reports
            .state({
                name: "root.fastReports",
                url: "/fastreports",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/FastReportsView.html",
                        controller: "FastReportsController"
                    }
                },
                menu: {
                    section: menuSections.reports,
                    item: "Main.LeftMenu_MenuItemFastReports"
                }
            })
            .state({
                name: "root.salesReportsByDay",
                url: "/salesreportsbyday",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/SalesByDayReportView.html",
                        controller: "SummariesReportsController"
                    }
                },
                reportUrl: "/SalesByDay"
            })
            .state({
                name: "root.salesReportsSummary",
                url: "/salesreportssummary",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/SalesSummaryReportView.html",
                        controller: "SummariesReportsController"
                    }
                },
                reportUrl: "/SalesSummary"
            })
            .state({
                name: "root.salesReportsByWeekdays",
                url: "/salesreportsbyweekdays",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/SalesByWeekdaysReportView.html",
                        controller: "SummariesReportsController"
                    }
                },
                reportUrl: "/SalesByWeekdays"
            })
            .state({
                name: "root.salesReportsByHours",
                url: "/salesreportsbyhours",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/SalesByHoursReportView.html",
                        controller: "SummariesReportsController"
                    }
                },
                reportUrl: "/SalesByHours"
            })
            .state({
                name: "root.salesReportsByEmployee",
                url: "/salesreportsbyemployee",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/SalesByEmployeeReportView.html",
                        controller: "SalesByEmployeeReportsController"
                    }
                },
                reportUrl: "/SalesByEmployee"
            })
            .state({
                name: "root.salesReportsByCustomer",
                url: "/salesreportsbycustomer",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/SalesByCustomerReportView.html",
                        controller: "SalesByCustomerReportsController"
                    }
                },
                reportUrl: "/SalesByCustomer"
            })
            .state({
                name: "root.salesReportsByCustomerGroup",
                url: "/salesreportsbycustomergroup",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/SalesByCustomerGroupReportView.html",
                        controller: "SalesByCustomerGroupReportsController"
                    }
                },
                reportUrl: "/SalesByCustomerGroup"
            })
            .state({
                name: "root.salesReportsByStore",
                url: "/salesreportsbystore",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/SalesByStoreReportView.html",
                        controller: "SummariesReportsController"
                    }
                },
                reportUrl: "/SalesByStore"
            })
            .state({
                name: "root.salesReportsByTerminal",
                url: "/salesreportsbyterminal",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/SalesByTerminalReportView.html",
                        controller: "TerminalPaymentsReportsController"
                    }
                },
                reportUrl: "/SalesByTerminal"
            })
            .state({
                name: "root.salesReportsByItem",
                url: "/salesreportsbyitem",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/SalesByMenuItemReportView.html",
                        controller: "SalesByMenuItemReportsController"
                    }
                },
                reportUrl: "/SalesByItem"
            })
            .state({
                name: "root.salesReportsByCategory",
                url: "/salesreportsbycategory",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/SalesByMenuCategoryReportView.html",
                        controller: "SalesByMenuCategoryReportsController"
                    }
                },
                reportUrl: "/SalesByCategory"
            })
            .state({
                name: "root.customersReportsAddresses",
                url: "/customersreportsaddresses",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/CustomersAddressesReportView.html",
                        controller: "CustomersReportsController"
                    }
                },
                reportUrl: "/CustomersAddresses"
            })
            .state({
                name: "root.customersReportsContacts",
                url: "/customersreportscontacts",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/CustomersContactsReportView.html",
                        controller: "CustomersReportsController"
                    }
                },
                reportUrl: "/CustomersContacts"
            })
            .state({
                name: "root.zReportsReport",
                url: "/zreportsreport",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/ZReportView.html",
                        controller: "TerminalPaymentsReportsController"
                    }
                },
                reportUrl: "/ZReport"
            })
            .state({
                name: "root.zReportsPaymentsSummary",
                url: "/zreportspaymentssummary",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/PaymentsSummaryReportView.html",
                        controller: "TerminalPaymentsReportsController"
                    }
                },
                reportUrl: "/PaymentsSummary"
            })
            .state({
                name: "root.zReportsPaymentsByDate",
                url: "/zreportspaymentsbydate",
                views: {
                    'content-view': {
                        templateUrl: "src/app/reports/views/PaymentsByDateReportView.html",
                        controller: "TerminalPaymentsReportsController"
                    }
                },
                reportUrl: "/PaymentsByDate"
            });
        //#endregion
    }
]).run();