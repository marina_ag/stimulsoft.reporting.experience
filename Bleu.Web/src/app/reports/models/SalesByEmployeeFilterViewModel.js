﻿angular.module('bleu.reports').factory('SalesByEmployeeFilterViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var salesByEmployeeFilterViewModel = function () {
            baseViewModel.call(this);

            this.startDate = null;
            this.endDate = null;
            this.employees = [];
            this.stores = [];
            this.isCustom = false;
        };

        salesByEmployeeFilterViewModel.prototype = Object.create(baseViewModel.prototype);

        return salesByEmployeeFilterViewModel;
    }
]);