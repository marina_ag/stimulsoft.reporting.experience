﻿angular.module('bleu.reports').factory('SalesByCustomerGroupParamsViewModel', [
    function () {
        var salesByCustomerGroupParamsViewModel = function () {
            this.startdate = null;
            this.enddate = null;
            this.storename = '';
            this.groupname = '';
            this.iscustom = false;
        };

        return salesByCustomerGroupParamsViewModel;
    }
]);