﻿angular.module('bleu.reports').factory('TerminalPaymentsParamsViewModel', [
    function () {
        var terminalPaymentsParamsViewModel = function () {
            this.startdate = null;
            this.enddate = null;
            this.storename = '';
            this.terminalname = '';
            this.iscustom = false;
        };

        return terminalPaymentsParamsViewModel;
    }
]);