﻿angular.module('bleu.reports').factory('SummariesParamsViewModel', [
    function () {
        var summariesParamsViewModel = function () {
            this.startdate = null;
            this.enddate = null;
            this.storename = '';
            this.iscustom = false;
        };

        return summariesParamsViewModel;
    }
]);