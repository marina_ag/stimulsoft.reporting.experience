﻿angular.module('bleu.reports').factory('SalesByMenuItemPageViewModel', [
    function () {
        var salesByMenuItemPageViewModel = function () {
            this.isFilterShow = true;
        };

        return salesByMenuItemPageViewModel;
    }
]);