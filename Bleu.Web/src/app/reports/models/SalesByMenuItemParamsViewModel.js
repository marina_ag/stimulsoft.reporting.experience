﻿angular.module('bleu.reports').factory('SalesByMenuItemParamsViewModel', [
    function () {
        var salesByMenuItemParamsViewModel = function () {
            this.startdate = null;
            this.enddate = null;
            this.storename = '';
            this.categoryname = '';
            this.itemname = '';
            this.iscustom = false;
        };

        return salesByMenuItemParamsViewModel;
    }
]);