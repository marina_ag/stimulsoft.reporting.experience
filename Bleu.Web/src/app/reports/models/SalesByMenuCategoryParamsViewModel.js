﻿angular.module('bleu.reports').factory('SalesByMenuCategoryParamsViewModel', [
    function () {
        var salesByMenuCategoryParamsViewModel = function () {
            this.startdate = null;
            this.enddate = null;
            this.storename = '';
            this.categoryname = '';
            this.iscustom = false;
        };

        return salesByMenuCategoryParamsViewModel;
    }
]);