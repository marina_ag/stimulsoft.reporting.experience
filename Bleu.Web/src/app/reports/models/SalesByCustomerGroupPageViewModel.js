﻿angular.module('bleu.reports').factory('SalesByCustomerGroupPageViewModel', [
    function () {
        var salesByCustomerGroupPageViewModel = function () {
            this.isFilterShow = true;
        };

        return salesByCustomerGroupPageViewModel;
    }
]);