﻿angular.module('bleu.reports').factory('TerminalPaymentsFilterViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var terminalPaymentsFilterViewModel = function () {
            baseViewModel.call(this);

            this.startDate = null;
            this.endDate = null;
            this.stores = [];
            this.terminals = [];
            this.isCustom = false;
        };

        terminalPaymentsFilterViewModel.prototype = Object.create(baseViewModel.prototype);

        return terminalPaymentsFilterViewModel;
    }
]);