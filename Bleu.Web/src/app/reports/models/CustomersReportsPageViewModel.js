﻿angular.module('bleu.reports').factory('CustomersReportsPageViewModel', [
    function () {
        var customersReportsPageViewModel = function () {
            this.isFilterShow = true;
        };

        return customersReportsPageViewModel;
    }
]);