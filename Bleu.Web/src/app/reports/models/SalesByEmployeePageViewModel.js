﻿angular.module('bleu.reports').factory('SalesByEmployeePageViewModel', [
    function () {
        var salesByEmployeePageViewModel = function () {
            this.isFilterShow = true;
        };

        return salesByEmployeePageViewModel;
    }
]);