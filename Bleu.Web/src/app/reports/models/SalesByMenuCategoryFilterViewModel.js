﻿angular.module('bleu.reports').factory('SalesByMenuCategoryFilterViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var salesByMenuCategoryFilterViewModel = function () {
            baseViewModel.call(this);

            this.startDate = null;
            this.endDate = null;
            this.stores = [];
            this.menuItemCategories = [];
            this.isCustom = false;
        };

        salesByMenuCategoryFilterViewModel.prototype = Object.create(baseViewModel.prototype);

        return salesByMenuCategoryFilterViewModel;
    }
]);