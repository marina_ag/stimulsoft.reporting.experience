﻿angular.module('bleu.reports').factory('SummariesPageViewModel', [
    function () {
        var summariesPageViewModel = function () {
            this.isFilterShow = true;
        };

        return summariesPageViewModel;
    }
]);