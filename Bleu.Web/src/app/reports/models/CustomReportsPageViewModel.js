﻿angular.module('bleu.reports').factory('CustomReportsPageViewModel', [
    function () {
        var customReportsPageViewModel = function () {
            this.isFilterShow = true;
        };

        return customReportsPageViewModel;
    }
]);