﻿angular.module('bleu.reports').factory('SalesByCustomerPageViewModel', [
    function () {
        var salesByCustomerPageViewModel = function () {
            this.isFilterShow = true;
        };

        return salesByCustomerPageViewModel;
    }
]);