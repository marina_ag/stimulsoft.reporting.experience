﻿angular.module('bleu.reports').factory('SummariesFilterViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var summariesFilterViewModel = function () {
            baseViewModel.call(this);

            this.startDate = null;
            this.endDate = null;
            this.stores = [];
            this.isCustom = false;
        };

        summariesFilterViewModel.prototype = Object.create(baseViewModel.prototype);

        return summariesFilterViewModel;
    }
]);