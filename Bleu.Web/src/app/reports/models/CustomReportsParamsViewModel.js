﻿angular.module('bleu.reports').factory('CustomReportsParamsViewModel', [
    function () {
        var customReportsParamsViewModel = function () {
            this.startdate = null;
            this.enddate = null;
            this.customer = '';
            this.group = null;
            this.employee = null;
            this.store = null;
            this.item = '';
            this.category = null;
            this.terminal = null;
            this.iscustom = true;
        };

        return customReportsParamsViewModel;
    }
]);