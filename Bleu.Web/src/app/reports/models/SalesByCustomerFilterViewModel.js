﻿angular.module('bleu.reports').factory('SalesByCustomerFilterViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var salesByCustomerFilterViewModel = function () {
            baseViewModel.call(this);

            this.startDate = null;
            this.endDate = null;
            this.customers = [];
            this.customerGroups = [];
            this.stores = [];
            this.isCustom = false;
        };

        salesByCustomerFilterViewModel.prototype = Object.create(baseViewModel.prototype);

        return salesByCustomerFilterViewModel;
    }
]);