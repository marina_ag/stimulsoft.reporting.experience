﻿angular.module('bleu.reports').factory('SalesByEmployeeParamsViewModel', [
    function () {
        var salesByEmployeeParamsViewModel = function () {
            this.startdate = null;
            this.enddate = null;
            this.storename = '';
            this.employeename = '';
            this.iscustom = false;
        };

        return salesByEmployeeParamsViewModel;
    }
]);