﻿angular.module('bleu.reports').factory('CustomReportsFilterViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var customReportsFilterViewModel = function () {
            baseViewModel.call(this);

            this.startDate = null;
            this.endDate = null;
            this.customerName = '';
            this.customerGroupId = null;
            this.employeeId = null;
            this.storeId = null;
            this.menuItem = '';
            this.menuItemCategoryId = null;
            this.terminalId = null;
            this.isCustom = true;
        };

        customReportsFilterViewModel.prototype = Object.create(baseViewModel.prototype);

        return customReportsFilterViewModel;
    }
]);