﻿angular.module('bleu.reports').factory('SalesByMenuItemFilterViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var salesByMenuItemFilterViewModel = function () {
            baseViewModel.call(this);

            this.startDate = null;
            this.endDate = null;
            this.stores = [];
            this.menuItems = [];
            this.menuItemCategories = [];
            this.isCustom = false;
        };

        salesByMenuItemFilterViewModel.prototype = Object.create(baseViewModel.prototype);

        return salesByMenuItemFilterViewModel;
    }
]);