﻿angular.module('bleu.reports').factory('SalesByCustomerGroupFilterViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var salesByCustomerGroupFilterViewModel = function () {
            baseViewModel.call(this);

            this.startDate = null;
            this.endDate = null;
            this.customerGroups = [];
            this.stores = [];
            this.isCustom = false;
        };

        salesByCustomerGroupFilterViewModel.prototype = Object.create(baseViewModel.prototype);

        return salesByCustomerGroupFilterViewModel;
    }
]);