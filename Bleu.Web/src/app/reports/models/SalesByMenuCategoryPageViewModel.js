﻿angular.module('bleu.reports').factory('SalesByMenuCategoryPageViewModel', [
    function () {
        var salesByMenuCategoryPageViewModel = function () {
            this.isFilterShow = true;
        };

        return salesByMenuCategoryPageViewModel;
    }
]);