﻿angular.module('bleu.reports').factory('SalesByCustomerParamsViewModel', [
    function () {
        var salesByCustomerParamsViewModel = function () {
            this.startdate = null;
            this.enddate = null;
            this.storename = '';
            this.groupname = '';
            this.customername = '';
            this.iscustom = false;
        };

        return salesByCustomerParamsViewModel;
    }
]);