﻿angular.module('bleu.reports').factory('TerminalPaymentsPageViewModel', [
    function () {
        var terminalPaymentsPageViewModel = function () {
            this.isFilterShow = true;
        };

        return terminalPaymentsPageViewModel;
    }
]);