﻿angular.module('bleu.customers.shared').factory('CustomersGroupSharedViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var customersGroupSharedViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.name = '';
            this.description = '';
            this.isAuto = false;
            this.yearsFrom = null;
            this.yearsTo = null;
            this.spentFrom = null;
            this.spentTo = null;
            this.pointsFrom = null;
            this.pointTo = null;
            this.genderFilter = null;
            this.isActive = true;
            this.customersCount = 0;
            this.totalSpent = 0;
            this.avgSpend = 0;
            this.totalOrders = 0;
            this.avgOrders = 0;
        };

        customersGroupSharedViewModel.prototype = Object.create(baseViewModel.prototype);

        return customersGroupSharedViewModel;
    }
]);