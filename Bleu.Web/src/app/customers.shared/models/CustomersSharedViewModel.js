﻿angular.module("bleu.customers.shared").factory("CustomersSharedViewModel", [
    "BaseViewModel",
    function(baseViewModel) {
        var customerViewModel = function() {
            baseViewModel.call(this);

            this.id = null;
            this.fullName = "";
            this.firstName = "";
            this.LastName = "";
        };

        customerViewModel.prototype = Object.create(baseViewModel.prototype);

        return customerViewModel;
    }
]);