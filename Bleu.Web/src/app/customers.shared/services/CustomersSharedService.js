﻿angular.module("bleu.customers.shared").factory("CustomersSharedService", [
    "CustomersTransport",
    "RequestHandlerHelper",
    "CustomersSharedMapper",
    function(customersTransport, requestHandlerHelper, customersSharedMapper) {
        var
            getCustomers = function(filter) {
                return requestHandlerHelper.handleRequest(customersTransport.getFilteredCustomers(filter))
                    .then(function(data) {
                        return customersSharedMapper.mapCollectionToClient(data.Results);
                    });
            },
            getCustomersByName = function(filter, key) {
                return getCustomers(filter).then(function(response) {
                    return response.filter(function(item) {
                        return item.fullName.toLowerCase().indexOf(key.toLowerCase()) != -1;
                    });
                });
            };

        return {
            getCustomers: getCustomers,
            getCustomersByName: getCustomersByName
        };
    }
]);