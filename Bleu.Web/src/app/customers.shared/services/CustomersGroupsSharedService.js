﻿angular.module('bleu.customers.shared').factory('CustomersGroupsSharedService', [
    'CustomersGroupsSharedTransport',
    'CustomersGroupsSharedMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (customersGroupsSharedTransport, customersGroupsSharedMapper, requestHandlerHelper, cacheService) {
        var
            getGroups = function () {
                return customersGroupsSharedTransport.getAllCustomersGroups().then(function (data) {
                    return customersGroupsSharedMapper.mapCollectionToClient(data);
                });
            },
            getAllCustomersGroups = function () {
                return requestHandlerHelper.handleRequest(cacheService.cache('customersGroups', getGroups));
            },
            getCustomersGroupsByName = function (key) {
                return getAllCustomersGroups().then(function (response) {
                    return response.filter(function (item) {
                        return item.name.toLowerCase().indexOf(key.toLowerCase()) != -1;
                    });;
                });
            };

        return {
            getAllCustomersGroups: getAllCustomersGroups,
            getCustomersGroupsByName: getCustomersGroupsByName
        }
    }
]);
