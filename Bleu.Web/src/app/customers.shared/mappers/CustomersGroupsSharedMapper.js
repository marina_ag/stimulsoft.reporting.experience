﻿angular.module('bleu.customers.shared').factory('CustomersGroupsSharedMapper', [
    'CustomersGroupSharedViewModel',
    function (customersGroupSharedViewModel) {
        var
            transformToClient = function (data) {
                var customersGroup = new customersGroupSharedViewModel();

                customersGroup.id = data.GroupId;
                customersGroup.name = data.Name;
                customersGroup.description = data.Description;
                customersGroup.isAuto = data.IsAutoFilled;
                customersGroup.yearsFrom = data.YearsFrom;
                customersGroup.yearsTo = data.YearsTo;
                customersGroup.spentFrom = data.SpentFrom;
                customersGroup.spentTo = data.SpentTo;
                customersGroup.pointsFrom = data.PointsFrom;
                customersGroup.pointsTo = data.PointsTo;
                customersGroup.gender = data.GenderFilter;
                customersGroup.isActive = data.IsActive;
                customersGroup.customersCount = data.CustomerAmount;
                customersGroup.totalSpent = data.TotalSpent;
                customersGroup.avgSpend = data.AvgSpend;
                customersGroup.totalOrders = data.TotalOrders;
                customersGroup.avgOrders = data.AvgOrders;

                return customersGroup;
            },
            transformToServer = function (customer) {
                return {
                    GroupId: customer.id,
                    Name: customer.name,
                    Description: customer.description,
                    IsAutoFilled: customer.isAuto,
                    YearsFrom: customer.yearsFrom,
                    YearsTo: customer.yearsTo,
                    SpentFrom: customer.spentFrom,
                    SpentTo: customer.spentTo,
                    PointsFrom: customer.pointsFrom,
                    PointsTo: customer.pointsTo,
                    GenderFilter: customer.gender,
                    IsActive: customer.isActive
                };
            },
            mapCollectionToClient = function (data) {
                return data.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (data) {
                return transformToClient(data);
            },
            mapCollectionToServer = function (data) {
                return data.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (data) {
                return transformToServer(data);
            };

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);