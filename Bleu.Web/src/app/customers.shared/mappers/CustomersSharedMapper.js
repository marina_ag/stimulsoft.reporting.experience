﻿angular.module("bleu.customers.shared").factory("CustomersSharedMapper", [
    "CustomersSharedViewModel",
    function(customerViewModel) {
        var
            transformToClient = function(data) {
                var customer = new customerViewModel();

                customer.id = data.CardNo;
                customer.fullName = data.FirstName + " " + data.LastName;
                customer.firstName = data.FirstName;
                customer.lastName = data.LastName;

                return customer;
            },
            transformToServer = function(customer) {
                return {
                    CardNo: customer.id,
                    FirstName: customer.firstName,
                    LastName: customer.lastName,
                    HomePhone: customer.phone,
                };
            },
            mapCollectionToClient = function(data) {
                return data.map(function(item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function(data) {
                return transformToClient(data);
            },
            mapCollectionToServer = function(data) {
                return data.map(function(item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function(data) {
                return transformToServer(data);
            };

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        };
    }
]);