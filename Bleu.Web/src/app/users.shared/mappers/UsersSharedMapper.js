﻿angular.module('bleu.users.shared').factory('UsersSharedMapper', [
    'UserSharedViewModel',
    'RolesSharedMapper',
    'DataMapHelper',
    function (userSharedViewModel, rolesSharedMapper, dataMapHelper) {
        var
            transformToClient = function (response) {

                var address = response.AddressDto != null ? response.AddressDto : {
                    Id: dataMapHelper.getGuidEmpty(),
                    Country: "",
                    City: "",
                    State: "",
                    Street: "",
                    ZipCode: ""
                };

                var user = new userSharedViewModel();

                user.id = response.Id;
                user.firstName = response.FirstName;
                user.lastName = response.LastName;
                user.fullName = response.FirstName + " " + response.LastName;
                user.phone = response.PhoneNumber;
                user.email = response.Email;
                user.roles = response.Roles != null ? rolesSharedMapper.mapCollectionToClient(response.Roles) : [];
                user.stores = [];
                user.country = address.Country;
                user.state = address.State;
                user.city = address.City;
                user.address = address.Street;
                user.zip = address.ZipCode;
                user.isActive = response.IsActive;
                user.pin = response.CashierPassword;
                user.login = response.Login;
                user.addressId = address.Id;

                return user;
            },
            transformToServer = function (request) {
                request.roles.forEach(function (role) {
                    role.storesIds = request.stores.map(function (store) {
                        return store.id;
                    });
                });

                var addressDto = {
                    Id: request.addressId,
                    Country: request.country,
                    City: request.city,
                    State: request.state,
                    Street: request.address,
                    ZipCode: request.zip
                };


                return {
                    Id: request.id,
                    FirstName: request.firstName,
                    LastName: request.lastName,
                    PhoneNumber: request.phone,
                    Email: request.email,
                    Roles: rolesSharedMapper.mapCollectionToServer(request.roles),
                    IsActive: request.isActive,
                    CashierPassword: request.pin,
                    Login: request.login,
                    AddressDto: addressDto
                }
            },
            mapCollectionToClient = function (response) {
                return response.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (response) {
                return transformToClient(response);
            },
            mapCollectionToServer = function (request) {
                return request.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            }

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);