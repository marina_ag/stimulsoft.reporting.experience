﻿angular.module('bleu.users.shared').factory('UserSharedViewModel', [
    'BaseViewModel',
    function (baseViewModel) {
        var userSharedViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.firstName = '';
            this.lastName = '';
            this.fullName = '';
            this.phone = '';
            this.email = '';
            this.roles = [];
            this.country = '';
            this.state = '';
            this.city = '';
            this.address = '';
            this.stores = [];
            this.zip = '';
            this.isActive = true;
            this.pin = null;
            this.login = '';
        };

        userSharedViewModel.prototype = Object.create(baseViewModel.prototype);

        return userSharedViewModel;
    }
]);