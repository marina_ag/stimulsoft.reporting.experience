﻿angular.module('bleu.users.shared', [
    'bleu.roles.shared',
    'core.users.shared',
    'bleu.main'
]).run();