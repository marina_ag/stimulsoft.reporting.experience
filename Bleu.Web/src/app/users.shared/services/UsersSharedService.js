﻿angular.module('bleu.users.shared').factory('UsersSharedService', [
    'EmployeesSharedTransport',
    'UsersSharedMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (employeesSharedTransport, usersSharedMapper, requestHandlerHelper, cacheService) {
        var
            getEmployees = function () {
                return employeesSharedTransport.getAllEmployees().then(function (response) {
                    return usersSharedMapper.mapCollectionToClient(response);
                });
            },
            getAllEmployees = function () {
                return requestHandlerHelper.handleRequest(cacheService.cache('employees', getEmployees));
            },
            getEmployeesByName = function (key) {
                return getAllEmployees().then(function (response) {
                    return response.filter(function (item) {
                        return item.fullName.toLowerCase().indexOf(key.toLowerCase()) != -1;
                    });
                });
            };

        return {
            getAllEmployees: getAllEmployees,
            getEmployeesByName: getEmployeesByName
        }
    }
]);