﻿angular.module('bleu.discounts').controller('AddDiscountController', [
    '$scope',
    '$state',
    'DiscountsService',
    'DiscountViewModel',
    'DiscountsViewService',
    'DiscountType',
    function ($scope, $state, discountsService, discountViewModel, discountsViewService, discountType) {
        var discount = new discountViewModel(),
            localizedDiscountTypes = discountsViewService.getDiscountsTypes(),
            saveDiscount = function () {
                $scope.discount.$validate().then(function () {
                    discountsService.addDiscount($scope.discount).then(function () {
                        $state.goBack('root.settingsDiscountsList');
                    }, function (validationInfo) {
                        discountsViewService.validateDiscount(validationInfo, $scope.discount);
                    });
                });
            },
            cancel = function () {
                $state.goBack('root.settingsDiscountsList');
            };

        $scope.discount = discount;
        $scope.discountTypes = discountType;
        $scope.localizedDiscountTypes = localizedDiscountTypes;
        $scope.saveDiscount = saveDiscount;
        $scope.cancel = cancel;
    }
]);