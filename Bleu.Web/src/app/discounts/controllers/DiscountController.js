﻿angular.module('bleu.discounts').controller('DiscountController', [
    '$scope',
    '$state',
    '$stateParams',
    'DiscountsService',
    'DiscountViewModel',
    'DiscountsViewService',
    'DiscountType',
    function ($scope, $state, $stateParams, discountsService, discountViewModel, discountsViewService, discountType) {
        var discount = new discountViewModel(),
            localizedDiscountTypes = discountsViewService.getDiscountsTypes(),
            updateDiscount = function () {
                $scope.discount.$validate().then(function () {
                    discountsService.updateDiscount($scope.discount).then(function () {
                        $state.goBack('root.settingsDiscountsList');
                    }, function (validationInfo) {
                        discountsViewService.validateDiscount(validationInfo, $scope.discount);
                    });
                });
            },
            cancel = function () {
                $state.goBack('root.settingsDiscountsList');
            },
            init = function () {
                discountsService.getDiscount($stateParams.id).then(function (d) {
                    $scope.discount = d;
                });
            };

        $scope.discount = discount;
        $scope.discountTypes = discountType;
        $scope.localizedDiscountTypes = localizedDiscountTypes;
        $scope.updateDiscount = updateDiscount;
        $scope.cancel = cancel;

        init();
    }
]);