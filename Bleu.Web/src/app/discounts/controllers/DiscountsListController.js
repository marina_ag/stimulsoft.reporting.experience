﻿angular.module('bleu.discounts').controller('DiscountsListController', [
    '$scope',
    '$state',
    'DiscountsService',
    'DiscountsGridOptionsModel',
    'DiscountsListPageViewModel',
    'RemoveDiscountsMsgBoxModel',
    'ClientGridDataHelper',
    'MessageBoxHelper',
    'DiscountsViewService',
    function (
        $scope,
        $state,
        discountsService,
        discountsGridOptionsModel,
        discountsListPageViewModel,
        removeDiscountsMsgBoxModel,
        clientGridDataHelper,
        messageBoxHelper,
        discountsViewService) {
        var
            gridOptions = new discountsGridOptionsModel(),
            viewModel = new discountsListPageViewModel(),
            gridHelper = new clientGridDataHelper(gridOptions),
            onSelectionChanged = function (selection) {
                var hasSelection = selection.length != 0;
                viewModel.canDelete = hasSelection;
            },
            getDiscountsIds = function () {
                return gridHelper.getSelection().map(function (item) {
                    return item.id;
                });
            },
            addDiscount = function () {
                $state.go('root.settingsAddDiscount');
            },
            removeDiscounts = function () {
                messageBoxHelper.openModal(new removeDiscountsMsgBoxModel()).then(function (result) {
                    if (result == 'ok') {
                        discountsService.removeDiscounts(getDiscountsIds()).then(function () {
                            gridHelper.reload();
                        });
                    }
                });
            },
            getDiscountTypeName = function (id) {
                return discountsViewService.getDiscountTypeName(id);
            },
            setActiveState = function (id, isActive) {
                discountsService.setActiveState(id, isActive).then(function () {
                    gridHelper.reload();
                });
            },
            init = function () {
                gridHelper.init({
                    grid: $scope,
                    getDataMethod: discountsService.getAllDiscounts,
                    onSelectionChanged: onSelectionChanged
                });
            };

        $scope.getDiscountTypeName = getDiscountTypeName;
        $scope.addDiscount = addDiscount;
        $scope.removeDiscounts = removeDiscounts;
        $scope.gridOptions = gridOptions;
        $scope.viewModel = viewModel;
        $scope.setActiveState = setActiveState;

        init();
    }
]);