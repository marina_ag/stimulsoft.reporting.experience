﻿angular.module('bleu.discounts').factory('DiscountsViewService', [
    'LocalizationService',
    'DiscountType',
    function (localizationService, discountType) {
        var
            validateDiscount = function (validationInfo, model) {
                if (validationInfo != null && validationInfo.Name != null) {
                    model.$setValidity('name', 'exists', false);
                }
            },
            discountTypes = [
                    { id: discountType.Percent, name: localizationService.getLocalizedString('Discounts.DiscountsTypes_Percent') },
                    { id: discountType.MoneyOff, name: localizationService.getLocalizedString('Discounts.DiscountsTypes_MoneyOff') }
            ],
            getDiscountsTypes = function () {
                return discountTypes;
            },
            getDiscountTypeName = function(discountId) {
                var type = discountTypes.filter(function (item) {
                    return item.id == discountId;
                });

                return type.length != 0 ? type[0].name : null;
            };

        return {
            validateDiscount: validateDiscount,
            getDiscountsTypes: getDiscountsTypes,
            getDiscountTypeName: getDiscountTypeName
        }
    }
]);