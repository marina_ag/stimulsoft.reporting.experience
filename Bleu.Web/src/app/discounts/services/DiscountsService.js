﻿angular.module('bleu.discounts').factory('DiscountsService', [
    'DiscountsTransport',
    'DiscountsMapper',
    'RequestHandlerHelper',
    'CacheService',
    function (discountsTransport, discountsMapper, requestHandlerHelper, cacheService) {
        var
            getDiscounts = function () {
                return discountsTransport.getAllDiscounts().then(function (response) {
                    return discountsMapper.mapCollectionToClient(response);
                });
            },
            getAllDiscounts = function () {
                return requestHandlerHelper.handleRequest(cacheService.cache('discounts', getDiscounts));
            },
            getDiscount = function (id) {
                return requestHandlerHelper.handleRequest(discountsTransport.getDiscount(id)).then(function (response) {
                    return discountsMapper.mapInstanceToClient(response);
                });
            },
            updateDiscount = function (discount) {
                return requestHandlerHelper.handleRequest(discountsTransport.updateDiscount(discountsMapper.mapInstanceToServer(discount))).then(function () {
                    cacheService.clear('discounts');
                });
            },
            addDiscount = function (discount) {
                return requestHandlerHelper.handleRequest(discountsTransport.addDiscount(discountsMapper.mapInstanceToServer(discount))).then(function (discountId) {
                    cacheService.clear('discounts');
                    return discountId;
                });
            },
            removeDiscounts = function (discountsIds) {
                return requestHandlerHelper.handleRequest(discountsTransport.removeDiscounts(discountsIds)).then(function () {
                    cacheService.clear('discounts');
                });
            },
            setActiveState = function (id, isActive) {
                return getDiscount(id).then(function (discount) {
                    discount.isActive = isActive;
                    return updateDiscount(discount);
                });
            };

        return {
            getAllDiscounts: getAllDiscounts,
            getDiscount: getDiscount,
            updateDiscount: updateDiscount,
            addDiscount: addDiscount,
            removeDiscounts: removeDiscounts,
            setActiveState: setActiveState
        }
    }
]);