﻿angular.module('bleu.discounts', [
    'core.discounts',
    'bleu.main',
    'ui.router'
]).config(['$stateProvider', 'MenuSections', function ($stateProvider, menuSections) {
    $stateProvider
        .state({
            name: 'root.settingsDiscountsList',
            url: '/discounts',
            views: {
                'content-view': {
                    templateUrl: 'src/app/discounts/views/DiscountsListView.html',
                    controller: 'DiscountsListController'
                }
            },
            menu: {
                section: menuSections.settings,
                item: 'Main.LeftMenu_MenuItemDiscounts',
                order: 50
            }
        })
        .state({
            name: 'root.settingsAddDiscount',
            url: '/adddiscount',
            views: {
                'content-view': {
                    templateUrl: 'src/app/discounts/views/AddDiscountView.html',
                    controller: 'AddDiscountController'
                }
            }
        })
        .state({
            name: 'root.settingsDiscount',
            url: '/discount/:id',
            views: {
                'content-view': {
                    templateUrl: 'src/app/discounts/views/DiscountView.html',
                    controller: 'DiscountController'
                }
            }
        });
}
]).run();