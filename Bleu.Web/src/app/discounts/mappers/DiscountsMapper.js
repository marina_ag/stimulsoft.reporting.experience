﻿angular.module('bleu.discounts').factory('DiscountsMapper', [
    'DiscountViewModel',
    function(discountViewModel) {
        var
            transformToClient = function (response) {
                var discount = new discountViewModel();

                discount.id = response.Id;
                discount.name = response.Name;
                discount.type = response.Type;
                discount.value = response.Value;
                discount.isActive = response.IsActive;
                discount.description = response.Description;

                return discount;
            },
            transformToServer = function (request) {
                return {
                    Id: request.id,
                    Name: request.name,
                    Type: request.type,
                    Value: request.value,
                    IsActive: request.isActive,
                    Description: request.description
                }
            },
            mapCollectionToClient = function (response) {
                return response.map(function (item) {
                    return transformToClient(item);
                });
            },
            mapInstanceToClient = function (response) {
                return transformToClient(response);
            },
            mapCollectionToServer = function (request) {
                return request.map(function (item) {
                    return transformToServer(item);
                });
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            };

        return {
            mapCollectionToClient: mapCollectionToClient,
            mapInstanceToClient: mapInstanceToClient,
            mapCollectionToServer: mapCollectionToServer,
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);