﻿angular.module('bleu.discounts').factory('DiscountViewModel', [
    'BaseViewModel',
    'DiscountType',
    function (baseViewModel, discountType) {
        var discountViewModel = function () {
            baseViewModel.call(this);

            this.id = null;
            this.name = '';
            this.type = discountType.Percent;
            this.value = 0;
            this.isActive = true;
            this.description = '';
        };

        discountViewModel.prototype = Object.create(baseViewModel.prototype);

        return discountViewModel;
    }
]);