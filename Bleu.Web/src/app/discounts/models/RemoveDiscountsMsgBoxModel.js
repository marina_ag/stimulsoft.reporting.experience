﻿angular.module('bleu.discounts').factory('RemoveDiscountsMsgBoxModel', [
    'LocalizationService',
    function (localizationService) {
        var removeDiscountsModel = function () {
            this.title = localizationService.getLocalizedString('Discounts.DiscountsListPageRemoveDiscountsDlg_Title');
            this.message = localizationService.getLocalizedString('Discounts.DiscountsListPageRemoveDiscountsDlg_Message');
            this.buttons = [
                {
                    text: localizationService.getLocalizedString('Discounts.DiscountsListPageRemoveDiscountsDlg_BtnTextOk'),
                    value: 'ok'
                },
                {
                    text: localizationService.getLocalizedString('Discounts.DiscountsListPageRemoveDiscountsDlg_BtnTextCancel'),
                    value: 'cancel'
                }
            ];
        };

        return removeDiscountsModel;
    }
]);