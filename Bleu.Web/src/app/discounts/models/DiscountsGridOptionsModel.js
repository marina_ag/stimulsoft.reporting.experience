﻿angular.module('bleu.discounts').factory('DiscountsGridOptionsModel', [
    'GridDefaultOptions',
    'LocalizationService',
    function (gridDefaultOptions, localizationService) {
        var discountsGridOptionsModel = function () {
            this.useExternalSorting = false;
            this.columnDefs = [
                {
                    field: 'name',
                    displayName: localizationService.getLocalizedString('Discounts.DiscountsGrid_HeaderName'),
                    type: 'string',
                    cellTemplate: 'src/app/discounts/views/grid/DiscountsGridNameTemplate.html'
                },
                {
                    field: 'type',
                    displayName: localizationService.getLocalizedString('Discounts.DiscountsGrid_HeaderType'),
                    type: 'string',
                    cellTemplate: 'src/app/discounts/views/grid/DiscountsGridTypeTemplate.html'
                },
                {
                    field: 'value',
                    displayName: localizationService.getLocalizedString('Discounts.DiscountsGrid_HeaderValue'),
                    type: 'string',
                    cellTemplate: 'src/app/discounts/views/grid/DiscountsGridValueTemplate.html'
                },
                {
                    field: 'isActive',
                    displayName: localizationService.getLocalizedString('Discounts.DiscountsGrid_HeaderIsActive'),
                    type: 'string',
                    cellTemplate: 'src/app/discounts/views/grid/DiscountsGridIsActiveTemplate.html'
                }
            ];
        };

        angular.extend(discountsGridOptionsModel.prototype, new gridDefaultOptions());

        return discountsGridOptionsModel;
    }
]);