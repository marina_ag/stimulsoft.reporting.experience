﻿angular.module('bleu.discounts').factory('DiscountsListPageViewModel', [
    function () {
        var discountsListPageViewModel = function () {
            this.canDelete = false;
        };

        return discountsListPageViewModel;
    }
]);