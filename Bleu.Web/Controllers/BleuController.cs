﻿using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

using Bleu.Web.Helpers;

using YumaPos.Shared.API.Models;

namespace Bleu.Web.Controllers
{
    public class BleuController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.ClientIdentificationTokken = ClientConfig.ClientIdentificationTokken;
            ViewBag.ServerConnectionString = RequestUrlHelper.GetConnectionString(Request.Url, ConfigurationManager.AppSettings["ExternalConnectionString"]);
            var employee = Request.GetFromCookie<EmployeeDto>(ClientConfig.EmployeeCookie);
            if (employee != null)
            {
                if (!string.IsNullOrWhiteSpace(employee.Languages))
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(employee.Languages);
                }

                if (!ClientConfig.SuportedLanguages.Contains(Thread.CurrentThread.CurrentUICulture.Name))
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(ClientConfig.DefaultLanguage);
                }

                return View();
            }

            return RedirectToAction("Login", "Account");
        }
    }
}