﻿using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bleu.Web.Helpers;
using Bleu.Web.Models;

namespace Bleu.Web.Controllers.Api
{
    public class GrecaptchaController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Get(string response)
        {
            var url = ConfigurationManager.AppSettings["GrecaptchaVerifyUrl"] + "?response=" + response + "&secret=" + ConfigurationManager.AppSettings["GrecaptchaSecretKey"];

            var result = WebOperationsHelper.SendRequest<GrecaptchaResponseModel, object>(url, "GET", null, null);

            return result.Success ? Request.CreateResponse(HttpStatusCode.OK) : Request.CreateResponse(HttpStatusCode.BadRequest, result.ErrorCodes);
        }
    }
}
