﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web.Http;
using Bleu.Web.Models;

namespace Bleu.Web.Controllers.Api
{
    public class ContactFormController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Post(ContactFormModel model)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            try
            {
                var mailMessage = new MailMessage();
                mailMessage.To.Add(ConfigurationManager.AppSettings["Email"]);
                mailMessage.Subject = string.Format("{0} by {1} {2}", model.Title, model.FirstName, model.LastName);
                mailMessage.Body = FormatMessage(model);
                mailMessage.IsBodyHtml = true;
                var smtpClient = new SmtpClient();
                smtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private static string FormatMessage(ContactFormModel model)
        {
            var message = new StringBuilder();

            message.AppendFormat("Title: {0}<br/>", model.Title);
            message.AppendFormat("First name: {0}<br/>", model.FirstName);
            message.AppendFormat("Last name: {0}<br/>", model.LastName);
            message.AppendFormat("Email: {0}<br/>", model.Email);
            message.AppendFormat("Phone number: {0}<br/>", model.PhoneNumber);
            message.AppendFormat("Company: {0}<br/>", model.Company);
            message.AppendFormat("How hear about us: {0}<br/>", model.HowHear);
            message.AppendFormat("How can we help: {0}<br/>", model.HowHelp);

            return message.ToString();
        }
    }
}
