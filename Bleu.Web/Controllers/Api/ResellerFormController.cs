﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web.Http;
using Bleu.Web.Models;

namespace Bleu.Web.Controllers.Api
{
    public class ResellerFormController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Post(ResellerFormModel model)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            try
            {
                var mailMessage = new MailMessage();
                mailMessage.To.Add(ConfigurationManager.AppSettings["Email"]);
                mailMessage.Subject = model.Name + " reseller form";
                mailMessage.Body = FormatMessage(model);
                mailMessage.IsBodyHtml = true;
                var smtpClient = new SmtpClient();
                smtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private static string FormatMessage(ResellerFormModel model)
        {
            var message = new StringBuilder();

            message.AppendFormat("Name: : {0}<br/>", model.Name);
            message.AppendFormat("Title: {0}<br/>", model.Title);
            message.AppendFormat("Website: {0}<br/>", model.Website);
            message.AppendFormat("Email: {0}<br/>", model.Email);
            message.AppendFormat("Phone number: {0}<br/>", model.PhoneNumber);
            message.AppendFormat("Company name: {0}<br/>", model.CompanyName);
            message.AppendFormat("Address: {0}<br/>", model.Address);
            message.AppendFormat("City: {0}<br/>", model.City);
            message.AppendFormat("State: {0}<br/>", model.State);
            message.AppendFormat("Zip: {0}<br/>", model.ZipCode);
            message.AppendFormat("Fax#: {0}<br/>", model.Fax);
            message.AppendFormat("Accounting contact: <br/>");
            message.AppendFormat("    Name: {0}<br/>", model.AccountingContactName);
            message.AppendFormat("    Phone: {0}<br/>", model.AccountingContactPhone);
            message.AppendFormat("Purchasing contact: <br/>");
            message.AppendFormat("    Name: {0}<br/>", model.PurchasingContactName);
            message.AppendFormat("    Phone: {0}<br/>", model.PurchasingContactPhone);
            message.AppendFormat("Marketing contact: <br/>");
            message.AppendFormat("    Name: {0}<br/>", model.MarketingContactName);
            message.AppendFormat("    Phone: {0}<br/>", model.MarketingContactPhone);
            message.AppendFormat("Years in business: {0}<br/>", model.YearsInBusiness);
            message.AppendFormat("Business type: <br/>");
            message.AppendFormat("    Corporation: {0}<br/>", model.BusinessTypeCorporation);
            message.AppendFormat("    Partnership: {0}<br/>", model.BusinessTypePartnership);
            message.AppendFormat("    Proprietor: {0}<br/>", model.BusinessTypeProprietor);
            message.AppendFormat("Which distributor purchase from: <br/>");
            message.AppendFormat("    Decision point: {0}<br/>", model.DistributorDecisionPoint);
            message.AppendFormat("    Bluestar: {0}<br/>", model.DistributorBluestar);
            message.AppendFormat("    Scansource: {0}<br/>", model.DistributorScansource);
            message.AppendFormat("    Ready distribution: {0}<br/>", model.DistributorReadyDistribution);
            message.AppendFormat("    Other distributor:{0}<br/>", model.OtherDistributor);
            message.AppendFormat("Distributor account: <br/>");
            message.AppendFormat("    Name: {0}<br/>", model.OtherDistributorAccountName);
            message.AppendFormat("    Phone: {0}<br/>", model.OtherDistributorAccountPhone);
            message.AppendFormat("Company class: <br/>");
            message.AppendFormat("    Reseller: {0}<br/>", model.ClassifyCompanyReseller);
            message.AppendFormat("    VAR: {0}<br/>", model.ClassifyCompanyVar);
            message.AppendFormat("    Sys integrator: {0}<br/>", model.ClassifyCompanySysIntegrator);
            message.AppendFormat("    Consultant: {0}<br/>", model.ClassifyCompanyConsultant);
            message.AppendFormat("    OEM: {0}<br/>", model.ClassifyCompanyOem);
            message.AppendFormat("    Developer: {0}<br/>", model.ClassifyCompanyDeveloper);
            message.AppendFormat("    Chain: {0}<br/>", model.ClassifyCompanyChain);
            message.AppendFormat("    Training: {0}<br/>", model.ClassifyCompanyTraining);
            message.AppendFormat("    Franchise: {0}<br/>", model.ClassifyCompanyFranchise);
            message.AppendFormat("    Distributor: {0}<br/>", model.ClassifyCompanyDistributor);
            message.AppendFormat("    Mail order: {0}<br/>", model.ClassifyCompanyMailOrder);
            message.AppendFormat("    E-Commerce: {0}<br/>", model.ClassifyCompanyECommerce);
            message.AppendFormat("    Other: {0}<br/>", model.ClassifyCompanyOtherList);
            message.AppendFormat("Prior year annual revenue: {0}<br/>", model.PriorYearAnnualRevenue);
            message.AppendFormat("Projected revenue this year: {0}<br/>", model.ProjectedRevenueThisYear);
            message.AppendFormat("Percentage of revenue: <br/>");
            message.AppendFormat("    Hardware: {0}<br/>", model.PercentageOfRevenueHardware);
            message.AppendFormat("    Software: {0}<br/>", model.PercentageOfRevenueSoftware);
            message.AppendFormat("    Services: {0}<br/>", model.PercentageOfRevenueServices);
            message.AppendFormat("Geographical area serve: <br/>");
            message.AppendFormat("    Local (50 mile): {0}<br/>", model.GeographicalAreaLocal);
            message.AppendFormat("    State wide: {0}<br/>", model.GeographicalAreaState);
            message.AppendFormat("    Regional: {0}<br/>", model.GeographicalAreaRegional);
            message.AppendFormat("    International: {0}<br/>", model.GeographicalAreaInternational);
            message.AppendFormat("Total: <br/>");
            message.AppendFormat("    Employees: {0}<br/>", model.TotalEmployees);
            message.AppendFormat("    Engineers: {0}<br/>", model.TotalEngineers);
            message.AppendFormat("    Inside sales: {0}<br/>", model.TotalInsideSales);
            message.AppendFormat("    Outside sales: {0}<br/>", model.TotalOutsideSales);
            message.AppendFormat("Types of marketing activities: <br/>");
            message.AppendFormat("    Seminars: {0}<br/>", model.MarketingActivitiesSeminars);
            message.AppendFormat("    Newsletters: {0}<br/>", model.MarketingActivitiesNewsletters);
            message.AppendFormat("    Print adds: {0}<br/>", model.MarketingActivitiesPrintAdds);
            message.AppendFormat("    Trade shows: {0}<br/>", model.MarketingActivitiesTradeShows);
            message.AppendFormat("    Direct response: {0}<br/>", model.MarketingActivitiesDirectResponse);
            message.AppendFormat("    Other: {0}<br/>", model.MarketingActivitiesOther);
            message.AppendFormat("List of other primary vendors: <br/>    {0}<br/>", model.OtherPrimaryVendors);
            message.AppendFormat("Description of primary 'value added' services: <br/>    {0}<br/>",
                model.ValueAddedServices);
            message.AppendFormat(
                "Other sell products that are similar or complementary to Bleu or other Bleu software: <br/>    {0}",
                model.SimilarProducts);

            return message.ToString();
        }
    }
}
