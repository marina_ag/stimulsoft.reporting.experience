﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

using Bleu.Web.Controllers.Reporting.Base;
using Bleu.Web.Helpers;
using Bleu.Web.Models.Reporting;

using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;

using YumaPos.Reporting.Background.Common;
using YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary;
using YumaPos.Reporting.Infrastructure.Logic.Services.SalesSummary;
using YumaPos.Reporting.Reports.Resources.Sales;

namespace Bleu.Web.Controllers.Reporting
{
    public class SalesByWeekdaysController : BaseReportingController
    {
        private readonly ISalesByWeekdaysService _service;
        private readonly IJobManager _manager;

        public SalesByWeekdaysController(ISalesByWeekdaysService service, IJobManager manager)
        {
            _service = service;
            _manager = manager;
        }

        [HttpGet]
        public ActionResult Index(ReportsParamsModel filter)
        {
            TempData["filter"] = filter;

            return View("ReportView");
        }

        public async Task<ActionResult> GetReportSnapshot()
        {
            await _manager.Start();
            var report = await CreateReport();
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        #region Private methods

        private async Task<StiReport> CreateReport()
        {
            List<SalesByWeekdaysModel> data = new List<SalesByWeekdaysModel>();
            string periodStr = string.Empty, storeStr = string.Empty;
            if (TempData["filter"] != null)
            {
                var filters = TempData["filter"] as ReportsParamsModel;
                DateTime startDate, endDate;
                Guid[] stores;

                if (filters != null && filters.IsCustom)
                {
                    string[] customers, terminals;
                    Guid[] menuItems, categories;
                    int[] customerGroups, employees;

                    // filtering by all parameters for custom report
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out customers, out customerGroups, out menuItems, out categories, out stores, out employees, out terminals);
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");

                    // todo: create method for getting data from operational db later
                    // data = await _service.DoWork;
                }

                if (filters != null && !filters.IsCustom)
                {
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out stores, out storeStr);
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                    if (stores != null)
                    {
                        data = await _service.GetSalesByWeekdays(startDate, endDate, stores);
                    }
                    else
                    {
                        data = await _service.GetSalesByWeekdays(startDate, endDate);
                    }
                }
            }

            var report = new StiReport();
            report.Load(string.Format("{0}SalesByWeekdaysReport.mrt", ReportsPath));

            report.Dictionary.Clear();

            report.Dictionary.Variables.Add(new StiVariable("String", "parPeriod", typeof(string), periodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCurrency", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_CurrencyStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCreated", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_CreatedStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parCurrency", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_CurrencyParam, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strDiscountTotalColumn", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_DiscountTotalColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strFilters", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_FiltersStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strHeader", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_HeaderStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strNameColumn", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_NameColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strNetSalesColumn", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_NetSalesColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strPeriod", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_PeriodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSalesWithTaxColumn", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_SalesWithTaxColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSoldQuantityColumn", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_SoldQuantityColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTax1Column", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_Tax1Column, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotal", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_TotalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strStore", typeof(string), SalesByWeekdaysResource.SalesByWeekdays_StoreStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parStore", typeof(string), storeStr, true));

            report.RegBusinessObject("SalesByWeekdaysBO", data);
            return report;
        }

        #endregion
    }
}