﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

using Bleu.Web.Controllers.Reporting.Base;
using Bleu.Web.Helpers;
using Bleu.Web.Models.Reporting;

using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;

using YumaPos.Reporting.Background.Common;
using YumaPos.Reporting.Infrastructure.Logic.Models.SalesByLocation;
using YumaPos.Reporting.Infrastructure.Logic.Services.SalesByLocation;
using YumaPos.Reporting.Reports.Resources.Sales;

namespace Bleu.Web.Controllers.Reporting
{
    public class SalesByTerminalController : BaseReportingController
    {
        private readonly ISalesByTerminalService _service;
        private readonly IJobManager _manager;

        public SalesByTerminalController(ISalesByTerminalService service, IJobManager manager)
        {
            _service = service;
            _manager = manager;
        }

        [HttpGet]
        public ActionResult Index(ReportsParamsModel filter)
        {
            TempData["filter"] = filter;

            return View("ReportView");
        }

        public async Task<ActionResult> GetReportSnapshot()
        {
            await _manager.Start();
            var report = await CreateReport();
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        #region Private methods

        private async Task<StiReport> CreateReport()
        {
            var data = new List<SalesByTerminalModel>();
            string periodStr = string.Empty, storeStr = string.Empty, terminalStr = string.Empty;
            if (TempData["filter"] != null)
            {
                var filters = TempData["filter"] as ReportsParamsModel;
                DateTime startDate, endDate;
                Guid[] stores;
                string[] terminals;

                if (filters != null && filters.IsCustom)
                {
                    string[] customers;
                    Guid[] menuItems, categories;
                    int[] customerGroups, employees;
                   
                    // filtering by all parameters for custom report
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out customers, out customerGroups, out menuItems, out categories, out stores, out employees, out terminals);

                    // todo: create method for getting data from operational db later
                    // data = await _service.DoWork;
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                }

                if (filters != null && !filters.IsCustom)
                {
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out stores, out terminals, out terminalStr, out storeStr);
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                    if (stores != null && terminals != null)
                    {
                        data = await _service.GetSalesByTerminal(startDate, endDate, stores, terminals);
                    }

                    if (stores != null && terminals == null)
                    {
                        data = await _service.GetSalesByTerminal(startDate, endDate, stores);
                    }

                    if (stores != null && terminals != null)
                    {
                        data = await _service.GetSalesByTerminal(startDate, endDate, terminals);
                    }

                    if (stores == null && terminals == null)
                    {
                        data = await _service.GetSalesByTerminal(startDate, endDate);
                    }
                }
            }

            var report = new StiReport();
            report.Load(string.Format("{0}SalesByTerminalReport.mrt", ReportsPath));

            report.Dictionary.Clear();

            report.Dictionary.Variables.Add(new StiVariable("String", "parPeriod", typeof(string), periodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCurrency", typeof(string), SalesByTerminalResource.SalesByTerminal_CurrencyStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCreated", typeof(string), SalesByTerminalResource.SalesByTerminal_CreatedStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parCurrency", typeof(string), SalesByTerminalResource.SalesByTerminal_CurrencyParam, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strDiscountTotalColumn", typeof(string), SalesByTerminalResource.SalesByTerminal_DiscountTotalColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strFilters", typeof(string), SalesByTerminalResource.SalesByTerminal_FiltersStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strHeader", typeof(string), SalesByTerminalResource.SalesByTerminal_HeaderStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strNameColumn", typeof(string), SalesByTerminalResource.SalesByTerminal_NameColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strNetSalesColumn", typeof(string), SalesByTerminalResource.SalesByTerminal_NetSalesColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strPeriod", typeof(string), SalesByTerminalResource.SalesByTerminal_PeriodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSalesWithTaxColumn", typeof(string), SalesByTerminalResource.SalesByTerminal_SalesWithTaxColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSoldQuantityColumn", typeof(string), SalesByTerminalResource.SalesByTerminal_SoldQuantityColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTax1Column", typeof(string), SalesByTerminalResource.SalesByTerminal_Tax1Column, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotal", typeof(string), SalesByTerminalResource.SalesByTerminal_TotalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strStore", typeof(string), SalesByTerminalResource.SalesByTerminal_StoreStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parStore", typeof(string), storeStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTerminal", typeof(string), SalesByTerminalResource.SalesByTerminal_TerminalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parTerminal", typeof(string), terminalStr, true));

            report.RegBusinessObject("SalesByTerminalBO", data);
            return report;
        }

        #endregion
    }
}