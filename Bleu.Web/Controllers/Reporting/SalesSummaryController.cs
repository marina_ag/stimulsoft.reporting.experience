﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;

using Bleu.Web.Controllers.Reporting.Base;
using Bleu.Web.Helpers;
using Bleu.Web.Models.Reporting;

using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;

using YumaPos.Reporting.Background.Common;
using YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary;
using YumaPos.Reporting.Infrastructure.Logic.Services.SalesSummary;
using YumaPos.Reporting.Reports.Resources.Sales;

namespace Bleu.Web.Controllers.Reporting
{
    public class SalesSummaryController : BaseReportingController
    {
        private readonly ISalesSummaryService _service;
        private readonly IJobManager _manager;

        public SalesSummaryController(ISalesSummaryService service, IJobManager manager)
        {
            _service = service;
            _manager = manager;
        }

        [HttpGet]
        public ActionResult Index(ReportsParamsModel filter)
        {
            TempData["filter"] = filter;
            return View("ReportView");
        }

        public async Task<ActionResult> GetReportSnapshot()
        {
            await _manager.Start();
            var report = await CreateReport();
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        #region Private methods

        private async Task<StiReport> CreateReport()
        {
            var data = new SalesSummaryModel();
            string periodStr = string.Empty, storeStr = string.Empty;
            if (TempData["filter"] != null)
            {
                var filters = TempData["filter"] as ReportsParamsModel;
                DateTime startDate, endDate;
                Guid[] stores;

                if (filters != null && filters.IsCustom)
                {
                    string[] customers, terminals;
                    Guid[] menuItems, categories;
                    int[] customerGroups, employees;

                    // filtering by all parameters for custom report
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out customers, out customerGroups, out menuItems, out categories, out stores, out employees, out terminals);
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");

                    // todo: create method for getting data from operational db later
                    // data = await _service.DoWork;
                }

                if (filters != null && !filters.IsCustom)
                {
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out stores, out storeStr);
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                    if (stores != null)
                    {
                        data = await _service.GetSalesSummary(startDate, endDate, stores);
                    }
                    else
                    {
                        data = await _service.GetSalesSummary(startDate, endDate);
                    }
                }
            }

            var report = new StiReport();
            report.Load(string.Format("{0}SalesSummaryReport.mrt", ReportsPath));

            report.Dictionary.Clear();

            report.Dictionary.Variables.Add(new StiVariable("String", "parPeriod", typeof(string), periodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCurrency", typeof(string), SalesSummaryResource.SalesSummary_CurrencyStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCreated", typeof(string), SalesSummaryResource.SalesSummary_CreatedStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parCurrency", typeof(string), SalesSummaryResource.SalesSummary_CurrencyParam, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strFilters", typeof(string), SalesSummaryResource.SalesSummary_FiltersStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strHeader", typeof(string), SalesSummaryResource.SalesSummary_HeaderStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strPeriod", typeof(string), SalesSummaryResource.SalesSummary_PeriodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSalesTotal", typeof(string), SalesSummaryResource.SalesSummary_SalesTotalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTaxesTotal", typeof(string), SalesSummaryResource.SalesSummary_TaxesTotalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSalesWithTaxTotal", typeof(string), SalesSummaryResource.SalesSummary_SalesWithTaxTotalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strStore", typeof(string), SalesSummaryResource.SalesSummary_StoreStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parStore", typeof(string), storeStr, true));

            report.RegBusinessObject("SalesSummaryBO", data);
            return report;
        }

        #endregion
    }
}