﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

using Bleu.Web.Controllers.Reporting.Base;
using Bleu.Web.Helpers;
using Bleu.Web.Models.Reporting;

using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;

using YumaPos.Reporting.Background.Common;
using YumaPos.Reporting.Infrastructure.Logic.Models.SalesSummary;
using YumaPos.Reporting.Infrastructure.Logic.Services.SalesSummary;
using YumaPos.Reporting.Reports.Resources.Sales;

namespace Bleu.Web.Controllers.Reporting
{
    public class SalesByDayController : BaseReportingController
    {
        private readonly IJobManager _manager;

        private readonly ISalesByDayService _service;

        public SalesByDayController(ISalesByDayService service, IJobManager manager)
        {
            _service = service;
            _manager = manager;
        }

        [HttpGet]
        public ActionResult Index(ReportsParamsModel filter)
        {
            TempData["filter"] = filter;

            return View("ReportView");
        }

        public async Task<ActionResult> GetReportSnapshot()
        {
            await _manager.Start();
            var report = await CreateReport();
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        #region Private methods

        private async Task<StiReport> CreateReport()
        {
            string periodStr = string.Empty, storeStr = string.Empty;
            var data = new List<SalesByDayModel>();
            if (TempData["filter"] != null)
            {
                var filters = TempData["filter"] as ReportsParamsModel;
                DateTime startDate, endDate;
                Guid[] stores;

                if (filters != null && filters.IsCustom)
                {
                    string[] customers, terminals;
                    Guid[] menuItems, categories;
                    int[] customerGroups, employees;
                    
                    // filtering by all parameters for custom report
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out customers, out customerGroups, out menuItems, out categories, out stores, out employees, out terminals);

                    // todo: create method for getting data from operational db later
                    // data = await _service.DoWork;
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                }

                if (filters != null && !filters.IsCustom)
                {
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out stores, out storeStr);
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                    if (stores != null)
                    {
                        data = await _service.GetSalesByDay(startDate, endDate, stores);
                    }
                    else
                    {
                        data = await _service.GetSalesByDay(startDate, endDate);
                    }
                }
            }

            var report = new StiReport();
            report.Load(string.Format("{0}SalesByDayReport.mrt", ReportsPath));

            report.Dictionary.Clear();

            report.Dictionary.Variables.Add(new StiVariable("String", "strNumOfSalesColumn", typeof(string), SalesByDayResource.SalesByDay_NumOfSalesColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strAverageUnitsColumn", typeof(string), SalesByDayResource.SalesByDay_AverageUnitsColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strAverageSoldColumn", typeof(string), SalesByDayResource.SalesByDay_AverageSold, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parPeriod", typeof(string), periodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCurrency", typeof(string), SalesByDayResource.SalesByDay_CurrencyStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCreated", typeof(string), SalesByDayResource.SalesByDay_CreatedStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parCurrency", typeof(string), SalesByDayResource.SalesByDay_CurrencyParam, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strDiscountTotalColumn", typeof(string), SalesByDayResource.SalesByDay_DiscountTotalColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strFilters", typeof(string), SalesByDayResource.SalesByDay_FiltersStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strHeader", typeof(string), SalesByDayResource.SalesByDay_HeaderStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strDateColumn", typeof(string), SalesByDayResource.SalesByDay_DateColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strNetSalesColumn", typeof(string), SalesByDayResource.SalesByDay_NetSalesColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strPeriod", typeof(string), SalesByDayResource.SalesByDay_PeriodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSalesWithTaxColumn", typeof(string), SalesByDayResource.SalesByDay_SalesWithTaxColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSoldQuantityColumn", typeof(string), SalesByDayResource.SalesByDay_SoldQuantityColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTax1Column", typeof(string), SalesByDayResource.SalesByDay_Tax1Column, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotal", typeof(string), SalesByDayResource.SalesByDay_TotalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strStore", typeof(string), SalesByDayResource.SalesByDay_StoreStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parStore", typeof(string), storeStr, true));

            report.RegBusinessObject("SalesByDayBO", data);
            return report;
        }

        #endregion
    }
}