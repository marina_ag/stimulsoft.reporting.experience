﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

using Bleu.Web.Controllers.Reporting.Base;
using Bleu.Web.Models.Reporting;

using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;

using YumaPos.Reporting.Infrastructure.Logic.Models.Customers;
using YumaPos.Reporting.Infrastructure.Logic.Services.Customers;
using YumaPos.Reporting.Reports.Resources;
using YumaPos.Reporting.Reports.Resources.Customers;

namespace Bleu.Web.Controllers.Reporting
{
    public class CustomersAddressesController : BaseReportingController
    {
        private readonly ICustomersAddressesService _service;

        public CustomersAddressesController(ICustomersAddressesService service)
        {
            _service = service;
        }

        [HttpGet]
        public ActionResult Index(CustomersReportParamsModel filter)
        {
            TempData["filter"] = filter;

            return View("ReportView");
        }

        public async Task<ActionResult> GetReportSnapshot()
        {
            var report = await CreateReport();
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        private async Task<StiReport> CreateReport()
        {
            var groupStr = Filters.AllCustomersGroupsStr;
            var report = new StiReport();
            report.Load(string.Format("{0}CustomersAddressesCRM.mrt", ReportsPath));

            int[] customerGroups = null;
            var filter = TempData["filter"] as CustomersReportParamsModel;
            if (filter != null)
            {
                customerGroups = filter.CustomerGroup;
                if (filter.GroupName != null)
                {
                    groupStr = filter.GroupName != string.Empty ? filter.GroupName : Filters.AllCustomersGroupsStr;
                }
            }

            List<CustomersAddressesModel> data;
            if (customerGroups != null)
            {
                data = await _service.GetAllCustomersAddressesForGroup(customerGroups);
            }
            else
            {
                data = await _service.GetAllCustomersAddresses();
            }

            report.Dictionary.Clear();

            report.Dictionary.Variables.Add(new StiVariable("String", "strCustomerColumn", typeof(string), AddressesCRMResource.AddressesCRM_CustomerColumnStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTypeColumn", typeof(string), AddressesCRMResource.AddressesCRM_TypeColumnStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strAddressColumn", typeof(string), AddressesCRMResource.AddressesCRM_AddressStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strPostcodeColumn", typeof(string), AddressesCRMResource.AddressesCRM_PostalCodeColumnStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCityColumn", typeof(string), AddressesCRMResource.AddressesCRM_CityColumnStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strStateColumn", typeof(string), AddressesCRMResource.AddressesCRM_StateColumnStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strHeader", typeof(string), AddressesCRMResource.AddressesCRM_HeaderStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCountryColumn", typeof(string), AddressesCRMResource.AddressesCRM_CountryColumnStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strActiveColumn", typeof(string), AddressesCRMResource.AddressesCRM_ActiveColumnStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strGroup", typeof(string), AddressesCRMResource.AddressesCRM_GroupStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parGroup", typeof(string), groupStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCreated", typeof(string), AddressesCRMResource.AddressesCRM_CreatedStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strFilters", typeof(string), AddressesCRMResource.AddressesCRM_FiltersStr, true));

            report.RegBusinessObject("GetAddressesBO", data);

            return report;
        }
    }
}