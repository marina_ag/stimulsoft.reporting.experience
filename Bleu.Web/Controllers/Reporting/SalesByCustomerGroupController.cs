﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

using Bleu.Web.Controllers.Reporting.Base;
using Bleu.Web.Helpers;
using Bleu.Web.Models.Reporting;

using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;

using YumaPos.Reporting.Background.Common;
using YumaPos.Reporting.Infrastructure.Logic.Models.SalesByCustomer;
using YumaPos.Reporting.Infrastructure.Logic.Services.SalesByCustomer;
using YumaPos.Reporting.Reports.Resources.Sales;

namespace Bleu.Web.Controllers.Reporting
{
    public class SalesByCustomerGroupController : BaseReportingController
    {
        private readonly ISalesByCustomerGroupService _service;
        private readonly IJobManager _manager;

        public SalesByCustomerGroupController(ISalesByCustomerGroupService service, IJobManager manager)
        {
            _service = service;
            _manager = manager;
        }

        [HttpGet]
        public ActionResult Index(ReportsParamsModel filter)
        {
            TempData["filter"] = filter;

            return View("ReportView");
        }

        public async Task<ActionResult> GetReportSnapshot()
        {
            await _manager.Start();
            var report = await CreateReport();
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        #region Private methods

        private async Task<StiReport> CreateReport()
        {
            var data = new List<SalesByCustomerGroupModel>();
            string periodStr = string.Empty, storeStr = string.Empty, groupStr = string.Empty;
            if (TempData["filter"] != null)
            {
                var filters = TempData["filter"] as ReportsParamsModel;
                DateTime startDate, endDate;
                Guid[] stores;
                int[] customerGroups;

                if (filters != null && filters.IsCustom)
                {
                    string[] customers, terminals;
                    Guid[] menuItems, categories;
                    int[] employees;
                    
                    // filtering by all parameters for custom report
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out customers, out customerGroups, out menuItems, out categories, out stores, out employees, out terminals);

                    // todo: create method for getting data from operational db later
                    // data = await _service.DoWork;
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                }

                if (filters != null && !filters.IsCustom)
                {
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out customerGroups, out stores, out groupStr, out storeStr);
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                    if (stores != null && customerGroups != null)
                    {
                        data = await _service.GetSalesByCustomerGroup(startDate, endDate, customerGroups, stores);
                    }

                    if (stores != null && customerGroups == null)
                    {
                        data = await _service.GetSalesByCustomerGroup(startDate, endDate, stores);
                    }

                    if (stores == null && customerGroups != null)
                    {
                        data = await _service.GetSalesByCustomerGroup(startDate, endDate, customerGroups);
                    }

                    if (stores == null && customerGroups == null)
                    {
                        data = await _service.GetSalesByCustomerGroup(startDate, endDate);
                    }
                }
            }


            var report = new StiReport();
            report.Load(string.Format("{0}SalesByCustomerGroupReport.mrt", ReportsPath));

            report.Dictionary.Clear();

            report.Dictionary.Variables.Add(new StiVariable("String", "parPeriod", typeof(string), periodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCurrency", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_CurrencyStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCreated", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_CreatedStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parCurrency", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_CurrencyParam, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strDiscountTotalColumn", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_DiscountTotalColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strFilters", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_FiltersStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strHeader", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_HeaderStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strNameColumn", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_NameColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strNetSalesColumn", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_NetSalesColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strPeriod", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_PeriodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSalesWithTaxColumn", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_SalesWithTaxColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSoldQuantityColumn", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_SoldQuantityColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTax1Column", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_Tax1Column, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strQuantityOfCustomersColumn", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_QtyOfCustomersColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strAverageSoldColumn", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_AverageSoldColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strAutoFilledColumn", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_AutoFilledColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotal", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_TotalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strStore", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_StoreStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parStore", typeof(string), storeStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strGroup", typeof(string), SalesByCustomerGroupResource.SalesByCustomerGroup_GroupStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parGroup", typeof(string), groupStr, true));

            report.RegBusinessObject("SalesByCustomerGroupBO", data);
            return report;
        }

        #endregion
    }
}