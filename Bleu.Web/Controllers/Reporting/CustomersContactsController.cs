﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

using Bleu.Web.Controllers.Reporting.Base;
using Bleu.Web.Models.Reporting;

using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;

using YumaPos.Reporting.Infrastructure.Logic.Models.Customers;
using YumaPos.Reporting.Infrastructure.Logic.Services.Customers;
using YumaPos.Reporting.Reports.Resources;
using YumaPos.Reporting.Reports.Resources.Customers;

namespace Bleu.Web.Controllers.Reporting
{
    public class CustomersContactsController : BaseReportingController
    {
        private readonly ICustomersContactsService _service;

        public CustomersContactsController(ICustomersContactsService service)
        {
            _service = service;
        }

        [HttpGet]
        public ActionResult Index(CustomersReportParamsModel filter)
        {
            TempData["filter"] = filter;

            return View("ReportView");
        }

        public async Task<ActionResult> GetReportSnapshot()
        {
            var report = await CreateReport();
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        private async Task<StiReport> CreateReport()
        {
            var groupStr = Filters.AllCustomersGroupsStr;
            var report = new StiReport();
            report.Load(string.Format("{0}CustomersContactsCRM.mrt", ReportsPath));

            int[] customerGroups = null;
            var filter = TempData["filter"] as CustomersReportParamsModel;
            if (filter != null)
            {
                customerGroups = filter.CustomerGroup;
                if (filter.GroupName != null)
                {
                    groupStr = filter.GroupName != string.Empty ? filter.GroupName : Filters.AllCustomersGroupsStr;
                }
            }

            List<CustomersContactsModel> data;
            if (customerGroups != null)
            {
                data = await _service.GetAllCustomersContactsForGroup(customerGroups);
            }
            else
            {
                data = await _service.GetAllCustomersContacts();
            }

            report.Dictionary.Clear();

            report.Dictionary.Variables.Add(new StiVariable("String", "strCustomerColumn", typeof(string), ContactsCRMResource.ContactsCRM_CustomerColumnStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strEmailColumn", typeof(string), ContactsCRMResource.ContactsCRM_EmailStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strHomePhoneColumn", typeof(string), ContactsCRMResource.ContactsCRM_HomePhoneStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strBusinessPhoneColumn", typeof(string), ContactsCRMResource.ContactsCRM_BusinessPhoneStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strHeader", typeof(string), ContactsCRMResource.ContactsCRM_HeaderStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strGroup", typeof(string), ContactsCRMResource.ContactsCRM_GroupStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parGroup", typeof(string), groupStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCreated", typeof(string), ContactsCRMResource.ContactsCRM_CreatedStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strFilters", typeof(string), ContactsCRMResource.ContactsCRM_FiltersStr, true));

            report.RegBusinessObject("CustomersContactsBO", data);

            return report;
        }
    }
}