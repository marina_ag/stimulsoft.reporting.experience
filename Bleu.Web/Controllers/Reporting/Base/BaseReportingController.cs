﻿using System;
using System.Web.Mvc;

namespace Bleu.Web.Controllers.Reporting.Base
{
    public class BaseReportingController : Controller
    {
        public BaseReportingController()
        {
            ReportsPath = string.Format("{0}bin/Reports/", AppDomain.CurrentDomain.BaseDirectory);
        }

        protected string ReportsPath { get; set; }
    }
}