﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

using Bleu.Web.Controllers.Reporting.Base;
using Bleu.Web.Helpers;
using Bleu.Web.Models.Reporting;

using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;

using YumaPos.Reporting.Background.Common;
using YumaPos.Reporting.Infrastructure.Logic.Models.ItemSales;
using YumaPos.Reporting.Infrastructure.Logic.Services.ItemSales;
using YumaPos.Reporting.Reports.Resources.Sales;

namespace Bleu.Web.Controllers.Reporting
{
    public class SalesByItemController : BaseReportingController
    {
        private readonly ISalesByMenuItemService _service;
        private readonly IJobManager _manager;

        public SalesByItemController(ISalesByMenuItemService service, IJobManager manager)
        {
            _service = service;
            _manager = manager;
        }

        [HttpGet]
        public ActionResult Index(ReportsParamsModel filter)
        {
            TempData["filter"] = filter;

            return View("ReportView");
        }

        public async Task<ActionResult> GetReportSnapshot()
        {
            await _manager.Start();
            var report = await CreateReport();
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        #region Private methods

        private async Task<StiReport> CreateReport()
        {
            var data = new List<SalesByItemModel>();
            string periodStr = string.Empty, storeStr = string.Empty, categoryStr = string.Empty, itemStr = string.Empty;
            if (TempData["filter"] != null)
            {
                var filters = TempData["filter"] as ReportsParamsModel;
                DateTime startDate, endDate;
                Guid[] stores, menuItems, categories;
               
                if (filters != null && filters.IsCustom)
                {
                    int[] employees, customerGroups;
                    string[] terminals, customers;

                    // filtering by all parameters for custom report
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out customers, out customerGroups, out menuItems, out categories, out stores, out employees, out terminals);

                    // todo: create method for getting data from operational db later
                    // data = await _service.DoWork;
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                }

                if (filters != null && !filters.IsCustom)
                {
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out stores, out menuItems, out categories, out itemStr, out categoryStr, out storeStr);
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");

                    if (stores != null)
                    {
                        if (categories != null)
                        {
                            if (menuItems != null)
                            {
                                data = await _service.GetSalesByMenuItem(startDate, endDate, categories, menuItems, stores);
                            }
                            else
                            {
                                data = await _service.GetSalesByMenuItemForCategories(startDate, endDate, categories, stores);
                            }
                        }
                        else
                        {
                            if (menuItems != null)
                            {
                                data = await _service.GetSalesByMenuItemForStore(startDate, endDate, menuItems, stores);
                            }
                            else
                            {
                                data = await _service.GetSalesByMenuItemForStore(startDate, endDate, stores);
                            }
                        }
                    }

                    if (stores == null)
                    {
                        if (categories != null && menuItems != null)
                        {
                            data = await _service.GetSalesByMenuItem(startDate, endDate, categories, menuItems);
                        }

                        if (categories != null && menuItems == null)
                        {
                            data = await _service.GetSalesByMenuItemForCategories(startDate, endDate, categories);
                        }

                        if (categories == null && menuItems != null)
                        {
                            data = await _service.GetSalesByMenuItem(startDate, endDate, menuItems);
                        }

                        if (categories == null && menuItems == null)
                        {
                            data = await _service.GetSalesByMenuItem(startDate, endDate);
                        }
                    }
                }
            }

            var report = new StiReport();
            report.Load(string.Format("{0}SalesByItemReport.mrt", ReportsPath));

            report.Dictionary.Clear();

            report.Dictionary.Variables.Add(new StiVariable("String", "parPeriod", typeof(string), periodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCurrency", typeof(string), SalesByItemResource.SalesByItem_CurrencyStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCreated", typeof(string), SalesByItemResource.SalesByItem_CreatedStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parCurrency", typeof(string), SalesByItemResource.SalesByItem_CurrencyParam, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strDiscountTotalColumn", typeof(string), SalesByItemResource.SalesByItem_DiscountTotalColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strFilters", typeof(string), SalesByItemResource.SalesByItem_FiltersStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strHeader", typeof(string), SalesByItemResource.SalesByItem_HeaderStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strNameColumn", typeof(string), SalesByItemResource.SalesByItem_NameColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strNetSalesColumn", typeof(string), SalesByItemResource.SalesByItem_NetSalesColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strPeriod", typeof(string), SalesByItemResource.SalesByItem_PeriodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSalesWithTaxColumn", typeof(string), SalesByItemResource.SalesByItem_SalesWithTaxColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSoldQuantityColumn", typeof(string), SalesByItemResource.SalesByItem_SoldQuantityColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCodeColumn", typeof(string), SalesByItemResource.SalesByItem_CodeColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotal", typeof(string), SalesByItemResource.SalesByItem_TotalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strUpcCodeColumn", typeof(string), SalesByItemResource.SalesByItem_UpcCodeColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strUnitColumn", typeof(string), SalesByItemResource.SalesByItem_UnitColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strStore", typeof(string), SalesByItemResource.SalesByItem_StoreStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parStore", typeof(string), storeStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strItem", typeof(string), SalesByItemResource.SalesByItem_ItemStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parItem", typeof(string), itemStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCategory", typeof(string), SalesByItemResource.SalesByItem_CategoryStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parCategory", typeof(string), categoryStr, true));

            report.RegBusinessObject("SalesByItemBO", data);
            return report;
        }

        #endregion
    }
}