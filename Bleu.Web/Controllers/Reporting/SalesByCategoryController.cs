﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

using Bleu.Web.Controllers.Reporting.Base;
using Bleu.Web.Helpers;
using Bleu.Web.Models.Reporting;

using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;

using YumaPos.Reporting.Background.Common;
using YumaPos.Reporting.Infrastructure.Logic.Models.ItemSales;
using YumaPos.Reporting.Infrastructure.Logic.Services.ItemSales;
using YumaPos.Reporting.Reports.Resources.Sales;

namespace Bleu.Web.Controllers.Reporting
{
    public class SalesByCategoryController : BaseReportingController
    {
        private readonly ISalesByMenuCategoryService _service;
        private readonly IJobManager _manager;

        public SalesByCategoryController(ISalesByMenuCategoryService service, IJobManager manager)
        {
            _service = service;
            _manager = manager;
        }

        [HttpGet]
        public ActionResult Index(ReportsParamsModel filter)
        {
            TempData["filter"] = filter;

            return View("ReportView");
        }

        public async Task<ActionResult> GetReportSnapshot()
        {
            await _manager.Start();
            var report = await CreateReport();
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        #region Private methods

        private async Task<StiReport> CreateReport()
        {
            List<SalesByCategoryModel> data = new List<SalesByCategoryModel>();
            string periodStr = string.Empty, storeStr = string.Empty, categoryStr = string.Empty;
            if (TempData["filter"] != null)
            {
                var filters = TempData["filter"] as ReportsParamsModel;
                DateTime startDate, endDate;
                Guid[] stores, categories;

                if (filters != null && filters.IsCustom)
                {
                    string[] customers, terminals;
                    Guid[] menuItems;
                    int[] customerGroups, employees;

                    // filtering by all parameters for custom report
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out customers, out customerGroups, out menuItems, out categories, out stores, out employees, out terminals);

                    // todo: create method for getting data from operational db later
                    // data = await _service.DoWork;
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                }

                if (filters != null && !filters.IsCustom)
                {
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out stores, out categories, out storeStr, out categoryStr);
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                    if (stores != null && categories != null)
                    {
                        data = await _service.GetSalesByCategory(startDate, endDate, categories, stores);
                    }

                    if (stores != null && categories == null)
                    {
                        data = await _service.GetSalesByCategoryForStore(startDate, endDate, stores);
                    }

                    if (stores == null && categories != null)
                    {
                        data = await _service.GetSalesByCategory(startDate, endDate, categories);
                    }

                    if (stores == null && categories == null)
                    {
                        data = await _service.GetSalesByCategory(startDate, endDate);
                    }
                }
            }

            var report = new StiReport();
            report.Load(string.Format("{0}SalesByCategoryReport.mrt", ReportsPath));

            report.Dictionary.Clear();

            report.Dictionary.Variables.Add(new StiVariable("String", "parPeriod", typeof(string), periodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCurrency", typeof(string), SalesByCategoryResource.SalesByCategory_CurrencyStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCreated", typeof(string), SalesByCategoryResource.SalesByCategory_CreatedStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parCurrency", typeof(string), SalesByCategoryResource.SalesByCategory_CurrencyParam, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strDiscountTotalColumn", typeof(string), SalesByCategoryResource.SalesByCategory_DiscountTotalColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strFilters", typeof(string), SalesByCategoryResource.SalesByCategory_FiltersStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strHeader", typeof(string), SalesByCategoryResource.SalesByCategory_HeaderStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strNameColumn", typeof(string), SalesByCategoryResource.SalesByCategory_NameColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strNetSalesColumn", typeof(string), SalesByCategoryResource.SalesByCategory_NetSalesColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strPeriod", typeof(string), SalesByCategoryResource.SalesByCategory_PeriodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSalesWithTaxColumn", typeof(string), SalesByCategoryResource.SalesByCategory_SalesWithTaxColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSoldQuantityColumn", typeof(string), SalesByCategoryResource.SalesByCategory_SoldQuantityColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTax1Column", typeof(string), SalesByCategoryResource.SalesByCategory_Tax1Column, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotal", typeof(string), SalesByCategoryResource.SalesByCategory_TotalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strStore", typeof(string), SalesByCategoryResource.SalesByCategory_StoreStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parStore", typeof(string), storeStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCategory", typeof(string), SalesByCategoryResource.SalesByCategory_CategoryStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parCategory", typeof(string), categoryStr, true));

            report.RegBusinessObject("SalesByMenuCategoryBO", data);
            return report;
        }

        #endregion
    }
}