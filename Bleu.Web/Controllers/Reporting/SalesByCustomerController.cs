﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

using Bleu.Web.Controllers.Reporting.Base;
using Bleu.Web.Helpers;
using Bleu.Web.Models.Reporting;

using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;

using YumaPos.Reporting.Background.Common;
using YumaPos.Reporting.Infrastructure.Logic.Models.SalesByCustomer;
using YumaPos.Reporting.Infrastructure.Logic.Services.SalesByCustomer;
using YumaPos.Reporting.Reports.Resources.Sales;

namespace Bleu.Web.Controllers.Reporting
{
    public class SalesByCustomerController : BaseReportingController
    {
        private readonly ISalesByCustomerService _service;
        private readonly IJobManager _manager;

        public SalesByCustomerController(ISalesByCustomerService service, IJobManager manager)
        {
            _service = service;
            _manager = manager;
        }

        [HttpGet]
        public ActionResult Index(ReportsParamsModel filter)
        {
            TempData["filter"] = filter;

            return View("ReportView");
        }

        public async Task<ActionResult> GetReportSnapshot()
        {
            await _manager.Start();
            var report = await CreateReport();
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        #region Private methods

        private async Task<StiReport> CreateReport()
        {
            var data = new List<SalesByCustomerModel>();
            string periodStr = string.Empty, storeStr = string.Empty, customerStr = string.Empty, groupStr = string.Empty;
            if (TempData["filter"] != null)
            {
                var filters = TempData["filter"] as ReportsParamsModel;
                DateTime startDate, endDate;
                Guid[] stores;
                int[] customerGroups;
                string[] customers;

                if (filters != null && filters.IsCustom)
                {
                    Guid[] menuItems, categories;
                    int[] employees;
                    string[] terminals;
                    
                    // filtering by all parameters for custom report
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out customers, out customerGroups, out menuItems, out categories, out stores, out employees, out terminals);

                    // todo: create method for getting data from operational db later
                    // data = await _service.DoWork;
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                }

                if (filters != null && !filters.IsCustom)
                {
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out stores, out customerGroups, out customers, out storeStr, out groupStr, out customerStr);
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");

                    if (stores != null)
                    {
                        if (customerGroups != null)
                        {
                            if (customers != null)
                            {
                                data = await _service.GetSalesByCustomer(startDate, endDate, customerGroups, customers, stores);
                            }
                            else
                            {
                                data = await _service.GetSalesByCustomer(startDate, endDate, customerGroups, stores);
                            }
                        }
                        else
                        {
                            if (customers != null)
                            {
                                data = await _service.GetSalesByCustomer(startDate, endDate, customers, stores);
                            }
                            else
                            {
                                data = await _service.GetSalesByCustomer(startDate, endDate, stores);
                            }
                        }
                    }

                    if (stores == null)
                    {
                        if (customers != null && customerGroups != null)
                        {
                            data = await _service.GetSalesByCustomer(startDate, endDate, customerGroups, customers);
                        }

                        if (customers != null && customerGroups == null)
                        {
                            data = await _service.GetSalesByCustomer(startDate, endDate, customers);
                        }

                        if (customers == null && customerGroups != null)
                        {
                            data = await _service.GetSalesByCustomer(startDate, endDate, customerGroups);
                        }

                        if (customers == null && customerGroups == null)
                        {
                            data = await _service.GetSalesByCustomer(startDate, endDate);
                        }
                    }
                }
            }

            var report = new StiReport();
            report.Load(string.Format("{0}SalesByCustomerReport.mrt", ReportsPath));

            report.Dictionary.Clear();

            report.Dictionary.Variables.Add(new StiVariable("String", "parPeriod", typeof(string), periodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCurrency", typeof(string), SalesByCustomerResource.SalesByCustomer_CurrencyStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCreated", typeof(string), SalesByCustomerResource.SalesByCustomer_CreatedStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parCurrency", typeof(string), SalesByCustomerResource.SalesByCustomer_CurrencyParam, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strDiscountTotalColumn", typeof(string), SalesByCustomerResource.SalesByCustomer_DiscountTotalColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strFilters", typeof(string), SalesByCustomerResource.SalesByCustomer_FiltersStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strHeader", typeof(string), SalesByCustomerResource.SalesByCustomer_HeaderStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strNameColumn", typeof(string), SalesByCustomerResource.SalesByCustomer_NameColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strNetSalesColumn", typeof(string), SalesByCustomerResource.SalesByCustomer_NetSalesColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strPeriod", typeof(string), SalesByCustomerResource.SalesByCustomer_PeriodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSalesWithTaxColumn", typeof(string), SalesByCustomerResource.SalesByCustomer_SalesWithTaxColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSoldQuantityColumn", typeof(string), SalesByCustomerResource.SalesByCustomer_SoldQuantityColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTax1Column", typeof(string), SalesByCustomerResource.SalesByCustomer_Tax1Column, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotal", typeof(string), SalesByCustomerResource.SalesByCustomer_TotalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strStore", typeof(string), SalesByCustomerResource.SalesByCustomer_StoreStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parStore", typeof(string), storeStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCustomer", typeof(string), SalesByCustomerResource.SalesByCustomer_CustomerStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parCustomer", typeof(string), customerStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strGroup", typeof(string), SalesByCustomerResource.SalesByCustomer_GroupStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parGroup", typeof(string), groupStr, true));

            report.RegBusinessObject("SalesByCustomerBO", data);
            return report;
        }

        #endregion
    }
}