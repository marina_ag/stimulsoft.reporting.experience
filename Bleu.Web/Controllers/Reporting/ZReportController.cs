﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

using Bleu.Web.Controllers.Reporting.Base;
using Bleu.Web.Helpers;
using Bleu.Web.Models.Reporting;

using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;

using YumaPos.Reporting.Background.Common;
using YumaPos.Reporting.Infrastructure.Logic.Models.ZReport;
using YumaPos.Reporting.Infrastructure.Logic.Services.ZReport;
using YumaPos.Reporting.Reports.Resources.Payments;

namespace Bleu.Web.Controllers.Reporting
{
    public class ZReportController : BaseReportingController
    {
        private readonly IPaymentTypesSummaryService _paymentTypesSummaryService;

        private readonly ITerminalsSummaryService _terminalsSummaryService;

        private readonly IJobManager _manager;

        public ZReportController(ITerminalsSummaryService terminalsSummaryService, IPaymentTypesSummaryService paymentTypesSummaryService, IJobManager manager)
        {
            _terminalsSummaryService = terminalsSummaryService;
            _paymentTypesSummaryService = paymentTypesSummaryService;
            _manager = manager;
        }

        [HttpGet]
        public ActionResult Index(ZReportsParamsModel filter)
        {
            TempData["filter"] = filter;

            return View("ReportView");
        }

        public async Task<ActionResult> GetReportSnapshot()
        {
            await _manager.Start();
            var report = await CreateReport();
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        #region Private methods

        private async Task<StiReport> CreateReport()
        {
            string storesStr = string.Empty, terminalsStr = string.Empty, periodStr = string.Empty;
            List<TerminalsSummaryModel> terminalsSummary = new List<TerminalsSummaryModel>();
            List<PaymentTypeSummaryModel> paymentTypesSummary = new List<PaymentTypeSummaryModel>();

            if (TempData["filter"] != null)
            {
                var filters = TempData["filter"] as ZReportsParamsModel;
                DateTime startDate, endDate;
                Guid[] stores;
                string[] terminals;

                if (filters != null && filters.IsCustom)
                {
                    string[] customers;
                    Guid[] menuItems, categories;
                    int[] customerGroups, employees;

                    // filtering by all parameters for custom report
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out customers, out customerGroups, out menuItems, out categories, out stores, out employees, out terminals, out storesStr, out terminalsStr);

                    // todo: create method for getting data from operational db later
                    // data = await _service.DoWork;
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                }

                if (filters != null && !filters.IsCustom)
                {
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out stores, out terminals, out storesStr, out terminalsStr);
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                    if (stores != null && terminals != null)
                    {
                        terminalsSummary = await _terminalsSummaryService.GetTerminalsSummary(startDate, endDate, stores, terminals);
                        paymentTypesSummary = await _paymentTypesSummaryService.GetPaymentsTypesSummary(startDate, endDate, stores, terminals);
                    }

                    if (stores != null && terminals == null)
                    {
                        terminalsSummary = await _terminalsSummaryService.GetTerminalsSummary(startDate, endDate, stores);
                        paymentTypesSummary = await _paymentTypesSummaryService.GetPaymentsTypesSummary(startDate, endDate, stores);
                    }

                    if (stores == null && terminals != null)
                    {
                        terminalsSummary = await _terminalsSummaryService.GetTerminalsSummary(startDate, endDate, terminals);
                        paymentTypesSummary = await _paymentTypesSummaryService.GetPaymentsTypesSummary(startDate, endDate, terminals);
                    }
                    if (stores == null && terminals == null)
                    {
                        terminalsSummary = await _terminalsSummaryService.GetTerminalsSummary(startDate, endDate);
                        paymentTypesSummary = await _paymentTypesSummaryService.GetPaymentsTypesSummary(startDate, endDate);
                    }
                }
            }

            var report = new StiReport();
            report.Load(string.Format("{0}ZReport.mrt", ReportsPath));

            report.Dictionary.Clear();

            report.Dictionary.Variables.Add(new StiVariable("String", "parPeriod", typeof(string), periodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parCurrency", typeof(string), ZReportResource.ZReport_Currency, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parStore", typeof(string), storesStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parTerminal", typeof(string), terminalsStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTerminal", typeof(string), ZReportResource.ZReport_TerminalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strStore", typeof(string), ZReportResource.ZReport_StoreStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCurrency", typeof(string), ZReportResource.ZReport_CurrencyStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCreated", typeof(string), ZReportResource.ZReport_CreatedStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strFilters", typeof(string), ZReportResource.ZReport_FiltersStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strHeader", typeof(string), ZReportResource.ZReport_HeaderStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strPeriod", typeof(string), ZReportResource.ZReport_PeriodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotal", typeof(string), ZReportResource.ZReport_TotalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTerminalColumn", typeof(string), ZReportResource.ZReport_TerminalColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strInitialAmountColumn", typeof(string), ZReportResource.ZReport_InitialAmountColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strOpenedColumn", typeof(string), ZReportResource.ZReport_OpenedColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strDayIncomeColumn", typeof(string), ZReportResource.ZReport_DayIncomeColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotalOrdersColumn", typeof(string), ZReportResource.ZReport_TotalOrdersColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strExpectedAmount", typeof(string), ZReportResource.ZReport_ExpectedAmountColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strClosedColumn", typeof(string), ZReportResource.ZReport_ClosedColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strWithdrawedColumn", typeof(string), ZReportResource.ZReport_WithdrawedColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strLeftToTerminalColumn", typeof(string), ZReportResource.ZReport_LeftToTerminalColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strShortOrOverColumn", typeof(string), ZReportResource.ZReport_ShortOrOverColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTransactionsNumberColumn", typeof(string), ZReportResource.ZReport_NumOfTransactionsColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strConfirmed", typeof(string), ZReportResource.ZReport_ConfirmedStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strName", typeof(string), ZReportResource.ZReport_NameStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strSignature", typeof(string), ZReportResource.ZReport_SignatureStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strDateColumn", typeof(string), ZReportResource.ZReport_DateColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTimeColumn", typeof(string), ZReportResource.ZReport_TimeColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strStoreColumn", typeof(string), ZReportResource.ZReport_StoreColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strPaidColumn", typeof(string), ZReportResource.ZReport_PaidColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotalColumn", typeof(string), ZReportResource.ZReport_TotalColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strRefundColumn", typeof(string), ZReportResource.ZReport_RefundColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTaxesColumn", typeof(string), ZReportResource.ZReport_TaxesColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotalDiscountsColumn", typeof(string), ZReportResource.ZReport_TotalDiscountsColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotalCustomersColumn", typeof(string), ZReportResource.ZReport_TotalCustomersColumn, true));

            report.RegBusinessObject("TerminalsSummaryBO", terminalsSummary);
            report.RegBusinessObject("PaymentsTypesBO", paymentTypesSummary);
            return report;
        }

        #endregion
    }
}