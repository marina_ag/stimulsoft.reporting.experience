﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

using Bleu.Web.Controllers.Reporting.Base;
using Bleu.Web.Helpers;
using Bleu.Web.Models.Reporting;

using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;

using YumaPos.Reporting.Background.Common;
using YumaPos.Reporting.Infrastructure.Logic.Models.ZReport;
using YumaPos.Reporting.Infrastructure.Logic.Services.ZReport;
using YumaPos.Reporting.Reports.Resources.Payments;

namespace Bleu.Web.Controllers.Reporting
{
    public class PaymentsByDateController : BaseReportingController
    {
        private readonly IJobManager _manager;

        private readonly IPaymentsByDateService _service;

        public PaymentsByDateController(IPaymentsByDateService service, IJobManager manager)
        {
            _service = service;
            _manager = manager;
        }

        [HttpGet]
        public ActionResult Index(ZReportsParamsModel filter)
        {
            TempData["filter"] = filter;

            return View("ReportView");
        }

        public async Task<ActionResult> GetReportSnapshot()
        {
            await _manager.Start();
            var report = await CreateReport();
            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        #region Private methods

        private async Task<StiReport> CreateReport()
        {
            string storesStr = string.Empty, terminalsStr = string.Empty, periodStr = string.Empty;
            List<PaymentsByDateModel> data = new List<PaymentsByDateModel>();

            if (TempData["filter"] != null)
            {
                var filters = TempData["filter"] as ZReportsParamsModel;
                DateTime startDate, endDate;
                Guid[] stores; 
                string[] terminals;

                if (filters != null && filters.IsCustom)
                {
                    string[] customers;
                    Guid[] menuItems, categories;
                    int[] customerGroups, employees;
                   
                    // filtering by all parameters for custom report
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out customers, out customerGroups, out menuItems, out categories, out stores, out employees, out terminals, out storesStr, out terminalsStr);

                    // todo: create method for getting data from operational db later
                    // data = await _service.DoWork;
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                }

                if (filters != null && !filters.IsCustom)
                {
                    ReportFiltersHelper.GetReportFilters(filters, out startDate, out endDate, out stores, out terminals, out storesStr, out terminalsStr);
                    periodStr = startDate.ToString("d") + " - " + endDate.ToString("d");
                    if (stores != null && terminals != null)
                    {
                        data = await _service.GetPaymentsByDate(startDate, endDate, stores, terminals);
                    }

                    if (stores != null && terminals == null)
                    {
                        data = await _service.GetPaymentsByDate(startDate, endDate, stores);
                    }

                    if (stores == null && terminals != null)
                    {
                        data = await _service.GetPaymentsByDate(startDate, endDate, terminals);
                    }

                    if (stores == null && terminals == null)
                    {
                        data = await _service.GetPaymentsByDate(startDate, endDate);
                    }
                }
            }

            var report = new StiReport();
            report.Load(string.Format("{0}PaymentsByDateReport.mrt", ReportsPath));

            report.Dictionary.Clear();

            report.Dictionary.Variables.Add(new StiVariable("String", "parPeriod", typeof(string), periodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parCurrency", typeof(string), PaymentsByDateResource.PaymentsByDate_CurrencyParam, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parStore", typeof(string), storesStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strStore", typeof(string), PaymentsByDateResource.PaymentsByDate_StoreStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTerminal", typeof(string), PaymentsByDateResource.PaymentsByDate_TerminalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "parTerminal", typeof(string), terminalsStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCurrency", typeof(string), PaymentsByDateResource.PaymentsByDate_CurrencyStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCreated", typeof(string), PaymentsByDateResource.PaymentsByDate_CreatedStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strFilters", typeof(string), PaymentsByDateResource.PaymentsByDate_FiltersStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strHeader", typeof(string), PaymentsByDateResource.PaymentsByDate_HeaderStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strPeriod", typeof(string), PaymentsByDateResource.PaymentsByDate_PeriodStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotal", typeof(string), PaymentsByDateResource.PaymentsByDate_TotalStr, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strDateColumn", typeof(string), PaymentsByDateResource.PaymentsByDate_DateColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCashColumn", typeof(string), PaymentsByDateResource.PaymentsByDate_InCashColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strByCardColumn", typeof(string), PaymentsByDateResource.PaymentsByDate_ByCardColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strCreditInvoiceColumn", typeof(string), PaymentsByDateResource.PaymentsByDate_CreditInvoiceColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strGiftCardColumn", typeof(string), PaymentsByDateResource.PaymentsByDate_GiftCardColumn, true));
            report.Dictionary.Variables.Add(new StiVariable("String", "strTotalColumn", typeof(string), PaymentsByDateResource.PaymentsByDate_TotalColumn, true));

            report.RegBusinessObject("PaymentsByDateBO", data);
            return report;
        }

        #endregion
    }
}