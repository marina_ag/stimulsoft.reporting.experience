﻿using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;

using Bleu.Web.Helpers;

using YumaPos.Shared.API.Models;

namespace Bleu.Web.Controllers
{
    public class AccountController : Controller
    {
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            var employee = Request.GetFromCookie<EmployeeDto>(ClientConfig.EmployeeCookie);
            if (employee != null)
            {
                return RedirectToAction("Index", "Bleu");
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Login(LoginDto model)
        {
            var employee = WebOperationsHelper.SendRequest<EmployeeDto, LoginDto>(
                url: string.Format("{0}/login", RequestUrlHelper.GetConnectionString(Request.Url,ConfigurationManager.AppSettings["InternalConnectionString"])),
                method: "POST",
                headers: new Dictionary<string, string>() { { ClientConfig.ClientIdentificationHeader, ClientConfig.ClientIdentificationTokken } },
                postData: model);
            if (employee != null)
            {
                Response.SetToCookie(ClientConfig.EmployeeCookie, employee, 1440);
                return RedirectToAction("Index", "Bleu");
            }

            ModelState.AddModelError(string.Empty, "Access Denied");
            return View();
        }

        public ActionResult Logout()
        {
            Response.SetToCookie(ClientConfig.EmployeeCookie, string.Empty, -1440);
            WebOperationsHelper.SendRequest(
                url: string.Format("{0}/logout", RequestUrlHelper.GetConnectionString(Request.Url, ConfigurationManager.AppSettings["InternalConnectionString"])),
                method: "POST",
                headers: new Dictionary<string, string>() { { ClientConfig.ClientIdentificationHeader, ClientConfig.ClientIdentificationTokken } });
            return RedirectToAction("Login", "Account");
        }
    }
}