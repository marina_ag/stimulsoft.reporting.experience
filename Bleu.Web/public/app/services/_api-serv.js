app.factory("apiServ", function ($http) {
    return {
        post: function (url, data) {
            var headers = { "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8" };
            return this.process(url, data, null, "POST", headers);
        },

        get: function (url, params) {
            return this.process(url, null, params, "GET", null);
        },

        process: function (url, data, params, method, headers) {

            var data = data ? $.param(data) : [];

            return $http({
                method: method,
                url: 'api/' + url,
                params: params,
                data: data,
                headers: headers,
                transformResponse: function (data, headersGetter) {
                    data = angular.fromJson(data);
                    return data;
                }
            });
        }
    };
});