﻿app.factory("googleRecaptchaService", [
    "apiServ",
    "contactFormMapper",
    function (apiServ) {
        var
            checkRecaptcha = function (grecaptchaResponse) {
                return apiServ.get("Grecaptcha", { response: grecaptchaResponse });
            };

        return {
            checkRecaptcha: checkRecaptcha
        };
    }
]);