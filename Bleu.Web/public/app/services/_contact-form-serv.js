﻿app.factory("contactFormService", [
    "apiServ",
    "contactFormMapper",
    function (apiServ, contactFormMapper) {
        var
            postContactForm = function (contactForm) {
                return apiServ.post("ContactForm", contactFormMapper.mapInstanceToServer(contactForm));
            };

        return {
            postContactForm: postContactForm
        };
    }
]);