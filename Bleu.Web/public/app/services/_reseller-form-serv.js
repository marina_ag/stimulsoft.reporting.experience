﻿app.factory("resellerFormService", [
    "apiServ",
    "resellerFormMapper",
    function (apiServ, resellerFormMapper) {
        var
            postResellerForm = function (resellerForm) {
                return apiServ.post("ResellerForm", resellerFormMapper.mapInstanceToServer(resellerForm));
            };

        return {
            postResellerForm: postResellerForm
        };
    }
]);