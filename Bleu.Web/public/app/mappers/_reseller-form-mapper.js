﻿app.factory('resellerFormMapper', [
    function () {
        var
            transformToServer = function (request) {
                return {
                    Name: request.your_name,
                    Title: request.title,
                    Website: request.website,
                    Email: request.email,
                    PhoneNumber: request.phone_number,
                    CompanyName: request.company_name,
                    Address: request.address,
                    City: request.city,
                    State: request.state,
                    ZipCode: request.zip_code,
                    Fax: request.fax,
                    AccountingContactName: request.accounting_contact_name,
                    AccountingContactPhone: request.accounting_contact_phone,
                    PurchasingContactName: request.purchasing_contact_name,
                    PurchasingContactPhone: request.purchasing_contact_phone,
                    MarketingContactName: request.marketing_contact_name,
                    MarketingContactPhone: request.marketing_contact_phone,
                    YearsInBusiness: request.years_in_business,
                    BusinessTypeCorporation: request.business_type_corporation,
                    BusinessTypePartnership: request.business_type_partnership,
                    BusinessTypeProprietor: request.business_type_proprietor,
                    DistributorDecisionPoint: request.distributor_decision_point,
                    DistributorBluestar: request.distributor_bluestar,
                    DistributorScansource: request.distributor_scansource,
                    DistributorReadyDistribution: request.distributor_ready_distribution,
                    OtherDistributor: request.other_distributor,
                    OtherDistributorAccountName: request.other_distributor_account_name,
                    OtherDistributorAccountPhone: request.other_distributor_account_phone,
                    ClassifyCompanyReseller: request.classify_company_reseller,
                    ClassifyCompanyVar: request.classify_company_var,
                    ClassifyCompanySysIntegrator: request.classify_company_sys_integrator,
                    ClassifyCompanyChain: request.classify_company_chain,
                    ClassifyCompanyTraining: request.classify_company_training,
                    ClassifyCompanyFranchise: request.classify_company_franchise,
                    ClassifyCompanyConsultant: request.classify_company_consultant,
                    ClassifyCompanyOem: request.classify_company_oem,
                    ClassifyCompanyDeveloper: request.classify_company_developer,
                    ClassifyCompanyDistributor: request.classify_company_distributor,
                    ClassifyCompanyMailOrder: request.classify_company_mail_order,
                    ClassifyCompanyECommerce: request.classify_company_e_commerce,
                    ClassifyCompanyOtherList: request.classify_company_other_list,
                    PriorYearAnnualRevenue: request.prior_year_annual_revenue,
                    ProjectedRevenueThisYear: request.projected_revenue_this_year,
                    PercentageOfRevenueHardware: request.percentage_of_revenue_hardware,
                    PercentageOfRevenueSoftware: request.percentage_of_revenue_software,
                    PercentageOfRevenueServices: request.percentage_of_revenue_services,
                    GeographicalAreaLocal: request.geographical_area_local,
                    GeographicalAreaState: request.geographical_area_state,
                    GeographicalAreaRegional: request.geographical_area_regional,
                    GeographicalAreaInternational: request.geographical_area_international,
                    TotalEmployees: request.total_employees,
                    TotalEngineers: request.total_engineers,
                    TotalInsideSales: request.total_inside_sales,
                    TotalOutsideSales: request.total_outside_sales,
                    MarketingActivitiesSeminars: request.marketing_activities_seminars,
                    MarketingActivitiesNewsletters: request.marketing_activities_newsletters,
                    MarketingActivitiesPrintAdds: request.marketing_activities_print_adds,
                    MarketingActivitiesTradeShows: request.marketing_activities_trade_shows,
                    MarketingActivitiesDirectResponse: request.marketing_activities_direct_response,
                    MarketingActivitiesOther: request.marketing_activities_other,
                    OtherPrimaryVendors: request.other_primary_vendors,
                    ValueAddedServices: request.value_added_services,
                    SimilarProducts: request.similar_products
                }
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            }

        return {
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);