﻿app.factory('contactFormMapper', [
    function () {
        var
            transformToServer = function (request) {
                return {
                    FirstName: request.first_name,
                    LastName: request.last_name,
                    Email: request.email,
                    PhoneNumber: request.phone_number,
                    Company: request.company,
                    Title: request.title,
                    HowHear: request.how_did_you_hear_about_us,
                    HowHelp: request.how_can_we_help_you
                }
            },
            mapInstanceToServer = function (request) {
                return transformToServer(request);
            }

        return {
            mapInstanceToServer: mapInstanceToServer
        }
    }
]);