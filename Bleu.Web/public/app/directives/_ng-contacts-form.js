﻿app.directive("contactsForm", [
    "apiServ",
    "contactFormService",
    "googleRecaptchaService",
    "ContactFormModel",
    function (apiServ, contactFormService, googleRecaptchaService, contactFormModel) {
    return {
        restrict: "A",
        link: function ($scope, element, attrs) {
            var
                errorHandle = function (errors) {
                    errors.forEach(function (error) {
                        var errorText = "";
                        switch (error) {
                        case "missing-input-secret":
                            errorText = "The secret parameter is missing.";
                            break;
                        case "invalid-input-secret":
                            errorText = "The secret parameter is invalid or malformed.";
                            break;
                        case "missing-input-response":
                            errorText = "The response parameter is missing.";
                            break;
                        case "invalid-input-response":
                            errorText = "The response parameter is invalid or malformed.";
                            break;
                        case "invalid-output-response":
                            errorText = "Server troubleshooting";
                            break;
                        default:
                            errorText = "Unknown error.";
                            break;
                        }
                        $scope.captchaErrors.push(errorText);
                    });
                },
                contactsSubmit = function (form) {
                    $scope.submitted = true;
                    $scope.captchaErrors = [];

                    var grecaptchaResponse = grecaptcha.getResponse();

                    if (form.$valid) {
                        if (!!grecaptchaResponse) {
                            $scope.isSubmitting = true;
                            googleRecaptchaService.checkRecaptcha(grecaptchaResponse).then(function () {
                                contactFormService.postContactForm($scope.contact).then(function () {
                                    $("#form_inputs").fadeOut("fast", function () {
                                        $("#form_success_message").fadeIn("fast");
                                    });
                                }).finally(function () {
                                    $scope.isSubmitting = false;
                                });
                            }, function (response) {
                                errorHandle(response.data);
                                $scope.isSubmitting = false;
                            });
                        } else {
                            $scope.captchaErrors = ["Please verify reCAPTCHA"];
                            $("html,body").animate({ scrollTop: $(".recaptcha", element).offset().top - 120 });
                        }
                    } else {
                        $("html,body").animate({ scrollTop: $(".ng-invalid:first", element).offset().top - 120 });
                    }
                },
                backToForm = function () {
                    window.location.reload(true);
                };

            $scope.submitted = false;
            $scope.isSubmitting = false;
            $scope.captchaErrors = [];
            $scope.contact = new contactFormModel();
            $scope.contactsSubmit = contactsSubmit;
            $scope.backToForm = backToForm;

            console.log("Contacts form directive loaded");
        }
    };
}]);