﻿app.directive("resellersForm", [
    '$location',
    '$state',
    'resellerFormService',
    'ResellerFormModel',
    function ($location, $state, resellerFormService, resellerFormModel) {
        return {
            restrict: "A",
            link: function ($scope, element, attrs) {
                var
                    checkValid = function (form) {
                        if (!form.$valid) {
                            $("html,body").animate({ scrollTop: $(".ng-invalid:first", element).offset().top - 120 });
                        }

                        return form.$valid;
                    },
                    switchPage = function (page) {
                        $state.go("resellers.step" + page);
                        $scope.submitted = false;
                    },
                    submitForm = function (form) {
                        if (checkValid(form)) {
                            $scope.isSubmitting = true;
                            resellerFormService.postResellerForm($scope.reseller).then(function (response) {
                                if (response.status === 200) {
                                    $("#form_inputs").fadeOut("fast", function () {
                                        $("#form_success_message").fadeIn("fast");
                                    });
                                }
                            }).finally(function () {
                                $scope.isSubmitting = false;
                            });
                        }
                    },
                    getPrevPage = function (page) {
                        switchPage(page);
                    },
                    getNextPage = function (page, form) {
                        $scope.submitted = true;
                        if (checkValid(form)) {
                            switchPage(page);
                        }
                    },
                    backToForm = function () {
                        window.location.reload(true);
                    };

                $scope.submitted = false;
                $scope.isSubmitting = false;
                $scope.reseller = new resellerFormModel();
                $scope.submitForm = submitForm;
                $scope.getPrevPage = getPrevPage;
                $scope.getNextPage = getNextPage;
                $scope.backToForm = backToForm;

                console.log("Resellers form directive loaded");
            }
        };
    }
]);