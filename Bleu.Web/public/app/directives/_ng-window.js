﻿app.directive("window", ["$window", function ($window) {
    return {
        restrict: "A",
        link: function ($scope, element, attrs) {

            $scope.isWindowXs = $("#mobile-indicator").is(":visible");

            //$(window).resize(function () {
            //    $scope.isWindowXs = $("#mobile-indicator").is(":visible");
            //});

            window.onresize = function() {
                $scope.isWindowXs = $("#mobile-indicator").is(":visible");
            }

            console.log("Window directive loaded");
        }
    };
}]);