﻿app.directive("lightbulb", function () {
    return {
        restrict: "A",
        link: function ($scope, element, attrs) {

            $(document).ready(function () {
                var controller = $.superscrollorama({
                    triggerAtCenter: true,
                    playoutAnimations: true
                });

                controller.addTween('#lightbulb-section', TweenMax.from($('#lightbulb-image'), .5, { css: { opacity: 0 } }), 0, 200);
            });

            console.log("Lightbulb directive loaded");
        }
    };
});