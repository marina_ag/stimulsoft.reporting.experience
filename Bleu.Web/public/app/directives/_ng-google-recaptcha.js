﻿app.directive("googleRecaptcha",["Constants", function (constants) {
    return {
        restrict: "A",
        link: function ($scope, element, attrs) {
            grecaptcha.render("google-recaptcha", {
                "sitekey": constants.googleRecaptchaSiteKey
            });

            console.log("Google recaptcha directive loaded");
        }
    };
}]);