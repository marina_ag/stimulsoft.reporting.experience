﻿app.directive("scrollMenu", ["Constants", function (constants) {
    return {
        restrict: "A",
        link: function ($scope, element, attrs) {
            //post: function postLink($scope, element, attrs, ctrl) {
            //    debugger;
            //    var pinOffset = ($(window).height() / 2) - 71;

            //    var controller = new ScrollMagic();

            //    var pinAnim = (new TimelineMax)
            //    .add([
            //        TweenMax.to(".scroll-menu", 1, {
            //            onStart: function () {
            //                $(".scroll-menu").removeClass("active");
            //                $(".scroll-menu:eq(0)").addClass("active");
            //                $("#scroll-menu-image").fadeOut("fast").attr("src", "images/device_consumer_3_payment-1.png").fadeIn("fast");
            //            }
            //        })
            //    ])
            //    .add([
            //        TweenMax.to(".scroll-menu", 1, {
            //            onStart: function () {
            //                $(".scroll-menu").removeClass("active");
            //                $(".scroll-menu:eq(1)").addClass("active");
            //                $("#scroll-menu-image").fadeOut("fast").attr("src", "images/device_consumer_3_payment-2.png").fadeIn("fast");
            //            }
            //        })
            //    ]);

            //    var scene = new ScrollScene({ triggerElement: "#scroll-menu", duration: 900, offset: pinOffset })
            //        .setTween(pinAnim)
            //        .setPin("#scroll-menu") // pins the element for a scroll distance of 900px
            //        .addTo(controller); // add scene to controller
            //}

            $scope.timer = null;
            $scope.isDocumentScrollStopped = false;
            $scope.currentScrollSection = 1;
            $scope.minSectionIndex = 1;
            $scope.maxSectionIndex = 3;

            function prevent(event) {
                event.preventDefault();
                event.stopPropagation();
                event.returnValue = false;
            }

            function setActiveClass() {
                $(".scroll-menu").removeClass("active");
                $(".scroll-menu:eq(" + ($scope.currentScrollSection - 1) + ")").addClass("active");
                $("#scroll-menu-image").fadeOut("fast").attr("src", "/public/assets/images/device_consumer_3_payment-" + $scope.currentScrollSection + ".png").fadeIn("fast");
            }

            function stopDocumentScroll(documentScrollStopPosition) {
                $scope.isDocumentScrollStopped = true;
                $("html,body").animate({ scrollTop: documentScrollStopPosition });
                prevent(event);
            }

            function scrollInMenu(menuStopScrollSection, event, isScrollUp) {
                prevent(event);

                if (!!$scope.timer) {
                    window.clearTimeout($scope.timer);
                }

                $scope.timer = window.setTimeout(function () {
                    isScrollUp ? $scope.currentScrollSection-- : $scope.currentScrollSection++;
                    setActiveClass();
                    if ($scope.currentScrollSection === menuStopScrollSection) {
                        $scope.isDocumentScrollStopped = false;
                    }
                }, 200);
            }

            function scrollInMenuOrStopDocumentScroll(documentScrollStopPosition, sectionStopIndex, event, isScrollUp) {
                if (!!$scope.isDocumentScrollStopped) {
                    scrollInMenu(sectionStopIndex, event, isScrollUp);
                } else {
                    stopDocumentScroll(documentScrollStopPosition);
                }
            }

            $(window).bind("mousewheel DOMMouseScroll", function(event) {
                var documentScrollStopPosition = element.offset().top - 71;
                var step;
                var nextPosition;
                
                if (event.deltaY === 1) {
                    //scroll top
                    step = -event.deltaFactor;
                    if ($(window).scrollTop() + step < documentScrollStopPosition && $scope.currentScrollSection !== $scope.minSectionIndex) {
                        scrollInMenuOrStopDocumentScroll(documentScrollStopPosition, $scope.minSectionIndex, event, true);
                    }
                } else {
                    //scroll down
                    step = event.deltaFactor;
                    if ($(window).scrollTop() + step > documentScrollStopPosition && $scope.currentScrollSection !== $scope.maxSectionIndex) {
                        scrollInMenuOrStopDocumentScroll(documentScrollStopPosition, $scope.maxSectionIndex, event, false);
                    }
                }
            });

            console.log("Scroll menu directive loaded");
        }
    };
}]);