﻿app.directive("solutionsSlide", ["Constants", function (constants) {
    return {
        restrict: "E",
        link: function ($scope, element, attrs) {

            $scope.activeTemplate = null;

            $scope.clickOnCircle = function (id) {

                if ($scope.activeTemplate !== id) {
                    if (!!$scope.activeTemplate) {
                        $("#" + $scope.activeTemplate).slideUp("slow", function() {
                            $("#" + id).slideDown("slow");
                        });
                    } else {
                        $("#" + id).slideDown("slow");
                    }
                    $scope.activeTemplate = id;
                } else {
                    $("#" + id).slideToggle("slow");
                    $scope.activeTemplate = null;
                }
            }

            console.log("Solutions slide directive loaded");
        }
    };
}]);