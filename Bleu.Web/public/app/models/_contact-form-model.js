﻿app.factory('ContactFormModel', [
    function () {
        var contactFormModel = function () {
            this.first_name = "";
            this.last_name = "";
            this.email = "";
            this.phone_number = "";
            this.company = "";
            this.title = "";
            this.how_did_you_hear_about_us = "";
            this.how_can_we_help_you = "";
        }

        return contactFormModel;
    }
]);