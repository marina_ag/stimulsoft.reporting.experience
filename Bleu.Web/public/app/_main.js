﻿"use strict";

var app = angular.module("app", ["ui.router", "ui.bootstrap"]);

app.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state("business", {
            url: "/",
            templateUrl: "/public/app/views/business.html",
            controller: "BusinessCtrl"
        })
        .state("consumer", {
            url: "/consumer",
            templateUrl: "/public/app/views/consumer.html",
            controller: "ConsumerCtrl"
        })
        .state("resellers", {
            url: "/resellers",
            templateUrl: "/public/app/views/resellers.html"
        })
        .state("resellers.step1", {
            url: "/",
            views: {
                'resellers-form': { templateUrl: "/public/app/views/templates/resellers-form-1.html" }
            }
        })
        .state("resellers.step2", {
            url: "/",
            views: {
                'resellers-form': { templateUrl: "/public/app/views/templates/resellers-form-2.html" }
            }
        })
        .state("resellers.step3", {
            url: "/",
            views: {
                'resellers-form': { templateUrl: "/public/app/views/templates/resellers-form-3.html" }
            }
        })
        .state("solutions", {
            url: "/solutions",
            templateUrl: "/public/app/views/solutions.html",
            controller: "SolutionsCtrl"
        })
        .state("privacy", {
            url: "/privacy",
            templateUrl: "/public/app/views/privacy.html"
        })
        .state("terms", {
            url: "/terms",
            templateUrl: "/public/app/views/terms.html"
        })
        .state("company", {
            url: "/company",
            templateUrl: "/public/app/views/company.html"
        })
        .state("support", {
            url: "/support",
            templateUrl: "/public/app/views/support.html"
        })
        .state("contacts", {
            url: "/contacts",
            templateUrl: "/public/app/views/contacts.html"
        });

    //$locationProvider.html5Mode(true);

    console.log("Router configurated");

}]);

app.run(["$rootScope", "$state", "$window", function ($rootScope, $state, $window) {

    $rootScope.$on("$stateChangeSuccess", function () {
        $rootScope.state = $state.current.name;
    });

    var window = angular.element($window);

    $rootScope.isSmallScreen = window.width() < 768;

    window.bind("resize", function () {
        $rootScope.$apply(function() {
            $rootScope.isSmallScreen = window.width() < 768;
        });
    });

    console.log("Application started");
}]);