﻿app.controller("HeaderCtrl", ["$scope", "$modal", function ($scope, $modal) {

    $scope.signIn = function () {
        $modal.open({
            templateUrl: "/public/app/views/popup/signin.html",
            controller: "SignInCtrl",
            size: "md"
        });
    }

    console.log("Consumer controller loaded");

}]);