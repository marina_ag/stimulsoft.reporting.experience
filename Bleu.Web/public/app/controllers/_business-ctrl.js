﻿app.controller("BusinessCtrl", ["$scope", "$modal", function ($scope, $modal) {

    $scope.videoUp = function () {
        $modal.open({
            templateUrl: "public/app/views/popup/video.html",
            controller: "VideoCtrl",
            size: "lg"
        });
    }

    console.log("Business controller loaded");

}]);