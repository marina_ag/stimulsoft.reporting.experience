﻿app.controller("VideoCtrl", [
    "$scope", "$modalInstance", function ($scope, $modalInstance) {
        $scope.close = function () {
            $modalInstance.close();
        }

        console.log("Video controller loaded");
    }
]);