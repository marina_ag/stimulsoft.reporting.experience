﻿app.constant("Constants", {
    googleRecaptchaSiteKey: "6LeC0_8SAAAAADqZ0bgJgnSlcSRmLXJuWB5BqmQ0",
    solutionTemplates: [
        {
            Group: [
            {
                id: "all-in-one",
                images: {
                    gray: "/public/assets/images/sol1_POS_1.png",
                    color: "/public/assets/images/sol1_POS.png"
                },
                background: "background-solutions-2-1",
                header: "All-in-one Point Of Sale",
                text: "Bleu's integrated point of sale is an elegant all in one platform for any retailer big and small. For the first time, Bleu delivers to merchants an out of the box, plug and play point of sale solution with the most advanced security and sophisticated applications."
            }, {
                id: "work-force-managment",
                images: {
                    gray: "/public/assets/images/sol2_work_forces_1.png",
                    color: "/public/assets/images/sol2_work_forces.png"
                },
                background: "background-solutions-2-2",
                header: "Work Force Managment",
                text: "Monitor employee time and attendance automatically and control labor costs, compliance and safety risks, and improve productivity. Our beacon network and back-office management platform minimizes the administrative time associated with monitoring employee tracking and labor activity. No punchards, key fobs, or clicker devices to maintain. Run our application on every employees mobile device instantly with no need for manual check in or check out."
            }, {
                id: "asset-managment",
                images: {
                    gray: "/public/assets/images/sol3_assets_1.png",
                    color: "/public/assets/images/sol3_assets.png"
                },
                background: "background-solutions-2-3",
                header: "Asset Managment",
                text: "Reduce the overhead in your operations decisions and understand your valuable assets movements through your warehouse, storage facility, retail space, etc. Get global tracking of all vehicles and cargo trucks in your fleet and gain insight into logistical managment and operation processes."
            }, {
                id: "campus-experience",
                images: {
                    gray: "/public/assets/images/sol4_campus_1.png",
                    color: "/public/assets/images/sol4_campus.png"
                },
                background: "background-solutions-2-4",
                header: "Campus Experience",
                text: "Institutions using our smart beacons can create rich content and expand the user experience in and around campus. Bridge the physical world with the digital and connect the people, places, and parts of your school's landmarks with timely location based content. Create an active and engaging classroom experience by using beacons installed in each class and transform the students mobile devices into powerful learning tools to make every lecture count."
            }]
        }, {
            Group: [
            {
                id: "petro-gas",
                images: {
                    gray: "/public/assets/images/sol5_gaz_1.png",
                    color: "/public/assets/images/sol5_gaz.png"
                },
                background: "background-solutions-2-5",
                header: "Petro and Gas",
                text: "Combine our integrated point of sale in the convenient store, with our pay at the pump mobile solution, all using our beacon technology. Allow customers to start and complete the fueling process at the pump all through their mobile devices and reduce the costly fees associated with upgrading your pump stations to new EMV standards."
            }, {
                id: "donation",
                images: {
                    gray: "/public/assets/images/sol6_donation_1.png",
                    color: "/public/assets/images/sol6_donation.png"
                },
                background: "background-solutions-2-6",
                header: "Donation",
                text: "Nonprofit and charitable organizations can use our beacon platform to boost retention and  garner support for fundraising efforts. Deliver the right message and connect with your supporters through your own application or on the Bleu network. Whether you're a faith-based organization or an animal care nonprofit, Bleu provides a low cost tool to enhance contributions at the local level."
            }, {
                id: "audience-response-system",
                images: {
                    gray: "/public/assets/images/sol7_audience_1.png",
                    color: "/public/assets/images/sol7_audience.png"
                },
                background: "background-solutions-2-7",
                header: "Audience Response System",
                text: "Leverage our technology to engage and measure instructional or training audience assessments. Conduct real time audience reactions and gather valuable feedback."
            }, {
                id: "transportation-authority",
                images: {
                    gray: "/public/assets/images/sol8_transport_1.png",
                    color: "/public/assets/images/sol8_transport.png"
                },
                background: "background-solutions-2-8",
                header: "Transportation Authority",
                text: "Metro and city authorities can lower costs and transaction fees by adopting beacon technology into their parking and toll systems. Allow motorists to pay via their mobile devices at the meter or hands free."
            }]
        }
    ],
    consumers: [
        {
            header: "PREPAYMENT",
            text: "Pay ahead of time on the go and never have to wait in line again. Get notified when your items are ready for pick up.",
            image: "/public/assets/images/device_consumer_3_payment-1.png"
        },
        {
            header: "SECURITY",
            text: "Your secure payment is never given to the merchant and we guarantee every transaction with a 0% fraud rate.",
            image: "/public/assets/images/device_consumer_3_payment-2.png"
        },
        {
            header: "SEND CASH",
            text: "Send cash to your friends or other users easily and instantly.",
            image: "/public/assets/images/device_consumer_3_payment-3.png"
        }
    ]
});