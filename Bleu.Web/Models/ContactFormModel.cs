﻿using System.ComponentModel.DataAnnotations;

namespace Bleu.Web.Models
{
    public class ContactFormModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Company { get; set; }

        public string Title { get; set; }

        public string HowHear { get; set; }

        [Required]
        public string HowHelp { get; set; }
    }
}