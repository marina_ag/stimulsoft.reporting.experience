﻿using System;

namespace Bleu.Web.Models.Reporting
{
    public class ReportsParamsModel
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string[] Customer { get; set; }

        public Guid[] Item { get; set; }

        public int[] Group { get; set; }

        public int[] Employee { get; set; }

        public Guid[] Category { get; set; }

        public Guid[] Store { get; set; }

        public string[] Terminal { get; set; }

        public bool IsCustom { get; set; }

        public string StoreName { get; set; }

        public string TerminalName { get; set; }

        public string CategoryName { get; set; }

        public string ItemName { get; set; }

        public string CustomerName { get; set; }

        public string GroupName { get; set; }

        public string EmployeeName { get; set; }
    }
}