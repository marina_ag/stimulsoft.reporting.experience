﻿namespace Bleu.Web.Models.Reporting
{
    public class CustomersReportParamsModel
    {
        public int[] CustomerGroup { get; set; }

        public string GroupName { get; set; }
    }
}