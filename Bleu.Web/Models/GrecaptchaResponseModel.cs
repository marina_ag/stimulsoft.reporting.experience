﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Bleu.Web.Models
{
    public class GrecaptchaResponseModel
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        [JsonProperty(PropertyName = "error-codes")]
        public IEnumerable<string> ErrorCodes { get; set; }
    }
}