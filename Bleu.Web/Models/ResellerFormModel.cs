﻿using System.ComponentModel.DataAnnotations;

namespace Bleu.Web.Models
{
    public class ResellerFormModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Title { get; set; }

        public string Website { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string CompanyName { get; set; }

        [Required]
        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public string Fax { get; set; }

        public string AccountingContactName { get; set; }

        public string AccountingContactPhone { get; set; }

        public string PurchasingContactName { get; set; }

        public string PurchasingContactPhone { get; set; }

        public string MarketingContactName { get; set; }

        public string MarketingContactPhone { get; set; }

        public string YearsInBusiness { get; set; }

        public string BusinessTypeCorporation { get; set; }

        public string BusinessTypePartnership { get; set; }

        public string BusinessTypeProprietor { get; set; }

        public string DistributorDecisionPoint { get; set; }

        public string DistributorBluestar { get; set; }

        public string DistributorScansource { get; set; }

        public string DistributorReadyDistribution { get; set; }

        public string OtherDistributor { get; set; }

        public string OtherDistributorAccountName { get; set; }

        public string OtherDistributorAccountPhone { get; set; }

        public string ClassifyCompanyReseller { get; set; }

        public string ClassifyCompanyVar { get; set; }

        public string ClassifyCompanySysIntegrator { get; set; }

        public string ClassifyCompanyChain { get; set; }

        public string ClassifyCompanyTraining { get; set; }

        public string ClassifyCompanyFranchise { get; set; }

        public string ClassifyCompanyConsultant { get; set; }

        public string ClassifyCompanyOem { get; set; }

        public string ClassifyCompanyDeveloper { get; set; }

        public string ClassifyCompanyDistributor { get; set; }

        public string ClassifyCompanyMailOrder { get; set; }

        public string ClassifyCompanyECommerce { get; set; }

        public string ClassifyCompanyOtherList { get; set; }

        public string PriorYearAnnualRevenue { get; set; }

        public string ProjectedRevenueThisYear { get; set; }

        public string PercentageOfRevenueHardware { get; set; }

        public string PercentageOfRevenueSoftware { get; set; }

        public string PercentageOfRevenueServices { get; set; }

        public string GeographicalAreaLocal { get; set; }

        public string GeographicalAreaState { get; set; }

        public string GeographicalAreaRegional { get; set; }

        public string GeographicalAreaInternational { get; set; }

        public string TotalEmployees { get; set; }

        public string TotalEngineers { get; set; }

        public string TotalInsideSales { get; set; }

        public string TotalOutsideSales { get; set; }

        public string MarketingActivitiesSeminars { get; set; }

        public string MarketingActivitiesNewsletters { get; set; }

        public string MarketingActivitiesPrintAdds { get; set; }

        public string MarketingActivitiesTradeShows { get; set; }

        public string MarketingActivitiesDirectResponse { get; set; }

        public string MarketingActivitiesOther { get; set; }

        public string OtherPrimaryVendors { get; set; }

        public string ValueAddedServices { get; set; }

        public string SimilarProducts { get; set; }
    }
}