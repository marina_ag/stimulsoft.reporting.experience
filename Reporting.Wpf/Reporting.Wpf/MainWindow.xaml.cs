﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;

namespace Reporting.Wpf
{
    public partial class MainWindow
    {

        public MainWindow()
        {
            InitializeComponent();
            webBrowser.Navigated += new NavigatedEventHandler(WebBrowser_OnNavigated);
            LoadReport();
        }

        private void LoadReport()
        {
            var uriString = "http://test.imidus.yumapos.com:8832/ZReport";
            webBrowser.Navigate(new Uri(uriString));
        }

        public static void SetSilent(WebBrowser browser, bool silent)
        {
            if (browser == null)
                throw new ArgumentNullException("browser");

            // get an IWebBrowser2 from the document
            IOleServiceProvider sp = browser.Document as IOleServiceProvider;
            if (sp != null)
            {
                Guid IID_IWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");
                Guid IID_IWebBrowser2 = new Guid("D30C1661-CDAF-11d0-8A3E-00C04FC9E26E");

                object webBrowser;
                sp.QueryService(ref IID_IWebBrowserApp, ref IID_IWebBrowser2, out webBrowser);
                if (webBrowser != null)
                {
                    webBrowser.GetType().InvokeMember("Silent", BindingFlags.Instance | BindingFlags.Public | BindingFlags.PutDispProperty, null, webBrowser, new object[] { silent });
                }
            }
        }

        [ComImport, Guid("6D5140C1-7436-11CE-8034-00AA006009FA"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        private interface IOleServiceProvider
        {
            [PreserveSig]
            int QueryService([In] ref Guid guidService, [In] ref Guid riid, [MarshalAs(UnmanagedType.IDispatch)] out object ppvObject);
        }

        private void WebBrowser_OnNavigated(object sender, NavigationEventArgs e)
        {
            SetSilent(webBrowser, true); // make it silent
        }
    }
}