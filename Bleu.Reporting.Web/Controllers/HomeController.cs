﻿using System;
using System.Web.Mvc;
using Bleu.Reporting.Infrastructure.Logic.Models;
using Bleu.Reporting.Infrastructure.Logic.Services;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;

namespace Bleu.Reporting.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISalesSummaryByDayService _service;
        public HomeController(ISalesSummaryByDayService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View(new FiltersModel());
        }

        [HttpGet]
        public ActionResult SalesByDay(DateTime start, DateTime end)
        {
            return View();
        }

        [HttpGet]
        public ActionResult SalesByDayStimulsoft(DateTime start, DateTime end)
        {
            return View();
        }
        public ActionResult GetReportSnapshot()
        {
            var data = _service.GetAllSalesByDay(new DateTime(2015, 1, 1), DateTime.Today);
            StiReport report = new StiReport();
            report.Load(Server.MapPath("../../Stimulsoft/SalesByDayReport.mrt"));
            report.RegBusinessObject("SalesByDayModel", data);

            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }
    }
}
