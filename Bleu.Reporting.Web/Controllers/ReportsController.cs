﻿using System;
using Bleu.Reporting.Infrastructure.Logic.Services;
using Bleu.Reporting.Reports;
using Telerik.Reporting;
using Telerik.Reporting.Cache.File;
using Telerik.Reporting.Services;
using Telerik.Reporting.Services.Engine;
using Telerik.Reporting.Services.WebApi;

namespace Bleu.Reporting.Web.Controllers
{
    public class ReportsController : ReportsControllerBase
    {
        public ReportsController(IReportResolver resolver)
        {
            ReportServiceConfiguration = new ReportServiceConfiguration
            {
                HostAppId = "Bleu.Reporting.Web",
                ReportResolver = resolver,
                Storage = new FileStorage()
            };
        }

        public class CustomReportResolver : IReportResolver
        {
            private readonly ISalesSummaryByDayService _service;
            public CustomReportResolver(ISalesSummaryByDayService service)
            {
                _service = service;
            }
            public ReportSource Resolve(string name)
            {
                return new InstanceReportSource
                {
                    ReportDocument = new SummaryByDayReport(_service, new DateTime(2014, 6, 1), DateTime.Today)
                };
            }
        }
    }
}