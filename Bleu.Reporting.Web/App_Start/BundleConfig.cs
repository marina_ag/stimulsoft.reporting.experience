﻿using System.Web.Optimization;

namespace Bleu.Reporting.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/scripts/layout").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/modernizr-2.6.2.js",
                "~/Scripts/knockout-2.2.0.js",
                "~/Scripts/modernizr-2.6.2.js"
                ));

            bundles.Add(new StyleBundle("~/styles/layout").Include(
                    "~/Styles/icons.css",
                    "~/Styles/jquery-ui.min.css",
                    "~/Styles/theme.css",
                    "~/Styles/main.css"));

            bundles.Add(new StyleBundle("~/styles/appBuild").Include());
        }
    }
}