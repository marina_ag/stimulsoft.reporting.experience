﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Bleu.Reporting.Infrastructure.Ioc;

namespace Bleu.Reporting.Web.Ioc
{
    public class Bootstrapper
    {
        private static IBootstrapperContainer container;

        public static void Register(HttpConfiguration config, Assembly addAssembly)
        {
            container = new AutofacConfig();
            RegisterModules();
            container.Build(config, addAssembly);
        }

        public static void UpdateRegistration(Type @interface, Type implementation, Lifetime lifetime)
        {
            container.Update(@interface, implementation, lifetime);
        }

        public static I Resolve<I>()
        {
            return container.Resolve<I>();
        }

        private static void RegisterModules()
        {
            new Logic.Ioc.Registrator().Register(container);
            new Data.Ioc.Registrator().Register(container);
            new Reports.Ioc.Registrator().Register(container);
            new Registrator().Register(container);
        }
    }
}