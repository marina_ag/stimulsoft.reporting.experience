﻿using Bleu.Reporting.Infrastructure.Ioc;
using Bleu.Reporting.Web.Controllers;
using Telerik.Reporting.Services.Engine;

namespace Bleu.Reporting.Web.Ioc
{
    public class Registrator : IRegistrator
    {
        public void Register(IBootstrapperContainer container)
        {
            //container.Register(typeof(IImageService), typeof(ImageService), Lifetime.PerLifetimeScope);
            //container.Register(typeof(IAuthorizationService), typeof(AuthorizationService), Lifetime.PerLifetimeScope);
            //container.Register(typeof(ISecurityService), typeof(SecurityService), Lifetime.PerLifetimeScope);
            container.Register(typeof(IReportResolver), typeof(ReportsController.CustomReportResolver), Lifetime.PerLifetimeScope);
        }
    }
}